import { isAdmin } from "../services/authService"
export const basePath = isAdmin() ? "/auth" : "/app"
