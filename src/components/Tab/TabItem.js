import React from "react"

const TabItem = ({ onClick, active, activeClassname, title }) => (
  <div className={active ? activeClassname : ""} onClick={onClick}
  role="button"
  tabIndex={0}
  onKeyPress={()=>{}}
  style={{outline: "none"}}
  >
    <p>{title}</p>
  </div>
)

export default TabItem
