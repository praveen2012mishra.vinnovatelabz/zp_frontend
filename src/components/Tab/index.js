import React,{useState,useEffect} from "react"
import { updateStats } from "../../services/Api/updateStats"

import styles from "./style.module.scss"
import TabItem from "./TabItem"

const Tab = ({ items, activeTab, onTabChange, display, routePath }) => {
  const [width,setWidth]=useState(800)
  useEffect(() => {
    setWidth(window.screen.width)
  }, [])
  return (
    <div style={{display: display ? "none !important" : "block" }} className={`${styles.tabContainer} ${items.length<4?styles.displaySingleItem:''}`}>
    <div 
    style={{justifyContent:width<700 && items.length>2?"normal":"space-around",
            marginLeft:routePath==="supply" && width<768?"10em":""
  }}
    className={`d-flex flex-row ${items.length<2?styles.tabs:styles.tabatsmallno} ${items.length<4?styles.displaySingleItemWidth:''} ${styles.blueBackground}`} wid={items.length>3?'100%':"auto"}>
    {items.map((item, idx) => (
      <TabItem
        key={item}
        active={idx === activeTab}
        activeClassname={styles.active}
        title={item}
        onClick={() => {
          updateStats(window.location.pathname,`/${item.trim()}`)
          onTabChange(idx, item)}}
      />
    ))}
  </div>
    </div>
  )
}

export default Tab
