import React from 'react'
import { Row, Col, Container, Media } from 'reactstrap'
import logoMark from '../../images/Logomark.png';
import './style.scss'
import {Link} from "gatsby"
export default function Index() {
    const [screenWidth, setScreenWidth] = React.useState("")
    React.useEffect(() => {
        setScreenWidth(window.screen.availWidth);
    },[])
    return (
        <footer className="py-4 px-lg-5" style={{ zIndex: 11, position: "absolute",width:"100%" }}>
            <Container fluid>
                {screenWidth < 1024 ?
                    <Row className="justify-content-between">
                        <Col lg={5} style={{ width: "100%" }}>
                            <ul className="list-unstyled footer-list d-flex justify-content-between flex-wrap">
                                <li><Link to="/AboutUs">About us</Link></li>
                                <li><Link to="/contact">Contact</Link></li>
                                <li><a target="_blank" href="https://www.linkedin.com/company/zuellig-pharma-analytics/" rel="noreferrer">LinkedIn</a></li>
                                {/* <li><a href="">Terms and Conditions</a></li>
                            <li><a href="">Privacy Policy</a></li> */}
                            </ul>
                        </Col>
                        <Col lg={6}>
                            <Media className="footer-media">
                                <Media left className="mr-4">
                                    <Media object src={logoMark} alt="Generic placeholder image" />
                                </Media>
                                <Media body>
                                    <p className="text-white">ZP Data Analytics started in 2016 as part of Zuellig Health Solutions. ZP Data
                                    Analytics turns data into insights and action plans to helps clients understand the
                                    market better, make better decisions and make healthcare more accessible.</p>
                                </Media>
                            </Media>
                        </Col>
                    </Row>
                    :
                    <Row className="justify-content-between">
                        <Col lg={6}>
                            <Media className="footer-media">
                                <Media left className="mr-4">
                                    <Media object src={logoMark} alt="Generic placeholder image" />
                                </Media>
                                <Media body>
                                    <p className="text-white">ZP Data Analytics started in 2016 as part of Zuellig Health Solutions. ZP Data
                                    Analytics turns data into insights and action plans to helps clients understand the
                                    market better, make better decisions and make healthcare more accessible.</p>
                                </Media>
                            </Media>
                        </Col>
                        <Col lg={5} style={{ width: "100%" }}>
                            <ul className="list-unstyled footer-list d-flex justify-content-between flex-wrap">
                                <li><Link to="/AboutUs">About us</Link></li>
                                <li><Link to="/contact">Contact</Link></li>
                                <li><a target="_blank" href="https://www.linkedin.com/company/zuellig-pharma-analytics/" rel="noreferrer">LinkedIn</a></li>
                                {/* <li><a href="">Terms and Conditions</a></li>
                            <li><a href="">Privacy Policy</a></li> */}
                            </ul>
                        </Col>
                    </Row>
                }
                <p className="copy-right-text text-center text-sm-left">©2020 ZuelligPharma Holdings Ltd</p>
            </Container>
        </footer>
    )
}
