import React from "react"
import { navigate } from "gatsby"

import { isAdmin } from "../services/authService"
const isBrowser = typeof window !== `undefined`
let pathName = "";
if (isBrowser) {
  pathName = window.location.pathname;
}

const AdminRoute = ({ component: Component, ...rest }) => {
  if (!isAdmin()) {
    localStorage.setItem("lastRoute", pathName)
    navigate("/Login")
    return null
  }
  return <Component {...rest} />
}

export default AdminRoute
