import React, { useEffect } from 'react'
import Cookies from "js-cookie"
import "../style.scss"
import axiosInstance from '../../../../axiosConfig'
import cogoToast from 'cogo-toast'
function DropDownRightWeb({
  selector,
  handleClick,
  basePath,
  innerOptions
}) {
  const [internal, setInternal] = React.useState(false)
  useEffect(() => {
    if (basePath === "/auth") {
      if (!Cookies.get("inner_option")) {
        if (innerOptions.length > 0) {
          setInternal(innerOptions[0].isInternal)
        }
      } else {
        let changedOption = Cookies.getJSON("inner_option")?Cookies.getJSON("inner_option"):null
        if (changedOption){
          axiosInstance.get(`/group/fetchGroupOptions/${changedOption.id}`)
        .then(res=>{
          setInternal(res.data.isInternal)
        }).catch(err=>cogoToast.error("Internal Server Error on Internal status"))
        }else{
          cogoToast.warn("The User is not assigned to any group!")
        }
      }
    }
  }, [innerOptions, basePath])
  return (
    <div className="custom-left-nav">
      <span
        className={
          selector.insider
            ? "active nav-left-link m-child dropDownButtonClass"
            : "nav-left-link m-child dropDownButtonClass"
        }
        style={{ outline: "none" }}
      >
        <button
          name="insider"
          role="button"
          style={{ color: selector.insider ? "#c3d425" : "", outline:"none" }}
          className="hoverbutton"
          onMouseEnter={(e) => {
            e.persist();
            handleClick(e)
          }}>Insider</button>
      </span>
      <span
        className={
          selector.commercialExcellence
            ? "active nav-left-link m-child dropDownButtonClass"
            : "nav-left-link m-child dropDownButtonClass"
        }
        style={{ outline: "none" }}
      >
        <button
          name="commercialExcellence"
          role="button"
          style={{ color: selector.commercialExcellence ? "#c3d425" : "", outline:"none"}}
          className="hoverbutton"
          onMouseEnter={(e) => {
            e.persist();
            // console.log("called", e);
            handleClick(e)
          }}>Commercial Excellence</button>
      </span>
      <span
        className={
          selector.supplyChain
            ? "active nav-left-link m-child dropDownButtonClass"
            : "nav-left-link m-child dropDownButtonClass"
        }
        // name="supplyChain"
        // onClick={handleClick}
        style={{ outline: "none" }}
      >
        <button
          style={{ color: selector.supplyChain ? "#c3d425" : "", outline:"none" }}
          className="hoverbutton"
          name="supplyChain"
          onMouseEnter={(e) => {
            e.persist();
            handleClick(e)
          }}>Supply Chain Analytics</button>
      </span>
      <span
        className={
          selector.businessIntelligence
            ? `active dropDownButtonClass nav-left-link ${basePath === "/auth" ? "m-child" : "lastChild"}`
            : `nav-left-link dropDownButtonClass ${basePath === "/auth" ? "m-child" : "lastChild"}`
        }
        style={{ marginBottom: !internal ? "1em" : "", outline: "none" }}
      // name="businessIntelligence"
      // onClick={handleClick}
      >
        <button
          style={{ color: selector.businessIntelligence ? "#c3d425" : "", outline:"none" }}
          className="hoverbutton"
          name="businessIntelligence"
          onMouseEnter={(e) => {
            e.persist();
            handleClick(e)
          }}>Business Intelligence</button>
      </span>
      {internal ?
        <span
          style={{ marginBottom: "1em", outline: "none" }}
          className={
            selector.internal
              ? "active nav-left-link lastChild dropDownButtonClass"
              : "nav-left-link lastChild dropDownButtonClass"
          }
          // name="internal"
          // onMouseEnter={handleClick}
        >
          <button
            style={{ color: selector.internal ? "#c3d425" : "", outline:"none" }}
            className="hoverbutton"
            name="internal"
            onMouseEnter={(e) => {
              e.persist();
              handleClick(e)
            }}>Internal</button>
        </span>
        : <div />}
    </div>
  )
}

export default DropDownRightWeb
