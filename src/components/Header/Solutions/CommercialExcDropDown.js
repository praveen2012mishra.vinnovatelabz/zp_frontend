import React from "react"
import { Nav, NavItem } from "reactstrap"
import Link from "gatsby-link"
import "../style.scss"
function CommercialExcDropDown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
  closeMobileHeader
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Understanding our customers</h5>
      <NavItem>
        {" "}
        <Link
          to={`${basePath}/commercialExp/InfraredSales`}
          name="InfraredSales"
          className={selectedLink === "InfraredSales" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
          Infrared
        </Link>
        {selectedLink === "InfraredSales" ? (
          <span className="heading_tagline">Understand your customers</span>
        ) : (
          ""
        )}
      </NavItem>
    </Nav>
  )
}

export default CommercialExcDropDown
