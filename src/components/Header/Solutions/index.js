import React from "react"
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from "reactstrap"
import "../style.scss"
import DropdownRightWeb from "./DropDownRightWeb"
import Insider from "./InsiderDropdown"
import CommercialExcellence from "./CommercialExcDropDown"
import SupplyChainAnalytics from "./SupplyChainDropdown"
import BIServices from "./BIDropdown"
import Internal from "./Internal"

function Solutions({
  toggle,
  removeToggle,
  mobileToggClick,
  isOpen,
  dropDownId,
  arrowUp,
  arrowDown,
  selector,
  handleClick,
  minus,
  plus,
  displayType,
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
  innerOptions,
  closeMobileHeader
}) {

  return (
    <UncontrolledDropdown
      nav
      inNavbar
      id="1"
      className="padd_sol"
      onMouseOver={() => toggle("1")}
      onMouseLeave={e => removeToggle(e, "1")}
      isOpen={dropDownId === "1" && isOpen}
      // isOpen={true}
    >
      <DropdownToggle
        nav
        id="1"
        className={dropDownId === "1" ? "active-nav" : ""}
        onClick={e => mobileToggClick(e, "1")}
      >
        Solutions
        <img
          src={dropDownId === "1" ? arrowUp : arrowDown}
          id="1"
          className="imageArrow"
          alt="arrow"
        />
      </DropdownToggle>

      <DropdownMenu right>
        {/* {isOpen ? ( */}
        <div className="menu-wrap">
          <DropdownRightWeb selector={selector} handleClick={handleClick} innerOptions={innerOptions} basePath={basePath} />
          <div className="custom-right-nav">
            <div className="mt-20"
            style={{outline:"none"}}
            role="button"
            tabIndex={0}
            onClick={handleClick}
            onKeyPress={()=>{}}
            >
              <img
                src={selector.insider ? minus : plus}
                className={selector.insider ? "minus" : "plus"}
                alt="plus"
                name="insider"
                // onClick={handleClick}
              />
              <a
                href="#insider"
                className={
                  selector.insider
                    ? "active nav-left-link m-child"
                    : "nav-left-link m-child"
                }
                name="insider"
                // onClick={handleClick}
              >
                Insider
              </a>
            </div>
            {displayType === "insider" ? (
              <Insider
                selectedLink={selectedLink}
                innerDropDownItems={innerDropDownItems}
                removeDropdownItems={removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={closeMobileHeader}
              />
            ) : (
                ""
              )}
            <div className="mt-20" 
            style={{outline:"none"}}
            role="button"
            tabIndex={0}
            onClick={handleClick}
            onKeyPress={()=>{}}
            >
              <img
                src={selector.commercialExcellence ? minus : plus}
                className={selector.commercialExcellence ? "minus" : "plus"}
                alt="plus"
                name="commercialExcellence"
                // onClick={handleClick}
              />
              <a
                href="#commercial"
                className={
                  selector.commercialExcellence
                    ? "active nav-left-link m-child"
                    : "nav-left-link m-child"
                }
                name="commercialExcellence"
                // onClick={handleClick}
              >
                Commercial Excellence
              </a>
            </div>
            {displayType === "commercialExcellence" ? (
              <CommercialExcellence
                selectedLink={selectedLink}
                innerDropDownItems={innerDropDownItems}
                removeDropdownItems={removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={closeMobileHeader}
              />
            ) : (
                ""
              )}
            <div className="mt-20"
            style={{outline:"none"}}
            role="button"
            tabIndex={0}
            onClick={handleClick}
            onKeyPress={()=>{}}
            >
              <img
                src={selector.supplyChain ? minus : plus}
                className={selector.supplyChain ? "minus" : "plus"}
                alt="plus"
                name="supplyChain"
                // onClick={handleClick}
              />
              <a
                href="#supplychain"
                className={
                  selector.supplyChain
                    ? "active nav-left-link m-child"
                    : "nav-left-link m-child"
                }
                name="supplyChain"
                // onClick={handleClick}
              >
                Supply Chain Analytics
              </a>
            </div>
            {displayType === "supplyChain" ? (
              <SupplyChainAnalytics
                selectedLink={selectedLink}
                innerDropDownItems={innerDropDownItems}
                removeDropdownItems={removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={closeMobileHeader}
              />
            ) : (
                ""
              )}
            <div className="mt-20"style={{outline:"none"}}
            role="button"
            tabIndex={0}
            onClick={handleClick}
            onKeyPress={()=>{}}
            >
              <img
                src={selector.businessIntelligence ? minus : plus}
                className={selector.businessIntelligence ? "minus" : "plus"}
                alt="plus"
                name="businessIntelligence"
                // onClick={handleClick}
              />
              <a
                href="#businessIntelligence"
                className={
                  selector.businessIntelligence
                    ? "active nav-left-link m-child"
                    : "nav-left-link m-child"
                }
                name="businessIntelligence"
                // onClick={handleClick}
              >
                Business Intelligence
              </a>
            </div>
            {displayType === "businessIntelligence" ? (
              <BIServices
                selectedLink={selectedLink}
                innerDropDownItems={innerDropDownItems}
                removeDropdownItems={removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={closeMobileHeader}
              />
            ) : null}
            {basePath === '/auth' ?
              <div className="mt-20"
              style={{outline:"none"}}
            role="button"
            tabIndex={0}
            onClick={handleClick}
            onKeyPress={()=>{}}
              >
                <img
                  src={selector.internal ? minus : plus}
                  className={selector.internal ? "minus" : "plus"}
                  alt="plus"
                  name="internal"
                  // onClick={handleClick}
                />
                <a
                  href="#internal"
                  className={
                    selector.internal
                      ? "active nav-left-link m-child"
                      : "nav-left-link m-child"
                  }
                  name="internal"
                  // onClick={handleClick}
                >
                  Internals
              </a>
              </div>
              : null}
            {basePath === '/auth' && displayType === "internal" ? (
              <Internal
                selectedLink={selectedLink}
                innerDropDownItems={innerDropDownItems}
                removeDropdownItems={removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={closeMobileHeader}
              />
            ) : null}
          </div>
        </div>
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default Solutions
