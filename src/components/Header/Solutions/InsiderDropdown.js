import React from "react"
import { Nav, NavItem, NavLink } from "reactstrap"
import Link from "gatsby-link"
import "../style.scss"
import Cookie from 'js-cookie'
function InsiderDropdown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
  closeMobileHeader
}) {

  const onClick = () => {
    Cookie.set("tabNo", "1", {expires: (1/86400) * 4 })
    closeMobileHeader()
  }
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Accessing the data you need</h5>
      <NavItem>
        {" "}
        <Link
          to={`${basePath}/insider?1`}
          // href="/auth/insider"
          name="insider"
          className={selectedLink === "insider" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
          Insider
        </Link>
        {selectedLink === "insider" ? (
          <span className="heading_tagline">Live access to your data</span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <NavLink
          href="https://csm.zuelligpharma.com/login"
          name="csm"
          target="_blank"
          className={selectedLink === "csm" ? "inner-nav-active" : ""}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
          CSM
        </NavLink>
        {selectedLink === "csm" ? (
          <span className="heading_tagline">
            Customized Matrix to organize your Sales Reps
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <Link
          to={`${basePath}/insider?2`}
          name="reach"
          className={selectedLink === "reach" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={() => onClick()}
        >
          Clinical Reach
        </Link>
        {selectedLink === "reach" ? (
          <span className="heading_tagline">
            Live access to your Clinical data
          </span>
        ) : (
          ""
        )}
      </NavItem>
    </Nav>
  )
}

export default InsiderDropdown
