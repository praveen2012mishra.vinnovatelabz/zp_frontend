import React from "react"
import { Nav, NavItem } from "reactstrap"
import Link from "gatsby-link"
import "../style.scss"
function SupplyChainDropdown({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  basePath,
  closeMobileHeader
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Sales and Operations Planning</h5>
      <NavItem>
        {" "}
        <Link
          to={`${basePath}/supplyChain/controlTower?1`}
          // href={basePath==='auth'?`${basePath}/supplyChain/inventoryPlanner`:'/app/supplyChain/planner'}
          name="supplyChain"
          className={selectedLink === "supplyChain" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
          Supply Chain Control Tower
        </Link>
        {selectedLink === "supplyChain" ? (
          <span className="heading_tagline">
            Visibility into your Supply Chain
          </span>
        ) : (
          ""
        )}
      </NavItem>
      <NavItem>
        {" "}
        <Link
          
          to={basePath==='auth'?`${basePath}/supplyChain/inventoryPlanner?2`:'/app/supplyChain/planner?2'}
          name="SupplyChainPlanner"
          className={selectedLink === "SupplyChainPlanner" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
         Sales and Operations Planner
        </Link>
        {selectedLink === "SupplyChainPlanner" ? (
          <span className="heading_tagline">
            S&OP Automated Planning Tool
          </span>
        ) : (
          ""
        )}
      </NavItem>
    </Nav>
  )
}

export default SupplyChainDropdown
