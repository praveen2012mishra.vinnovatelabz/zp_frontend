import React from "react"
import { Nav, NavItem } from "reactstrap"
import Link from "gatsby-link"
import "../style.scss"
function Internal({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  closeMobileHeader
}) {
  return (
    <Nav className="sub-dropdown">
      <h5 className="w-100">Zuellig Pharma Employee Portal</h5>
      <NavItem>
        {" "}
        <Link
          to="/auth/internal"
          name="internalHome"
          className={selectedLink === "internalHome" ? "inner-nav-active nav-link" : "nav-link"}
          onMouseEnter={innerDropDownItems}
          onMouseLeave={removeDropdownItems}
          onClick={closeMobileHeader}
        >
        Internal home
        </Link>
        {selectedLink === "internalHome" ? (
          <span className="heading_tagline">Internal Home</span>
        ) : (
          ""
        )}
      </NavItem>
    </Nav>
  )
}

export default Internal
