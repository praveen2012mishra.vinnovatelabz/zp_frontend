import React, { Component } from "react"
import {
  Collapse,
  Navbar,
  // NavbarBrand,
  Nav,
  NavItem,
  // NavLink,
} from "reactstrap"
import { BsSearch } from "react-icons/bs"
import { Modal, Button } from "react-bootstrap"
import logo from "../../assets/images/ZHA-Logo.png"
import user from "../../assets/images/Account.png"
import userHover from "../../assets/images/Account-lime.png"
import search from "../../assets/images/Search.png"
import "./style.scss"
import dot from "../../assets/images/dot.png"
import vertDot from "../../assets/images/ZHA Home for dev-26.png"
import arrowDown from "../../assets/images/ZHA Home for dev-27.png"
import arrowUp from "../../assets/images/ZHA Home for dev-28.png"
import plus from "../../assets/images/ZHA Home for dev-29.png"
import minus from "../../assets/images/ZHA Home for dev-30.png"
import ContactModal from "./contactModal"
// import Close from "../../assets/images/svg/close.svg"
// import limeSearch from "../../assets/images/svg/limeSearch.svg"
import Solutions from "./Solutions"
import DigitalOperations from "./DigitalOperations"
import Search from "./Search"
// import About from "./About"
import MySubscriptions from "./MySubscriptions"
import { searchData } from "./data"
import Cookie from "js-cookie"
import cogoToast from "cogo-toast"
import axios from "../../../axiosConfig"
import { basePath } from "../basePath"
import {
  twofactor,
  twofactorOtpVerify,
  twofactorEnforceCheck,
} from "../../services/Api/2FA.api"
import { samlLogout } from "../../services/Api/auth.api"
import { group } from "../getGroupName"
import Link from "gatsby-link"
import {FiSearch} from "react-icons/fi"
import { updateStats } from "../../services/Api/updateStats"
const _ = require("underscore")
class Header extends Component {
  constructor(props) {
    super(props)
    this.state = {
      getUserName: false,
      signStatus: false,
      modalOpen: false,
      fixedHeader: false,
      isOpen: false,
      selector: {
        commercialExcellence: false,
        supplyChain: false,
        insider: false,
        businessIntelligence: false,
        internal: false,
      },
      innerWidth: 0,
      displayType: "",
      dropDownId: "",
      dropDownIdInsight: "",
      selectedLink: "",
      mobileToggle: false,
      searchBoxDesktop: false,
      displaySearchData: [],
      selectedSearchValue: "",
      authData: null,
      samlToken: null,
      innerOptions: [],
      errorReason: "",
      twoFactorResponse: "",
      qrModalOpen: false,
      twoFaToken: "",
      subsData: [],
    }
    this.inputWidth = React.createRef()
    this.iconDisplayRef = React.createRef()
    this.headerRef = React.createRef()
    this.handleClick = this.handleClick.bind(this)
    this.handler= this.handler.bind(this)
  }

  //On click on get user down arraw
  getUser = event => {
    this.setState({ getUserName: true })
  }

  //On hover in sign element
  onToggleOpen = event => {
    this.setState({ signStatus: true })
  }

  onToggleLeave = event => {
    this.setState({ signStatus: false })
  }

  onUserLeave = event => {
    this.setState({ getUserName: false })
  }
  toggle = id => {
    if (window.innerWidth > 992) {
      this.setState({
        isOpen: true,
        dropDownId: id,
      })
    }
  }

  modalToggle = () => {
    this.setState({ modalOpen: true })
  }
  modalClose = () => {
    this.setState({ modalOpen: false })
    this.noDropdownSideBrackets("")
  }
  twofactorAuth = async name => {
    let enforced = await twofactorEnforceCheck(name)
    try {
      // console.log(enforced)
      if (enforced) {
        let twoFa = await twofactor(name)
        this.setState({ twoFactorResponse: twoFa })

        _.debounce(this.setState({ qrModalOpen: true }), 2000)
      }
    } catch {
      cogoToast.error("Authentication Failed")
    }
  }
  componentDidUpdate(prevProps) {
    if(this.props.location !== prevProps.location) 
    {
      // console.log(this.props.location);
      updateStats(this.props.location.pathname,null)
      window.scrollTo(0, 0)
    }
  } 
  componentDidMount() {
    window.addEventListener("click", this.handleClickOutside)
    window.addEventListener("resize", this.resize)
    window.addEventListener("scroll", event => {
      const top = event.target.scrollingElement.scrollTop
      document.getElementById(
        "zp-home-header"
      ).style.background = `rgba(9, 70, 88, ${top > 200 ? 1 : top * 0.005})`
    })
    if (!this.props.toggle) {
      this.setState({ dropDownId: "" })
    }

    this.setState({
      errorReason: Cookie.get("error_reason") ? Cookie.get("error_reason") : "",
    })
    updateStats(this.props.location.pathname,null)
  }
  componentWillMount(){
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null

    if (samlToken) {
      this.setState({
        authData: {
          nameId: samlToken.user.name_id,
          sessionIndex: samlToken.user.session_index,
          inResponse: samlToken.response_header.in_response_to,
        },
      })

      if (Cookie.getJSON("ADgroups")) {
        let adGrp = Cookie.getJSON("ADgroups")
        let twoFaCookie = Cookie.get("2fa") ? Cookie.get("2fa") : null
        if (twoFaCookie === null) {
          if (!group) {
            this.twofactorAuth(adGrp[0].label)
          } else {
            this.twofactorAuth(group.label)
          }
        }
        this.setState({ innerOptions: adGrp })
      } else {
        const fetchOptions= async ()=>{
          let res= await axios.get(`/group/getADGroups/user/${samlToken.user.name_id}`)
          try{
            let ar = res.data.data.groupnames
            let inner_option=[]
            // let gNames=[]
            await ar.forEach((item, key) => {
              inner_option.push({
                label: item.group_name,
                id: item._id,
                href: null,
                type: "dropdown",
                display_name:item.displayName?item.displayName:item.group_name,
                isAdmin: item.isAdmin || false,
                isInternal: item.isInternal || false,
                country:item.country
              })
              // gNames.push(item.group_name)
            })
            // await loop()
            this.setState({ innerOptions: inner_option })
            // console.log(this.state.innerOptions);
            setTimeout(console.log(this.state.innerOptions,"timeout"),500)
            this.forceUpdate()
            inner_option.length>0 && Cookie.set("ADgroups", inner_option)
            // inner_option.length>0 && Cookie.set("Group_names", gNames)
            inner_option.length>0 && Cookie.set("inner_option", inner_option[0], { expires: 1 })
            let twoFaCookie = Cookie.get("2fa") ? Cookie.get("2fa") : null
            if (twoFaCookie === null) {
              if (!group) {
                this.twofactorAuth(inner_option[0].label)
              } else {
                this.twofactorAuth(group.label)
              }
            }
          }
          catch{
            console.log(res.data.data)
            this.setState({ innerOptions: [] })
          }
        }
        fetchOptions()
      }
    } else {
      this.setState({ authData: null })
    }
  }
  getSubs = () => {
    axios
      .post("/subs/subsDatabase", {
        groupName: group
          ? group.label
          : null,
      })
      .then(res => {
        let fields = res.data
        const localSubsData = []
        fields.map((item, index) => {
          let localObj = {}
          if (item.status === "Active" || item.status === "Ending Soon") {
            localObj.Product = item.product
            localObj.isSubscribed = true
            localSubsData.push(localObj)
          } else {
            localObj.Product = item.product
            localObj.isSubscribed = false
            localSubsData.push(localObj)
          }
          return item
        })
        localStorage.setItem("subsData", JSON.stringify(localSubsData))
        this.setState({
          subsData: localSubsData,
        })
      })
      .catch(err => {
        console.error("Failed to get the data", err)
      })
  }
  resize = () => this.setState({ innerWidth: window.innerWidth })
  handleClickOutside = e => {
    this.inputWidth &&
      this.inputWidth.current &&
      !this.inputWidth.current.contains(e.target) &&
      this.setState({ displaySearchData: [] })
      if(this.state.mobileToggle && 
        this.headerRef.current && 
        !this.headerRef.current.contains(e.target)) {
        this.closeHeader()
        }
  }
  noDropdownSideBrackets = id => {
    this.setState({ dropDownIdInsight: id })
    this.setState({ dropDownId: id })
  }
  handleClick(event) {
    const { name } = event.target
    // console.log(name);
    let selectorStatus = this.state.selector
    if (window.innerWidth <= 992) {
      Object.keys(selectorStatus).forEach(function (key) {
        key === name && !selectorStatus[name]
          ? (selectorStatus[key] = true)
          : (selectorStatus[key] = false)
      })
    } else {
      // let _this = this
      Object.keys(selectorStatus).forEach(function (key) {
        key === name
          ? (selectorStatus[key] = true)
          : (selectorStatus[key] = false)
      })
    }
    if (selectorStatus[name]) {
      this.setState({
        selector: selectorStatus,
        displayType: name,
      })
    } else {
      this.setState({
        selector: selectorStatus,
        displayType: "",
      })
    }
  }
  innerDropDownItems = e => {
    const { name } = e.target
    this.setState({ selectedLink: name })
  }
  removeDropdownItems = () => {
    this.setState({ selectedLink: "" })
  }
  removeToggle = (e, id) => {
    if (window.innerWidth > 992) {
      let selector = {
        commercialExcellence: false,
        supplyChain: false,
        insider: false,
        businessIntelligence: false,
        internal: false,
      }
      this.setState({
        dropDownId: "",
        isOpen: false,
        selector,
        displayType: "",
      })
    }
  }
  mobileToggle = () => {
    this.setState({ mobileToggle: !this.state.mobileToggle }, () => {
      if (!this.state.mobileToggle) {
        let selectorStatus = { ...this.state.selector }
        Object.keys(selectorStatus).forEach(function (key) {
          selectorStatus[key] = false
        })
        this.setState({
          displayType: "",
          selector: selectorStatus,
          dropDownId: "",
          selectedSearchValue:""
        })
      }else{
        this.setState({
          selectedSearchValue:""
        })
      }
    })
  }
  closeHeader = () => {
    this.setState({ mobileToggle: false })
    let selectorStatus = { ...this.state.selector }
        Object.keys(selectorStatus).forEach(function (key) {
          selectorStatus[key] = false
        })
        this.setState({
          displayType: "",
          selector: selectorStatus,
          dropDownId: "",
        })
  }
  mobileToggClick = (e, id) => {
    //if (window.innerWidth <= 1052) {
    if (id === this.state.dropDownId)
      this.setState({ isOpen: false, dropDownId: "" })
    else this.setState({ isOpen: true, dropDownId: id })
    //}
  }
  filterSearch = e => {
    let searchItems = [...searchData]
    let searchValue = []
    if (e.target.value.length >= 2) {
      searchValue = searchItems.filter((a, i) => {
        if (a.label.toLowerCase().includes(e.target.value.toLowerCase())) {
          // this.setState({
          //   selectedSearchValue:a.label
          // })
          return a
        } else {
          return false
        }
      })
      if (!searchValue.length) {
        searchValue = [{ label: "No Data Found", url: "#", clickable: false }]
      }
    }

    this.setState({
      displaySearchData: searchValue,
      selectedSearchValue: e.target.value,
    })
  }
  searchToggle = () => {
    this.setState({
      searchBoxDesktop: !this.state.searchBoxDesktop,
      displaySearchData: [],
      selectedSearchValue: "",
    })
  }
  handleClose = () => this.setState({ qrModalOpen: false })
  handelChange = e => {
    this.setState({ twoFaToken: e.target.value })
  }
  submitOtp = async () => {
    let otpResponse = await twofactorOtpVerify(
      this.state.authData.nameId,
      this.state.twoFaToken 
    )
    if (otpResponse.authorized) {
      // isAdmin(true)
      Cookie.set("2fa", { authorized: true }, { expires: 1 })
      this.setState({ qrModalOpen: false })
    } else {
      samlLogout("2FA", this.state.authData)
    }
  }

  handler=(e)=>{
    // console.log(e);
    this.setState({selectedSearchValue:e})}
  componentWillUnmount() {
    window.removeEventListener("resize", this.resize)
  }

  render() {
    if (this.state.searchBoxDesktop) {
      this.inputWidth.current.style.width = Cookie.get("ADgroups") && Cookie.getJSON("ADgroups").length > 0
        ? "17em"
        : "17em"
      this.inputWidth.current.style.transition = "all 1s ease-in"
      this.inputWidth.current.style.padding = "0 0 0 10px"
      this.iconDisplayRef.current.style.visibility = "visible"
      this.iconDisplayRef.current.style.opacity = 1
    } else {
      if (this.inputWidth.current) {
        this.inputWidth.current.style.width = "0"
        this.inputWidth.current.style.padding = "0"
      }
      if (this.iconDisplayRef.current) {
        this.iconDisplayRef.current.style.visibility = "hidden"
        this.iconDisplayRef.current.style.opacity = 0
        this.iconDisplayRef.current.style.transition =
          "visibility 0.5s, opacity 0.5s linear"
      }
    }
    return (
      <div ref={this.headerRef} id="zp-home-header">
        {this.state.errorReason !== ""
          ? cogoToast.error(this.state.errorReason)
          : null}
        <Modal show={this.state.qrModalOpen}>
          <Modal.Header closeButton>
            <Modal.Title>2FA QRcode</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img
              src={
                this.state.twoFactorResponse
                  ? this.state.twoFactorResponse.qr_uri
                  : ""
              }
              alt="pic"
            />
            <input type="text" onChange={this.handelChange}></input>
          </Modal.Body>
          <Modal.Footer>
            <Button variant="secondary" onClick={this.submitOtp}>
              Submit
            </Button>
          </Modal.Footer>
        </Modal>
        <Navbar
          className={
            "custom-navbar fixed-navbar"
          }
          style={{
            backgroundColor: "#094658"
          }}
          light
          expand="lg"
        >
          <Link to="/" className="navbar-brand">
            <img src={logo} className="w-100" alt="logo" />
          </Link>
          <div 
            role="button"
            tabIndex={0}
            onClick={this.mobileToggle}
            onKeyPress={()=>{}}
            className="menu-hamburger"
            >            
            <img
            src={this.state.mobileToggle ? dot : vertDot}
            className={
              this.state.mobileToggle
                ? "openMenuForMobile"
                : "openMenuForMobileVert"
            }
            alt="pic"
          />
          </div>
          

          <Collapse
            isOpen={this.state.mobileToggle}
            navbar
            className="navbar-col"
          >
            <Nav className="ml-8" navbar>
              <div className="searchPos" ref={this.inputWidth}>
                <input
                  type="text"
                  className="mobileSearch"
                  onChange={this.filterSearch}
                  value={this.state.selectedSearchValue}
                />
                {/* <img src={search} className="imgPos" alt="search" /> */}
                <FiSearch size="2em" className="imgPos"/>
              </div>
              <Nav className="nav-search" navbar>
                <Search
                  handler={this.handler}
                  searchBoxDesktop={this.state.searchBoxDesktop}
                  displaySearchData={this.state.displaySearchData}
                  basePath={basePath}
                />
              </Nav>
              <Solutions
                {...this.props}
                toggle={id => this.toggle(id)}
                removeToggle={(e, id) => this.removeToggle(e, id)}
                mobileToggClick={(e, id) => this.mobileToggClick(e, id)}
                isOpen={this.state.isOpen}
                dropDownId={this.state.dropDownId}
                arrowUp={arrowUp}
                arrowDown={arrowDown}
                selector={this.state.selector}
                handleClick={this.handleClick}
                minus={minus}
                plus={plus}
                displayType={this.state.displayType}
                selectedLink={this.state.selectedLink}
                innerDropDownItems={this.innerDropDownItems}
                removeDropdownItems={this.removeDropdownItems}
                basePath={basePath}
                innerOptions={this.state.innerOptions}
                closeMobileHeader={()=>this.closeHeader()}
              />
              <DigitalOperations
                {...this.props}
                toggle={id => this.toggle(id)}
                removeToggle={(e, id) => this.removeToggle(e, id)}
                mobileToggClick={(e, id) => this.mobileToggClick(e, id)}
                isOpen={this.state.isOpen}
                dropDownId={this.state.dropDownId}
                arrowUp={arrowUp}
                arrowDown={arrowDown}
                selectedLink={this.state.selectedLink}
                innerDropDownItems={this.innerDropDownItems}
                removeDropdownItems={this.removeDropdownItems}
                basePath={basePath}
                closeMobileHeader={()=>this.closeHeader()}
              />
              <NavItem
                className="dropdown"
                onMouseEnter={() => this.noDropdownSideBrackets(3)}
                onMouseLeave={() => this.noDropdownSideBrackets("")}
              >
                <Link
                  to="/AboutUs/"
                  className={
                    this.state.dropDownId === 3 ||
                    this.props.urlRoute === "/AboutUs/"
                      ? "active-nav nav-link"
                      : "nav-link"
                  }
                  onClick={this.closeHeader}
                  style={{
                    color: this.props.urlRoute === "/AboutUs/" ? "#c3d422" : "",
                  }}
                >
                  About
                </Link>
              </NavItem>
              <NavItem
                className="dropdown"
                onMouseEnter={() => this.noDropdownSideBrackets(4)}
                onMouseLeave={() => this.noDropdownSideBrackets("")}
              >
                <ContactModal
                  open={this.state.modalOpen}
                  onClose={this.modalClose}
                  basePath={basePath}
                />
                <Link
                  to="/contact/"
                  id="contact"
                  className={
                    this.state.dropDownId === 4 ||
                    this.props.urlRoute === "/contact/"
                      ? "active-nav nav-link"
                      : "nav-link"
                  }
                  onClick={this.closeHeader}
                  style={{
                    color: this.props.urlRoute === "/contact/" ? "#c3d422" : "",
                  }}
                >
                  Contact
                </Link>
              </NavItem>
              <NavItem
                className="dropdown"
                onMouseEnter={() => this.noDropdownSideBrackets(5)}
                onMouseLeave={() => this.noDropdownSideBrackets("")}
              >
                <Link
                  to="/insights/"
                  id="insight"
                  className={
                    this.state.dropDownIdInsight === 5 ||
                    this.props.urlRoute === "/insights/" ||
                    this.props.urlRoute === "/ReadArticle/"
                      ? "active-nav nav-link"
                      : "nav-link"
                  }
                  onClick={this.closeHeader}
                  style={{
                    color:
                      this.props.urlRoute === "/insights/" ? "#c3d422" : "",
                  }}
                >
                  Insights
                </Link>
              </NavItem>
            </Nav>
            {/* {this.state.authData?this.state.innerOptions.length>0 && <MySubscriptions
              searchBoxDesktop={this.state.searchBoxDesktop}
              user={user}
              userHover={userHover}
              search={search}
              searchToggle={this.searchToggle}
              authData={this.state.authData}
              innerOptions={this.state.innerOptions}
              basePath={basePath}
              closeMobileHeader={()=>this.closeHeader()}
              mobileToggle={this.state.mobileToggle}
            />:<MySubscriptions
            searchBoxDesktop={this.state.searchBoxDesktop}
            user={user}
            userHover={userHover}
            search={search}
            searchToggle={this.searchToggle}
            authData={this.state.authData}
            innerOptions={this.state.innerOptions}
            basePath={basePath}
            closeMobileHeader={()=>this.closeHeader()}
            mobileToggle={this.state.mobileToggle}
          />} */}
          <MySubscriptions
            searchBoxDesktop={this.state.searchBoxDesktop}
            user={user}
            userHover={userHover}
            search={search}
            searchToggle={this.searchToggle}
            authData={this.state.authData}
            innerOptions={this.state.innerOptions}
            basePath={basePath}
            closeMobileHeader={()=>this.closeHeader()}
            mobileToggle={this.state.mobileToggle}
          />
            <Nav
              className={
                this.state.searchBoxDesktop
                  ? "custom-right-navbar show searchList"
                  : "custom-right-navbar hide searchList"
              }
              navbar
            >
              <NavItem className="nav-icon position">
                <input
                  ref={this.inputWidth}
                  type="text"
                  style={{ width: "-webkit-fill-available" }}
                  onChange={this.filterSearch}
                  value={this.state.selectedSearchValue}
                  className="searchToggle"
                  placeholder="What are you looking for?"
                />
                <div ref={this.iconDisplayRef}>
                  <BsSearch
                    color="#0f4b5d"
                    size="20px"
                    className="limeSearch"
                    onClick={this.searchToggle}
                  />
                </div>
              </NavItem>
              <Nav
                className={
                  Cookie.getJSON("ADgroups") && Cookie.getJSON("ADgroups").length > 0 
                    ? "overflow-scroll searchScrollWidthCookie"
                    : "overflow-scroll searchScrollWidth"
                }
                navbar
              >
                <Search
                  handler={this.handler}
                  searchBoxDesktop={this.state.searchBoxDesktop}
                  displaySearchData={this.state.displaySearchData}
                  basePath={basePath}
                />
              </Nav>
            </Nav>
          </Collapse>
        </Navbar>
      </div>
    )
  }
}

export default Header
