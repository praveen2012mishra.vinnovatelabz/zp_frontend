import React, { useState } from "react"
import {
  Modal,
  Dropdown,
} from "react-bootstrap"
import { X } from "react-bootstrap-icons"
import "./style.scss"
import Axios from "../../../axiosConfig"

import "simplebar/dist/simplebar.min.css"
import PhoneInput from "react-phone-input-2"
import 'react-phone-input-2/lib/bootstrap.css'
import cogo from "cogo-toast"

let _ = require("underscore");

const countries = [
  {key:"", value:"Regional"},
  {key:"bn", value:"Brunei"},
  {key:"kh", value:"Cambodia"},
  {key:"hk", value:"Hong Kong"},
  {key:"idn", value:"Indonesia"},
  {key:"kr", value:"Korea"},
  {key:"mo", value:"Macau"},
  {key:"my", value:"Malaysia"},
  {key:"mm", value:"Myanmar"},
  {key:"ph", value:"Philippines"},
  {key:"sg", value:"Singapore"},
  {key:"tw", value:"Taiwan"},
  {key:"th", value:"Thailand"},
  {key:"vn", value:"Vietnam"},
  {key:"", value:"Other"},
]

const ContactModal = ({ open, onClose=()=>null, reciverName, reciverDesignation }) => {
  const [name, setName] = useState("");
  const [nameError, setNameError] = useState("");
  const [companyName, setCompanyName] = useState("");
  const [companyNameError, setCompanyNameError] = useState("");
  const [country, setCountry] = useState("SINGAPORE");
  const [countryError, setCountryError] = useState("");
  const [designation, setDesignation] = useState("");
  const [designationError, setDesignationError] = useState("");
  const [conatct, setContact] = useState("");
  const [email, setEmail] = useState("");
  const [remarks, setRemarks] = useState("");
  const [contactError, setContactError] = useState("");
  const [emailError, setEmailError] = useState("");
  const [intrest, setIntrest] = useState({ insider: false, commercial: false, supplyChain: false, busniess: false });
  const [contactCountry, setContactCountry] = useState('sg');

  const onCountrySelect = newCountry => {
    if (newCountry !== country) setCountry(newCountry)
  }

  const valid = () => {
    if(!name && !companyName && !designation && !email){
      setNameError("This field cannot be empty")
      setCompanyNameError("This field cannot be empty")
      setDesignationError("This field cannot be empty")
      setEmailError("This field cannot be empty");
    }
    else if(!companyName && !designation && !email){
      setCompanyNameError("This field cannot be empty")
      setDesignationError("This field cannot be empty")
      setEmailError("This field cannot be empty");
    }
    else if(!name && !designation && !email){
      setNameError("This field cannot be empty")
      setDesignationError("This field cannot be empty")
      setEmailError("This field cannot be empty");
    }
    else if(!companyName && !name && !email){
      setCompanyNameError("This field cannot be empty")
      setNameError("This field cannot be empty")
      setEmailError("This field cannot be empty");
    }
    else if (!(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(email)) && !name && !companyName && !designation){
      setEmailError("Invalid Email")
      setNameError("This field cannot be empty")
      setCompanyNameError("This field cannot be empty")
      setDesignationError("This field cannot be empty")
    }
    else if (!(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(email)) && !companyName && !designation){
      setEmailError("Invalid Email")
      setCompanyNameError("This field cannot be empty")
      setDesignationError("This field cannot be empty")
    }
    else if (!(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(email)) && !designation){
      setEmailError("Invalid Email")
      setDesignationError("This field cannot be empty")
    }
    else if (!(/^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(email))){
      setEmailError("Invalid Email")
    }
    else if(!designation && !email) {
      setDesignationError("This field cannot be empty")
      setEmailError("This field cannot be empty");
    }
    else if(!name){
      setNameError("This field cannot be empty")
    }else if(!companyName){
      setCompanyNameError("This field cannot be empty")
    }else if(!designation){
      setDesignationError("This field cannot be empty")
    }else if(!email){
      setEmailError("This field cannot be empty");
    }else if(/^\s*(?:\+?(\d{1,3}))?[-. (]*(\d{3})[-. )]*(\d{3})[-. ]*(\d{4})(?: *x(\d+))?\s*$/.test(conatct)===false && conatct){
      setContactError("Invalid Contact Number")
    }else{
      return true
    }
  }


  const submit = () => {
    setNameError("");
    setCompanyNameError("");
    setCountryError("");
    setDesignationError("");
    setEmailError("");
    setContactError("");
    let result = valid()
    let ar = [];
    for (const prop in intrest) {
      if (intrest[prop]) {
        if (prop === "insider")
          ar.push("Insider");
        if (prop === "commercial")
          ar.push("Commercial Excellence");
        if (prop === "supplyChain")
          ar.push("Supply Chain Analytics");
        if (prop === "business")
          ar.push("Business Intelligence");
      }
    }
    let body = {
      body: {
        reciverName: reciverName ? reciverName : "",
        reciverDesignation: reciverDesignation ? reciverDesignation : "",
        name: name,
        companyName: companyName,
        country: country,
        designation: designation,
        contact: conatct,
        email: email,
        areaOfIntrest: ar === [] ? "" : ar,
        remarks: remarks,
        subject: `Customer:${name}`
      }
    }

    if (result) {
      onClose();
      Axios.post("/mail/sendInfo",
        body
      );
      Axios.post("/mail/sendAuto",
        { receiver: body.body.email }
      );
      cogo.success("Email sent");
    } else {
      cogo.error("Could not send email")
    }
  }

  React.useEffect(()=>{
    window.addEventListener('scroll', handleScroll, true);
  },[])

  const handleScroll = (e) => {
    if (e.target.classList && e.target.classList.contains("on-scrollbar") === false) {
      e.target.classList.add("on-scrollbar");
    }
  }

  const handleChange = (e) => {
    let validity = /^([a-zA-Z0-9_\-.]+)@([a-zA-Z0-9_\-.]+)\.([a-zA-Z]{2,5})$/.test(email)
     
    setEmail(e);
    if(!validity){
      setEmailError("Invalid Email")
    } else {
       
      setEmailError("");
    }
  }

  
  return (
    <Modal size="md" centered show={open} dialogClassName="modal" style={{paddingRight: "0px"}}>
      <Modal.Header className="header safari_only" >
        <div className="d-flex justify-content-between w-100 align-items-center">
              <h2 className="font mb-0">Contact Us</h2>
            <button className="closeBtn p-0" onClick={onClose}>
              <X />
            </button>
        </div>
      </Modal.Header>

        <Modal.Body className="modal__body">
        {/* <Simplebar style={{maxHeight: "100%"}} autoHide="false"> */}
          <div>
            <div className="colums">
              <p className="text-capitalize text-bold font-weight-bold mb-1">
                Name*
              </p>
              <input
                className={nameError ? "error-input" : "contact-input"}
                placeholder="Name"
                value={name}
                onChange={e => _.debounce(setName(e.target.value), 1000)}
                required={true}
              />
              <p style={{ color: "red", fontSize: "12px" }}>{nameError}</p>
            </div>

            <div className="colums">
              <p className="text-capitalize text-bold font-weight-bold mb-1">
                Company Name*
              </p>
              <input
                className={companyNameError ? "error-input" : "contact-input"}
                placeholder="Company Name"
                value={companyName}
                onChange={e => setCompanyName(e.target.value)}
              />
              <p style={{ color: "red", fontSize: "12px" }}>{companyNameError}</p>
            </div>

            <div className="colums">
              <p className="text-uppercase text-bold font-weight-bold mb-1">
                Country*
              </p>
              <Dropdown
                onSelect={(_, e) => {
                 return onCountrySelect(e.target.innerText.toUpperCase())
                }
                }
              >
                <Dropdown.Toggle className="drpDwn">
                  {`${country}`}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                  {countries.map((country, index) => (
                    <Dropdown.Item
                      key={index}
                      href="#"
                      value={country.key}
                      onClick={() => setContactCountry(country.key)}
                      className="text-uppercase"
                    >
                      {country.value}
                    </Dropdown.Item>
                  ))}
                </Dropdown.Menu>
              </Dropdown>
              <p style={{ color: "red", fontSize: "12px" }}>{countryError}</p>
            </div>

            <div className="colums">
              <p className="text-capitalize text-bold font-weight-bold mb-1">
                Designation*
              </p>
              <input
                className={designationError ? "error-input" : "contact-input"}
                placeholder="Designation"
                value={designation}
                onChange={e => setDesignation(e.target.value)}
              />
              <p style={{ color: "red", fontSize: "12px" }}>{designationError}</p>
              </div>

            <div className="colums">
             <p className="text-capitalize text-bold font-weight-bold mb-1">
                Contact
              </p>
               <PhoneInput 
               inputStyle={!contactError ? {width: "100%", border: "2px solid rgba(6, 51, 63, 0.3)", borderRadius: "10px"} : {width: "100%", border: "2px solid red", borderRadius: "10px"}}
               country={contactCountry}
               value={conatct}
               placeholder="Contact"
               enableSearch={true}
               disableSearchIcon={true}
               onChange={e => setContact(e)}
               />
               <p style={{ color: "red", fontSize: "12px" }}>{contactError}</p>
            </div>

            <div className="colums">
              <p className="text-capitalize text-bold font-weight-bold mb-1">
                Email*
              </p>
              <input
                className={emailError ? "error-input" : "contact-input"}
                placeholder="Email"
                value={email}
                onChange={e => handleChange(e.target.value)}
              />
              <p style={{ color: "red", fontSize: "12px" }}>{emailError}</p>
            </div>

            <div style={{ display: "flex" }}>
              <div className="checkbox" >
                <p className="text-capitalize text-bold font-weight-bold mb-1">
                  Please Indicate Area of Interest
          </p>
                <label className="checkboxcontainer">Insider
                <input type="checkbox" onClick={() => { _.debounce(setIntrest(prev => { return { ...prev, insider: !prev.insider } }), 1000) }} />
                  <span className="checkmark"></span>
                </label>

                <label className="checkboxcontainer">Commercial Excellence
                <input type="checkbox" onClick={() => { _.debounce(setIntrest(prev => { return { ...prev, commercial: !prev.commercial } }), 1000) }} />
                  <span className="checkmark"></span>
                </label>
                <label className="checkboxcontainer">Supply Chain Analytics
                <input type="checkbox" onClick={() => { _.debounce(setIntrest(prev => { return { ...prev, supplyChain: !prev.supplyChain } }), 1000) }} />
                  <span className="checkmark"></span>
                </label>

                <label className="checkboxcontainer">Business Intelligence Service
                <input type="checkbox" onClick={() => { _.debounce(setIntrest(prev => { return { ...prev, business: !prev.business } }), 1000) }} />
                  <span className="checkmark"></span>
                </label>
              </div>
              </div>

              <div className="colums">
                <p className="text-capitalize text-bold font-weight-bold mb-1">
                Remarks
          </p>
              <textarea
                className="contact-input"
                placeholder="Remarks"
                value={remarks}
                onChange={e => setRemarks(e.target.value)}
              />
            </div>
          </div>
          {/* </Simplebar> */}
        </Modal.Body>
      <Modal.Footer className="footer">
        <button className="submitBtn" onClick={submit}>Submit</button>
      </Modal.Footer>
    </Modal>
  )
}

export default ContactModal