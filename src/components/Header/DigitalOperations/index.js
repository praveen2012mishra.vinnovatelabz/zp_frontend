import React from "react"
import { UncontrolledDropdown, DropdownToggle, DropdownMenu } from "reactstrap"
import DigitalOperationsSubDropdown from "./DropDownRight"
import "../style.scss"
function DigitanOperations({
  toggle,
  removeToggle,
  mobileToggClick,
  isOpen,
  dropDownId,
  arrowUp,
  arrowDown,
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  closeMobileHeader
}) {
  return (
    <UncontrolledDropdown
      nav
      inNavbar
      id="2"
      onMouseEnter={() => toggle("2")}
      onMouseLeave={e => removeToggle(e, "2")}
      isOpen={dropDownId === "2" && isOpen}
    >
      <DropdownToggle
        nav
        id="2"
        className={dropDownId === "2" ? "active-nav" : ""}
        onClick={e => mobileToggClick(e, "2")}
      >
        Digital Operations
        <img
          src={dropDownId === "2" ? arrowUp : arrowDown}
          id="2"
          alt="arrow"
          className="imageArrow"
        />
      </DropdownToggle>
      <DropdownMenu right>
        <DigitalOperationsSubDropdown
          selectedLink={selectedLink}
          innerDropDownItems={innerDropDownItems}
          removeDropdownItems={removeDropdownItems}
          closeMobileHeader={closeMobileHeader}
        />
      </DropdownMenu>
    </UncontrolledDropdown>
  )
}

export default DigitanOperations
