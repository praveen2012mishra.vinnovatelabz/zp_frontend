import React from "react"
import "../style.scss"
import { Nav, NavItem, NavLink } from "reactstrap"

function DropDownRight({
  selectedLink,
  innerDropDownItems,
  removeDropdownItems,
  closeMobileHeader
}) {
  return (
    <div className="menu-wrap">
      <div className="custom-left-nav">
        <button className="nav-left-link mob active dropDownButtonClass">Digital Operations</button>
        {/* //active */}
      </div>
      <div className="custom-right-nav">
        <Nav right="right" className="sub-dropdown">
          <NavItem>
            {" "}
            <NavLink
              href="https://zuelligpharmadev1.bigmachines.com/commerce/display_company_profile.jsp?page=%2Fcommerce%2Fdisplay_company_profile.jsp&BM_URL_CAPTURE_COOKIE=zuelligpharmadev1&BM_BROWSER_COOKIE=Netscape"
              name="createorder"
              className={
                selectedLink === "createorder" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
            >
              Create Order
            </NavLink>
          </NavItem>
          <NavItem>
            {" "}
            <NavLink
              href="https://www.eztracker.io/"
              name="tracker"
              target="_blank"
              className={
                selectedLink === "tracker" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
             // style={{ color: "#ffffff" }}
            >
              ezTracker
            </NavLink>
          </NavItem>
          {/* <NavItem>
            {" "}
            <NavLink
              href="#"
              name="realworld"
              className={
                selectedLink === "realworld"
                  ? "inner-nav-active lastChild"
                  : "lastChild"
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
            >
              Real World Data
            </NavLink>
          </NavItem> */}
          <NavItem>
            {" "}
            <NavLink
              href="https://zpmt.zuelligpharma.com/"
              target='_blank'
              name="passwordMaintenance"
              className={
                selectedLink === "passwordMaintenance" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
            >
              Password Maintenance
            </NavLink>
          </NavItem>
          {/* <NavItem>
            {" "}
            <NavLink
              href="https://zptc.zuelligpharma.com/"
              name="tablaeuCommunity"
              target="_blank"
              className={
                selectedLink === "tablaeuCommunity" ? "inner-nav-active" : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
            >
              Tableau Community
            </NavLink>
          </NavItem> */}
          <NavItem>
            {" "}
            <NavLink
              href="https://iwi.zuelligpharma.com/"
              name="infectionWatchInitiative"
              target="_blank"
              className={
                selectedLink === "infectionWatchInitiative"
                  ? "inner-nav-active"
                  : ""
              }
              onMouseEnter={innerDropDownItems}
              onMouseLeave={removeDropdownItems}
              onClick={closeMobileHeader}
            >
              Infection Watch Initiative
            </NavLink>
          </NavItem>
        </Nav>
      </div>
    </div>
  )
}

export default DropDownRight
