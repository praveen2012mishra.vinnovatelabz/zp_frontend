import React from "react"
import "../style.scss"
import { Link } from "gatsby"

function Search(props) {
  let zpUrl = "https://";
      return props.displaySearchData.map((item, index) => {
        return (
          <div
            key={index}
            style={{
              backgroundColor: "#378889",
              color: "white",
              padding: "5px",
              cursor: "pointer",
            }}
          >
          {item.url.includes(zpUrl) ? 
            <a
              // onClick={()=>onClick(item.label)}
              href={item.url}
              target="_blank"
              style={{ color: "#ffffff",textDecoration:"none"}}
            >
              {item.label}
            </a>
            :
            <Link
              onMouseDown={()=>props.handler(item.label)}
              onTouchStart={()=>props.handler(item.label)}
              to={item.url}
              style={{ color: "#ffffff",textDecoration:"none"}}
            >
              {item.label}
            </Link>}
          </div>
        )
      })
}

export default Search
