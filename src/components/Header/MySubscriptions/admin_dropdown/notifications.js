import React from 'react'
// import moment from 'moment'
import {
    FaCaretUp,
    // FaEllipsisV,
    // FaExclamation,
  } from "react-icons/fa"
  //AiFillCaretDown
  import {BsDot} from "react-icons/bs"
  import { Nav, NavItem } from "reactstrap"
  import Styles from "./style.module.scss"
  import {basePath} from "../../../../components/basePath"
  import { Link } from "gatsby"

function Notifications({
    showIds,
    setNotificationToggle,
    notificationsData,
    notificationsLength,
    closeMobileHeader,
    notificationToggle
}) {

    // const [notificationNumber, setNotificationNumber] = useState("")

    return (
        <div style={{width:"90%", display:notificationToggle? "block":"none"}}>
                <p className={Styles.innerToggle} style={{marginBottom:"0"}}>
                  <FaCaretUp className="icon" width="0.5em" />
                  <span style={{marginLeft: "0px", cursor:"pointer"}} onClick={setNotificationToggle}
                  role="button"
                  tabIndex={0}
                  onKeyPress={()=>{}}
                  >
                    Notifications
                  </span>
                  <span className={Styles.notification}>({notificationsLength}) </span>
                </p>
                <Nav vertical style={{paddingBottom:"10px"}}>
                  {notificationsData.map((item, index) => {
                    // let notificationTime = `${new Date(item.createdAt).toLocaleString('en-US', { hour: 'numeric', minute: 'numeric', hour12: true })} ${moment(new Date(item.createdAt)).format("MMM DD,YYYY")}`
                    let header = item.header.slice(0,21)
                    return showIds.includes(item._id) ? 
                    <NavItem id={Styles.unreadnotiParent}>
                        <Link className={Styles.unreadnoti}>
                          <span>
                            <span
                              style={{
                                color: "white",
                              }}>
                              <BsDot/> {`${header}...`}
                            </span>
                          </span>
                        </Link>
                        </NavItem> 
                        : ""
                  })}
                  </Nav>

                <div id={Styles.nott}>
                <Link
                to={`${basePath}/notifications/`}
                onClick={()=>closeMobileHeader()}
                // onTouchStart={()=>closeMobileHeader()}
                className={Styles.allNot}
                  style={{
                    color: "#c3d422",
                    paddingLeft:"28px",
                    textDecoration:"none",
                    cursor: "pointer",
                    fontSize:11
                  }}
                >
                  See all notifications
                </Link>
                </div>
              </div>
    )
}

export default Notifications
