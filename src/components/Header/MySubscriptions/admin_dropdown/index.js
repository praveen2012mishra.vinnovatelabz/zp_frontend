import React, { useState, useEffect } from "react"
import { Nav } from "reactstrap"
import Link from "gatsby-link"
import Styles from "./style.module.scss"
import {
  FaCaretDown
} from "react-icons/fa"
import axios from "../../../../../axiosConfig"
import { connect } from "react-redux"
import Cookies from "js-cookie"
import { setUserGroupName } from "../../../../services/Redux/actions/useGroupAction"
import Notifications from "./notifications"
import axiosInstance from "../../../../../axiosConfig"
import cogoToast from "cogo-toast"
import { useSelector } from "react-redux";
import { keyBy } from "lodash"
function Dropdown({
  authData = null,
  innerOptions = [],
  notificationsData,
  notificationsLength,
  showIds,
  closeMobileHeader,
  mobileToggle,
  isAdmin
}) {
  const [options, setOptions] = useState([
    { label: "Sign in", type: null, href: "/Login" },
  ])

  const [notificationToggle, setNotificationToggle] = React.useState(false)
  const notificationsLengthCount = useSelector(state => state.notification.count);
  useEffect(() => {
    if (authData) {
      if (innerOptions.length > 0) {
        if (!Cookies.get("inner_option")) {
          let ar = [
            { label: `Notifications`, type: null, href: null },
            { label: `My Subscriptions`, type: null, href: "/auth/subscription" },
            innerOptions[0].isAdmin ? { label: "Admin", type: null, href: "/auth/admin" } : null,
            { label: "Profile", type: null, href: "/auth/profile" },
            { label: "Sign Out", type: null, href: null },
          ]
          setOptions(ar)
        } else {
          let changedOption = Cookies.getJSON("inner_option")  
          innerOptions.map((i) => {
            if (i.label === changedOption.label) {
              let ar = isAdmin ? [
                { label: `Notifications`, type: null, href: null },
                { label: `My Subscriptions`, type: null, href: "/auth/subscription" },
                { label: "Admin", type: null, href: "/auth/admin" },
                { label: "Profile", type: null, href: "/auth/profile" },
                { label: "Sign Out", type: null, href: null },
              ] : [
                  { label: `Notifications`, type: null, href: null },
                  { label: `My Subscriptions`, type: null, href: "/auth/subscription" },
                  { label: "Profile", type: null, href: "/auth/profile" },
                  { label: "Sign Out", type: null, href: null },
                ]
              setOptions(ar)
            }
            return i
          })
        }
      } else {
        setOptions([
          { label: `Notifications`, type: null, href: null },
          { label: "Profile", type: null, href: "/auth/profile" },
          { label: "Sign Out", type: null, href: null },
        ])
      }
    } else {
      setOptions([{ label: "Sign in", type: null, href: "/Login" }])
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  useEffect(() => {
    if (!mobileToggle) {
      setNotificationToggle(false)
    }
  }, [mobileToggle])
  return (
    <div className={Styles.dropdownToggle}>
      {options.map((item, key) =>
        item ? item.label === "Notifications" ? (
          <Nav key={key}>
            <Link
              role="button"
              className={Styles.not}
              style={{ display: !notificationToggle ? "block" : "none" }}
              onClick={closeMobileHeader}
              to="/auth/notifications"
              key={key}
            >

              <span><span style={{ padding: "2px" }}>{item.label}</span>{notificationsLengthCount > 0 ? (
                <span className={Styles.notification}>
                  ({notificationsLengthCount}){" "}
                </span>
              ) : (
                  ""
                )}</span>
            </Link>
          </Nav>
        ) : (
            <Link
              to={item.href ? item.href : "./"}
              onClick={() => {
                if (item.label === "Sign Out") {
                  axios
                    .post("/saml/singleLogout", {
                      name: authData.nameId,
                      session: authData.sessionIndex,
                      in_resp: authData.inResponse,
                      status: "not known",
                    })
                    .then(res => {
                      Cookies.remove("inner_option")
                      Cookies.remove("ADgroups")
                      Cookies.remove("saml_response")
                      Cookies.remove("Group_names")
                      localStorage.removeItem('subsData')
                      window.location.href = res.data.redirect
                    })
                    .catch(e => console.log(e))
                  Cookies.remove("2fa")
                } else if (item.label !== "Notifications") {
                  closeMobileHeader()
                }
              }}
              key
            >
              {item.label}
            </Link>
          ) : null
      )}

    </div>
  )
}
const mapStateToProps = state => ({
  notification: state.notification,
  userGroup: state.userGroup.name,
})

const mapDispatchToProps = dispatch => ({
  setSelectedGroupName: name => dispatch(setUserGroupName(name)),
})
export default connect(mapStateToProps, mapDispatchToProps)(Dropdown)
