import React, { useState, useEffect } from "react"
import Dropdown from "./admin_dropdown"
import {
  Nav,
  NavItem,
  NavLink,
  Dropdown as Drop,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
} from "reactstrap"
// import {Dropdown as Drop} from "react-bootstrap"
import "../style.scss"
import { FiSearch } from "react-icons/fi"
import axios from "../../../../axiosConfig"
import Cookie from "js-cookie"
import Styles from "./admin_dropdown/style.module.scss"
import StylesSubs from "./style.module.scss"
// import {
//   FaCaretDown
// } from "react-icons/fa"
import Cookies from "js-cookie"
import axiosInstance from "../../../../axiosConfig"
import cogoToast from "cogo-toast";
import { countNotification } from "../../../services/Redux/actions/notification"
import { useDispatch } from "react-redux";
import { useSelector } from "react-redux";
function MySubscriptions({
  searchBoxDesktop,
  user,
  userHover,
  mobileToggle,
  searchToggle,
  authData,
  innerOptions,
  closeMobileHeader,
}) {
  const [toggle, setToggle] = useState(false)
  const [notifications, setNotifications] = useState([])
  const [notificationsLength, setNotificationsLength] = useState(0)
  const [showIds, setShowIds] = useState([])
  const [usericon, setUser] = useState([])
  const [isAdmin, setAdmin] = useState(false)

  const notificationsLengthCount = useSelector(state => state.notification.count);
  const dispatch = useDispatch();

  const getNotiCount = async (groups,userId) => {
    let ar = groups.map(item =>item.label);
    const unReadNotifications = await axiosInstance.post("notification/getNotificationCountByGroups", {
      "groupArray":ar,
      "userId":userId
    })
    try {
        console.log(unReadNotifications.data);
        dispatch(countNotification(unReadNotifications.data.length))
    } catch {
        console.log(unReadNotifications);
    }
}
  useEffect(() => {
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    const userId = samlToken ? samlToken.user.name_id : null
    const groupName = Cookie.getJSON("inner_option")
    if (userId) {
      // console.log(innerOptions);
      if(innerOptions.length>0){
        getNotiCount(innerOptions,userId)
        const label=groupName?.label ? groupName.label : innerOptions[0].label
        const id=groupName?.id ? groupName.id : innerOptions[0].id
        // let isAdmin=false
          axiosInstance.get(`/group/fetchGroupOptions/${id}`)
          .then(res=>{
            setAdmin(res.data.isAdmin)
          }).catch(err=>cogoToast.error("Internal Server Error on Admin status"))  
          
      axios
        .get(`/notification/getNotificationsGroupName/${label}`)
        .then(res => {
          // console.log(res);
          setNotifications(res.data.notification)
          setNotificationsLength(res.data.checkedLength)
          // props.countNotification(res.data.checkedLength)
          let ids = []
          res.data.notification.map((item, index) => {
            if (!item.checked) ids.push(item._id)
            return item
          })
          setShowIds(ids)
        })
      }
      axios
        .get(`/users/getUserProfile/${userId}`)
        .then(res => {
          setUser(res.data.userProfile.profile_pic)
          // setLoading(false)
        })
        .catch(err => {
          console.error("Failed to get the data", err)
        })
    }
  }, [innerOptions])

  useEffect(() => {
    if (!mobileToggle) {
      setToggle(false)
    }
  }, [mobileToggle])

  // console.log("redux data --->", notificationsLengthCount);

  return (
    <Nav
      // className={Styles.ulDrop}
      style={{ alignItems: "center" }}
    >
      <Nav
        onMouseLeave={() => setToggle(false)}
        className={
          !searchBoxDesktop
            ? "ml-auto custom-right-navbar w!dt subBack"
            : "subBack"
        }
        navbar
        style={{
          position: "relative",
          backgroundColor: toggle ? "rgb(9, 70, 88)" : "transparent",
          // borderBottom: toggle ? "0.8px solid #2d3436" : "none",
        }}
      >
        {/* {authData && ( */}
        <NavItem onMouseEnter={() => setToggle(false)}>
          <GroupChangeDropDown options={innerOptions} />
        </NavItem>
        {/* )} */}
        <NavItem className="nav-icon">
          <NavLink
            className="user-nav-icon"
            onMouseEnter={() => setToggle(true)}
          >
            {usericon && usericon?.length > 0 ? (
              <>
                <img
                  src={usericon}
                  className={Styles.userimage}
                  alt="usericon"
                />
              </>
            ) : (
                <>
                  <img src={user} className="user" alt="user" />
                  <img src={userHover} className="user-hover" alt="user" />
                </>
              )}
            {notificationsLengthCount ? (
              <span
                className={Styles.notify}
                style={{ backgroundColor: "rgb(251, 93, 92)" }}
              />
            ) : null}
            <div
              onTouchStart={() => {
                setToggle(!toggle)
              }}
              className="userAccount"
            >
              Account
            </div>
          </NavLink>
          {toggle && (
            <div
              className="innerdropMenu"
              style={{ outline: "none" }}
              role="button"
              tabIndex={0}
              onMouseLeave={() => setToggle(false)}
            >
              <Dropdown
                authData={authData}
                innerOptions={innerOptions}
                notificationsData={notifications}
                notificationsLength={notificationsLength}
                showIds={showIds}
                closeMobileHeader={closeMobileHeader}
                mobileToggle={mobileToggle}
                isAdmin={isAdmin}
              />
            </div>
          )}
        </NavItem>
        <NavItem className="nav-icon">
          {/* <img src={search} className="search" onClick={searchToggle} /> */}
          <FiSearch
            size="1.4em"
            style={{ marginBottom: 0 }}
            className="icon"
            onClick={searchToggle}
          />
        </NavItem>
      </Nav>
    </Nav>
  )
}


export default MySubscriptions

const GroupChangeDropDown = ({ options = [] }) => {
  const [selectedGroupName, setSelectedGroupName] = useState({})
  const [toggle, setToggle] = useState(false)
  useEffect(() => {
    // console.log(options, "i am options");
    let inner = Cookies.getJSON("inner_option")
      ? Cookies.getJSON("inner_option")
      : null
    setSelectedGroupName(inner ? inner : options[0])
  }, [options])
  const handleInnerDropDown = item => {
    setSelectedGroupName(item)
    Cookies.set("inner_option", item, { expires: 1 })
    window.location.reload()
  }
  return options.length > 0 ? (
    <Drop
      isOpen={toggle}
      onTouchStart={() => setToggle(!toggle)}
      onMouseOver={() => setToggle(true)}
      onMouseLeave={() => setToggle(false)}
      className={StylesSubs.DropdownListGroups}
    >
      <DropdownToggle
      style={{backgroundColor:"#094658 !important"}}
      className={StylesSubs.dropButton} caret>
        {selectedGroupName?.display_name}
      </DropdownToggle>
      <DropdownMenu className={StylesSubs.dropMenu}>
        {options.map(item => (
          <DropdownItem
            onClick={() => handleInnerDropDown(item)}
            onTouchStart={() => handleInnerDropDown(item)}
          >
            {item.display_name}
          </DropdownItem>
        ))}
      </DropdownMenu>
    </Drop>
  ) : (
      <div />
    )
}
