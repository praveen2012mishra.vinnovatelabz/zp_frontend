import React from "react"
// import { Button } from 'reactstrap';
import styles from "./style.module.scss"

const ButtonItem = ({BColor,Color, title, onClick }) => (
<button className={styles.buttonStyle} onClick={onClick}
style={{backgroundColor:`${BColor}`,color:`${Color}`}}>{title}</button>
)

export default ButtonItem
