import React from 'react'
import ButtonItem from './Button'
import Styles from "./style.module.scss"

function DownButton({ modalListener }) {
    const mainModalListner = () => {
        modalListener()
    }
    return (
        <div className={Styles.btn} style={{ padding: "20px", display: "flex" }}>
            <div style={{ margin: "auto" }}>
                <ButtonItem
                    BColor="#c3d422"
                    title="Don't know where to start? Please reach out to us"
                    onClick={mainModalListner}
                />
            </div>
        </div>
    )
}

export default DownButton
