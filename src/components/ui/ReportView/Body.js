import React from "react"
import styles from "./style.module.scss"
import { navigate } from "gatsby"
import helpicon from "../../../assets/images/helpicon2.png"
import ModelImage from "./ModelImage"
import { Row, Col, Button } from "react-bootstrap"
import Assist from "../../../assets/images/DashboardAssist.png"
// import { Bar } from "react-chartjs-2"
// import Tab from "../../Tab"
// import TableauReport from 'tableau-react';
import loadable from "@loadable/component"
import { Container } from "reactstrap"
const TableauReport = loadable(() => import("tableau-react"))
function SalesFlashBody({
  url = "",
  label = "",
  height = "",
  description = "",
  backListener,
}) {
  const [hover, setHover] = React.useState(false)
  const [open, setOpen] = React.useState(false)
  const [width, setWidth] = React.useState(1280)
  const [, updateState] = React.useState()
  const forceUpdate = React.useCallback(() => updateState({}), [])
  // const [tabItems, setTabItems] = React.useState([])
  // const [activeTab, setActiveTab] = React.useState(0)
  const option = {
    height: `${height || "2050"}px`,
    width: "100%",
    hideTabs: true,
    marginTop: 20,
    overflowX: "hidden",
    borderStyle: "none",
  }
  const [tableauURL, setTableauURL] = React.useState(url)
  let phone = ":mobile=true&:device=phone"
  let tablet = ":tablet=true&:device=tablet"
  React.useEffect(() => {
    let str = url
    let result = str?.match(/#\/site/)
    let result2 = str?.match(`/#/`)
    if (result) {
      let newStr = str?.replace(/#\/site/, "t")
      setTableauURL(newStr)
    } else if (result2) {
      let newStr = str?.replace("/#/", "/")
      setTableauURL(newStr)
    } else {
      setTableauURL(str)
    }
    // alert(str)
    setWidth(window.screen.width)
    forceUpdate()
  }, [url, label])
  const mainBackListener = () => {
    backListener()
  }
  return (
    <div>
      <Container fluid style={{ padding: 20 }}>
        <Row>
        {backListener ? (
              <div style={{float:"left",marginLeft:10}} 
              >
                <Button className={styles.btn} onClick={mainBackListener}>
                  Back
                </Button>
              </div>
            ) : null}
        </Row>
        <Row>
          <Col xs={6} style={{textAlign:"start"}}>
            <div className={`${styles.headingColor}`}>{label}</div>
          </Col>
          <Col xs={6} className="justify-content-end">
            <div
              className={`${styles.helpContainer}`}
              style={{ width: "70px" }}
              role="button"
              onMouseOver={() => setHover(true)}
              onMouseOut={() => setHover(false)}
              tabIndex={0}
              onClick={() => setOpen(true)}
              onKeyPress={() => { }}
            >{!hover ? (
              <div className={styles.help}>Help</div>
            ) : (
                <img src={helpicon} className={styles.helpicon} alt="" />
              )}
            </div>
          </Col>
        </Row>
        <Row>
          <Col className="text-align-start">
            <div className={`${styles.subHeadingColor}`}>
              {/* Analyse your historical monthly and yearly sales here */}
              {description}
            </div>
          </Col>
        </Row>
        <hr />
        {url && url !== "" ? (
          <TableauReport
            // url={`https://zplive.zuelligpharma.com/t/ZiP-Demo/views/TableauTemplate/WhiteDashboard?:isGuestRedirectFromVizportal=y&:embed=y` }
            // url={tabs[0].elements[activeTab].url}
            url={tableauURL}
            query={`?${width < 600 ? phone : tablet}`}
            options={option}
          />
        ) : (
            <div>Invalid url</div>
          )}
        <Row style={{ textAlign: "center", marginTop: 20 }}>
          <Col>
            <button
              className={`btn btn-sm ${styles.button}`}
              onClick={() => navigate("/auth/commercialExp/InfraredSales")}
            >
              ANALYSE SALES REPORT
            </button>
          </Col>
        </Row>
        <ModelImage open={open} close={() => setOpen(false)} image={Assist} />
      </Container>
    </div>
  )
}

export default SalesFlashBody
