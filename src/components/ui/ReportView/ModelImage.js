import React from "react"
import { Modal } from "react-bootstrap"
// import Assist from "../../../assets/images/DashboardAssist.png"
import { X } from "react-bootstrap-icons"
import Style from "./style.module.scss"
function ModelImage({ open, close, image }) {
  return (
    <Modal size="lg" centered show={open} dialogClassName="modal">
      <img src={image} style={{ maxWidth: "100%", height: "100%" }} alt="" />
      <div
        className={Style.modalCross}
        onClick={close}
        role="button"
        tabIndex={0}
        onKeyPress={() => {}}
      >
        <X />
      </div>
    </Modal>
  )
}

export default ModelImage
