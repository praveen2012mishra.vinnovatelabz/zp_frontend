import React from "react"
import Styles from "./style.module.scss"
import helpicon from "../../../assets/images/helpicon2.png"
import ModelImage from "../ReportView/ModelImage"
// import BIs from "../../../assets/images/BI.png"
// import Insider from "../../../assets/images/Insider.png"
// import Infrared from "../../../assets/images/Infrared.png"
// import SupplyChain from "../../../assets/images/spplyChain.png"
export const SubHeader = ({ title, subTitle, onBtnClick,image }) => {
  const [hover, setHover] = React.useState(false)
  const [open, setOpen] = React.useState(false)
  const [img, setImage] = React.useState("")

  const openImage = () => {
    // if (window.location.href.includes("insider")) setImage(Insider)
    // else if (window.location.href.includes("Infrared")) setImage(Infrared)
    // else if (window.location.href.includes("supplyChain")) setImage(SupplyChain)
    // else if (window.location.href.includes("datamanagement")) setImage(BIs)
    setImage(image)
    setOpen(true)
  }
  return (
    <div className={Styles.tabDiv}>
      <div className={Styles.tabHeader}>
        <div>
          <span style={{ color: "#094658" }} className={Styles.title}>
            {title}
          </span>{" "}
          <br />
          <span style={{ color: "#378889", fontSize: 15 }}>{subTitle}</span>
        </div>
        <span
          onMouseOver={() => setHover(true)}
          onFocus={() => {}}
          onBlur={() => {}}
          onMouseOut={() => setHover(false)}
          role="button"
          tabIndex={0}
          onClick={() => openImage()}
          className={Styles.hlpSpan}
        >
          <div style={{ width: "70px" }}>
            {!hover ? (
              <div className={Styles.help}>Help</div>
            ) : (
              <img src={helpicon} className={Styles.helpicon} alt="icon" />
            )}
          </div>
        </span>
      </div>
      <ModelImage open={open} close={() => setOpen(false)} image={img} />
    </div>
  )
}

export default SubHeader
