import React from "react"
import styles from "./style.module.scss"

const TableHeader = (props) => (
    
    <div className={`${styles.TableHeaderContainer} m-2`}>
        {props.tableHeaderList.map((item,id)=>(
            <div className={styles.headerContent} id={id} style={{ minWidth: id===0 ? "206px" : "", marginRight: "1rem" }} >
                {item}
            </div>
        ))}
    </div>
)

export default TableHeader
