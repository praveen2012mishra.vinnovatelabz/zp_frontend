import React from "react"

import styles from "./style.module.scss"
import ButtonTabItem from "./ButtonTabItem"

const ButtonTab = ({ items, activeTab, onTabChange }) => {
  // console.log(items.length);
  return (
    <div
      className={items.length > 6 ? `d-flex flex-row justify-content-around ${styles.buttonTabs} ${styles.btnTab} button-pills` : `d-flex flex-row justify-content-around ${styles.buttonTabs} button-pills`}
    >
      {items?.map((item, idx) => (
        item ? 
        <ButtonTabItem
          key={idx}
          active={idx === activeTab}
          activeClassname={styles.active}
          title={item.lable}
          onClick={() => onTabChange(idx)}
        /> : null
      ))}
    </div>
  )
}

export default ButtonTab
