import React from "react"
import styles from "./style.module.scss"
const ButtonTabItem = ({ onClick, active, activeClassname, title }) => (
  <div className={active ? activeClassname : styles.normal} onClick={onClick}
  role="button"
  tabIndex={0}
  style={{outline: "none"}}
  onKeyPress={()=>{}}
  >
    <p>{title}</p>
  </div>
)

export default ButtonTabItem
