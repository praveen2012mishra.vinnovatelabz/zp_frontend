import React from 'react'
import Styles from "./style.module.scss"
export default function tabOption(props) {
    return (
        <span
            key={props.tabIndex}
            role="button"
            className={props.status==props.tabIndex? Styles.tabActive:Styles.tab}
            onClick={() => props.onClick()}
        ><p>{props.tabName}</p>
        </span>
    )
}
