import React, { useState } from "react"
import { Dropdown } from "react-bootstrap"
// import { FaCaretDown } from "react-icons/fa"
import styles from "./style.module.scss"
// import { style } from "d3"

const DropDownButton = props => {
  // const [dropStatus, setStatus] = useState(false)
  const [dropdownOpen, setDropdownOpen] = useState(false)

  const toggle = () => setDropdownOpen(prevState => !prevState)
  return (
    <Dropdown
      direction="down"
      className={styles.buttonFlex}
      isOpen={dropdownOpen}
      toggle={toggle}
      onClick={() => props.onClick(props.item.heading)}
    >
      <Dropdown.Toggle drop={"up"} caret className={styles.toggleBtn}>
        {props.searchData[props.item.value]
          ? props.item.heading==="Month"?props.keyToMonth(props.searchData[props.item.value]):props.searchData[props.item.value]
          : "all"}
      </Dropdown.Toggle>
      <Dropdown.Menu
        className={
          !props.dropdownData.length
            ? `${styles.toggleMenu} ${styles.padd}`
            : `${styles.toggleMenu}`
        }
      >
        {props.item.value!=="MonthRun"?<Dropdown.Item 
        name={props.item.value} 
        className={styles.item} 
        style={{textTransform:"capitalize"}} 
        onClick={e => props.dropdownClick(e, props.item.heading, "all")}>
          All
        </Dropdown.Item>:""}
        {props.dropdownData.map((item, index) => {
          return (
            <Dropdown.Item
              key={index}
              name={props.item.value}
              className={styles.item}
              onClick={e => props.dropdownClick(e, props.item.heading, item)}
            >
              {props.item.heading==="Month"?props.keyToMonth(item): item}
            </Dropdown.Item>
          )
        })}
      </Dropdown.Menu>
    </Dropdown>
  )
}

export default DropDownButton
