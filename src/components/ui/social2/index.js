import React, { useState, useEffect } from 'react'
import { AiFillLinkedin } from "react-icons/ai"
import { FaYammer } from "react-icons/fa"
import styles from "./style.module.scss"


function Social2({ lineHeight, noMr }) {

  const [bottom, setBottom] = useState("fixed")
  const [he, setHe] = useState("50vh")


  // useEffect(() => {
  //   let mounted = true
  //   const changeCss = () => {
  //     if (mounted) {
  //       window.scrollY >= 1000 ? setHe("28vh") : setHe("50vh")
  //     } else return null
  //   }
  //   window.addEventListener("scroll", changeCss, false)
  //   return () => {
  //     mounted = false
  //     window.removeEventListener("scroll", changeCss)
  //   }
  // }, [lineHeight])
  return (
    <div
      // style={{ position: bottom, marginLeft: noMr ? "0" : "4%", top: bottom === "fixed" && "85px" }}
      style={{ position: "fixed", marginLeft: noMr ? "0" : "4%", height: "100vh", top:"0"}}
    >
      <div className={styles.line} style={{ height: "50vh" }}></div>
      <div className={styles.marg} role="button" tabIndex={0} onClick={() => {
        window.open(
          "https://www.linkedin.com/company/zuellig-pharma-analytics/")
      }} onKeyPress={() => { }}>
        <AiFillLinkedin className={styles.SocialIcons} style={{ height: "1.1em", width: "1.1em" }} />
      </div>
      <div className={styles.marg} role="button" tabIndex={0}
        onClick={() => { window.open("https://www.yammer.com/zuelligpharma.com/") }}
        onKeyPress={() => { }}
      >
        <FaYammer className={styles.SocialIcons} style={{ height: "1em", width: "1em" }} />
      </div>
      <div className={styles.line} style={{ height: "100%" }}></div>
    </div>
  )
}

export default Social2
