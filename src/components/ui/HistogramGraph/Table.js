import React from 'react'
import styles from "./style.module.scss"
import { Table } from "react-bootstrap"
// import ButtonItem from "../Button/Button"
import { FiSave} from "react-icons/fi"
// import {ImCancelCircle} from "react-icons/im"
// import {GiSave} from "react-icons/gi"
import {BsCheckBox, BsInfoCircle} from "react-icons/bs"
// import {BiEditAlt} from "react-icons/bi"
import {RiDeleteBin6Line} from "react-icons/ri"
import {GrView} from "react-icons/gr"
function HistogramTable({
    inventoryHeader,
    beginningInventory,
    changeInventoryInputs,
    incomingListData,
    getDropdown,
    dropdownData,
    outgoingListData,
    endingInventory,
    saveUpdate,
    previewChanges,
    previewData={},
    previewInventory,
    dataArray=[],
    selectedDate,
    showApprove
}) {

  
  const dropDownView = (mainItem,type) => (
   mainItem.dropdown?.map((ddItem,ddIndex)=>(
     ddItem && <div key={ddIndex} className={styles.dateHeader}>
    <div className={styles.dateHeading}>
      <input type="radio" checked={previewData.id===ddIndex && previewData.heading===mainItem.heading} onClick={()=>previewChanges(ddIndex,mainItem.heading)} style={{margin: "auto 0.5em auto auto"}}/>
    <input type="text" value={ddItem.Label} name="title" disabled={previewData.id!==ddIndex || previewData.heading!==mainItem.heading} className={previewData.id===ddIndex && previewData.heading===mainItem.heading?styles.inputOnChecked:styles.histoInputSea} style={{textTransform:"capitalize"}} onChange={e=>changeInventoryInputs(e,{},"dd",ddIndex,mainItem.heading)}/>
    </div>
    <tr className={styles.dateContent}>
      {
        ddItem.Series?.map((series,ind)=>(
          <td key={ind} style={{display:dataArray.includes(series.key)?"block":"none",
          borderRight:selectedDate===series.key?"1px solid #06333f":"none"
          }}>
            {setContent(series,changeInventoryInputs,"dd",ddIndex,mainItem,previewData,selectedDate)}
          </td>
          ))
      }
      {/* <td onClick={(e)=>getDropdown(e,mainItem,type,false,ddItem.Label)}>{minusSign()}</td> */}
      <td className={styles.buttonId} style={{marginLeft:"5em"}}>
        <BsCheckBox {...{title: "Approve"}} style={{display:showApprove?"block":"none"}} onClick={(e)=>saveUpdate(e,ddItem,ddIndex,mainItem.heading,"approve")}/>
        </td>
      <td className={styles.buttonId}>
        <FiSave {...{title: "Save"}} onClick={(e)=>saveUpdate(e,ddItem,ddIndex,mainItem.heading,"save")}/>
        </td>
      <td className={styles.buttonId}>
        <GrView {...{title: "Preview"}} onClick={()=>previewInventory(ddItem,mainItem.heading,ddIndex)}/>
        </td>
      <td className={styles.buttonId}>
        <RiDeleteBin6Line {...{title: "Delete"}} onClick={(e)=>getDropdown(e,mainItem,type,false,ddIndex,ddItem)}/>
        </td>
    </tr>
    </div>
    ))
  )
  
    return (
            <Table className={styles.graphDetailsContainer}>
          <thead className={styles.dateHeader}>
            <div className={styles.dateHeading}></div>
            <tr className={styles.dateContent}>
              {inventoryHeader?.map((item, id) => (
                <th key={id} style={{borderRight:selectedDate===item.a?"1px solid #06333f":"none"}}><div className={styles.w_90}>{item.x}</div></th>
              ))}
              <th style={{marginLeft:"5em"}}><div className={styles.w_90}></div>Action</th>
            </tr>
          </thead>
          <tbody style = {{width:"fit-content"}}>
          <div className={styles.biginnerHeader}>
            <div className={styles.dateHeading} style={{paddingLeft:"20px"}}>Beginning Inventory </div>
            <tr className={styles.dateContent} style={{marginRight:"17.8em"}}>
              {beginningInventory?.map((item, id) => (
                <td key={id} style={{display:dataArray.includes(item.key)?"block":"none",borderRight:selectedDate===item.key?"1px solid #06333f":"none"}}>{setContentXY(item)}</td>
              ))}
            </tr>
          </div>
          <div className={styles.resultContainer}>
            <div className={styles.incomingContainer}>
              <div
                style={{
                  fontSize: ".9em",
                  fontWeight: "200",
                  padding: "5px 18px",
                }}
              >
                INCOMING
              </div>
              <div className={styles.resultContainer}>
                {incomingListData?.map((mainItem, id) => (
                  <>
                  <div className={styles.dateHeader}>
                    <div className={styles.dateHeading}>
                      <div style={{outline: "none"}} onClick={(e)=>getDropdown(e,mainItem,"incoming",true)}
                      role="button"
                      tabIndex={0}
                      onKeyPress={()=>{}}
                      > {!dropdownData.incoming?.includes(mainItem.heading) || mainItem.heading==="Planned Purchase Orders"?plusSign():minusSign()}</div>
                      <div>{mainItem.heading}</div>
                      {
                      mainItem.heading==="Planned Purchase Orders"?<div className={styles.buttonId} ><BsInfoCircle {...{title: "This is an Editable field"}}/></div>:""
                    }
                    </div>
                    <tr className={styles.dateContent}>
                      {mainItem.number?.map((item, id2) => (
                          <td key={id2} style={{display:dataArray.includes(item.key)?"block":"none",
                          borderRight:selectedDate===item.key?"1px solid #06333f":"none"
                          }}>
                          {setContent(item,
                            changeInventoryInputs,
                            "incoming",
                            id2,
                            mainItem,
                            previewData,
                            selectedDate
                            )}
                        </td>
                      ))}
                      
                    </tr>
                  </div>
                  {/* Incoming Dropdown */}
                  {
                    mainItem.heading==="Planned Purchase Orders"?dropDownView(mainItem,"incoming"):""
                  }
                  </>
                ))}
              </div>
            </div>
            <div style={{
              width:"100%",
              display:"flex",
              justifyContent:"flex-end"
            }}>
            {/* {rodCircleSign(
                  "You may run out of stock if orders are placed here"
                )} */}
            </div>
            {/* <div className={styles.buttonId}>
              <ButtonItem
                title={"Confirm orders with Zuellig"}
                BColor={"#c3d422"}
                Color={"#094658"}
              />
            </div> */}
            <div className={styles.incomingContainer}>
              <div
                style={{
                  fontSize: ".9em",
                  fontWeight: "200",
                  padding: "5px 18px",
                }}
              >
                OUTGOING
              </div>
              <div className={styles.resultContainer}>
                {outgoingListData?.map((mainItem, id) => (
                  <>
                  
                  <div className={styles.dateHeader}>
                    <div className={styles.dateHeading}>
                      <div onClick={(e)=>getDropdown(e,mainItem,"outgoing",true)}
                      role="button"
                      tabIndex={0}
                      onKeyPress={()=>{}}
                      > {!dropdownData.outgoing?.includes(mainItem.heading)  || mainItem.heading==="Forecast Adjustment"?plusSign():minusSign()}</div>
                      <div>{mainItem.heading}</div>
                      {
                        mainItem.heading==="Forecast Adjustment"?<div className={styles.buttonId}><BsInfoCircle {...{title: "This is an Editable field"}}/></div>:""
                      }
                    </div>
                    <tr className={styles.dateContent}>
                      {mainItem.number?.map((item, id2) => (
                        <td key={id2} style={{display:dataArray.includes(item.key)?"block":"none",
                        borderRight:selectedDate===item.key?"1px solid #06333f":"none"
                        }}>
                          
                          {setContent(item, 
                            changeInventoryInputs,
                            "outgoing",
                            id2,
                            mainItem,
                            previewData,
                            selectedDate
                            )}
                        </td>
                      ))}
                      
                    </tr>
                  </div>
                  {/* Outgoing Dropdown */}
                  {
                    mainItem.heading==="Forecast Adjustment"?
                    dropDownView(mainItem,"outgoing")
                    :""
                  }
                  </>
                ))}
              </div>
            </div>
            <div className={styles.biginnerHeader}>
              <div className={styles.dateHeading}>Ending Inventory </div>
              <tr className={styles.dateContent} style={{marginRight:"17.8em"}}>
                {endingInventory?.map((item, id) => (
                  <td key={id} style={{display:dataArray.includes(item.key)?"block":"none",
                  borderRight:selectedDate===item.key?"1px solid #06333f":"none"
                }}>{setContentXY(item)}</td>
                ))}
              </tr>
            </div>
          </div>
          </tbody>
        </Table>
    )
}
const setContent = (
  item, 
  changeInventoryInputs,
  stateId,
  ddIndex,
  mainItem,
  previewData,
  selectedDate
  ) => {
  let inputDisabled = stateId!=="dd" || previewData.id!==ddIndex || previewData.heading!==mainItem.heading
      if(stateId==="dd")
      return <input type="number" value={item.value===null?0:item.value} disabled={inputDisabled} className={inputDisabled?styles.histoInputSea:parseInt(item.key)<=selectedDate?styles.histoInputSea:styles.inputOnChecked} onChange={e=>changeInventoryInputs(e,item,stateId,ddIndex,mainItem.heading)}/>
      else return <input type="number" 
      disabled className={styles.histoInputSea} value={item.value===null?0:item.value}
      // className={parseInt(item.key)<=selectedDate?styles.histoInputSea:styles.inputOnChecked} disabled={parseInt(item.key)<=selectedDate} value={item.value===null?0:item.value} onChange={e=>changeInventoryInputs(e,mainItem,stateId,ddIndex,mainItem.heading)}
      />
    //}
}
const setContentXY = (item) => {
    return <input type="number" value={item.value===null?0:item.value} disabled className={styles.histoInput}/>
}
const plusSign = () => <div className={styles.plusSign}><div className={styles.signs}>+</div></div>
const minusSign = () => <div className={styles.plusSign}><div className={styles.signs}>-</div></div>

export default HistogramTable






