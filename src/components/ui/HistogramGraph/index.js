import React from "react"
import styles from "./style.module.scss"
import HistogramTable from "./Table"
import '../../../../node_modules/react-vis/dist/style.css';
import { 
  XYPlot, 
  LineSeries, 
  VerticalBarSeries, 
  HorizontalGridLines,
  YAxis, 
  XAxis } from 'react-vis';
import axios from "../../../../axiosConfig"
import {format} from "d3"
import {group} from "../../getGroupName"
import cogoToast from "cogo-toast";

let data = [
  { x: "Jan 2018", y: 0 },
  { x: "June 2018", y: 0 },
  { x: "Jan 2019", y: 0 },
  { x: "June 2019", y: 0 },
  { x: "Jan 2020", y: 0 },
  { x: "June 2020", y: 0 },
  { x: "Jan 2021", y: 0 },
]

let beginnerList = [0, 10645, 19458, 16233, 12535, 10642]

let incomingList = [
  { heading: "Pending Stock", number: [0, 0, 0, 3800, 2620, 0] },
  { heading: "Goods Received", number: [9980, 0, 0, 0, 2620, 0] },
  { heading: "Suggested Purchase Orders", number: [0, 0, 0, 0, 7750, 0] },
  { heading: "Planned Purchase Orders", number: [0, 0, 0, 0, 0, 7550] },
]

let outgoingList = [
  { heading: "Actual Sales", number: [3960, 2460, 2890, 3800, 0, 0] },
  { heading: "Other Outflows", number: [0, 0, 0, 0, 2620, 0] },
  {
    heading: "Model Forecast",
    number: [4022, 2500, 3550, 4500, 4500, 0],
  },
  { heading: "Forecast Adjustment", number: [0, 0, 0, 0, 3500, 3680] },
]
class HistogramGraph extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      graphData: JSON.parse(JSON.stringify(this.props.graphData)),
      inventoryHeader: data,
      beginningInventory: beginnerList,
      incomingListData: incomingList,
      outgoingListData: outgoingList,
      endingInventory: [],
      graphPlottingData:{},
      dropdownData: {
        incoming: [],
        outgoing: []
      },
      selectedDate: "",
      previewData:{},
      dateArray:[]
    }

  }
  componentDidMount() {
    let { searchData, monthArray } = this.props
    const graphData  = JSON.parse(JSON.stringify(this.state.graphData))
    let fIFlow = graphData[0].ForecastInventoryFlow
    let selectedDate = ""
    selectedDate = searchData.MonthRun ? searchData.MonthRun : monthArray[0]
    let fIFlowData = fIFlow.find(a=>a.MonthRun===selectedDate)
    let begInventory = fIFlowData.BeginningInventory
    let endInventory = fIFlowData.EndingInventory
    let dateArray = this.dataArrayKeys(begInventory, selectedDate)
    let incomingListData = this.getIncomingList(graphData[0], selectedDate)
    let outgoingListData = this.getOutgoingList(graphData[0], selectedDate)

    /** For Graph */
    let actualData = this.getInventoryHeader(selectedDate,endInventory)
    let endingInv = this.getFilteredArray(endInventory,selectedDate)
    let eIPast = endingInv.filter(a=>parseInt(a.key)<selectedDate)
    let eIFuture = endingInv.filter(a=>parseInt(a.key)>=selectedDate)
    let endingInventoryFuture = this.getInventoryHeader(selectedDate,eIFuture)
    let endingInventoryPast = this.getInventoryHeader(selectedDate,eIPast)
    // let forecastData = this.getInventoryHeader(selectedDate, outgoingListData, "model forecast")
    // let actualGraphData = actualData?.filter(a=>a.y>0)
    // let actualEndingIndex = forecastData.findIndex(a=>a.x===actualGraphData[actualGraphData.length-1].x)
    // let forecastGraphData = forecastData.slice(actualEndingIndex+1)
    // let splicedEndingInventory = this.getFilteredArray(begInventory,selectedDate)
     
    let safetyStock = this.getInventoryHeader(selectedDate,graphData[0].SafetyStock,false)
    /** Graph End */
    
    this.setState({
      inventoryHeader: actualData,
      beginningInventory:begInventory,
      incomingListData,
      outgoingListData,
      endingInventory:endInventory,
      selectedDate,
      dateArray,
      graphPlottingData:{
        forecastGraphData:endingInventoryFuture,
        actualGraphData:endingInventoryPast,
        safetyStock:safetyStock
      }
    })
  }

  componentWillUnmount() {
    this.props.eraseGraphData()
  }
  // returns 15 months data array
  getFilteredArray = (arr, selectedDate) => {
    let selectedIndex = arr.findIndex(a => a.key === selectedDate)
    if(selectedIndex!==-1){
    return arr.slice(selectedIndex - 3, selectedIndex + 12)
    }
  }

  //returns an array of 15 months "keys"
  dataArrayKeys = (arr,selectedDate) => {
    let selectedIndex = arr.findIndex(a => a.key === selectedDate)
    if(selectedIndex!==-1){
      let selectedArray = arr.slice(selectedIndex - 3, selectedIndex + 12)
      let dataArray = []
      selectedArray.forEach(a=>dataArray.push(a.key))
      return dataArray
    }
  }

  // returns months Array for(graph and table header)
  getInventoryHeader = (selectedDate, begInventory, heading) => {
    var months = ["JAN","FEB","MAR","APR","MAY","JUN","JUL","AUG","SEP","OCT","NOV","DEC"];
    let dataGraph = []
    let bInventory = JSON.parse(JSON.stringify(begInventory))
    if(heading){
      let data = bInventory.find(a => a.heading.toLowerCase() === heading.toLowerCase())
      dataGraph = this.getFilteredArray(data.number,selectedDate)
    }else{
      let ind = bInventory.findIndex(a=>a.key===selectedDate)
      if(ind>2){
        dataGraph = this.getFilteredArray(bInventory,selectedDate)
      }
      else dataGraph = bInventory
    }
      return dataGraph.map((item,index)=>{
        //  
        let obj = {x:"",y:0}
        let allMonth = parseInt(item.key.slice(4))
        let allYear = item.key.slice(0,4)
        obj.x = months[allMonth-1]+" "+allYear
        obj.y = item.value
        obj.a=item.key
        return obj
      })
    }

  //returns complete incoming data list
  getIncomingList = (graphData, selectedDate) => {
    let suggestedPo = []
    let plannedPo = []
    let goodsReceivedArray = graphData.HistoricalInventoryFlow.Incoming.GoodsReceived
    let pendingStockArray = graphData.HistoricalInventoryFlow.Incoming.PendingStock
    let incomingListData = JSON.parse(JSON.stringify(this.state.incomingListData))
    graphData.ForecastInventoryFlow.forEach(item => {
      if (item.MonthRun === selectedDate) {
        suggestedPo = item.Incoming.SuggestedPO
        plannedPo = item.Incoming.PlannedPO
      }
      return;
    })
    // let obj = {}
    // obj.suggestedPoNumber = this.getFilteredArray(suggestedPo, selectedDate)
    // obj.goodsReceived = this.getFilteredArray(goodsReceivedArray, selectedDate)
    // obj.pendingStock = this.getFilteredArray(pendingStockArray, selectedDate)
    incomingListData.forEach(item => {
      item.dropdown = []
      if (item.heading === "Suggested Purchase Orders") {
        item.number = suggestedPo;
      } else if (item.heading === "Planned Purchase Orders") {
       
        // let data = plannedPo.map(a=>{a.Series=this.getFilteredArray(a.Series,selectedDate);return a})
        
        let palannedNumber = plannedPo.find(a=>a.Status==="Approved")
        item.number = palannedNumber.Series
        item.dropdown = plannedPo.filter(a=>a.Status==="Pending");
      }
      else if (item.heading === "Goods Received") {
        item.number = goodsReceivedArray;
      }
      else if (item.heading === "Pending Stock") {
        item.number = pendingStockArray;
      }
      return item
    })
    
    return incomingListData
  }

  // returns complete outgoing data list
  getOutgoingList = (graphData, selectedDate) => {
    let consensusForecast = []
    let modelForecast = []
    let actualSales = graphData.HistoricalInventoryFlow.Outgoing.ActualSales
    let otherOutflows = graphData.HistoricalInventoryFlow.Outgoing.OthersOutflow
    let outgoingListData = JSON.parse(JSON.stringify(this.state.outgoingListData))
    graphData.ForecastInventoryFlow.forEach(item => {
      if (item.MonthRun === selectedDate) {
        consensusForecast = item.Outgoing.ConsensusForecast
        modelForecast = item.Outgoing.ModelForecast
      }
      return;
    })
    // let obj = {}
    // obj.actualSales = this.getFilteredArray(actualSales, selectedDate)
    // obj.modelForecast = this.getFilteredArray(modelForecast, selectedDate)
    let otherOutflowsData = this.getOtherOutflows(otherOutflows, selectedDate)
    outgoingListData.forEach(item => {
      item.dropdown = []
      if (item.heading === "Actual Sales") {
        item.number = actualSales;
      } if (item.heading === "Other Outflows") {
        item.number = otherOutflowsData;
        item.dropdown = [ 
          {
            Label:"expired",Series:otherOutflowsData.ExpiredStock
          }, 
          {
            Label:"scrapped",Series:otherOutflowsData.Scrap
          } 
        ]
      }
      if (item.heading === "Model Forecast") {
        item.number = modelForecast;
      }
      if (item.heading === "Forecast Adjustment") {
        // let data = consensusForecast.map(a=>{a.Series=this.getFilteredArray(a.Series,selectedDate);return a})
        let concensusNumber = consensusForecast.find(a=>a.Status==="Approved")
        item.number = concensusNumber.Series;
        item.dropdown = consensusForecast.filter(a=>a.Status==="Pending")
      }
      return item
    })
    return outgoingListData
  }

  // returns otherOutFlow Array for outgoing list
  getOtherOutflows = (otherOutflows, selectedDate) => {
    // let expired = this.getFilteredArray(otherOutflows.ExpiredStock, selectedDate)
    // let scrapped = this.getFilteredArray(otherOutflows.Scrap, selectedDate)
    let otherOutFlowArray = []
    otherOutflows.ExpiredStock.forEach(exp => {
      otherOutflows.Scrap.forEach(scr => {
        let obj = { key: "", value: 0 }
        if (exp.key === scr.key) {
          obj.key = exp.key;
          obj.value = exp.value + scr.value
          otherOutFlowArray.push(obj)
        }
      })
    })
    return otherOutFlowArray;
  }


  //returns the dropdown list
  getDropdown = (e, item, key, toggle, mainIndex,ddItem) => {
    let dropdownData = { ...this.state.dropdownData }
    let stateData = []
    let mainItem = JSON.parse(JSON.stringify(item))
    // For all dropdowns except Planned Purchase Orders && Forecast Adjustment
    if (mainItem.heading !== "Planned Purchase Orders" && mainItem.heading !== "Forecast Adjustment") {
      if (!dropdownData[key].includes(mainItem.heading)) {
        dropdownData[key].push(mainItem.heading)
      } else {
        dropdownData[key].splice(dropdownData[key].indexOf(mainItem.heading), 1)
      }
    } else {
      // For Planned Purchase Orders && Forecast Adjustment dropdowns
      let dropdownData = {
        ApprovedBy: null,
        ApprovedDate: null,
        Label: "Change Title",
        ModifiedBy: "",
        ModifiedDate: "",
        Series: [],
        Status: "Pending"
      }
      if (mainItem.heading === "Planned Purchase Orders") {
        stateData = [...this.state.incomingListData]
        let headingIndex = stateData.findIndex(a => a.heading === mainItem.heading)
        if (toggle) {
          // Add new dropdown for Planned Purchase Orders
          dropdownData.Series=JSON.parse(JSON.stringify(this.props.graphData[0].ForecastInventoryFlow[0].Incoming.PlannedPO[0].Series))
          mainItem.dropdown = [ ...mainItem.dropdown, dropdownData ]
        }else{
          // Delete existing dropdown from Planned Purchase Orders
          mainItem.dropdown.splice(mainIndex,1)
        }
        stateData.splice(headingIndex, 1, mainItem)
        this.setState({ incomingListData: stateData },()=>{
          if(!toggle)
          this.saveUpdate(e,ddItem,mainIndex,mainItem.heading,"save",true)
        })
      } else {
        stateData = [...this.state.outgoingListData]
        let headingIndex = stateData.findIndex(a => a.heading === mainItem.heading)
        if (toggle) {
          // Add new dropdown for Forecast Adjustment
          dropdownData.Series=JSON.parse(JSON.stringify(this.props.graphData[0].ForecastInventoryFlow[0].Outgoing.ConsensusForecast[0].Series))
          mainItem.dropdown = [ ...mainItem.dropdown, dropdownData ]
        }else{
          // Delete existing dropdown from Forecast Adjustment
          mainItem.dropdown.splice(mainIndex,1)
        }
        stateData.splice(headingIndex, 1, mainItem)
        this.setState({ outgoingListData: stateData },()=>{
          if(!toggle) this.saveUpdate(e,mainItem,mainIndex,mainItem.heading,"save",true)
        })
      }
    }
    this.setState({ dropdownData})
  }

  changeInventoryInputs = (e, item, stateId, ddIndex, head) => {
    let { name, value } = e.target
    let stateData;
    if (stateId === "incoming") {
      stateData = [...this.state.incomingListData]
      let dataIndex = stateData.findIndex(a => a.heading === head)
      let data = stateData.find(a => a.heading === head)
      data.number[ddIndex].value = value ? parseInt(value) : ""
      stateData.splice(dataIndex, 1, data)
      this.setState({ incomingListData: stateData })
    }
    if (stateId === "outgoing") {
      stateData = [...this.state.outgoingListData]
      let dataIndex = stateData.findIndex(a => a.heading === head)
      let data = stateData.find(a => a.heading === head)
      data.number[ddIndex].value = value ? parseFloat(value) : ""
      stateData.splice(dataIndex, 1, data)
      this.setState({ outgoingListData: stateData })
    }
    // if (stateId === "beginning") {
    //   stateData = [...this.state.beginningInventory]
    //   let dataIndex = stateData.findIndex(a => a.key === item.key)
    //   let data = stateData.find(a => a.key === item.key)
    //   data.value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ beginningInventory: stateData })
    // }
    // if (stateId === "ending") {
    //   stateData = [...this.state.endingInventory]
    //   let dataIndex = stateData.findIndex(a => a.key === item.key)
    //   let data = stateData.find(a => a.key === item.key)
    //   data.value = value ? parseInt(value) : ""
    //   stateData.splice(dataIndex, 1, data)
    //   this.setState({ endingInventory: stateData })
    // } 
    if (stateId === "dd") {
      if (head === "Forecast Adjustment") {
        stateData = JSON.parse(JSON.stringify(this.state.outgoingListData))
        let data = this.ddInputsFor(stateData, head, ddIndex, item, value, name)
        this.setState({ outgoingListData: data })
      } else {
        stateData = JSON.parse(JSON.stringify(this.state.incomingListData))
        let data = this.ddInputsFor(stateData, head, ddIndex, item, value, name)
        this.setState({ incomingListData: data })
      }
    }
  }
  ddInputsFor = (stateData, heading, ddIndex, item, value, name) => {
    let dataIndex = stateData.findIndex(a => a.heading === heading)
    let data = stateData.find(a => a.heading === heading)
    data.dropdown.forEach((ddItem,ddInd)=>{
      if (ddInd === ddIndex) {
        if(name==="title"){
          ddItem.Label=value
        }else{
          let changedObjectIndex = ddItem.Series.findIndex(a => a.key === item.key)
          let changedObject = ddItem.Series.find(a => a.key === item.key)
          changedObject.value = value? parseFloat(value) :""
          ddItem.Series.splice(changedObjectIndex, 1, changedObject)
        }
      }
    })
    stateData.splice(dataIndex, 1, data)
    return stateData
  }
  graphHeader = (leadTime) => (
    <div style={{ overflowX: "auto" }}>
      <div style={{ display: "flex", justifyContent: "space-between" }}>
        <div className={styles.graphHeading}>
          <div >SKU CODE</div>
          <div>{this.props.graphData[0]?.MaterialCode}</div>
        </div>
        <div className={styles.graphHeading}>
          <div >MANAGER</div>
          <div>{this.props.graphData[0]?.Manager}</div>
        </div>
        <div className={styles.graphHeading}>
          <div >LEAD TIME (MONTHS)</div>
          <div>{leadTime}</div>
        </div>
      </div>
      <div style={{ margin: "3em 6em 0 6em", display: "flex" }}>
        <span style={{ marginLeft: "5em", cursor: "pointer" }} 
        // onClick={() => this.setState({ inventoryHeader: this.getInventoryHeader(this.state.selectedDate, this.state.outgoingListData, "Actual Sales") })}
        >
          <span style={{ color: "#06333f", fontWeight: "bolder", paddingRight: "5px" }}>-----</span>

          <span style={{ color: "#06333f", fontWeight: "bolder" }}>Safety Stock</span>
        </span>
        {/* <span style={{ marginLeft: "1em", cursor: "pointer" }} onClick={() => this.setState({ inventoryHeader: this.getInventoryHeader(this.state.selectedDate, this.state.outgoingListData, "Model Forecast") })}>
          <span style={{ color: "#c3d422", fontWeight: "bolder", paddingRight: "5px" }}>-----</span>

          <span style={{ color: "#c3d422", fontWeight: "bolder" }}>Forecast</span>
        </span> */}
        <span style={{ marginLeft: "1em", cursor: "pointer", display: "flex" }}>
          <div style={{ height: "12px", width: "35px", backgroundColor: "#ccd8d8", marginTop: "auto", marginBottom: "auto", marginRight: "5px" }}></div>

          <span style={{ color: "#ccd8d8", fontWeight: "bolder" }}>Actual Inventory</span>
        </span>
        <span style={{ marginLeft: "1em", cursor: "pointer", display: "flex" }}>
          <div style={{ height: "12px", width: "35px", backgroundColor: "#c3d422", marginTop: "auto", marginBottom: "auto", marginRight: "5px" }}></div>

          <span style={{ color: "#c3d422", fontWeight: "bolder" }}>Forecast Inventory</span>
        </span>
      </div>
    </div>
  )
  saveUpdate = (e,obj,i,heading,type,del) =>{
    let graphData = JSON.parse(JSON.stringify(this.props.graphData))
    let flow = graphData[0].ForecastInventoryFlow[0]
    let message = ""
    let arr = []
    let flowType=""
    if(type==="save"){
      if(heading==="Planned Purchase Orders"){
          if(del) flow.Incoming.PlannedPO.splice(i+1,1)
          else flow.Incoming.PlannedPO.splice(i+1,1,obj)
        arr = flow.Incoming.PlannedPO
        flowType="planned"
      }else if(heading==="Forecast Adjustment"){
        if(del) flow.Outgoing.ConsensusForecast.splice(i+1,1)
        else flow.Outgoing.ConsensusForecast.splice(i+1,1,obj)
        arr = flow.Outgoing.ConsensusForecast
        flowType="consensus"
      }
      message="Inventory Updated Successfully"
      this.updateInventory(graphData,arr,message,flowType)
    }else{
      if(heading==="Planned Purchase Orders"){
        obj.Status="Approved"
        flow.Incoming.PlannedPO.splice(0,flow.Incoming.PlannedPO.length,obj)
        arr = flow.Incoming.PlannedPO
        flowType="planned"
      }else if(heading==="Forecast Adjustment"){
        obj.Status="Approved"
        flow.Outgoing.ConsensusForecast.splice(0,flow.Outgoing.ConsensusForecast.length,obj)
        arr = flow.Outgoing.ConsensusForecast
        flowType="consensus"
      }
      message="Status Approved Successfully"
      this.previewInventory(obj,heading,"approve",flow)
    }
  }
  updateInventory = (graphData,flow,message,flowType) => {
    axios.post("/supplyChain/updateInventory",{
      _id:graphData[0]._id,
      obj:flow,MonthRun:this.state.selectedDate,
      flowType,
      groupName:group?group.label:""
    }).then(res=>{
      if(res.data.status){
        cogoToast.success(message)
        this.props.setGraphStateData(res.data.updatedObject)
      }
    }).catch(err=>{
      cogoToast.error(JSON.stringify("Please try again"))
    })
  }
  previewInventory = (obj,head,type,flow) => {
    let body = {}
    if(head==="Planned Purchase Orders"){
      body = {
        ppo:obj.Series,
        MonthRun:this.state.selectedDate,
        cForecast:[],
        type,
        data:flow
      }
    }else{
      body = {
        ppo:[],
        MonthRun:this.state.selectedDate,
        cForecast:obj.Series,
        type,
        data:flow
      }
    }
    
    axios.post(`supplyChain/getInventoryPreview/${this.props.graphData[0]?.MaterialCode}`,body).then(res=>{
      if(res.data.status){
        if(!res.data.approved){
        if(res.data.data.suggestedPo.length){
          let incomingListData = JSON.parse(JSON.stringify(this.state.incomingListData))
          let spo = incomingListData.find(a=>a.heading==="Suggested Purchase Orders")
          let spoIndex = incomingListData.findIndex(a=>a.heading==="Suggested Purchase Orders")
          spo.number=res.data.data.suggestedPo
          incomingListData.splice(spoIndex,1,spo)
          this.setState({incomingListData})
        }
         
        this.setState({beginningInventory:res.data.data.beginningInventory,endingInventory:res.data.data.endingInventory})
       } else {
          this.props.setGraphStateData(res.data.data)
        }
      }
    })
  }
  render() {
    let leadTime = this.props.graphData[0]?.LeadTime.slice(0, 1)
    return (
      <div className={styles.graphContainer}>
        {this.graphHeader(leadTime)}
        <GraphPlots
          actual={this.state.graphPlottingData.actualGraphData}
          forecast={this.state.graphPlottingData.forecastGraphData}
          safetyStockBelowInventory={this.state.graphPlottingData.safetyStock}
          safetyStockAboveInventory={this.state.graphPlottingData.safetyStock}
        />
        <div className={styles.graphFooterContent}>INVENTORY LEVEL DETAILS</div>
        <HistogramTable
          inventoryHeader={this.state.inventoryHeader}
          beginningInventory={this.state.beginningInventory}
          changeInventoryInputs={this.changeInventoryInputs}
          incomingListData={this.state.incomingListData}
          getDropdown={this.getDropdown}
          dropdownData={this.state.dropdownData}
          outgoingListData={this.state.outgoingListData}
          endingInventory={this.state.endingInventory}
          saveUpdate={(e,obj,i,heading,type)=>this.saveUpdate(e,obj,i,heading,type)}
          previewChanges={(id,heading)=>this.setState({previewData:{id,heading}})}
          cancelPreview={()=>this.setState({previewData:{}})}
          previewData={this.state.previewData}
          previewInventory={(obj,head,ind)=>this.previewInventory(obj,head,ind)}
          dataArray = {this.state.dateArray}
          selectedDate = {this.state.selectedDate}
          showApprove={this.props.showApprove}
        />
      </div>
    )
  }
}

const GraphPlots = ({
  actual,
  forecast,
  safetyStockBelowInventory,
  safetyStockAboveInventory
}) => {
  return <div className={styles.graphDiv}>
    <XYPlot
      xType="ordinal"
      height={500} width={1000}>
      <XAxis/>
      <YAxis tickFormat={tick => format('.2s')(tick)}/>
      <HorizontalGridLines/>
      <VerticalBarSeries barWidth={0.5} color="#ccd8d8" data={actual} />
      <VerticalBarSeries barWidth={0.5} stroke={10} color="#c3d422" data={forecast}/>
      <LineSeries color={"#06333f"} data={safetyStockBelowInventory} />
      {/* <LineSeries color="#c3d422" data={forecast} /> */}
    </XYPlot>
  </div>
}


export default HistogramGraph