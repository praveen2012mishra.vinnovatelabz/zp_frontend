import React from "react"
import styles from "./style.module.scss"
import Tab from "../../Tab"
export default function HeaderCommonComponent(props) {
  let customStyle = {}
  return (
    <div
      style={
        props.head === "contact"
          ? {
              backgroundImage: props.HeaderImage
                ? `url(${props.HeaderImage})`
                : "",
              backgroundRepeat: "no-repeat",
              backgroundSize: `100% ${props.height}`,
              backgroundPosition: "0 75px",
              position: "absolute",
              zIndex: "11",
              width: "100%",
            }
          : {
              backgroundImage: props.HeaderImage
                ? `url(${props.HeaderImage})`
                : "",
              backgroundRepeat: "no-repeat",
              backgroundSize: `100% ${props.height}`,
              backgroundPosition: "0 75px",
            }
      }
      className={`${styles.main}`}
    >
      <h1 className={`text-center ${styles.title} ${styles.pageHeader}`} style={{paddingBottom: props.tabs.length > 0 ? "0" : "1em"}}>
        {props.heading}
      </h1>
      {props.sub_heading}
      {props.tabs && props.tabs.length > 0 && (
        <Tab
          routePath={props.routePath}
          items={props.tabs}
          activeTab={props.tabSelected}
          onTabChange={props.onTabChange}
          display={props.childInstructions}
        />
      )}

      {props.children}
    </div>
  )
}
