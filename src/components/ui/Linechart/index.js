import React from "react";
import styles from "./style.module.scss"
import * as d3 from "d3";



function Linechart(props)  {
   
      
      const {data, width, height,margin} = props
  
      const h = height - 2 * margin, w = width - 2 * margin
  
      
      const xFormat = d3.format('.2')
      
   
      const x = d3.scaleLinear()
        .domain(d3.extent(data, d => d.a)) 
        .range([margin, w])
      
  
      const y = d3.scaleLinear()
        .domain([0, d3.max(data, d => d.b)]) 
        .range([h, margin])
      
      
      const line = d3.line()
        .x(d => x(d.a))
        .y(d => y(d.b))
        .curve(d3.curveCatmullRom.alpha(0.5)) 
       
      const xTicks = x.ticks(6).map(d => (
          x(d) > margin && x(d) < w ? 
            <g transform={`translate(${x(d)},${h + margin})`}>  
              <text>{xFormat(d)}</text>
              <line x1='0' x2='0' y1='0' y2='5' transform="translate(0,-20)"/>
            </g>
          : null
      ))
  
      const yTicks = y.ticks(5).map(d => (
          y(d) > 10 && y(d) < h ? 
            <g transform={`translate(${margin},${y(d)})`}>  
              <text x="-12" y="5">{xFormat(d)}</text>
              <line x1='0' x2='5' y1='0' y2='0' transform="translate(-5,0)"/>
              <line className={styles.gridline} x1='0' x2={w - margin} y1='0' y2='0' transform="translate(-5,0)"/> 
            </g>
          : null
      ))
  
      return  (
        <div className={styles.graphbody}>
        <svg className={styles.svgpart} width={width} height={height}>
           <line className={styles.axis} x1={margin} x2={w} y1={h} y2={h}/>
           <line className={styles.axis} x1={margin} x2={margin} y1={margin} y2={h}/>
           <path className={styles.pathpart} d={line(data)}/>
           <g className={styles.axislabels}>
             {xTicks}
           </g>
           <g className={styles.axislabels}>
             {yTicks}
           </g>
        </svg></div>
      )
    
  }


  export default Linechart
  
   
  
