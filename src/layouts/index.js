import React, { useState, useEffect } from "react"
import "bootstrap/dist/css/bootstrap.min.css"
import Header from "../components/Header"
import Footer from "../components/Footer"
import "./style.scss"
import { getAdminSettingsAction } from "../services/Redux/actions/adminSettingsAction"
import { connect } from "react-redux";

const LayOut = ({ children, messageData, getAdminSettingsAction,location }) => {
  const [childPath, setChildPath] = useState("")
  const [screenWidth, setScreenWidth] = useState("")
  let togg= false
  const result = async () => {
    await getAdminSettingsAction()
  }
  useEffect(() => {
    setScreenWidth(window.screen.width)
    setChildPath(children.props.path)
  // eslint-disable-next-line react-hooks/exhaustive-deps
  },[])
  useEffect(() => {
    result()
    if (childPath === "/") children.props.navigate("/")
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [childPath])
  return (
    <div>
      <Header
        toggle={togg}
        urlRoute={children.props.path}
        location={location}
      />
      <div className="bodyDiv" style={{ paddingTop:messageData.data?.maintenance_mode_status ? "":screenWidth < 900? 0 :"1.5em"}}>
        {messageData.data?.maintenance_mode_status ? (
          <div className="maintenance_message">
            <b>MAINTENANCE ALERT: </b>
            {messageData.data?.maintenance_message}
          </div>
        ) : (
            ""
          )}
        {children}
      </div>
      <Footer />
    </div>
  )
}
const mapStateToProps = state => {
  // console.log(state.adminSettings.getMessages);
  return {
    messageData: state.adminSettings.getMessages,
  }
}
const mapDispatchToProps = { getAdminSettingsAction }
export default connect(mapStateToProps, mapDispatchToProps)(LayOut)
