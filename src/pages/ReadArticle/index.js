import React, { useState, useEffect } from 'react'
import { Row, Col, Container, Media, Button } from "reactstrap"
import Style from "./style.module.scss"
import medicine1 from "../../assets/images/kendal-L4iKccAChOc-unsplash.jpg"
import { Markup } from 'interweave';
import { Spinner } from "react-bootstrap"
import { createApolloFetch } from "apollo-fetch";
import cogoToast from "cogo-toast"
import moment from "moment"
import Axios from "../../../axiosConfig";
import {
  FacebookShareButton,
  FacebookIcon,
  LinkedinShareButton,
  LinkedinIcon,
  TwitterShareButton,
  TwitterIcon,
  WhatsappShareButton,
  WhatsappIcon,
  FacebookMessengerShareButton,
  FacebookMessengerIcon,
  EmailShareButton,
  EmailIcon,
  RedditShareButton,
  RedditIcon
} from "react-share"
import ReactHelmet from '../helmet';
// const uri = "https://ezpz.zuelligpharma.com/graphql"
// const query = `
// query MyQuery($fetchId: ID!) {
//     post(id: $fetchId) {
//           id,
//           uri,
//           authorId,
//           title,
//           date,
//           excerpt,
//           content,
//           tags{
//             nodes{
//               name
//             }
//           },
//           featuredImage{
//             node{
//               sourceUrl
//             }
//           },
//           categories {
//             nodes {
//                 id
//                 name
//                 parentId
//                 uri
//                 categoryId
//             }
//         }
//       }
//   }`
// const apolloFetch = createApolloFetch({ uri })
const arr = {
  imageUrl: medicine1,
  title: "Lorem Ipsum is simply dummy",
  excerpt:
    "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
  uri: "item.uri",
}
function SelectedArticle({ location }) {
  const [data, setData] = useState({});
  const [loading, setLoading] = useState(true);
  const [shareUrl, setShareUrl]=useState("");
  const getReadTime = (obj) => {
    var time = null;
    if (obj && obj.length > 0) {
      for (var key in obj) {
        if (obj[key].name.includes("MIN")) {
          time = obj[key].name.match(/\d+/)[0] + " MIN READ";
        }
      }
    }
    return time
  }
  useEffect(() => {
    if (location.search) {
      setShareUrl(location.href);
      let variables = {
        fetchId: location.search.substring(1),
      };
      console.log("variable--->", variables.fetchId)
      // apolloFetch({ query, variables })
      Axios.get("insights/getArticlesById/"+variables.fetchId)
      .then(res => {
        console.log("id res--->",res);
        let item = res.data?.articles[0]
        let dataSet = {
          imageUrl: item.featuredImage?.node.sourceUrl,
          // imageUrl:item.imageUrl,
          title: item.title,
          excerpt: `${item.excerpt.slice(3, 150)}...`,
          id: item.id,
          uri: item.uri,
          content: item.content,
          tags: item.tags.nodes[0]?.name,
          categories: item.categories.nodes,
          date: moment(item.date).format("DD MMMM YYYY"),
          readTime: getReadTime(item.tags.nodes)
        }
        console.log(item);
        setData(dataSet);
        setLoading(false)
      }).catch(err => {
        cogoToast.error("Internal Server Error")
        setLoading(false)
        console.log(err)})
    }
    else if(location.state?.data) {
      // console.log("data", location.state?.data);
      setData(location.state?.data)
      setLoading(false)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  let ar = Array(0).fill(arr)
  return (
    loading?
    <div style={{height:"100vh"}}><Spinner animation="border" color="#084658" style={{ marginTop: "50vh", marginLeft: "50%" }} /></div> :
    <section className={Style.insightSection}>
      {/*div style={{ marginLeft: "5%" }}>
        <Social />
  </div> */}
      <Container>
        <ReactHelmet 
        title={data?.title}
        image={data?.imageUrl}
        quote={data?.title}
        description={data.excerpt?.substring(3,100)}
        currentUrl={shareUrl}
        />
        <div>
          <Row >
            <Col xl={12} lg={12} className="pt-4 px-4">
              <div className="row" style={{ zIndex: 1, marginLeft: "0em" }}>
                <Button className={Style.btn} onClick={() => window.history.back()}>Back</Button>
              </div>
              <h2 className={Style.insightTitle}>Our Insights</h2>
            </Col>
          </Row>
          <Row >
            <Col xl={12} lg={12} className="pt-3 px-4">
              <h2 className={Style.dataTitle}>{data.title}</h2>
            </Col>
          </Row>
          <Row >
            <Col xl={12} lg={12} className="px-4">
              <h2 className={Style.readingTime}>{data.date}</h2>
            </Col>
          </Row>
        </div>
        <Row style={{ justifyContent: "center" }}>
          <Col xl={12} lg={12} className="pt-5 px-4">
            <img className={Style.image} src={data.imageUrl} alt="urlimage" />
          </Col>
        </Row>
        <Row style={{ justifyContent: "center" }}>
          <Col xl={12} lg={12} className="px-4">
            <p className={Style.content} >
              <Markup content={data.content} />
            </p>
          </Col>
        </Row>


        {/* Artical part */}
        <Row>
          {ar.map((item, index) => {
            return index === 0 ?
              <Col md={12} sm={12} className="mt-4">
                <Media className="custom-media h-100">
                  <Media left style={{ width: "35%" }}>
                    <Media
                      className="w-100 h-100 object-cover"
                      object
                      src={item.imageUrl}
                      alt="Generic placeholder image"
                    />
                  </Media>
                  <Media
                    body
                    className="border-left-0 pl-0 d-flex flex-column justify-content-between" style={{ backgroundColor: "white", padding: "0" }}
                  >
                    <div style={{ padding: "25px" }}>
                      <h3 className="my-3">{item.title}</h3>
                      {item.excerpt}
                    </div>

                  </Media>
                </Media>
              </Col> : index === 1 ?
                <Col md={12} sm={12} className="mt-4">
                  <Media className="custom-media h-100">
                    <Media left style={{ width: "65%" }}>
                      <div style={{ padding: "25px" }}>
                        <h3 className="my-3">{item.title}</h3>
                        {item.excerpt}
                      </div>
                    </Media>
                    <Media
                      body
                      className="border-left-0 pl-0 d-flex flex-column justify-content-between" style={{ backgroundColor: "white", padding: "0" }}
                    >
                      <Media
                        className="w-100 h-100 object-cover"
                        object
                        src={item.imageUrl}
                        alt="Generic placeholder image"
                      />
                    </Media>
                  </Media>
                </Col> : <Col md={12} sm={12} className="mt-4">
                  <Media className="custom-media h-100">
                    <Media>
                      <div style={{ padding: "25px" }}>
                        <h3 className="my-3">{item.title}</h3>
                        {item.excerpt}
                      </div>
                    </Media>
                  </Media>
                </Col>
          })}
        </Row>
        {/* //DO NOT REMOVE UNDER OBSERVATION */}
        {/* <Row style={{justifyContent:"center"}}>
        <FacebookShareButton
        image={data.imageUrl}
        title={data.title}
        quote={data.title}
        imageUrl={data.imageUrl}
        style={{marginLeft:5}} className={Style.shareBtn} url={shareUrl}>
                <FacebookIcon size={32} round={true}/>
                </FacebookShareButton>
                <LinkedinShareButton
                title={data.title}
                imageUrl={data.imageUrl}
                className={Style.shareBtn} url={shareUrl}>
                <LinkedinIcon size={32} round={true}/>
                </LinkedinShareButton>
                <TwitterShareButton
                title={data.title}
                imageUrl={data.imageUrl}
                className={Style.shareBtn} url={shareUrl}>
                <TwitterIcon size={32} round={true}/>
                </TwitterShareButton>
                <WhatsappShareButton
                title={data.title}
                imageUrl={data.imageUrl}
                className={Style.shareBtn} url={shareUrl}>
                <WhatsappIcon size={32} round={true}/>
                </WhatsappShareButton>
                <RedditShareButton
                subject={data.title}
                body={shareUrl}
                className={Style.shareBtn} url={shareUrl}>
                <RedditIcon size={32} round={true}/>
                </RedditShareButton>
        </Row> */}
      </Container>
    </section>
  )
}

export default SelectedArticle
