import React from 'react'
import { Row, Col} from 'reactstrap'
import Link from "gatsby-link";
import styles from "../style.module.scss"
import ProductIcon1 from "../../../assets/images/homeIcon1.png"
import ProductIcon2 from "../../../assets/images/homeIcon2.png"
import ProductIcon3 from "../../../assets/images/homeIcon3.png"
import ProductIcon4 from "../../../assets/images/homeIcon4.png"
import ProductGroup1 from "../../../assets/images/homeGroup1.png"
import ProductGroup2 from "../../../assets/images/homeGroup2.png"
import ProductGroup3 from "../../../assets/images/homeGroup3.png"
import ProductGroup4 from "../../../assets/images/ADYN.png"
import { basePath } from "../../../components/basePath"

const product = [
    {
        color: "#7E9567",
        heading: "Understanding Customers",
        descriptions: "Identify areas of growth and discover purchasing patterns by understanding your customer's behaviour better with Infrared",
        hoverMsg: "Discover Commercial Excellence",
        hoverColor: "",
        number: "",
        image: ProductGroup1,
        link: `${basePath}/commercialExp/InfraredSales`
    }, {
        color: "#467D7E",
        heading: "Optimising sales & operations",
        descriptions: "Keep track of inventory and material movement across the entire supply chain. Avoid out of stock situations with accurate forecasts of demand. All of this made possible by our Supply Chain Analytics",
        hoverColor: "",
        hoverMsg: "Discover Supply Chain Analytics ",
        number: "",
        image: ProductGroup3,
        link: `${basePath}/supplyChain/controlTower`
    },
    {
        color: "#234B57",
        heading: "Capability & Process Development",
        descriptions: "Utilise our technical skills in data management to manage your data better, or learn from our data scientists on how to build your own set of data tools",
        hoverColor: "",
        hoverMsg: "Discover Business Intelligence",
        number: "",
        image: ProductGroup2,
        link: `${basePath}/businessInt/datamanagement`
    }
]
function Products() {
    const [screenWidth, setScreenWidth] = React.useState(0);
    React.useEffect(() => {
        setScreenWidth(window.screen.width);
    }, []);

    return (
        <div>
            <section className={styles.containerPaddingProduct}>
                <h4 className={styles.productHead}>Our Product Focus</h4>
                <Row style={{ height: "100%" }}>
                    <Col className={styles.products} lg={10} sm={12} xs={12}>
                        <Row className={styles.icons}>
                            <Col md={2} sm={6} xs={6}>
                                <img alt="Product Icon" src={ProductIcon1} />
                                <p style={{color:screenWidth<700?"#4b5a3b":"#5a5a5a"}}>Accessing the<br/>data you need</p>
                            </Col>
                            <Col md={2} sm={6} xs={6}>
                                <img alt="Product Icon" src={ProductIcon2} />
                                <p style={{color:screenWidth<700?"#4b5a3b":"#5a5a5a"}} >Understanding<br/>Customers</p>
                            </Col>
                            <Col md={2} sm={6} xs={6}>
                                <img alt="Product Icon" src={ProductIcon3} />
                                <p style={{color:screenWidth<700?"#b0c7cf":"#5a5a5a"}} >Optimizing Sales and<br/>Operations</p>
                            </Col>
                            <Col md={2} sm={6} xs={6}>
                                <img alt="Product Icon" src={ProductIcon4} />
                                <p style={{color:screenWidth<700?"#b0c7cf":"#5a5a5a"}}>Capability and Process<br/>Development</p>
                            </Col>
                        </Row>
                        {screenWidth > 700 ? (
                            <Row className={styles.productWrapper}>
                                <Col md={5} xs={12} sm={12}>
                                    <img alt="Product Group" src={ProductGroup4} className={styles.iphone} />
                                </Col>
                                <Col md={7} xs={12} sm={12} className={styles.firstFloatbox}>
                                    <FloatingBox screenWidth={screenWidth} link="/auth/insider?1" />
                                </Col>
                            </Row>
                        ) : (
                                <Row className={styles.productWrapper}>
                                    <Col md={7} xs={12} sm={12} className={styles.firstFloatbox}>
                                        <FloatingBox screenWidth={screenWidth} link="/auth/insider?1" />
                                    </Col>
                                    <Col md={5} xs={12} sm={12} style={{marginTop:"4em"}}>
                                        <img src={ProductGroup4} className={styles.iphone} alt="Product Group"/>
                                    </Col>
                                </Row>
                            )}
                    </Col>
                </Row>
            </section>
            {
                screenWidth > 700 ?
                    product.map((item, key) => (
                        ((key + 2) % 2 === 0) === true ? (
                            <section key={key} className={styles.containerPaddingProduct2}>
                                <Row style={{ height: "100%" }}>
                                    <Col className={styles.product2} lg={10} md={10} xs={12} sm={12}>
                                        <Row>
                                            <Col style={{ paddingLeft: 0 }} lg={7} md={7} xs={12} sm={12}>
                                                <FloatingBoxLeft
                                                    number={key + 2}
                                                    description={item.descriptions}
                                                    heading={item.heading}
                                                    color={item.color}
                                                    hoverMsg={item.hoverMsg}
                                                    link={item.link}
                                                />
                                            </Col>
                                            <Col lg={5} md={5} xs={12} sm={12}>
                                                <img src={item.image} className={styles.iphone} alt = "iphone"/>
                                            </Col>
                                        </Row>
                                    </Col>
                                </Row>
                            </section>
                        ) : (
                                <section key={key} className={styles.containerPaddingProduct2}>
                                    <Row style={{ height: "100%" }}>
                                        <Col className={styles.productRight} lg={10}>
                                            <Row>
                                                <Col lg={5} md={5} xs={5} sm={5}>
                                                    <img src={item.image} className={styles.iphone} alt="iphone"/>
                                                </Col>
                                                <Col style={{ paddingRight: 0 }} lg={7} md={7} xs={7} sm={7}>
                                                    <FloatingBoxRight
                                                        number={key + 2}
                                                        description={item.descriptions}
                                                        heading={item.heading}
                                                        color={item.color}
                                                        hoverMsg={item.hoverMsg}
                                                        link={item.link}
                                                    />
                                                </Col>
                                            </Row>
                                        </Col>
                                    </Row>
                                </section>
                            )
                    ))
                    :
                    product.map((item, key) => (
                        <section key={key} className={styles.containerPaddingProduct2}>
                            <Row style={{ height: "100%" }}>
                                <Col className={styles.product2} lg={10} md={10} xs={12} sm={12}>
                                    <Row>
                                        <Col style={{ paddingLeft: 0 }} lg={7} md={7} xs={12} sm={12}>
                                            <FloatingBoxLeft
                                                number={key + 2}
                                                description={item.descriptions}
                                                heading={item.heading}
                                                color={item.color}
                                                hoverMsg={item.hoverMsg}
                                                link={item.link}
                                            />
                                        </Col>
                                        <Col lg={5} md={5} xs={12} sm={12}>
                                            <img src={item.image} className={styles.iphone} alt=""/>
                                        </Col>
                                    </Row>
                                </Col>
                            </Row>
                        </section>
                    ))
            }
        </div>
    )
}

export default Products
const FloatingBox = ({
    color,
    number,
    link,
    screenWidth
}) => {
    return (
        <div className={screenWidth > 700 ? styles.floatingBox : styles.floatingBoxLeft}>
            {/* <div className={styles.floatingBox}> */}
            <div>
                <div />
                <span>01</span>
            </div>
            <h4>Accessing the data you need</h4>
            <p className={styles.descriptions} >ZP harnesses sales and inventory data, and provides it to you at your fingertips. Keep track of current sales figures or inventory movement with Insider</p>
            <Link className={styles.discoverInsider} role="button" to={link}>Discover Insider</Link>
        </div>
    )
}
const FloatingBoxLeft = ({
    color,
    number,
    heading,
    description,
    hoverMsg,
    link
}) => {
    return (
        <div className={styles.floatingBoxLeft}>
            <div style={{ backgroundColor: color }}>
                {/* <div /> */}
                <span>{`0${number}`}</span>
            </div>
            <h4>{heading}</h4>
            <p className={styles.descriptions}>{description}</p>
            <Link role="button" to={link} className={styles.discoverInsider}>{hoverMsg}</Link>
        </div>
    )
}
const FloatingBoxRight = ({
    color,
    number,
    heading,
    description,
    hoverMsg,
    link
}) => {
    return (
        <div className={styles.floatingBoxRight}>
            <div style={{ backgroundColor: color }}>
                {/* <div /> */}
                <span>{`0${number}`}</span>
            </div>
            <h4>{heading}</h4>
            <p className={styles.descriptions}>{description}</p>
            <Link role="button" to={link} className={styles.discoverInsider}>{hoverMsg}</Link>
        </div>
    )
}