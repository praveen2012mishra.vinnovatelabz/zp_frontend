import React from 'react'
import styles from "../style.module.scss"
import { Col, Row } from 'reactstrap';
import { Link } from 'gatsby';
function ActionableInsights() {
    return (
        <div>
            <section className={styles.insightcontainer}>
                <h1 className={styles.heading}>Actionable Insights</h1>
                <Row style={{ height: "100%" }}>
                    <Col lg={7} md={7} sx={7} sm={7} className={styles.actionable}>
                        <span className={styles.subHead}>
                        <span className={styles.tree}>Smarter</span> Decisions</span>
                        <div className={styles.txtContainer}>
                            <Row>
                                <Col lg={9} md={9} xs={12} sm={12} className={styles.longText}>
                                    <p className={styles.para}>
                                        These are the words we live by in what we do. If our existing
                                        solutions dont meet your needs, we will create ones that do, also that
                                        you can focus on what really matters.
                                    </p>
                                </Col>
                            </Row>
                        </div>
                    </Col>
                    <Col lg={5} md={5} sx={5} sm={5} className={styles.actionableRight}>
                        <div className={styles.discoverContainer}>
                            <Link to="/AboutUs" style={{textDecoration:"none"}}>
                               <div className={styles.discoverHover}>
                                <div className={styles.line} />
                                <p>Discover</p>
                                </div>
                            </Link>
                        </div>
                    </Col>
                </Row>
            </section>
        </div>
    )
}

export default ActionableInsights
