import React from 'react'
import { Col, Container, Row } from 'reactstrap'
import styles from "../style.module.scss"
function FindClarity() {
    return (
        <div>
            <section className={styles.containerPadding}>
                <Row className={styles.findClarity}>
                    <Col lg={1} md={1} sx={1} sm={1} className={styles.textRotation}>
                        <h1>Find clarity in chaos</h1>
                    </Col>
                    <Container className={styles.clarityContainer}>
                        <Row className={styles.findBox}>
                            <Col lg={5} md={5} sx={5} sm={5} xl={6}>
                                <p className={styles.upPara}>
                                    Data in its raw form, are just numbers that do not immediately make sense. As it grows exponentially, it does so in a chaotic fashion. To find clarity within the chaos, the right partner is required.
                                </p>
                            </Col>
                            <Col lg={5} md={5} sx={5} sm={5} xl={6}>
                                <p className={styles.downPara}>
                                    ZP Data Analytics understands how to manage, analyse and turn data into meaningful insights. As a trusted pharma and logistics partner with over a hundred years of experience, it also means that we understand your challenges. Our understanding of data and your challenges makes us the right partner to find clarity in chaos.
                                </p>
                            </Col>
                        </Row>
                    </Container>
                </Row>
            </section>
        </div>
    )
}

export default FindClarity
