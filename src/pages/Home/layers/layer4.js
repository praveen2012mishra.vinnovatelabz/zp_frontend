import React from 'react'
import styles from "../style.module.scss"
import "../style.scss"
import { Row, Col, Container} from "reactstrap"
import Insight from "../../insights"

function Insights() {
    return (
        <div>
            <section className={styles.ourinsightcontainer}>
                <Row style={{ height: "100%" }}>
                    <Col lg={12} md={12} sx={12} sm={12} className={styles.insights} >
                        <Container fluid>
                            <h2 className="title">Our Insights</h2>
                            <Insight count={8} />
                        </Container>
                    </Col>
                </Row>
            </section>
        </div>
    )
}

export default Insights
