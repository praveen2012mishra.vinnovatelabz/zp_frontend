import React from 'react'
import styles from "./style.module.scss"
import { getAdminSettingsAction } from "../../services/Redux/actions/adminSettingsAction"
import { connect } from "react-redux";
import ActionableInsights from "./layers/layer1"
import FindClarity from "./layers/layer2"
import Products from "./layers/layer3"
import Insights from "./layers/layer4"
import { Col, Container, Row } from 'reactstrap';
import Social from "../../components/ui/social2"
import { navigate } from "gatsby"
import Cookies from "js-cookie"
import cogoToast from 'cogo-toast';
function Home(props) {
  const [screenWidth, setScreenWidth] = React.useState(0);
  React.useEffect(() => {
    let errMsg= Cookies.getJSON("error_reason");
    if (errMsg){
      cogoToast.error(errMsg.reason);
      Cookies.remove("error_reason");
    }
    let lastRoute=window.localStorage.getItem("lastRoute")
    if (lastRoute){
      window.localStorage.removeItem("lastRoute")
      navigate(`${lastRoute}`)
    }
    setScreenWidth(window.screen.width);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);
  return (
    <Container fluid className={styles.mainContainer}>
      {
        screenWidth > 700 ?
          <Row>
            <Col xs={2} style={{ display: "flex", flexDirection: "column", alignItems: "center" }}>
              <Social lineHeight={96} noMr = {true}/>
            </Col>
            <Col xs={10}>
              <ActionableInsights />
              <FindClarity />
              <Products />
              <Insights />
            </Col>
          </Row>
          :
          <Row>
            <Col xs={12}>
              <ActionableInsights />
              <FindClarity />
              <Products />
              <Insights />
            </Col>
          </Row>
      }
    </Container>
  )
}
const mapStateToProps = state => {
  return {
    messageData: state.adminSettings.getMessages,
  }
}
const mapDispatchToProps = { getAdminSettingsAction }
export default connect(mapStateToProps, mapDispatchToProps)(Home)
