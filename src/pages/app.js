import React from "react"
import { Router } from "@reach/router"
// import { basePath } from "../components/basePath"
import LocalRoute from "../components/LocalRoute"
import InsiderRoute from "./Admin/Insider"
// import InsiderReports from "../components/Admin/Insider/insiderRep/index"
// import ControlTower from "./Admin/supplyChain/controlTower"
// import ClinicalReach from "../components/Admin/Insider/Clinical/index"
import DataManagement from "./Admin/BusinessIntelligence/DataManagement/index"
import InfraredSales from "./Admin/CommercialExp/InfraredSales/index"
import DoctorPrescription from "./Admin/CommercialExp/DoctorsPrescribing/index"
import SupplyChainPlanner from "./Admin/supplyChain/supplyChainPlanner"

const App = () => {
  return (
    <Router basepath="/app">
      {/* <LocalRoute path="/insider" component={InsiderRoute} />
      <LocalRoute path="/insider/insiderDetails" component={InsiderReports} />
      <LocalRoute path="/insider/clinicalReach" component={ClinicalReach} /> */}
      <LocalRoute
        path="/commercialExp/InfraredSales"
        component={InfraredSales}
      />
      <LocalRoute
        path="/businessInt/datamanagement"
        component={DataManagement}
      />
      <LocalRoute
        path="/commercialExp/InfraredSales/doctorPrescription"
        component={DoctorPrescription}
      />
      <LocalRoute path="/supplyChain/planner" component={SupplyChainPlanner} />
      <LocalRoute path="/supplyChain/controlTower" component={SupplyChainPlanner} />
      <LocalRoute path="/insider" component={InsiderRoute} />
    </Router>
  )
}

export default App
