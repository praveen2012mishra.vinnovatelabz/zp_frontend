import React, { useEffect, useState } from "react"
import {
  Modal,
  Row,
  Col,
  Container,
  Accordion,
  Dropdown,
  Spinner,
  InputGroup,
} from "react-bootstrap"
import { X } from "react-bootstrap-icons"
import cloneDeep from "lodash/cloneDeep"

import styles from "./style.module.scss"
import AccordionHeader from "./AccordionHeader"
import AccordionBody from "./AccordionBody"

// css for custom scrollbar
import "simplebar/dist/simplebar.min.css"
import Simplebar from "simplebar-react"
import axios from "../../../../axiosConfig"
import { getADGroups } from "../../../services/Api/authGroups"
import { getAdGroupsByKeyword } from "../../../services/Api/authGroups"

import ToggleButton from "../../../components/ui/ToggleButton"
const _ = require("underscore")
const seedData = [
  {
    type: "insider",
    name: "insider",
    items: [
      { type: "Sales Report", elements: [] },
      { type: "Sales Flash", elements: [] },
      { type: "Inventory Balance", elements: [] },
      { type: "Ask ZiP", elements: [] },
      { type: "Detailed Reports", elements: [] },
      { type: "Custom Reports", elements: [] },
    ],
  },
  {
    type: "clinicalReach",
    name: "clinical reach",
    items: [
      { type: "Inbound", elements: [] },
      { type: "Distribution", elements: [] },
      { type: "Inventory", elements: [] },
      { type: "Budget & Expense", elements: [] },
      { type: "Detailed Reports", elements: [] },
      { type: "Custom Reports", elements: [] },
    ],
  },
  {
    type: "infrared",
    name: "Infrared",
    items: [
      { type: "Infrared Base", elements: [] },
      { type: "Promotion Optimisation", elements: [] },
      { type: "Product Bundling", elements: [] },
      { type: "Patient Journey", elements: [] },
      { type: "Doctor Prescription Behaviour", elements: [] },
      { type: "Tender Utilisation", elements: [] },
      { type: "Order Timing", elements: [] },
      { type: "Price Sensitivity", elements: [] },
    ],
  },
  {
    type: "supplyChainAnalytics",
    name: "Supply Chain Analytics",
    items: [
      { type: "Replenishment Scheduling", elements: [] },
      { type: "Executive Summary", elements: [] },
      { type: "Inventory Management", elements: [] },
      { type: "Demand Review", elements: [] },
      { type: "Optimal Safety Stock", elements: [] },
    ],
  },
  {
    type: "businessIntelligenceServices",
    name: "Business Intelligence Services",
    items: [
      { type: "Data Management and Integration", elements: [] },
      { type: "Robotic Process Automation", elements: [] },
      { type: "Customised Reports", elements: [] },
    ],
  },
  {
    type: "Internal",
    name: "Internal",
    items: [
      { type: "Management Overview", elements: [] },
      { type: "Finance", elements: [] },
      { type: "Procurement", elements: [] },
      { type: "HR", elements: [] },
      { type: "Operations", elements: [] },
      { type: "QBR", elements: [] },
      { type: "CRM", elements: [] },
      { type: "IMS", elements: [] },
    ],
  },
]
// array of availabe types
const types = ["external", "internal"]

// array of available countries
const countries = [
  "Regional",
  "Brunei",
  "Cambodia",
  "Hong Kong",
  "Indonesia",
  "Korea",
  "Macau",
  "Malaysia",
  "Myanmar",
  "Philippines",
  "Singapore",
  "Taiwan",
  "Thailand",
  "Vietnam",
  "Other",
]

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

const GroupAddEditModal = ({ fromEdit, open, onClose, onSaveAndExit }) => {
  const [activeAccordion, toggleActiveAccordion] = useState("0") // activeAccordion takes index as string
  const [data, setData] = useState(seedData)
  const [groupName, setGroupName] = useState("Select")
  const [displayName, setDisplayName] = useState("Select")
  const [type, setType] = useState("external") // default type to external
  const [country, setCountry] = useState("singapore") // default country to singapore
  const [keyword, setKeyword] = useState("")
  const [isSearch, setIsSearch] = useState(false)
  const [refData, setRefData] = useState({})
  const [isAdmin, setIsAdmin] = useState(false)

  const onDataChange = (value, item, title, inputIdx, field, idx) => {
    let newData = cloneDeep(data)
    const innerIndex = newData[idx].items.findIndex(a => a.type === title)
    newData[idx].items[innerIndex].elements[inputIdx][field] = value
    setData(newData)
  }
  const [groupArray, setGroupArray] = useState({ array: [], arrayClone: [] })
  const [loading, setLoading] = useState(true)
  useEffect(() => {
    fetchGroupDetails()
    getADGroups().then(res => {
      setGroupArray({ array: res, arrayClone: res })
      setLoading(false)
    })
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  const fetchGroupDetails = async () => {
    if (fromEdit) {
      const response = await axios.get(
        `/group/fetchGroupOptions/${fromEdit.mappingId}`
      )
      console.log(response.data.options, "line 158/groupModal")
      setRefData(response.data)
      setData(response.data.options)
      setGroupName(response.data.group_name)
      setDisplayName(response.data.display_name)
      setType(response.data.type)
      setCountry(response.data.country)
      setIsAdmin(response.data.isAdmin)
    }
  }

  const fetchAdGroups = async () => {
    setIsSearch(false)
    setLoading(true)
    const data = await getAdGroupsByKeyword(type, keyword)
    setGroupArray({ array: data, arrayClone: data })
    setLoading(false)
    setIsSearch(true)
  }

  const onAddField = (key, key2) => {
    let newData = cloneDeep(data)
    let parentIndex = newData.findIndex(a => a.name === key)
    newData[parentIndex].items.map((i, k) => {
      if (i.type.toLowerCase() === key2.toLowerCase()) {
        i.elements.push({ label: "", height: 0, description: "", url: "" })
      }
      return i
    })
    setData(newData)
  }

  const onDeleteField = (idx, key, key2) => {
    let newData = cloneDeep(data)
    let parentIndex = newData.findIndex(a => a.name === key)
    newData[parentIndex].items.map((i, k) => {
      if (i.type.toLowerCase() === key2.toLowerCase()) {
        i.elements.splice(idx, 1)
      }
      return i
    })
    //
    setData(newData)
  }

  const onTypeSelect = newType => {
    if (newType !== type) setType(newType)
  }

  const onKeywordSelect = newType => {
    if (newType !== type) setKeyword(newType)
  }

  // const onCountrySelect = newCountry => {
  //   if (newCountry !== country) setCountry(newCountry)
  // }
  const resetInitial = () => {
    setCountry("Singapore")
    setGroupName("")
    setDisplayName("")
    setType("external")
    setData(seedData)
  }
  const onClickSaveAndExit = () => {
    onSaveAndExit({
      groupName,
      displayName,
      type,
      country,
      data,
      isInternal: type.toLowerCase() === "internal" ? true : false,
      isAdmin: isAdmin,
      refData,
    })
    groupName.toLowerCase() !== "select" && resetInitial()
  }
  // const onCloseModal = () => { }

  const onAccordianToggleClick = key => {
    if (key !== activeAccordion) toggleActiveAccordion(key)
  }

  const filterGroupNames = words => {
    if (words.length === 3) {
      let filtered = groupArray.array.filter(item =>
        item.toLowerCase().includes(words.toLowerCase())
      )
      _.debounce(
        setGroupArray({ array: filtered, arrayClone: groupArray.arrayClone }),
        3000
      )
    } else {
      setGroupArray({
        array: groupArray.arrayClone,
        arrayClone: groupArray.arrayClone,
      })
    }
  }

  return (
    <Modal size="lg" centered show={open} dialogClassName={styles.modal}>
      <Modal.Header className={styles.header}>
        <Container fluid>
          <Row className="justify-content-between align-items-center w-100">
            <Col sm={6}>
              <h2>Group Information</h2>
            </Col>
            <Col sm={6} className="pr-0">
              <Row>
                <button
                  className={`btn rounded-pill d-block ml-auto ${styles.btn}`}
                  onClick={onClickSaveAndExit}
                >
                  SAVE & EXIT
                </button>
                <button
                  className={`btn ${styles.closeBtn}`}
                  onClick={resetInitial && onClose}
                >
                  <X />
                </button>
              </Row>
            </Col>
          </Row>
        </Container>
      </Modal.Header>

      <Simplebar style={{ maxHeight: "80%" }} autoHide="false">
        <Modal.Body className={styles.modal__body}>
          <Container className={`mb-3 ${styles.px35}`}>
            <Row>
              <Col style={{ maxWidth: "36%" }}>
                <p
                  className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                >
                  {fromEdit ? "display name" : "group name"}
                </p>
                {!fromEdit && isSearch ? (
                  <Dropdown
                    className={styles.dropdown}
                    onSelect={(_, e) => setGroupName(e.target.innerText)}
                  >
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                      {groupName}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className={styles.countryDropdown}>
                      <Dropdown.Header
                        key="input#223"
                        href="#"
                        className={`${styles.searchHeader}`}
                      >
                        <input
                          className={[styles.input].join(" ")}
                          placeholder="WEBPORTAL ZP SG 1"
                          onChange={e => filterGroupNames(e.target.value)}
                        />
                      </Dropdown.Header>
                      {loading ? (
                        <Spinner animation="border" role="status">
                          <span className="sr-only">Loading...</span>
                        </Spinner>
                      ) : (
                        groupArray.array.map((item, key) => (
                          <Dropdown.Item
                            key={`#${key}/${item}`}
                            href="#"
                            className={styles.dropItems}
                            // className="text-uppercase"
                          >
                            {item}
                          </Dropdown.Item>
                        ))
                      )}
                    </Dropdown.Menu>
                  </Dropdown>
                ) : (
                  <div>
                    {!isSearch && !fromEdit ? (
                      <p>Load the group data</p>
                    ) : (
                      <input
                        className={[styles.input].join(" ")}
                        placeholder="WEBPORTAL ZP SG 1"
                        value={displayName}
                        onChange={e => setDisplayName(e.target.value)}
                      />
                    )}
                  </div>
                )}
              </Col>
              <div className={styles.toggleBtn}>
                <p
                  className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                  style={{ marginBottom: "5%" }}
                >
                  Admin
                </p>
                <ToggleButton
                  checked={isAdmin}
                  onChange={() => setIsAdmin(!isAdmin)}
                />
              </div>
              <Col>
                <Row>
                  {fromEdit === false ? (
                    <>
                      <Col>
                        <p
                          className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                        >
                          select type
                        </p>
                        <Dropdown
                          className={styles.dropdown}
                          onSelect={(_, e) =>
                            onTypeSelect(e.target.innerText.toLowerCase())
                          }
                        >
                          <Dropdown.Toggle as={CustomDropdownToggle}>
                            {type}
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            {types.map(type => (
                              <Dropdown.Item
                                key={type}
                                href="#"
                                // className="text-uppercase"
                              >
                                {type}
                              </Dropdown.Item>
                            ))}
                          </Dropdown.Menu>
                        </Dropdown>
                      </Col>
                      <Col>
                        <p
                          className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                        >
                          Keyword
                        </p>
                        <input
                          className={[styles.input].join(" ")}
                          placeholder="Keyword"
                          onChange={e =>
                            onKeywordSelect(e.target.value.toLowerCase())
                          }
                        />
                      </Col>

                      <Col>
                        <button
                          style={{ marginTop: 5 }}
                          className={`btn rounded-pill d-block ml-auto ${styles.btn}`}
                          onClick={fetchAdGroups}
                        >
                          load groups
                        </button>
                      </Col>
                    </>
                  ) : (
                    <>
                      <Col>
                        <label
                          className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.label} ${styles.colorDarkBlue}`}
                          htmlFor="type"
                        >
                          Country
                        </label>
                        <InputGroup>
                          <Dropdown className={styles.dropdown}>
                            <Dropdown.Toggle as={CustomDropdownToggle}>
                              {country}
                            </Dropdown.Toggle>
                            <Dropdown.Menu className={styles.countryDropdown}>
                              {countries.map(country => (
                                <Dropdown.Item
                                  key={country}
                                  href="#"
                                  // className="text-uppercase"
                                  onClick={() => setCountry(country)}
                                >
                                  {country}
                                </Dropdown.Item>
                              ))}
                            </Dropdown.Menu>
                          </Dropdown>
                        </InputGroup>
                      </Col>

                      <Col>
                        <p
                          className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                        >
                          select type
                        </p>
                        <Dropdown
                          className={styles.dropdown}
                          onSelect={(_, e) =>
                            onTypeSelect(e.target.innerText.toLowerCase())
                          }
                        >
                          <Dropdown.Toggle as={CustomDropdownToggle}>
                            {type}
                          </Dropdown.Toggle>
                          <Dropdown.Menu>
                            {types.map(type => (
                              <Dropdown.Item
                                key={type}
                                href="#"
                                className="text-uppercase"
                              >
                                {type}
                              </Dropdown.Item>
                            ))}
                          </Dropdown.Menu>
                        </Dropdown>
                      </Col>
                    </>
                  )}
                </Row>
              </Col>
            </Row>
          </Container>

          <div>
            <Accordion defaultActiveKey="0" activeKey={activeAccordion}>
              {data.map((item, idx) => (
                <div key={idx}>
                  <AccordionHeader
                    eventKey={`${idx}`}
                    onClick={onAccordianToggleClick}
                  >
                    {item.name}
                  </AccordionHeader>
                  <Accordion.Collapse eventKey={`${idx}`}>
                    <>
                      {item.items.map((key2, i) => (
                        <AccordionBody
                          key={i}
                          title={key2.type}
                          data={data[idx].items[i].elements}
                          onDataChange={(value, title, inputIdx, field) =>
                            onDataChange(
                              value,
                              item,
                              title,
                              inputIdx,
                              field,
                              idx
                            )
                          }
                          onAddField={() => onAddField(item.name, key2.type)}
                          onDeleteField={idx =>
                            onDeleteField(idx, item.name, key2.type)
                          }
                        />
                      ))}
                    </>
                  </Accordion.Collapse>
                </div>
              ))}
            </Accordion>
          </div>
        </Modal.Body>
      </Simplebar>
    </Modal>
  )
}

export default GroupAddEditModal
