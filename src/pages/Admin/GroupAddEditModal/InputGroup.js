import React from "react"
import { Row, Col } from "react-bootstrap"
import { Dash } from "react-bootstrap-icons"

import styles from "./style.module.scss"

const InputGroup = ({
  data = [],
  onLabelChange,
  onUrlChange,
  onDelete,
  onDescChange,
  onHeightChange,
}) => {
  return (
    <Row className={`py-2 ${styles.borderBottom}`}>
      <Col sm={3}>
        <label htmlFor="type">LABEL</label>
        <input
          className={styles.input}
          value={data.lable || data.label}
          onChange={onLabelChange}
        />
      </Col>
      <Col sm={3}>
        <label htmlFor="type">URL</label>
        <input
          className={styles.input}
          value={data.url}
          onChange={onUrlChange}
        />
      </Col>
      <Col sm={2}>
        <label htmlFor="type">Height</label>
        <input
          type="number"
          className={styles.input}
          value={data.height || 2050}
          onChange={onHeightChange}
          placeholder="px"
        />
      </Col>
      <Col sm={3}>
        <label htmlFor="type">DESCRIPTION</label>
        <textarea
          className={styles.input}
          value={data.description}
          onChange={onDescChange}
        />
      </Col>
      <Col sm={1}>
        <button
          className={`btn rounded-circle ${styles.deleteBtn}`}
          onClick={onDelete}
          style={{ margin: "auto" }}
        >
          <Dash />
        </button>
      </Col>
    </Row>
  )
}

export default InputGroup
