import React from "react"
import { ChevronDown } from "react-bootstrap-icons"
import { Container, Row, Col } from "react-bootstrap"

import styles from "./style.module.scss"

const AccordionHeader = ({ children, eventKey, onClick }) => {
  return (
    <Container
      className={[styles.accordion__header, styles.px35, "py-2"].join(" ")}
      onClick={() => onClick(eventKey)}
    >
      <Row className="align-items-center">
        <Col>
          <p
            className={`text-uppercase text-bold font-weight-bold mb-0 ${styles.colorDarkBlue}`}
          >
            {children}
          </p>
        </Col>
        <Col>
          <ChevronDown className={`ml-auto d-block ${styles.colorDarkBlue}`} />
        </Col>
      </Row>
    </Container>
  )
}

export default AccordionHeader
