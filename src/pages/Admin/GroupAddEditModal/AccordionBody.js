import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import { Plus } from "react-bootstrap-icons"

import styles from "./style.module.scss"
import InputGroup from "./InputGroup"

const AccordionBody = ({
  title,
  data=[],
  onDataChange,
  onAddField,
  onDeleteField,
}) => {
  const onLabelChange = (value, idx) => {
    onDataChange(value, title, idx, "lable")
  }

  const onUrlChange = (value, idx) => {
    onDataChange(value, title, idx, "url")
  }

  const onDescChange = (value, idx) => {
    onDataChange(value, title, idx, "description")
  }

  const onHeightChange = (value, idx) => {
    onDataChange(value, title, idx, "height")
  }
  return (
    <Container
      className={[styles.accordion__body, styles.px48, "py-2"].join(" ")}
    >
      <Row className={`py-3 ${styles.borderBottom}`}>
        <Col sm={11}>
          <p className={`text-bold font-weight-bold h6`}>{title}</p>
        </Col>
        <Col sm={1}>
          <button
            className={`btn rounded-circle ${styles.addBtn}`}
            onClick={onAddField}
          >
            <Plus />
          </button>
        </Col>
      </Row>
      {data.map((d, idx) => (
        <InputGroup
          key={idx}
          data={d}
          onLabelChange={e => onLabelChange(e.target.value, idx)}
          onUrlChange={e => onUrlChange(e.target.value, idx)}
          onDescChange={e => onDescChange(e.target.value, idx)}
          onHeightChange={e => onHeightChange(e.target.value, idx)}
          onDelete={() => onDeleteField(idx)}
        />
      ))}
    </Container>
  )
}

export default AccordionBody
