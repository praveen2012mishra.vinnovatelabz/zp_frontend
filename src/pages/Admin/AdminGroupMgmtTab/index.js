import React, { useState, useEffect } from "react"
import { Table, Container, Row, Col } from "react-bootstrap"
import { Plus, PencilSquare, Trash } from "react-bootstrap-icons"
import { connect } from "react-redux"
import cogo from "cogo-toast"
import LoadingOverlay from "react-loading-overlay"
// import { changeSingleSessionStatus } from "../../../services/Api/auth.api"
import { options } from "./../../../components/logicalConst"
import Cookie from "js-cookie"
import AdminPageSearch from "../AdminPageSearch"
import styles from "./style.module.scss"
import ToggleButton from "../../../components/ui/ToggleButton"
import GropuAddEditModal from "../GroupAddEditModal"
import {
  getAllGroupData,
  deleteGroupData,
  toggle,
} from "../../../services/Redux/actions/admin"
import Axios from "../../../../axiosConfig"
import cogoToast from "cogo-toast"
import { group } from "../../../components/getGroupName"
// import { set } from "js-cookie"
// let group = {
//   country:"Regional"
// }
const _ = require("underscore")
const AdminGroupMgmtTab = ({
  data,
  err,
  getAllGroupMgmtData,
  deleteGroupMgmtData,
  notification,
  switched,
  setTypeCountry,
}) => {
  let getData = {}
  const [modalOpen, toggleModalOpen] = useState(false)
  // const [getData, setGetData] = useState({})
  const [flag, setFlag] = useState(false)
  const [loading, setLoading] = useState(true)
  // const [country, setCountry] = useState(true)
  useEffect(() => {
    getAllGroupMgmtData()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [flag])
  const [fromEdit, setFromEdit] = useState(false)
  // const [update,setUpdate]= useState("")
  const modalOnClose = () => {
    toggleModalOpen(false)
  }

  const modalOnSaveAndExit = data => {
    // do something to save the data
    let dataObj = {
      group_name: data.groupName,
      display_name: data.displayName,
      mappingId: Date.now(),
      type: data.type,
      country: data.country,
      options: data.data,
      isInternal: data.isInternal,
      isAdmin: data.isAdmin,
    }

    if (fromEdit === false) {
      if (
        data.groupName === "" ||
        data.groupName.toLowerCase() === "select"
      ) {
        cogoToast.error("Please select a group name")
      } else {
        Axios.post("/group/createGroup", {
          group_name: data.groupName,
          type: data.type,
          country: data.country,
          isInternal: data.type.toLowerCase() === "internal" ? true : false,
          isAdmin: data.isAdmin,
          display_name: data.groupName,
          group_options: dataObj,
        })
          .then(data => {
            setFlag(prevState => !prevState)
            setLoading(true)
            cogoToast.success("Group Created Successfully")
          })
          .catch(err => {
            console.log(err)
            cogoToast.error("Internal Server Error")
          })
        toggleModalOpen(false)
      }
    } else {
      // if (data.refData.display_name !== data.displayName) {
      //   Axios.post("/group/updateGroup",
      //     {
      //       _id: fromEdit._id,
      //       display_name: data.displayName,
      //     }).then((data) => {
      //       setFlag((prevState) => !prevState)
      //       setLoading(true)
      //       cogoToast.success("Group Updated Successfully")
      //     }).catch(err => {
      //       console.log(err)
      //       cogoToast.err("Internal Server Error")
      //     })
      // } else if (data.refData.isInternal !== data.isInternal) {
      //   Axios.post("/group/updateGroup",
      //     {
      //       _id: fromEdit._id,
      //       isInternal: data.isInternal,
      //       isAdmin: data.isAdmin
      //     }).then((data) => {
      //       setFlag((prevState) => !prevState)
      //       setLoading(true)
      //       cogoToast.success("Group Updated Successfully")
      //     }).catch(err => {
      //       console.log(err)
      //       cogoToast.err("Internal Server Error")
      //     })
      // }
      Axios.post("/group/updateGroupOptions", {
        ...dataObj,
        mappingId: fromEdit.mappingId,
      })
        .then(data => {
          setFlag(prevState => !prevState)
          cogoToast.success("Group Updated Successfully")
        })
        .catch(err => {
          console.log(err)
          setLoading(true)
          cogoToast.err("Internal Server Error")
        })
      // .then((res)=>window.location.reload(false))
      toggleModalOpen(false)
    }
  }

  // delete handler
  const onDelete = (id, username, groupName) => {
    setLoading(true)
    // group name and username is required for saving in activity log
    // currently username is hardcoded
    // TODO: update it to use current username in future
    deleteGroupMgmtData(id, "CPhoa", groupName)
  }
  const forKeyDown = () => {}
  // handle error
  if (err) {
    if (loading) setLoading(false)
  }

  //handle toggle
  const handleStatus = (mode, name, key, button) => {
    setLoading(true)
    switched(button, name, !mode, key)
  }
  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  // structurize the data from the raw data array
  if (data.length > 0) {
    //
    if (setTypeCountry) {
      //setCountry(setTypeCountry.countryState)
      data = data
        .filter(item => {
          //
          return item.country
            ? item.country.toLowerCase() ===
                setTypeCountry.countryState.toLowerCase() &&
                item.type.toLowerCase() ===
                  setTypeCountry.typeState.toLowerCase()
            : false
        })
        .map(data => ({
          _id: data._id,
          name: data.group_name,
          displayName: data.display_name || "",
          mappingId: data.mappingId ? data.mappingId : "",
          enforcedSingleSessionStatus: data.single_session_enforced,
          enforced2FAStatus: data.two_factor_authentication_enforced,
          type: data.type,
          country: data.country,
          options: options,
        }))
    } else {
      data = data.map(data => ({
        _id: data._id,
        name: data.group_name,
        displayName: data.display_name || "",
        mappingId: data.mappingId ? data.mappingId : "",
        enforcedSingleSessionStatus: data.single_session_enforced,
        enforced2FAStatus: data.two_factor_authentication_enforced,
        type: data.type,
        country: data.country,
        options: options,
      }))
    }

    //returns groups based on country level
    if (
      Cookie.get("inner_option") &&
      Cookie.getJSON("inner_option").country?.toLowerCase() !== "regional"
    ) {
      data = data.filter(
        a =>
          a.country.toLowerCase() ===
          Cookie.getJSON("inner_option").country.toLowerCase()
      )
    }
    // check if loading is true, otherwise it is causing infinite render loops
    if (loading) setLoading(false)
  }
  return (
    <Container fluid>
      <AdminPageSearch />
      {modalOpen ? (
        <GropuAddEditModal
          open={modalOpen}
          onClose={modalOnClose}
          onSaveAndExit={modalOnSaveAndExit}
          fromEdit={fromEdit}
          getData={getData}
        />
      ) : (
        ""
      )}

      <Row className="justify-content-between mt-5 mb-3">
        <Col>
          <p className="h1">
            <b>
              {setTypeCountry ? `${setTypeCountry.countryState}` : "Regional"}
            </b>
          </p>
        </Col>
        <Col>
          <button
            className={`btn rounded-pill ${styles.btnRight}`}
            onClick={() => {
              toggleModalOpen(true)
              setFromEdit(false)
            }}
          >
            <span>
              <Plus />
            </span>
            <span>ADD GROUP</span>
          </button>
        </Col>
      </Row>

      <LoadingOverlay active={loading} spinner>
      <div id={styles.tableNoti}>
        <Table className={styles.table}>
          <thead>
            <tr>
              <th>Group Name</th>
              <th>Display Name</th>
              <th>Enforced Single Session Status</th>
              <th>Enforced 2FA Status </th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            {data &&
              data.map((d, key) => (
                <tr key={d._id}>
                  <td>{d.name}</td>
                  <td>{d.displayName}</td>
                  <td>
                    <div className="d-flex align-items-center">
                      <ToggleButton
                        checked={d.enforcedSingleSessionStatus}
                        onChange={() => {
                          // setLoad(!load)
                          handleStatus(
                            d.enforcedSingleSessionStatus,
                            d.name,
                            key,
                            "session"
                          )
                        }}
                      />
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <ToggleButton
                        checked={d.enforced2FAStatus}
                        onChange={() =>
                          handleStatus(d.enforced2FAStatus, d.name, key, "2FA")
                        }
                      />
                    </div>
                  </td>
                  <td>
                    {/* edit button ################################################################ */}
                    <div
                      style={{ outline: "none" }}
                      className="d-flex align-items-center"
                    >
                      <span
                        style={{ outline: "none" }}
                        role="button"
                        tabIndex={0}
                        onClick={() => {
                          Axios.get(`/group/fetchGroupOptions/${d.mappingId}`)
                            .then(res => {
                              setFromEdit({
                                mappingId: d.mappingId,
                                _id: d._id,
                                group_name: d.name,
                              })
                              _.debounce(toggleModalOpen(true), 2000)
                            })
                            .catch(err => {
                              let payload = {
                                group_name: d.name,
                                mappingId: d.mappingId,
                                type: d.type,
                                country: d.country,
                                options: d.options,
                              }

                              Axios.post(`/group/updateGroupOptions`, payload)
                                .then(res => {
                                  setFromEdit({
                                    mappingId: d.mappingId,
                                    _id: d._id,
                                    group_name: d.name,
                                  })
                                  _.debounce(toggleModalOpen(true), 2000)
                                })
                                .catch(err => console.log(err, "========catch"))
                            })
                        }}
                        onKeyDown={forKeyDown}
                      >
                        <PencilSquare />
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex align-items-center">
                      <Trash
                        className={styles.cursorPointer}
                        onClick={() => onDelete(d._id, "username", d.name)}
                      />
                    </div>
                  </td>
                </tr>
              ))}
          </tbody>
        </Table>
        </div>
      </LoadingOverlay>
    </Container>
  )
}

const mapStateToProps = state => {
  //
  return {
    data: state.admin.group,
    err: state.admin.err,
    notification: state.notification,
    setTypeCountry: state.admin.setTypeCountry,
  }
}

const mapDispatchToProps = dispatch => ({
  getAllGroupMgmtData: () => dispatch(getAllGroupData()),
  deleteGroupMgmtData: (id, username, groupName) =>
    dispatch(deleteGroupData(id, username, groupName)),
  switched: (button, name, type, key) =>
    dispatch(toggle(button, name, type, key)),
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(AdminGroupMgmtTab)
