import React from "react"
// import { Row, Col } from "react-bootstrap"
const AdminLogsButtonTabs = ({ onClick, active, activeClassname, title }) => (
  <div className={active ? activeClassname : ""} onClick={onClick} onKeyDown={onClick} role="button" tabIndex={0}>
    <p>{title}</p>
  </div>
)
export default AdminLogsButtonTabs
