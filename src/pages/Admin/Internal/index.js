import React, { useState, useEffect } from 'react'
import { getGroupOptionsbyGroupName } from '../../../services/Api/authGroups'
import HeaderCommonComponent from '../../../components/ui/HeaderCommon/HeaderCommon'
import InternalBody from "./internalBody"
import styles from "./style.module.scss"
import { Spinner } from "react-bootstrap"
function Internal(props) {
    const [tabSelected, setTab] = useState(0)
    const [loading, setLoading] = useState(false)
    // const [activeData, setActiveData] = useState([])
    const [tabState, setTabState] = useState([])

    const [tabs, setTabNames] = useState(["Management Overview",
        "Finance", "Procurement",
        "HR", "Operations", "QBR",
        "CRM", "IMS", "Templates"])
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
            // setActiveData(tabState[tabSelected])
        }
    }

    useEffect(() => {
        const getOptions = async () => {
            setLoading(true)
            let tabNames = []
            const data = await getGroupOptionsbyGroupName('internal')
            try {
                // eslint-disable-next-line
                let tabData = tabs.map(t => {

                    const obj = data[0].items.find(m => m.type.replace(/ /g, "").toLowerCase() === t.replace(/ /g, "").toLowerCase())
                    if (obj) {
                        if (obj.elements.length > 0) {
                            if (obj.type.toLowerCase() === "management overview") {
                                tabNames.push("Control Tower")
                            } else {
                                tabNames.push(obj.type)
                            }
                        }
                        return obj.elements
                    }
                    // return null
                })
                setTabState(tabData)
                setTabNames(tabNames)

                setLoading(false)
            } catch {
                setTabNames(tabNames)
                setLoading(false)
            }
        }
        getOptions()
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])

    return (
        <>
            <HeaderCommonComponent heading={'Internal'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} ></HeaderCommonComponent>
            <div className={`py-4 px-lg-5 ${styles.contentWrapper}`} style={{ minHeight: "75vh" }}>
                {!loading ? tabs.length === 0 ? <div
                    style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}
                >
                    <h4 style={{ color: "#084658" }}>No Dashboards Available</h4>
                </div> : <InternalBody buttonItems={tabState[tabSelected]} /> : <div style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
                        <Spinner animation="border" role="status" />
                    </div>}
            </div>

        </>
    )
}

export default Internal;