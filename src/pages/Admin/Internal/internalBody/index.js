import React, { useState } from "react"
import Body from "../../../../components/ui/ReportView/Body"
import ButtonTabs from "../../../../components/ui/ButtonsTab"
import styles from "./style.module.scss"
import Msg from "../../../../components/constant"

const InternalBody = ({ buttonItems = [], head = "hello" }) => {
  const [activeTab, setActiveTab] = useState(0)
  React.useEffect(() => {
    setActiveTab(0)
  }, [buttonItems])
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }

  return (
    <div
      className={`${styles.header} inside-pills`}
      style={{
        marginTop: 0,
        marginLeft: -50,
        marginRight: -48,
        marginBottom: 30,
      }}
    >
      <ButtonTabs
        items={buttonItems}
        activeTab={activeTab}
        onTabChange={onTabChange}
        className={buttonItems.length > 10 ? styles.btnStyle : ""}
      />
      {buttonItems.length === 0 ? (
        <div
          style={{
            background: "#ffffff",
            minHeight: "100vh",
          }}
        >
          <Msg />
        </div>
      ) : (
        buttonItems[activeTab] && (
          <Body
            url={buttonItems[activeTab].url}
            label={buttonItems[activeTab].lable}
            height={buttonItems[activeTab].height}
            description={buttonItems[activeTab].description}
          />
        )
      )}
    </div>
  )
}

export default InternalBody
