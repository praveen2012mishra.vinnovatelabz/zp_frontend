import React, { useState } from "react"
import JourneyIcon from "../../../../../assets/images/png/journey.png"
import Styles from "./style.module.scss"
import { Container} from "reactstrap"
import { SubHeader } from "../../../../../components/ui/subHeader"
import cogo from "cogo-toast"
import Insider from "../../../../../assets/images/Insider.png"

const circleData = [
  {
    head: "Inbound",
    icon: JourneyIcon,
    description: `Access to comprehensive inbound stock receipt 
    and warehousing location of Clinical Trial Materials.`,
  },
  {
    head: "Distribution",
    icon: JourneyIcon,
    description: `Access to comprehensive distribution and 
    supply of Clinical Trial Materials to sites.`,
  },
  {
    head: "Inventory",
    icon: JourneyIcon,
    description: `Access to comprehensive Inventory information such as level of stock 
    supply/resupply, stock expiration, location, and stock availability.`,
  },
  {
    head: "Budget & Expense",
    icon: JourneyIcon,
    description: `Access to tracking of project budget and expense, with forecast.`,
  },
  {
    head: "Detailed Reports",
    icon: JourneyIcon,
    description: `Browse and download from our library 
    of detailed tabular reports for your project.`,
  },
  {
    head: "Custom Reports",
    icon: JourneyIcon,
    description: `Get ZP to customize specific dashboards and reports for you`,
  },
]
function ClinicalReach(props) {
  const [show, setShow] = useState("")
  return (
    <div className={Styles.main}>
      <SubHeader
        title="Clinical Reach Reports"
        subTitle="Your portal to essential business information."
        image={Insider}
      />
      <Container className={`text-center ${Styles.diagramContainer}`}>
        <span className={Styles.centerHead}>
          <span>Clinical Reach Reports</span>
        </span>

        <ul className={Styles.businnessContainer}>
          {/* <img src={Pentagon} className={Styles.pentagon} /> */}
          {circleData.map((item, key) => (
            <li key={key} style={{ textAlign: "center" }}>
              <span
                className={Styles.spanBtn}
                role="button"
                tabIndex={key}
                onMouseEnter={() => setShow(key)}
                onMouseLeave={() => setShow(false)}
                key={key}
                heading={item.head}
              >
                <Circles icon={item.icon} />
                <div
                  className={Styles.item_description}
                  style={{
                    position: "absolute",
                    transition: "all 0.2s",
                    zIndex: "-3",
                    transform: key === show ? "scale(1)" : "scale(0)",
                  }}
                >
                  <span>
                    <div className={Styles.underline}>
                      <p style={{ fontWeight: "bold" }}>{item.head}</p>
                    </div>
                    <p>{item.description}</p>
                  </span>
                </div>
              </span>
            </li>
          ))}
        </ul>
      </Container>
    </div>
  )
}
const Circles = props => (
  <div className={Styles.businessBorder}>
    <img 
    onClick={() => cogo.warn("No Dashboard Available")}
    src={props.icon} style={{ height: "3rem" }} alt="icon" />
  </div>
)
export default ClinicalReach
