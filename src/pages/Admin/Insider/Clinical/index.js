import React, { useState } from "react"
// import TabOptions from "../../../ui/taboption/index"
// import styles from "./style.module.scss"
import ClinicalReach from "./ClinicalReach/index"
// import Tab from "../../../Tab"
import HeaderCommonComponent from '../../../../components/ui/HeaderCommon/HeaderCommon'

function DataManagement(props) {
  // const lime = "#c3d422"
  // const white = "#ffffff"
  const [tabSelected, setTab] = useState(0)
  const tabs = ["Clinical Reach Reports"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }
  // const { location, buttonItems, heading } = props

  return (
    <HeaderCommonComponent heading={'Clinical Reach Reports'} previousProps={props} tabs={tabs} tabSelected={tabSelected} onTabChange={onTabChange} ><ClinicalReach {...props}/></HeaderCommonComponent>
    
  )
}

export default DataManagement
