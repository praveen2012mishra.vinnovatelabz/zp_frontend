import React, { useState, useEffect } from "react"
// import TabOptions from "../../../ui/taboption/index"
//import styles from "./style.module.scss"
import InsiderReports from "./InsiderReports"
// import Tab from "../../../Tab"
import HeaderCommonComponent from "../../../../components/ui/HeaderCommon/HeaderCommon"
import insiderBanner from "../../../../assets/images/ZPA-Banners-08.png"
import Msg from "../../../../components/constant"
// import { group } from "../../../../components/getGroupName"
import Cookie from "js-cookie"
import axios from "../../../../../axiosConfig"
import Clinical from "../Clinical/ClinicalReach"
import { Spinner } from "react-bootstrap"
import Model from '../../../../components/Header/contactModal';
//import ButtonItem from '../../../../components/ui/Button/Button';
import DownButton from '../../../../components/ui/Button/DownButton';


function DataManagement(props) {
  const [loading, setLoading] = useState(true)
  const [subs, setSubs] = useState([])
  const [childInstructions, setChildInstrustions] = useState(false)
  const [openModel, setOpenModel] = useState(false);
  const [tabSelected, setTab] = useState(0)
  const [groupName, setGroupName] = useState(null)
  useEffect(() => {
    // console.log(props.location);
    let group = Cookie.getJSON("inner_option") ? Cookie.getJSON("inner_option") : null
    // { label: "Tableau Regional - Data Analytics - Demo" }
    setGroupName(group)
    axios
      .post("/subs/subsDatabase", {
        groupName: group? group.label : null,
      })
      .then(res => {
        let fields = res.data.subs
        const localSubsData = []
        fields.map((item, index) => {
          let localObj = {}
          if (item.status === "Active" || item.status === "Ending Soon") {
            localObj.Product = item.product
            localObj.isSubscribed = true
            localSubsData.push(localObj)
          } else {
            localObj.Product = item.product
            localObj.isSubscribed = false
            localSubsData.push(localObj)
          }
          return item
        })
        localStorage.setItem("subsData", JSON.stringify(localSubsData))
        setSubs(localSubsData)
        setLoading(false)
      })
      .catch(err => {
        console.error("Failed to get the data", err)
        setLoading(false)
      })
    let tabNo = Cookie.get("tabNo")
    if (parseInt(tabNo) === 1) {
      setTab(1)
    } else if (props.query) {
      setTab(parseInt((props.query).substring(1)))
    }
  }, [])

  useEffect(() => {
    if (parseInt(props.query[1]) === 1) {
      setTab(0)
    } else setTab(1)
  }, [props.query])


  const tabs = ["Insider Reports", "Clinical Reach"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
      Cookie.remove("tabNo")
    }
  }
  const getInstructionsFromChild = () => {
    // console.log(inst);
    setChildInstrustions(!childInstructions)
  }
  // const { location, buttonItems, heading } = props
  return (
    <>
      <HeaderCommonComponent
        height={"115%"}
        HeaderImage={insiderBanner}
        heading={childInstructions ? "Insider Reports" : "Insider"}
        previousProps={props}
        tabs={childInstructions ? [] : tabs}
        tabSelected={tabSelected}
        onTabChange={onTabChange}
        childInstructions={childInstructions}
      ></HeaderCommonComponent>
      {
        loading && subs.length === 0 ?
          (<div style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
            <Spinner animation="border" role="status" />
          </div>) :
          tabSelected === 0 ? (
            <div
              style={{
                minHeight: "80vh",
                justifyContent: "center",
                display: "flex",
              }}
            >
              <InsiderReports
                subsData={subs}
                {...props}
                getInstructionsFromChild={() => getInstructionsFromChild()}
              />
            </div>
          ) : tabSelected === 1 ? (
            <div
              style={{
                minHeight: "80vh",
                justifyContent: "center",
                display: "flex",
              }}
            >
              <Clinical />
            </div>
          ) : (
                <Msg />
              )}
      <Model open={openModel} onClose={() => setOpenModel(false)} />
      <DownButton modalListener={() => setOpenModel(true)} />
    </>
  )
}

export default DataManagement
