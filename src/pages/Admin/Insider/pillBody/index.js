import React, { useState } from "react"
import Boby from "../../../../components/ui/ReportView/Body"
import ButtonTabs from "../../../../components/ui/ButtonsTab"
import styles from "./style.module.scss"

const PillBody = ({ buttonItems=[],head="hello" }) => {
  const [activeTab, setActiveTab] = useState(0)
  // const [url, setUrl] = useState("hello")
  // const [label, setLabel] = useState("hello")
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }
  return (
      <div
        className={`${styles.header} inside-pills`}
        style={{
          marginTop: -50,
          marginLeft: -50,
          marginRight: -48,
          marginBottom: 30,
        }}
      >
        <ButtonTabs
          items={buttonItems}
          activeTab={activeTab}
          onTabChange={onTabChange}
        />{
          buttonItems.length===0? <h1>No Data</h1> :
          <Boby url={buttonItems[activeTab].url} label={buttonItems[activeTab].lable} />
        }
      </div>
  )
}

export default PillBody
