import React  from "react"
import styles from "./style.module.scss"
// import HeaderCommonComponent from '../../ui/HeaderCommon/HeaderCommon'
// import PillBody from "./pillBody"
// import insiderBanner from "../../../assets/images/ZPA-Banners-08.png"/
// import { getGroupOptionsbyGroupName } from "../../../services/Api/authGroups"
import InsiderReports from './insiderRep'
// const _ = require("underscore")
const InsiderPage = ({location}) => {
  // console.log(location)
  return (
    <>
      <div className={`py-4 px-lg-5 ${styles.contentWrapper}`} style={{ minHeight: "75vh" }}>
        <InsiderReports query={location.search} />
        
      </div>
    </>
  )
}

export default InsiderPage
