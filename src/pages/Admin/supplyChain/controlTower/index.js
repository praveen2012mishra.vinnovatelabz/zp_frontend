import React, { useEffect, useState } from 'react'
import ControlHome from "./controlHome"
import Msg from '../../../../components/constant'
import axios from "../../../../../axiosConfig"
import HeaderCommonComponent from '../../../../components/ui/HeaderCommon/HeaderCommon'
import controlTowerBanner from '../../../../assets/images/controlTower.png'
import { group } from '../../../../components/getGroupName'
function ControlTower(props) {
    const [subs, setSubs] = useState([])
    const [childInstructions, setChildInstrustions] = useState(false)
    useEffect(() => {
        axios
            .post("/subs/subsDatabase", {
                groupName: group ? group.label : null })
            .then(res => {
                let fields = res.data.subs
                const localSubsData = []
                fields.map((item, index) => {
                    let localObj = {}
                    if (item.status === "Active" || item.status === "Ending Soon") {
                        localObj.Product = item.product
                        localObj.isSubscribed = true
                        localSubsData.push(localObj)
                    } else {
                        localObj.Product = item.product
                        localObj.isSubscribed = false
                        localSubsData.push(localObj)
                    }
                    return item
                })
                localStorage.setItem("subsData", JSON.stringify(localSubsData))
                setSubs(localSubsData)
                // setLoading(false)
            })
            .catch(err => {
                console.error("Failed to get the data", err)
                // setLoading(false)
            })
    }, [])
    const getInstructionsFromChild = () => {
        // console.log(inst);
        setChildInstrustions(!childInstructions)
      }
    const [tabSelected, setTab] = useState(0)
    const tabs = ["Control Tower", "Inventory Optimisation", "Replenishment Optimisation", "Demand Forecasting"]
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
        }
    }
    const TabView = ({ index }) => {
        if (index === 0) {
            return <ControlHome 
            subsData={subs} 
            getInstructionsFromChild={getInstructionsFromChild} />
        }
        else if (index === 1) {
            return <Msg />
        }
        else if (index === 2) {
            return <Msg />
        }
        else if (index === 3) {
            return <Msg />
        }
    }

    return (
        <>
            <HeaderCommonComponent height={'15%'} 
            HeaderImage={controlTowerBanner} 
            heading={'Supply Chain Control Tower'} 
            previousProps={props} 
            tabs={childInstructions?[]:tabs} 
            childInstructions={childInstructions}
            tabSelected={tabSelected} 
            onTabChange={onTabChange} >
                <TabView index={tabSelected} />
            </HeaderCommonComponent>
        </>
    )
}

export default ControlTower
