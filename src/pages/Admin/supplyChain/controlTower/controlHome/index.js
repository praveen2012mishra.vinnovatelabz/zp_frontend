import React, { useState, useEffect } from "react"
import Styles from "./style.module.scss"
import { SubHeader } from "../../../../../components/ui/subHeader"
import DemandForecasting from "../../../../../assets/images/Asset 21.png"
import ControlTower from "../../../../../assets/images/Asset 22.png"
import ReplenishmentOptimisation from "../../../../../assets/images/Asset 24.png"
import InventoryOptimisation from "../../../../../assets/images/Asset 23.png"
// import Model from "../../../../../components/Header/contactModal"
// import DownButton from "../../../../../components/ui/Button/DownButton"
// import cogo from "cogo-toast"
import { Spinner } from "reactstrap"
import { getGroupOptionsbyGroupName } from "../../../../../services/Api/authGroups"
import Body from "../../../../../components/ui/ReportView/Body"
import ButtonTab from "../../../../../components/ui/ButtonsTab"
import { basePath } from "../../../../../components/basePath"
import Tab from "../../../../../components/Tab"
import Cookie from "js-cookie"
import SupplyChain from "../../../../../assets/images/spplyChain.png"
// import { group } from "../../../../../components/getGroupName"
import cogoToast from "cogo-toast"

// const group = "Tableau Regional - Data Analytics - Demo"

const circleData = [
  {
    head: "Replenishment Scheduling",
    icon: DemandForecasting,
    description: `Automated algorithms efficiently predict 
             the most accurate forecasts across your 
             portfolio.`,
  },
  {
    head: "Executive Summary",
    icon: ControlTower,
    description: `Provides real-time information via fully customized
        dashboards that display specific metrics, allowing
        users to have end-to-end visibility over the entire
        supply chain`,
  },
  {
    head: "Inventory Management",
    icon: ReplenishmentOptimisation,
    description: `Improve replenishment activity into Zuellig Pharma's network across your portfolio.`,
  },
  {
    head: "Demand Review",
    icon: InventoryOptimisation,
    description: `Efficiently maintain and deploy your stock across Zuellig Pharma's network.`,
  },
  {
    head: "Optimal Safety Stock",
    icon: DemandForecasting,
    description: `Automated algorithms efficiently predict 
             the most accurate forecasts across your 
             portfolio.`,
  },
]
function InventoryOptimization(props) {
  const [show, setShow] = useState("")
  const [dataSet, setDataSet] = useState(circleData)
  const [showReport, setShowReport] = useState(false)
  const [linkData, setLinkData] = useState([])
  const [loading, setLoading] = useState(true)
  const [activeTab, setActiveTab] = useState(0)
  const [activePill, setActivePill] = useState(0)
  const [item, setItem] = useState([])
  const [insiderSubscription, setInsiderSubscription] = useState([])
  const [array, setArray] = useState([])
  const [group, setGroup] = useState([])
  const onPillChange = pill => {
    setActivePill(pill)
  }
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
      setLinkData(array[tabIdx].elements)
      setActivePill(0)
    }
  }
  const Circles = props => {
    let bgColor =
      basePath === "/auth" && group
        ? insiderSubscription?.isSubscribed
          ? props.urlItems?.length
            ? "#a4da66"
            : "#b6cba3"
          : "#c8d9b5"
        : "#ffffff"
    let border = basePath === "/auth" && group ? "none" : ""
    const handleEvent = () => {
      //
      if (basePath === "/auth" && props.urlItems?.length) {
        // setActiveTab(props.id)
        let idx = item.indexOf(props.head)
        setActiveTab(idx)
        setLinkData(props.urlItems)
        setShowReport(true)
        onPillChange(0)
        props.getInstructionsFromChild(true)
        props.heading(true)
      } else {
        cogoToast.warn("No Dashboard Available")
      }
    }
    //console.log(props.urlItems);
    return (
      <div
        className={`${Styles.circleOutline} ${Styles.businessBorder}`}
        onClick={handleEvent}
        onKeyDown={() => {
          if (props.urlItems?.length) {
            let idx = item.indexOf(props.head)
            setActiveTab(idx)
          }
        }}
        role="button"
        tabIndex={0}
        style={{
          backgroundColor:
            !insiderSubscription?.isSubscribed && show === props.id
              ? "#ffffff"
              : bgColor,
          border: props.id === show ? "#c3d422" : border,
        }}
      >
        <img src={props.icon} style={{ height: "3rem" }} alt="icon" />
      </div>
    )
  }
  useEffect(() => {
    // console.log(props, "Mello");
    setLoading(true)
    const get = async () => {
      let groups = Cookie.getJSON("inner_option")
        ? Cookie.getJSON("inner_option")
        : null
      setGroup(groups)
      if (basePath === "/auth" && groups) {
        let subsData = props.subsData
        let insiderSubs = subsData
          ? subsData.find(a => a.Product === "Supply Chain Control Tower")
          : {}
        setInsiderSubscription(insiderSubs)
        let data = await getGroupOptionsbyGroupName("supplyChainAnalytics")
        // console.log(insiderSubs);
        try {
          if (insiderSubs?.isSubscribed) {
            // console.log(data[0]);
            if (data[0].type.toLowerCase() === "supplychainanalytics") {
              let ar = []
              let dataAr = []
              data[0].items.map(i => {
                // console.log(i)
                if (i.elements.length > 0) {
                  dataAr.push(i)
                  ar.push(i.type)
                }
                return i
              })
              // console.log(ar)
              setArray(dataAr)
              setItem(ar)
            }

            setDataSet(
              circleData.map(m => {
                // console.log(m.head.toLowerCase().replace(/ /g, ""));
                const temp = data?.[0]?.items.find(
                  e =>
                    e.type.toLowerCase().replace(/ /g, "") ===
                    m.head.toLowerCase().replace(/ /g, "")
                )
                // console.log(temp,m, ".......THIS")
                m.urlItems = temp ? temp.elements : false
                //  console.log(temp,m, ".......THAT")
                return m
              })
            )
            setLoading(false)
          } else {
            cogoToast.warn("Current Group is not subscribed to this service")
            setLoading(false)
          }
        } catch {
          cogoToast.error("Internal Server Error")
          console.log(data)
          setLoading(false)
        }
      } else {
        // console.log();
        setLoading(false)
      }
    }
    get()
  }, [props.subsData])

  return (
    <div className={Styles.main}>
      {!loading && !showReport ? (
        <SubHeader
          title="Control Tower"
          subTitle="End-to-end visibility across the entire supply chain with focused optimizations"
          image={SupplyChain}
        />
      ) : null}
      {showReport ? (
        <div style={{ paddingTop: 10, backgroundColor: "#094658" }}>
          <Tab items={item} activeTab={activeTab} onTabChange={onTabChange} />
        </div>
      ) : null}
      <div className={`text-center ${Styles.diagramContainer}`}>
        {!loading && !showReport ? (
          <span className={Styles.centerHead}>
            <span>Control Tower</span>
          </span>
        ) : null}

        {loading ? (
          <Spinner color="#0A4658" style={{ marginTop: "23vh" }} />
        ) : showReport ? (
          <div className="d-flex flex-column w-100">
            <ButtonTab
              items={linkData}
              activeTab={activePill}
              onTabChange={onPillChange}
            />
            <Body
              tabs={array}
              url={linkData[activePill]?.url}
              label={linkData[activePill]?.lable}
              height={linkData[activePill]?.height}
              description={linkData[activePill]?.description}
              backListener={() => {
                setShowReport(false)
                props.getInstructionsFromChild()
                props.heading(false)
              }}
            />
          </div>
        ) : (
          <ul className={Styles.businnessContainer}>
            {dataSet.map((item, key) => (
              <li key={key} style={{ textAlign: "center" }}>
                <span
                  role="button"
                  tabIndex={key}
                  onMouseEnter={() => setShow(key)}
                  onMouseLeave={() => setShow(false)}
                  key={key}
                  heading={item.head}
                >
                  <Circles
                    urlItems={item.urlItems}
                    head={item.head}
                    icon={item.icon}
                    id={key}
                    heading={props.heading}
                    getInstructionsFromChild={props.getInstructionsFromChild}
                  />
                  {/* </Link> */}
                  <div
                    className={Styles.item_description}
                    style={{
                      position: "absolute",
                      transition: "all 0.2s",
                      zIndex: "-3",
                      transform: key === show ? "scale(1)" : "scale(0)",
                    }}
                  >
                    <span>
                      <div className={Styles.underline}>
                        <p style={{ fontWeight: "bold" }}>{item.head}</p>
                      </div>
                      <p>{item.description}</p>
                    </span>
                  </div>
                </span>
              </li>
            ))}
          </ul>
        )}
      </div>
    </div>
  )
}
export default InventoryOptimization
