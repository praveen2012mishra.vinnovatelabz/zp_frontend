import React, { useState } from 'react'
import styles from "./style.module.scss"
import { Container } from 'reactstrap'
import { SubHeader } from '../../../../../components/ui/subHeader'
import DemandForecasting from '../../../../../assets/images/Asset 21.png'
import ControlTower from '../../../../../assets/images/Asset 22.png'
import ReplenishmentOptimisation from '../../../../../assets/images/Asset 24.png'
import InventoryOptimisation from '../../../../../assets/images/Asset 23.png'
import Model from "../../../../../components/Header/contactModal"
import DownButton from "../../../../../components/ui/Button/DownButton"
import cogo from "cogo-toast"

const circleData = [
    {
        head: "Demand Forecasting",
        icon: DemandForecasting,
        description:
            `Automated algorithms efficiently predict 
             the most accurate forecasts across your 
             portfolio.`
    },
    {
        head: "Control Tower Base",
        icon: ControlTower,
        description: `Provides real-time information via fully customized
        dashboards that display specific metrics, allowing
        users to have end-to-end visibility over the entire
        supply chain`},
    {
        head: "Replenishment Optimisation",
        icon: ReplenishmentOptimisation,
        description: `Improve replenishment activity into Zuellig Pharma's network across your portfolio.`},
    {
        head: "Inventory Optimisation",
        icon: InventoryOptimisation,
        description: `Efficiently maintain and deploy your stock across Zuellig Pharma's network.`},
]
function InventoryOptimization() {
    const [show,setShow]=useState(false)
    const [openModel,setOpenModel] = useState(false)
    const title = "Control Tower";
    const subTitle = "End-to-end visibility across the entire supply chain with focused optimizations";
    return (
        <div className={styles.main}>
            <SubHeader title={title} subTitle={subTitle} />
            <div style={{ width: "100%" }}>
                
                <Container style={{paddingBottom:20}} className={styles.diagramContainer}>
                    <ul className={styles.diagram}>
                        {
                            circleData.map((item, index) => (<li key={index} style={{ textAlign: "center" }}>
                                <span role="button" key={index} 
                                    onMouseEnter={() => setShow(index)}
                                    onMouseLeave={() => setShow(false)}
                                    onClick={() => cogo.warn("No Dashboard Available")}
                                    tabIndex={0}
                                    heading={item.head}
                                >
                                    {/* eslint-disable-next-line */}
                                    <a href="#">
                                        <Circles icon={item.icon} />
                                    </a>
                                    <div
                                        className={styles.item_description}
                                        style={{
                                            position: "absolute",
                                            transition: "all 0.2s",
                                            zIndex: "-3",
                                            transform: index === show ? "scale(1)" : "scale(0)",
                                        }}
                                    >
                                        <span>
                                            <div className={styles.underline}>
                                                <p style={{ fontWeight: "bold" }}>{item.head}</p>
                                            </div>
                                            <p>{item.description}</p>
                                        </span>
                                    </div>
                                </span>
                            </li>))
                        }
                    </ul>
                    <Model 
                    open={openModel} onClose={()=>setOpenModel(false)}
                    />
                </Container>
                <DownButton modalListener={()=>setOpenModel(true)}/>
            </div>
        </div>
    )
}

const Circles = props => (
    <div className={styles.circleBorder}>
        {/* <svg style={{height:"3rem"}}> */}
        <img src={props.icon} style={{ height: "3rem" }} alt="icon"/>
        {/* </svg> */}
    </div>
)

export default InventoryOptimization
