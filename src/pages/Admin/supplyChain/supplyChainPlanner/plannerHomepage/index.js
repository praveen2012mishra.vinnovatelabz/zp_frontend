import React from 'react'
import styles from "./style.module.scss"
import plannerImg from "../../../../../assets/images/planner.png"
import scenarioImg from "../../../../../assets/images/scenario.png"
import cogoToast from 'cogo-toast'
import { basePath } from '../../../../../components/basePath'
function SupplyChainHomePage(props) {
    // React.useEffect(()=>{
    //     if (props.unSubscribed && props.tabSelected===0 && basePath==="/auth")
    //     {cogoToast.warn("Not subscribed to planner")}
    // },[])
    const subHead = `The modern supply chain requires an agile and collaborative approach across all key stakeholders.Become more nimble and responsive to customer demands,and more efficient in managing your inventory today.Log in or sign up now to get access to a collaborative planner,accurate and advanced demand forecasts and better ways to optimise your supply chain.`
    const innerPart1SubHead = `Your entire inventory and supply chain at one glance.Manage and optimise your supply chain across all key stakeholders.`
    const innerPart2SubHead = `Plan for the uncertain future and analyse how your inventory will respond to different condition in the market`
    return (
        <>
            <div className={styles.container} style={{backgroundColor: "white"}}>
                <div className={styles.header}>
                    <div className={styles.outerpart}>
                        <h1>The tool you trust to<br /><b>manage tomorrow's supply chain</b>
                        </h1>
                        <h6 className={styles.details} >{subHead}</h6>
                    </div>
                </div>
                <div style={{ paddingTop: "6em" }}>
                    <div className={styles.part1}>
                        <div className={styles.innerpart}>
                            <div className={styles.box1}></div>
                            <div className={styles.writtingContainer}>
                                <h1 className={styles.inventory}>Inventory<br />Planner</h1>
                                <div className={styles.writeup}>
                                    <h6 className={styles.subHeadLeft}>{innerPart1SubHead}</h6>
                                </div>
                            </div>
                            <div className={styles.rectanglebox} style={{backgroundImage:`url(${plannerImg})`}}/>
                        </div>
                    </div>
                    <div className={styles.part2}>
                        <div className={styles.innerpart}>
                            <div className={styles.rectanglebox} style={{backgroundImage:`url(${scenarioImg})`}}/>
                            <div className={styles.writtingContainer}>
                                <div className={styles.writeup}>
                                    <h6 className={styles.subHeadRight}>{innerPart2SubHead}</h6>
                                </div>
                                <h1 className={styles.scenario}>Scenario<br />Analyser</h1>
                                <div className={styles.box1} />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}
export default SupplyChainHomePage

