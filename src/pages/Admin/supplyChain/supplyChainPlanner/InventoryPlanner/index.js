import React, { useState, useEffect } from "react"
import styles from "./style.module.scss"
import { Container } from "react-bootstrap"
// import ButtonItem from "../../../../ui/Button/Button"
import DropDownButton from "../../../../../components/ui/DropDownButton/DropDownButton"
import limeSearch from "../../../../../assets/images/png/limeSearch.png"
import { FaCaretRight, FaCaretLeft } from "react-icons/fa"
import TableHeader from "../../../../../components/ui/Table/TableHeader"
import TableRow from "../../../../../components/ui/Table/TableRow"
import HistogramGraph from "../../../../../components/ui/HistogramGraph"
import axios from "../../../../../../axiosConfig"
import { Spinner } from "reactstrap";
import { group } from "../../../../../components/getGroupName"
import Cookie from "js-cookie"
import cogoToast from "cogo-toast"

function InventoryPlanner(props) {
  // const [tabSelected, setTab] = useState("")
  const [pageNumber, setPage] = useState(1)
  const [totalPages, setTotalPages] = useState(false)
  const [toggleOpen, setToggleOpen] = useState(false)
  const [tableValues, setTableValues] = useState([])
  const [dropdownData, setDropdownData] = useState([])
  const [graphData, setGraphData] = useState([])
  const [graphDataUpdated, updateGraphData] = useState(false)
  const [showApprove, setShowApprove] = useState(false)
  const [searchData, setSearchData] = useState({
    ApprovalStatus: "all",
    Product: "all",
    MaterialDescription: "",
    InventoryHealth: "all",
    MonthRun: "",
  })
  const [monthArray, setMonthArray] = useState([])
  const [limit, setLimit] = useState([0, 5])

  // const tabs = []

  // const onTabChange = tabIdx => {
  //   if (tabSelected !== tabIdx) {
  //     setTab(tabIdx)
  //   }
  // }

  // returns the filtered data in initial stage
  useEffect(() => {
    getMonth().then(res => {
      let arr = []
      res.data.map(a => a.ForecastInventoryFlow.map(b => arr.push(b.MonthRun)))
      getFilterData(searchData).then(res => {
        let currentCount = res.currentCount
        setTotalPages(Math.ceil(currentCount / limit[1]))
        setTableValues(res.data)
        setMonthArray(arr)
      })
    })
    let samlCookie = Cookie.get("saml_response")
    console.log(Cookie.get("saml_response"));
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    let body = {
      userId: samlToken ?
        samlToken.user.name_id :
        null,
      groupId: group ?
        group.label :
        null
    }
    if (props.tabSelected === 0) {
      axios.post("/supplyChain/permission", body).then(res => {
        // console.log(res.data);
        if (res.data.status === 200) {
          let perm = res.data.data.permissions
          if (perm) {
            let approveIndex = perm.findIndex(a => a.module === "forecasts" && a.permission === "approve")
            if (approveIndex !== -1) {
              setShowApprove(true)
            }
            else setShowApprove(false)
          }
        } else {
          cogoToast.warn(res.data.message)
        }
      }).catch(err => {
        cogoToast.error(err.message)
      })
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [props.tabSelected])

  // returns tableValues according to the current month
  useEffect(() => {
    if (monthArray[0]) {
      //let arr = ["202008", "202010", "202009"]
      let month = monthArray.sort((a, b) => {
        let c = parseInt(a) - parseInt(b)
        return parseInt(c)
      })
      const newDataArray = finalData(tableValues, month[month.length - 1])
      setTableValues(newDataArray)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [monthArray[0]])

  //updates the graph data
  useEffect(() => {
    updateGraphData(true)
  }, [graphData])

  //set Table Values as per the month selected
  useEffect(() => {
    if (searchData.MonthRun) {
      const newDataArray = finalData(tableValues, searchData.MonthRun)
      setTableValues(newDataArray)
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [searchData.MonthRun])

  //get Material data on the limit change
  useEffect(() => {
    getMaterialData(searchData)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [limit])

  // hides the graph on material Click
  useEffect(() => {
    setGraphData([])
    setToggleOpen(false)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [tableValues])


  const getMaterialData = (searchData) => {
    getFilterData(searchData).then(res => {
      let currentCount = res.currentCount
      setTotalPages(Math.ceil(currentCount / limit[1]))
      const newDataArray = finalData(res.data, searchData.MonthRun)
      setTableValues(newDataArray)
    })
  }
  const getMonth = () => {
    // let arr = []
    return axios.get(`/supplyChain/getMonths`).then(res => {
      return res.data
    })
  }
  const finalData = (res, month) => {
    let dataArray = [...res]
    let averageSales = []
    let newDataArray = dataArray.map(item => {
      let actualSales = item.HistoricalInventoryFlow.Outgoing.ActualSales
      let currentStockArray = item.HistoricalInventoryFlow.EndingInventory
      let definedMonth = !month ? monthArray[0] ? monthArray[0].toString() : "" : month;
      let averageSalesIndex = actualSales.findIndex(a => a.key === definedMonth)
      let currentStock = currentStockArray.find(a => a.key === definedMonth)
      if (averageSalesIndex !== -1) {
        let avgDiff = averageSalesIndex - 6
        if (avgDiff < 0) avgDiff = 0
        averageSales = actualSales.slice(avgDiff + 1, averageSalesIndex + 1)
        let value = 0
        averageSales.map(arrItem => (value += parseInt(arrItem.value)))
        item = { ...item, averageSales: value, currentStock }
        return item
      }
      return item
    })
    return newDataArray
  }

  const dropButtonItem = [
    { heading: "Approval Status", value: "ApprovalStatus" },
    { heading: "Month", value: "MonthRun" },
    { heading: "Inventory Health", value: "InventoryHealth" },
    { heading: "Brand", value: "Product" },
  ]
  const resultStatus = ["Export results", "Approve all"]
  const tableHeaderList = [
    "SKU",
    "APPROVAL STATUS",
    "INVENTORY HEALTH ",
    "CURRENT STOCK",
    `AVERAGE SALES
    (PAST 6 MONTHS)
    `,
    `STOCK-OUT IN
    (EST. MONTHS)`,
    `SAFETY STOCK
    (MONTHS)`,
  ]
  const toggleChange = (id, code) => {
    setToggleOpen(togg => {
      if (togg && togg.id === id) {
        setToggleOpen(false)
      } else {
        return axios
          .get(`/supplyChain/getDataByMaterialDescription/${code}`)
          .then(res => {
            setGraphData(res.data.data)
            return res.data
          }).then(res => {
            setToggleOpen({ toggleOpen: true, id })
          })

      }
    })
  }
  const dropDownName = async value => {
    setDropdownData([])
    let arr = []
    if (value === "Approval Status") arr = ["Approved", "Pending", "Rejected"]
    else if (value === "Month") arr = monthArray
    else if (value === "Inventory Health") arr = ["Urgent", "Medium", "Low"]
    else {
      await axios
        .get("/supplyChain/getBrands")
        .then(res => res.data.data)
        .then(res => {
          res.map(item => arr.push(item.Product))
        })
    }
    setDropdownData(arr)
  }
  const getFilterData = (obj) => {
    return axios.post(`/supplyChain/getFilterData/${limit}`, obj).then(res => {
      return res.data
    })
  }

  const keyToMonth = (monthKey) => {
    var months = ["JAN", "FEB", "MAR", "APR", "MAY", "JUN", "JUL", "AUG", "SEP", "OCT", "NOV", "DEC"];
    let allMonth = parseInt(monthKey.slice(4))
    let allYear = monthKey.slice(0, 4)
    return months[allMonth - 1] + " " + allYear
  }
  const dropdownClick = async (e, dataName, dataValue) => {
    let obj = { ...searchData }
    let { name } = e.target;
    obj[name] = dataValue
    setSearchData(obj)
    getMaterialData(obj)
  }
  const searchFilter = async e => {
    let { name, value } = e.target
    let obj = { ...searchData }
    obj[name] = value
    setSearchData(obj)
    getMaterialData(obj)
  }
  const pageRight = async () => {
    if (pageNumber < totalPages) {
      setPage(pageNumber + 1)
      let arr = [limit[0] + limit[1], limit[1]]
      setLimit(arr)
    }
  }
  const pageLeft = async () => {
    if (pageNumber > 1) {
      setPage(pageNumber - 1)
      let arr = [limit[0] - limit[1], limit[1]]
      setLimit(arr)
    }
  }
  const eraseGraphData = () => {
    setGraphData([])
    updateGraphData(false)
  }
  const updateGData = (res) => {
    updateGraphData(false)
    setGraphData(res)
    updateGraphData(true)
  }
  return (
    <section className={`${styles.insightsSection}`}>
      <Container fluid>
        <div className={`${styles.headerBox}`}>
          <div className={styles.headerContent}>
            <h2 className={`${styles.insightTitle} mr-3`}>Inventory Planner</h2>
            <div className={styles.buttonGN}>
              {" "}
              {/* <ButtonItem
                title={"3 forecast updates pending approval"}
                BColor={"#f75c5c"}
                Color={"#fff"}
              /> */}
            </div>
          </div>
          <div className={styles.headerEmpty}></div>
        </div>
        <div className={`${styles.dropButtonBox} my-3`}>
          <div className={styles.dropButton}>
            {dropButtonItem.map((item, id) => (
              <div id={id} key={id} className={`${styles.dropButtonRow} mr-4`}>
                <div className={`${styles.subHeading} pl-2 mb-2`}>
                  {item.heading}
                </div>
                <DropDownButton
                  onClick={value => dropDownName(value)}
                  item={item}
                  searchData={searchData}
                  dropdownData={dropdownData}
                  keyToMonth={(monthKey) => keyToMonth(monthKey)}
                  dropdownClick={(e, dataName, dataValue) =>
                    dropdownClick(e, dataName, dataValue)
                  }
                />
              </div>
            ))}
          </div>
          <div className={`${styles.searchBox}`}>
            <div className={styles.inputContainer}>
              <input
                type="text"
                className={styles.inputBox}
                placeholder="Search by SKU keywords"
                name="MaterialDescription"
                onChange={searchFilter}
              />
            </div>
            <div className={styles.limeSearch}>
              <img src={limeSearch} className={styles.inputSearch} alt="search"></img>
            </div>
          </div>
        </div>

        <div className={`${styles.paginationBox} mt-4`}>
          <div className={styles.reportStatus}>
            {resultStatus.map((item, id) => (
              <div key={id} className={`${styles.content} mr-4`} id={id}>
                {item}
              </div>
            ))}
          </div>
          <div className={styles.paginationContent}>
            <div className={styles.pageContent}>
              <FaCaretLeft
                style={{ color: "#06333f" }}
                onClick={() => pageLeft()}
                className={styles.caret}
              />
              Page{" "}
              <span
                style={{
                  backgroundColor: "#e4ebed",
                  padding: "5px",
                  marginLeft: "5px",
                  marginRight: "5px",
                }}
              >
                {pageNumber}
              </span>{" "}
              of {totalPages}{" "}
              <FaCaretRight
                style={{ color: "#06333f" }}
                onClick={() => pageRight()}
                className={styles.caret}
              />
            </div>
            {/* <div><FaCaretRight style={{ color: '#06333f' }} /></div> */}
          </div>
        </div>
        <div className={styles.tableBox}>
          <TableHeader tableHeaderList={tableHeaderList} />
          <div className={`${styles.tableValueContainer}`}>
            {tableValues.length ?
              tableValues.map((item, id) => (
                <>
                  <TableRow
                    key={id}
                    tableValues={item || {}}
                    toggleChange={(propId, code) => toggleChange(propId, code)}
                    toggleOpen={toggleOpen}
                    id={id}
                  />

                  {toggleOpen && toggleOpen.id === id && graphDataUpdated ? (
                    <HistogramGraph width={1134} height={500} graphData={graphData} monthArray={monthArray} searchData={searchData} eraseGraphData={eraseGraphData} setGraphStateData={(res) => updateGData(res)}
                      showApprove={showApprove}
                    />
                  ) : (
                      ""
                    )}
                </>
              )) : <Spinner color="#0A4658" style={{ margin: "auto" }} />}
          </div>
        </div>
      </Container>
    </section>
  )
}
export default InventoryPlanner
