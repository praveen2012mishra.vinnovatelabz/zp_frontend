import React, { useState, useEffect } from 'react'
// import { Container } from 'react-bootstrap'
import HeaderCommonComponent from '../../../../components/ui/HeaderCommon/HeaderCommon'
import { basePath } from '../../../../components/basePath'
// import ScenarioPlanner from './ScenarioPlanner'
import ControlHome from "../controlTower/controlHome"
import InventoryPlanner from './InventoryPlanner'
import SupplyChainHomePage from './plannerHomepage/index'
import controlTowerBanner from '../../../../assets/images/controlTower.png'
import axios from "../../../../../axiosConfig"
// import { group } from "../../../../components/getGroupName"
import Cookie from "js-cookie"
import { Spinner } from "react-bootstrap"
import cogoToast from 'cogo-toast'
import { getSupplyChainAdmin } from '../../../../services/Api/supplyChain.api'
function SupplyChainPlanner(props) {
    const [tabSelected, setTab] = useState("")
    const [tabs,setTabs]=useState(["Sales and Operations Planner", "Supply Chain Control Tower"]);
    const [subs, setSubs] = useState([])
    const [childInstructions, setChildInstrustions] = useState(false)
    const [loading, setLoading] = useState(true)
    const [headingPlanner, setHeadingPlanner] = useState(false)
    const [unSubscribed, setUnSubscribed] = useState(true);
    const [groupName, setGroupName] = useState("");
    const [isSuppyChainAdmin, setSupplyChainAdmin]=useState(false)
    useEffect(() => {
        if (parseInt(props.location.search[1]) === 1) {
            setTab(1)
        } else setTab(0)
        if (basePath === "/auth") {
            let group = Cookie.getJSON("inner_option") ? Cookie.getJSON("inner_option") : null
            // {label:"Tableau Regional - Data Analytics - Demo"}
            setGroupName(group)
            let getSupplyAdminStatus= async ()=>getSupplyChainAdmin().then(data=>{
                setSupplyChainAdmin(data)
            }).catch(err=>console.log(err))
            getSupplyAdminStatus()
            axios
                .post("/subs/subsDatabase", {
                    groupName: group ? group.label : null,
                })
                .then(res => {
                    let fields = res.data.subs
                    const localSubsData = []
                    fields.map((item, index) => {
                        let localObj = {}
                        if (item.status === "Active" || item.status === "Ending Soon") {
                            localObj.Product = item.product
                            localObj.isSubscribed = true
                            localSubsData.push(localObj)
                        } else {
                            localObj.Product = item.product
                            localObj.isSubscribed = false
                            localSubsData.push(localObj)
                        }
                        return item
                    })
                    let plannerSub = localSubsData.find((e) => e.Product === "Sales and Operations Planner")
                    if (!plannerSub?.isSubscribed) {
                        cogoToast.warn("Not Subscribed to Planner")
                        setUnSubscribed(true)
                    } else {
                        setUnSubscribed(false);
                    }
                    localStorage.setItem("subsData", JSON.stringify(localSubsData))
                    setSubs(localSubsData)
                    setLoading(false)
                })
                .catch(err => {
                    console.error("Failed to get the data", err)
                    setLoading(false)
                })
        }
        // eslint-disable-next-line react-hooks/exhaustive-deps
    }, [])
    const getInstructionsFromChild = () => {
        // console.log(inst);
        setChildInstrustions(!childInstructions)
    }
    const onTabChange = tabIdx => {
        if (tabSelected !== tabIdx) {
            setTab(tabIdx)
        }

    }
    return (
        <>
            <HeaderCommonComponent
                height={'75%'}
                HeaderImage={controlTowerBanner}
                heading={headingPlanner ? 'Supply Chain Control Tower' : 'Supply Chain Excellence'}
                previousProps={props}
                tabs={childInstructions ? [] : tabs}
                childInstructions={childInstructions}
                tabSelected={tabSelected}
                onTabChange={onTabChange}
                routePath={"supply"} >
                {
                    (tabSelected === 0) ?
                        basePath === "/auth" ?
                            (
                                unSubscribed ? <SupplyChainHomePage tabSelected={tabSelected} /> :
                                    <>
                                        <InventoryPlanner tabSelected={tabSelected} />
                                    </>
                            ) : <SupplyChainHomePage
                            /> :
                        loading && groupName ? <div style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
                            <Spinner animation="border" role="status" />
                        </div> : (tabSelected === 1) ? <ControlHome
                            heading={(toggle) => setHeadingPlanner(toggle)}
                            subsData={subs}
                            {...props}
                            getInstructionsFromChild={() => getInstructionsFromChild()} /> : null
                }
            </HeaderCommonComponent>
            {/* <TabView index={tabSelected} getInstructionsFromChild={getInstructionsFromChild}/> */}
        </>
    )
}



export default SupplyChainPlanner
