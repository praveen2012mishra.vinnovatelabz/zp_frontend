import React, { useState, useEffect } from "react"

import styles from "./style.module.scss"

import Tab from "../../components/Tab"
import AdminPageSearch from "./AdminPageSearch"
import AdminGroupMgmtTab from "./AdminGroupMgmtTab/index"
import Admin2FAAuthMgmtTab from "./Admin2FAAuthMgmtTab"
import AdminEnforceSingleSessionMgmtTab from "./AdminEnforceSignleSessionMgmtTab"
import AdminSettingsTab from "./AdminSettingsTab"
import AdminLogsTab from "./AdminLogsTab"
import AdminSolutionMgmtTab from "./AdminSolutionMgmtTab"
import AdminNotification from "./AdminNotification"
import Cookie from "js-cookie"
import {Spinner} from "react-bootstrap"
import axiosInstance from "../../../axiosConfig"
import { navigate } from "gatsby"
import cogoToast from "cogo-toast"
// import { ToggleButton } from "react-bootstrap"
// import Axios from "axios"

const AdminPage = () => {
  const [activeTab, setActiveTab] = useState(0)
  const [loading, setLoading] = useState(false)
  useEffect(() => {
    let changedOption = Cookie.getJSON("inner_option")
    axiosInstance.get(`/group/fetchGroupOptions/${changedOption?.id}`).then((res) => {
      if (!res.data.isAdmin) {
        cogoToast.warn("Your present group do not have Admin rights!")
        navigate("/")
      }
      setLoading(false)
    }).catch(err => {
      setLoading(false)
      cogoToast.error("Something went wrong while Switching the groups")
    })
  })
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }
  return (
    <>
      {loading ?
        <div style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
          <Spinner animation="border" role="status" />
        </div> : <>
          <div className={`${styles.header}`}>
            <h1 className={`text-center ${styles.title}`}>Administration</h1>

            <Tab
              items={[
                "Group Management",
                "Solution Management",
                "Enforced Single Session Management",
                "Two Factor Authentication Management",
                "Settings",
                "Logs",
                "Notifications"
              ]}
              activeTab={activeTab}
              onTabChange={onTabChange}
            />
          </div>
          <div className={`py-4 px-lg-5 ${styles.contentWrapper}`}>
            {activeTab === 0 ? (
              <AdminGroupMgmtTab />
            ) : null}
            {activeTab === 1 ? (
              <>
                <AdminSolutionMgmtTab />
              </>
            ) : null}
            {activeTab === 2 ? (
              <>
                <AdminPageSearch activeTab={2} />
                <AdminEnforceSingleSessionMgmtTab />
              </>
            ) : null}
            {activeTab === 3 ? (
              <>
                <AdminPageSearch activeTab={2} />
                <Admin2FAAuthMgmtTab />
              </>
            ) : null}
            {activeTab === 4 ? (
              <AdminSettingsTab />
            ) : null}
            {
              activeTab === 5 ? (
                <AdminLogsTab data={[]} />
              ) : null
            }
            {
              activeTab === 6 ? (
                <AdminNotification />
              ) : null
            }
          </div>

        </>}
    </>
  )
}

export default AdminPage
