import React from "react"
import {
  Container,
  Row,
  Col,
  InputGroup,
  Dropdown,
} from "react-bootstrap"
import styles from "./style.module.scss"

const countries = [
  "Regional",
  "Brunei",
  "Cambodia",
  "Hong Kong",
  "Indonesia",
  "Korea",
  "Macau",
  "Malaysia",
  "Myanmar",
  "Philippines",
  "Singapore",
  "Taiwan",
  "Thailand",
  "Vietnam",
  "Other",
]
const localeData = [
  "SGT (+08:00)",
  "SGT (+07:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
]
const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

function Locale({ profileData, changeProfileData, submitProfile }) {
  let countryState = "Singapore"
  let localeTime = "SGT (+08:00)"
  return (
    <div className={styles.main}>
      <Container style={{ padding: "40px" }}>
        <div>
          <label className={`${styles.label} ${styles.mt_10}`} htmlFor="type">
            Country
          </label>
          <InputGroup>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle as={CustomDropdownToggle}>
                <span style={{ float: "left" }}>
                  {profileData?.time_locale?.country || countryState}
                </span>
              </Dropdown.Toggle>
              <Dropdown.Menu className={styles.countryDropdown}>
                {countries.map(country => (
                  <Dropdown.Item
                    key={country}
                    name="country"
                    href="#"
                    className="text-uppercase"
                    onClick={e => changeProfileData(e, country)}
                    // onClick={() => setCountry(country)}
                  >
                    {country}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </InputGroup>
        </div>
        <div style={{ marginTop: "30px" }}>
          <label className={`${styles.label} ${styles.mt_10}`} htmlFor="type">
            Time Zone
          </label>
          <InputGroup>
            <Dropdown className={styles.dropdown}>
              <Dropdown.Toggle as={CustomDropdownToggle}>
                <span style={{ float: "left" }}>
                  {profileData?.time_locale?.time_zone || localeTime}
                </span>
              </Dropdown.Toggle>
              <Dropdown.Menu className={styles.countryDropdown}>
                {localeData.map(locale => (
                  <Dropdown.Item
                    key={locale}
                    name="time_zone"
                    href="#"
                    className="text-uppercase"
                    onClick={e => changeProfileData(e, locale)}
                    // onClick={() => setLocaleTime(locale)}
                  >
                    {locale}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </InputGroup>
        </div>
        <Row>
          <Col
            md={4}
            style={{
              paddingTop: "40px",
              paddingRight: "24px",
              textAlign: "right",
            }}
          >
            <button
              className={styles.btn}
              onClick={e => submitProfile(e, "locale")}
            >
              SAVE
            </button>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Locale
