import React, { useState, useEffect } from 'react';
import { Table, Row, Col, Button, Dropdown } from "react-bootstrap";
import styles from "./style.module.scss";
// import { FaCheck, FaTimes } from 'react-icons/fa';
import SupplyChainModal from './SupplyChainModal'
import { getGroups, getGroupUsers, updateSupplyChainAdmin } from '../../../../services/Api/supplyChain.api';
import Simplebar from "simplebar-react"
import ToggleButton from "../../../../components/ui/ToggleButton";
import cogoToast from 'cogo-toast';
// import { stubTrue } from 'lodash';
// import LoadingOverlay from "react-loading-overlay"


const tableHeaderList = [
    { label: 'User ID' },
    { label: 'Full Name' },
    { label: 'On Zip' },
    { label: 'User Role' },
    { label: 'Supply Chain Admin'}

    // { label: 'Product Master', children: ['Read', 'Write', 'Approve'] },
    // { label: 'Product Restrictions', children: ['Read', 'Write', 'Approve'] },
    // { label: 'Forecasts', children: ['Read', 'Write', 'Approve'] }
]

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
        ref={ref}
        onClick={onClick}
        className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
    >
        {children}
    </button>
))


const SupplyChainSettings = (props) => {
    const [showModal, setShowModal] = useState(false);
    const [tableData, setTableData] = useState([]);
    const [roles, setRoles] = useState([]);
    const [users, setUsers] = useState([]);
    const [groups, setGroups] = useState([]);
    const [selectedGroup, setSelectedGroup] = useState()
    const [loading, setLoading] = useState(true)
    const [grpLoading, setGrpLoading] = useState(false)
    const [searchItem, setSearchItem] = useState([])
    const [searchInput, setSearchInput] = useState("")
    const [adminDataSet, setAdminDataSet] = useState([]);
    const [toggleLoading, setToggleLoading] = useState(false)
    const [toggleIndex, setToggleIndex] = useState()
    const [adminchecker, setAdminChecker] = useState([])

    const trStyle = { display: "flex", justifyContent: "space-evenly" }

    useEffect(() => {
        const groupData = async () => {
            setGrpLoading(true);
            try {
                const groups = await getGroups()
                setGroups(groups.data)
                setSelectedGroup(groups.data[0])
                setGrpLoading(false);
            } catch {
                setGrpLoading(false);
            }
        }
        groupData()
    }, [])

    useEffect(() => {
        if (selectedGroup) {
            const loadTableData = async () => {
                setLoading(true)
                try {
                    const values = await getGroupUsers(selectedGroup.group_name)
                    const temp = values.rolePermissions.find(e => e.solution === "Supply Chain")
                    setTableData(values.userData)
                    setRoles(temp ? temp.roles : [])
                    setUsers(values.userData);
                    setLoading(false)
                    setAdminChecker(values.userData)
                } catch {
                    setTableData([]);
                    setRoles([]);
                    setUsers([]);
                    setLoading(false)
                    setAdminChecker([]);
                }
            }
            loadTableData()
        }
    }, [selectedGroup])

    const handleAdminChange = async (id, isAdmin, i, e) => {
        const toggleData = (i, e) => {
        const obj = {...e, isSupplyChainAdmin: !e.isSupplyChainAdmin}
        const tempState = [...adminchecker]
        tempState[i] = obj
        setAdminChecker(tempState)
        }
        const toggleFailData = (i, e) => {
            const obj = {...e}
            const tempState = [...adminchecker]
            tempState[i] = obj
            setAdminChecker(tempState)
            }
        try{
            toggleData(i, e)
            setToggleLoading(true)
            setToggleIndex(i)
        let changedAdmin = !isAdmin
        const payload = {
            userName: id,
            adminState: changedAdmin
        }
        const response = await updateSupplyChainAdmin(payload)
        if(response){
        // setAdminDataSet(response.userData)
        setToggleLoading(false)
        setToggleIndex()
        }
    } catch {
        toggleFailData(i, e)
        cogoToast.error("Internal server error")
        setToggleLoading(false)
        setToggleIndex()
    } 
    }

    return <div>
        <SupplyChainModal show={showModal} toggleModal={setShowModal} groups={groups} userData={users} roles={roles} groupName={selectedGroup ? selectedGroup.group_name : null}></SupplyChainModal>
        <Row className="justify-content-between mt-5 mb-3">
            <Col xs='3'>
                <Dropdown
                    className={styles.dropdown}
                    onSelect={(_, e) => {
                        setSelectedGroup(groups.find(grp => grp.group_name.toLowerCase() === e.target.innerText.toLowerCase()))
                    }}
                >
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                        {selectedGroup ? selectedGroup.group_name : 'Select Group'}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className={[styles.dropdown_height, grpLoading ? styles.ddheight : undefined]}>
                        <Dropdown.Header>
                            <input type="text" placeholder="Search.." id="myInput" className={[styles.input].join(" ")} value={searchInput} onChange={e => {
                                if (!e.target.value) {
                                    setSearchInput(e.target.value)
                                    setSearchItem(groups)
                                    return
                                }
                                else {
                                    setSearchInput(e.target.value)
                                    const items = groups.filter((data) => data.group_name.toLowerCase().includes(e.target.value.toLowerCase()))
                                    setSearchItem(items)
                                }
                            }} autoComplete="off" />

                        </Dropdown.Header>
                        {searchItem?.map(grp =>
                            <Dropdown.Item
                                className="text-uppercase"
                            >
                                {grp.group_name}
                            </Dropdown.Item>
                        )}

                        {grpLoading ?
                            <Dropdown.Item>{<div style={{marginTop:"10px"}} id={styles.loading_bar_spinner} className={styles.spinner}><div className={styles.spinner_icon}></div></div>}</Dropdown.Item>
                            : groups?.map(grp => <Dropdown.Item
                                className="text-uppercase"
                            >
                                {grp.group_name}
                            </Dropdown.Item>
                            )
                        }
                    </Dropdown.Menu>
                </Dropdown>
            </Col>
            {/*
            <Col className='d-flex text-right justify-content-end'>
                <Button
                    className={`align-self-center btn rounded-pill float-right mr-3 ${styles.btnRight}`}
                    onClick={() => {
                        setShowModal(true);
                    }}
                >
                    Add/Edit Users
                </Button>
            </Col>
        */}
        </Row>
        {!loading ? tableData.length ?
            <Simplebar style={{ maxHeight: "100%" }} autoHide="false">
            <div id={styles.tableNoti}>
                <Table className={styles.table}>
                    <thead>
                        <tr >
                            {tableHeaderList.map((e, key) => {
                                if (!e.children) {
                                    return <th key={key}>{e.label}</th>
                                } else {
                                    return <th key={key}>
                                        <tr className='d-flex justify-content-center'>{e.label}</tr>
                                        <tr style={trStyle}>
                                            {e.children.map(child => <th>{child}</th>)}
                                        </tr>
                                    </th>
                                }
                            })}
                        </tr>
                    </thead>

                        <tbody>
                        {adminchecker.map((data, i) => {
                            // {adminDataSet.userData.map((admin) => {
                            const e = data;
                            const selectedRole = e.roles && e.roles.length ? e.roles.find(r => r.groupId.toLowerCase().replace(/ /g, "") === selectedGroup.group_name.toLowerCase().replace(/ /g, "")) : {}
                            const rolesData = selectedRole ? selectedRole.role : e.roles.length ? e.roles[0].role : null;
                            // const rolePermissions = roles?.find(e => e.type.toLowerCase().replace(/ /g, "") === rolesData?.toLowerCase().replace(/ /g, ""))
                            // const productMaster = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "productmaster")
                            // const productRestrictions = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "productrestrictions")
                            // const forecasts = rolePermissions?.permissions.find(e => e.module.toLowerCase().replace(/ /g, "") === "forecasts")
                            return <tr key={i}>
                                <td>{e._id}</td>
                                <td>{e.name}</td>
                                <td>{e.onZip ? "True" : "False"}</td>
                                <td>{e.onZip ? (rolesData ? rolesData : "NOT ASSIGNED") : null}</td>
                                <td>{rolesData ? <ToggleButton checked={e.isSupplyChainAdmin} onChange={() => handleAdminChange(e._id, e.isSupplyChainAdmin, i, e)} /> : null}</td>
                        {/*
                                <td>
                                    {e.onZip ? (e.roles ? (() =>
                                        const perm = productMaster?.permission.toLowerCase().replace(/ /g, '');
                                        return <div className={styles.iconWrapperRow}>
                                            <td>{perm === 'read' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'write' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                        </div>
                                    })()
                                        : null) : null}
                                </td>
                                <td>
                                    {e.onZip ? (e.roles ? (() => {
                                        const perm = productRestrictions?.permission.toLowerCase().replace(/ /g, '');
                                        return <div className={styles.iconWrapperRow}>
                                            <td>{perm === 'read' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'write' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                        </div>
                                    })()
                                        : null) : null}
                                </td>
                                <td>
                                    {e.onZip ? (e.roles ? (() => {
                                        const perm = forecasts?.permission.toLowerCase().replace(/ /g, '');
                                        return <div className={styles.iconWrapperRow}>
                                            <td>{perm === 'read' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'write' || perm === 'readandwrite' || perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                            <td>{perm === 'approve' ? <FaCheck color='#c3d422' /> : <FaTimes color='#dc3545' />}</td>
                                        </div>
                                    })()
                                        : null) : null}
                                </td>
                            */}
                            </tr>
                        // })}
                        })}
                    </tbody>                 
                </Table>
                </div>
            </Simplebar>
            : <div className="text-center h4">User Information not available for the selected group</div> : <div id={styles.loading_bar_spinner} className={styles.spinner}><div className={styles.spinner_icon}></div></div>}
    </div>
}

export default SupplyChainSettings;