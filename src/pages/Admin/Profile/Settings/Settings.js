/* eslint-disable */
import React from "react"
import { Container, Row, Col, InputGroup, Dropdown } from "react-bootstrap"
import styles from "./style.module.scss"
import Account from "../../../../assets/images/Account.png"
import { FaPen, FaCaretDown } from "react-icons/fa"
// import { bottom } from "@popperjs/core"

const countries = [
  "Regional",
  "Brunei",
  "Cambodia",
  "Hong Kong",
  "Indonesia",
  "Korea",
  "Macau",
  "Malaysia",
  "Myanmar",
  "Philippines",
  "Singapore",
  "Taiwan",
  "Thailand",
  "Vietnam",
  "Other",
]
const localeData = [
  "SGT (+08:00)",
  "SGT (+07:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
  "SGT (+08:00)",
]
const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

function Settings({
  profileData=[],
  submitProfile,
  changeProfileData,
  handleValidation,
  error={},
  isValidMail,
}) {
  let countryState = "Singapore"
  let localeTime = "SGT (+08:00)"
  return (
    <div className={styles.main}>
      <Container>
        <Row>
          <Col
            lg={2}
            style={{
              display: "flex",
              flexDirection: "column",
              alignItems: "center",
            }}
          >
          </Col>
          <Col className={styles.mobCol} lg={10}>
            <Row>
              <div className={styles.mt_10}>Profile </div>
            </Row>
            <Row style={{ marginBottom: "50px" }}>
              <Col lg={2}>
                <img
                  src={
                    profileData.profile_pic ? profileData.profile_pic : Account
                  }
                  alt="profile.png"
                  className={styles.image}
                />
                <label className={styles.inputLabel}>
                  <input
                    type="file"
                    style={{ display: "none" }}
                    name="profile_pic"
                    onChange={e => changeProfileData(e)}
                  />
                  <a name="profile_pic" className={styles.inputFile}>
                    Change Photo
                  </a>
                </label>
              </Col>
              <Col>
                <Row className={styles.mobRow}>
                  <div style={{ paddingLeft: "14px", paddingBottom: "14px" }}>
                    <div className={styles.headline}>First Name</div>
                    <div
                      className={styles.input}
                      style={{
                        borderBottom: error["firstName"] ? "1px solid red" : "",
                      }}
                    >
                      <input
                        name="firstName"
                        className={styles.inputname}
                        value={profileData.firstName?profileData.firstName:""}
                        onChange={e => changeProfileData(e)}
                        onBlur={handleValidation}
                      />
                      <FaPen color="#A9A9A9" />
                    </div>
                    {error["firstName"] ? (
                      <div style={{ color: "red" }}>{error["firstName"]}</div>
                    ) : (
                      ""
                    )}
                  </div>
                  <div style={{ paddingLeft: "14px", paddingBottom: "14px" }}>
                    <div className={styles.headline}>Last Name</div>
                    <div
                      className={styles.input}
                      style={{
                        borderBottom: error["lastName"] ? "1px solid red" : "",
                      }}
                    >
                      <input
                        name="lastName"
                        className={styles.inputname}
                        value={profileData.lastName?profileData.lastName:""}
                        onChange={e => changeProfileData(e)}
                        onBlur={handleValidation}
                      />
                      <FaPen color="#A9A9A9" />
                    </div>
                    {error["lastName"] ? (
                      <div style={{ color: "red" }}>{error["lastName"]}</div>
                    ) : (
                      ""
                    )}
                  </div>
                </Row>
                <div className={styles.mobHead}>
                  <div className={styles.headline}>Email</div>
                  <div
                    className={styles.inputemail}
                    style={{
                      borderBottom: error["email"] ? "1px solid red" : "",
                    }}
                  >
                    <input
                      name="email"
                      className={styles.inputemailname}
                      value={profileData.email?profileData.email:""}
                      onChange={e => changeProfileData(e)}
                      onBlur={handleValidation}
                    />
                    <FaPen color="#A9A9A9" />
                  </div>
                  {error["email"] ? (
                    <div style={{ color: "red" }}>{error["email"]}</div>
                  ) : !isValidMail ? (
                    <div style={{ color: "red" }}>Email Id is Invalid.</div>
                  ) : (
                    ""
                  )}
                </div>
                <Row
                  className={styles.pd_14}
                  style={{
                    marginTop: 5,
                  }}
                >
                  <div className={styles.mobCntry}>
                    <label className={`${styles.label}`} htmlFor="type">
                      Country
                    </label>
                    <InputGroup>
                      <Dropdown className={styles.dropdown}>
                        <Dropdown.Toggle as={CustomDropdownToggle}>
                          <span style={{ float: "left" }}>
                            {profileData.time_locale?.country || countryState}
                          </span>
                          <span
                            style={{
                              float: "right",
                            }}
                          >
                            <FaCaretDown
                              style={{
                                marginBottom: 3,
                                marginLeft: 5,
                              }}
                            />
                          </span>
                        </Dropdown.Toggle>
                        <Dropdown.Menu className={styles.countryDropdown}>
                          {countries.map(country => (
                            <Dropdown.Item
                              key={country}
                              name="country"
                              href="#"
                              className="text-uppercase"
                              onClick={e => changeProfileData(e, country)}
                              // onClick={() => setCountry(country)}
                            >
                              {country}
                            </Dropdown.Item>
                          ))}
                        </Dropdown.Menu>
                      </Dropdown>
                    </InputGroup>
                  </div>

                  <div  className={styles.mobTime} style={{ marginLeft: "15px" }}>
                    <label className={`${styles.label}`} htmlFor="type">
                      Time Zone
                    </label>
                    <InputGroup>
                      <Dropdown className={styles.dropdown}>
                        <Dropdown.Toggle as={CustomDropdownToggle}>
                          <span style={{ float: "left" }}>
                            {profileData.time_locale?.time_zone || localeTime}
                          </span>
                          <span
                            style={{
                              float: "right",
                            }}
                          >
                            <FaCaretDown
                              style={{
                                marginBottom: 3,
                                marginLeft: 5,
                              }}
                            />
                          </span>
                        </Dropdown.Toggle>
                        <Dropdown.Menu className={styles.countryDropdown}>
                          {localeData.map(locale => (
                            <Dropdown.Item
                              key={locale}
                              name="time_zone"
                              href="#"
                              className="text-uppercase"
                              onClick={e => changeProfileData(e, locale)}
                              // onClick={() => setLocaleTime(locale)}
                            >
                              {locale}
                            </Dropdown.Item>
                          ))}
                        </Dropdown.Menu>
                      </Dropdown>
                    </InputGroup>
                  </div>
                </Row>
                <Row>
                  <Col
                    md={9}
                    style={{
                      paddingTop: "30px",
                      paddingRight: "20px",
                      textAlign: "right",
                    }}
                    className={styles.mobSave}
                  >
                    <button
                      className={styles.btn}
                      onClick={e => submitProfile(e, "settings")}
                    >
                      SAVE
                    </button>
                  </Col>
                </Row>
              </Col>
            </Row>
          </Col>
        </Row>
      </Container>
    </div>
  )
}

export default Settings
