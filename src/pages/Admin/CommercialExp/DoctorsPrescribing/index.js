import React from "react"
import DoctorPriscribing from "../InfraredSales/index"
function DoctorPrescribing(props) {
  const { location } = props
  return (
    <div>
      <DoctorPriscribing
        {...props}
        buttonItems={location.state?.buttonItems}
        heading={location.state?.heading}
      />
    </div>
  )
}

export default DoctorPrescribing
