import React, { useState, useEffect } from "react"
//import styles from "./style.module.scss"
import style2 from "../../Insider/pillBody/style.module.scss"
import Tab1 from "./tab1"
import DoctorsPrescription from "../../Insider/pillBody/index"
// import TabOptions from "../../../ui/taboption/index"
import HeaderImage from "../../../../assets/images/commercialBanner.png"
// import Tab from "../../../Tab"
import Msg from "../../../../components/constant"
import { basePath } from "../../../../components/basePath"
import HeaderCommonComponent from "../../../../components/ui/HeaderCommon/HeaderCommon"
// import { group } from "../../../../components/getGroupName"
import axios from "../../../../../axiosConfig"
import { Spinner } from "react-bootstrap"
import Model from "../../../../components/Header/contactModal"
import DownButton from "../../../../components/ui/Button/DownButton"
import Cookie from "js-cookie"
export default function InfraredSales(props) {
  // const lime = "#c3d422"
  // const white = "#ffffff"
  const [tabSelected, setTab] = useState(0)
  const [subs, setSubs] = useState([])
  const [loading, setLoading] = useState(true)
  const [hideTab, setHideTab] = useState(false)
  const [openModel, setOpenModel] = useState(false)
  const [groupName, setGroupName] = useState("")
  useEffect(() => {
    let group= Cookie.getJSON("inner_option")?Cookie.getJSON("inner_option"):null
    setGroupName(group)
    axios
      .post("/subs/subsDatabase", {
        groupName: group ? group.label : null,
      })
      .then(res => {
        let fields = res.data.subs
        const localSubsData = []
        fields.forEach((item, index) => {
          let localObj = {}
          if (item.status === "Active" || item.status === "Ending Soon") {
            localObj.Product = item.product
            localObj.isSubscribed = true
            localSubsData.push(localObj)
          } else {
            localObj.Product = item.product
            localObj.isSubscribed = false
            localSubsData.push(localObj)
          }
          return item
        })
        localStorage.setItem("subsData", JSON.stringify(localSubsData))
        setSubs(localSubsData)
        setLoading(false)
      })
      .catch(err => {
        console.error("Failed to get the data", err)
        setLoading(false)
      })
  }, [])
  const tabs = ["Infrared"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }
  const { location, buttonItems, heading } = props
  const hide = () => setHideTab(!hideTab)
  return (
    <>
      <HeaderCommonComponent
        heading={"Commercial Excellence"}
        HeaderImage={HeaderImage}
        height={""}
        previousProps={props}
        tabs={hideTab ? [] : tabs}
        tabSelected={tabSelected}
        onTabChange={onTabChange}
      >
        {tabSelected === 0 ? (
          location &&
          location.pathname === `${basePath}/commercialExp/InfraredSales` ? (
            subs.length===0 && loading ? (
              <div style={{ minHeight: "100vh", backgroundColor: "white" }}>
                <Spinner
                  animation="border"
                  color="#0A4658"
                  style={{ marginLeft: "50%", marginTop: "50vh" }}
                />
              </div>
            ) : (
              <Tab1 hideTab={() => hide()} subsData={subs} {...props} />
            )
          ) : location &&
            location.pathname ===
              `${basePath}/commercialExp/InfraredSales/doctorPrescription` ? (
            <div className={`py-4 px-lg-5 ${style2.contentWrapper}`}>
              <DoctorsPrescription
                buttonItems={buttonItems}
                heading={heading}
              />
            </div>
          ) : (
            ""
          )
        ) : (
          <Msg />
        )}
      </HeaderCommonComponent>
      <Model open={openModel} onClose={() => setOpenModel(false)} />
      <DownButton modalListener={()=>setOpenModel(true)}/>
    </>
  )
}
