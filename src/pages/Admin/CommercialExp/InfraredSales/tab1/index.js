import React, { useEffect, useState } from "react"
import { Container } from "reactstrap"
import Styles from "./style.module.scss"
import Optimization from "../../../../../assets/images/png/optimization.png"
import JourneyIcon from "../../../../../assets/images/png/journey.png"
import BundlingIcon from "../../../../../assets/images/png/bundling.png"
import DoctorIcon from "../../../../../assets/images/png/doctor.png"
import PriceIcon from "../../../../../assets/images/png/price.png"
import TimingIcon from "../../../../../assets/images/png/timing.png"
import UtilizationIcon from "../../../../../assets/images/png/utilization.png"
import { Link } from "gatsby"
import { SubHeader } from "../../../../../components/ui/subHeader"
import { getGroupOptionsbyGroupName } from "../../../../../services/Api/authGroups"
import Body from "../../../../../components/ui/ReportView/Body"
import { Spinner } from "react-bootstrap"
// import { style } from "d3"
import { basePath } from "../../../../../components/basePath"
import Tab from "../../../../../components/Tab"
// import { group } from "../../../../../components/getGroupName"
import ButtonTab from "../../../../../components/ui/ButtonsTab"
import cogoToast from "cogo-toast"
import Cookie from "js-cookie"
import Infrared from "../../../../../assets/images/Infrared.png"
// import { Button } from "react-bootstrap"
// import SubHeader from "../../../../../components/ui/subHeader"
//  let group = "Tableau Regional - Data Analytics - Demo"
const circleData = [
  {
    head: "Patient Journey",
    icon: JourneyIcon,
    description: `Real-world clinical insights, such as comorbidities of patients.`,
  },
  {
    head: "Doctor Prescription Behaviour",
    icon: DoctorIcon,
    description: `Insights into Doctor's prescription habits and tendancies.`,
  },
  {
    head: "Tender Utilisation",
    icon: UtilizationIcon,
    description: `Benchmark utilisation rates to proactively prepare for tenders and use them effectively.`,
  },
  {
    head: "Order Timing",
    icon: TimingIcon,
    description: `Analyze purchase order history to optimize sales effectiveness and planning.`,
  },
  {
    head: "Price Sensitivity",
    icon: PriceIcon,
    description: `Identify individual price sensitivities in the market to increase trade spend ROI.`,
  },
  {
    head: "Promotion Optimisation",
    icon: Optimization,
    description: `Optimise promotional schemes based on individual customer behavior.`,
  },
  {
    head: "Product Bundling",
    icon: BundlingIcon,
    description: `Recommend high probability cross-selling opportunities.`,
  },
  {
    head: "Infrared Base",
    icon: BundlingIcon,
    description: `Identify potential areas of opportunity and growth.`,
  },
]
export default function Tab1(props) {
  const [show, setShow] = React.useState(false)
  const [dataSet, setDataSet] = useState(circleData)
  const [showReport, setShowReport] = useState(false)
  // const [link, setLink] = useState({})
  const [array, setArray] = useState([])
  const [loading, setLoading] = useState(true)
  const [infraredSubscription, setInfraredSubscription] = useState({})
  // const [tabItems, setTabItems] = React.useState([])
  const [activeTab, setActiveTab] = React.useState(0)
  const [linkData, setLinkData] = useState([])
  const [item, setItem] = useState([])
  const [activePill, setActivePill] = useState(0)
  const [group, setGroup] = useState("")
  const hideParentTab = () => {
    props.hideTab()
  }
  const Circles = props => {
    // console.log("urlItems",props.urlItem);
    let bgColor =
      basePath === "/auth" && group
        ? infraredSubscription?.isSubscribed
          ? props.urlItems?.length
            ? "#457186"
            : "#B6C7CD"
          : "#6F8F9A"
        : "#ffffff"
    let border = basePath === "/auth" && group ? "none" : ""
    const handleEvent = () => {
      if (basePath === "/auth" && props.urlItems?.length) {
        hideParentTab()
        // console.log(props,"PROPS");
        let idx = item.indexOf(props.head)
        setActiveTab(idx)
        setLinkData(props.urlItems)
        setShowReport(true)
        onPillChange(0)
      } else {
        cogoToast.warn("No Dashboard Available")
      }
    }
    return (
      <div
        onClick={handleEvent}
        onKeyPress={() => {}}
        role="button"
        tabIndex={0}
        style={{
          backgroundColor:
            !infraredSubscription?.isSubscribed && show === props.id
              ? "#ffffff"
              : bgColor,
          border: props.id === show ? "#c3d422" : border,
        }}
        className={Styles.circleBorder}
      >
        <img src={props.icon} style={{ height: "3rem" }} alt="icon" />
      </div>
    )
  }

  const doctorButtons = [
    "Prescription Behaviour",
    "Patient Journey",
    "Doctor Profile",
  ]
  const heading = [
    "Doctor's Prescribing Behaviour",
    "Doctor's Patient Journey",
    "Doctor Profile",
  ]
  useEffect(() => {
    const func = async () => {
      let groups = Cookie.getJSON("inner_option")
        ? Cookie.getJSON("inner_option")
        : null
      setGroup(groups)
      setLoading(true)
      let subsData = props.subsData
      // console.log(props);
      let infraredSubs = subsData
        ? subsData.find(a => a.Product === "Infrared")
        : {}
      setInfraredSubscription(infraredSubs)
      const data = await getGroupOptionsbyGroupName("infrared")
      try {
        // console.log(data,"-----------HERE I AM");
        if (infraredSubs?.isSubscribed) {
          if (data[0].type.toLowerCase() === "infrared") {
            // setArray(data[0].items)
            let ar = []
            let dataAr = []
            data[0].items.map(i => {
              if (i.elements.length > 0) {
                ar.push(i.type)
                dataAr.push(i)
              }
              return i
            })
            setArray(dataAr)
            setItem(ar)
            // console.log(ar, "i am Item");
          }
          setDataSet(
            circleData.map(m => {
              const temp = data?.[0]?.items.find(
                e =>
                  e.type.toLowerCase().replace(/ /g, "") ===
                  m.head.toLowerCase().replace(/ /g, "")
              )
              m.urlItems = temp ? temp.elements : false
              return m
            })
          )
          // setTimeout(console.log(circleData),1000)
          setLoading(false)
        } else {
          cogoToast.warn("Current Group is not subscribed to this service")
          setLoading(false)
        }
      } catch (err) {
        // cogoToast.error("Internal Server Error")
        console.log(err)
        setLoading(false)
      }
    }
    func()
  }, [props.subsData])

  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      console.log(array[tabIdx], "hello", array)
      setActiveTab(tabIdx)
      setLinkData(array[tabIdx].elements)
      onPillChange(0)
    }
  }
  const onPillChange = pill => {
    setActivePill(pill)
  }
  return (
    <div className={Styles.main}>
      {/* tab div */}
      {!loading && !showReport ? (
        <SubHeader
          title="Infrared"
          subTitle="Make more informed targeting decisions through tailored and impactful engagement strategies based on customer commercial potential and purchasing behaviors."
          image={Infrared}
        />
      ) : null}
      {/* diagram */}
      {showReport ? (
        <div>
          <Tab items={item} activeTab={activeTab} onTabChange={onTabChange} />
          <ButtonTab
            items={linkData}
            activeTab={activePill}
            onTabChange={onPillChange}
          />

          <Body
            tabs={array}
            url={linkData[activePill]?.url}
            label={linkData[activePill]?.lable}
            height={linkData[activePill]?.height}
            description={linkData[activePill]?.description}
            backListener={() => {
              setShowReport(false)
              hideParentTab()
            }}
          />
        </div>
      ) : (
        <Container className={`text-center ${Styles.diagramContainer}`}>
          {loading ? (
            <div
              style={{
                minHeight: "13vh",
                display: "flex",
                justifyContent: "center",
                alignItems: "center",
              }}
            >
              <Spinner animation="border" role="status" />
            </div>
          ) : (
            <ul className={Styles.circleContainer}>
              {dataSet.map((item, key) => (
                <li key={key} style={{ textAlign: "center" }}>
                  <span
                    role="button"
                    tabIndex={key}
                    onMouseEnter={() => setShow(key)}
                    onMouseLeave={() => setShow(false)}
                    key={key}
                    heading={item.head}
                  >
                    <Link
                      to={props.location.pathname}
                      state={{ buttonItems: doctorButtons, heading: heading }}
                      style={{ color: "#094658" }}
                      className={Styles.link}
                    >
                      <Circles
                        icon={item.icon}
                        head={item.head}
                        urlItems={item.urlItems}
                        id={key}
                      />
                    </Link>
                    <div
                      className={Styles.item_description}
                      style={{
                        position: "absolute",
                        transition: "all 0.2s",
                        zIndex: show === 7 ? "2" : "-3",
                        transform: key === show ? "scale(1)" : "scale(0)",
                      }}
                    >
                      <span>
                        <div className={Styles.underline}>
                          <p style={{ fontWeight: "bold" }}>{item.head}</p>
                        </div>
                        <p>{item.description}</p>
                      </span>
                    </div>
                  </span>
                </li>
              ))}
            </ul>
          )}
        </Container>
      )}
    </div>
  )
}
