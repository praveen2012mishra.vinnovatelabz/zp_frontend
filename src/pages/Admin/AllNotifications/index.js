import React, { useState, useEffect } from "react"
import axios from "../../../../axiosConfig"
import Cookie from "js-cookie"
import Social from "../../../components/ui/social2"
import styles from "./style.module.scss"
import NotificationTable from "./notificationTable"
import {connect} from "react-redux"
import { countNotification } from "../../../services/Redux/actions/notification"
// import { Spinner } from 'reactstrap'
function Notifications(props) {
  const [notifications, setNotifications] = useState([])
  const [loading, setLoading] = useState(true)
  const [loadingId, setLoadingId] = useState("")
  const [checkedLength, setCheckedLength] = useState(0)
  const [groupName, setGroupName] = useState({})
  const [flag, setFlag] = useState(false)
  const [grpArray,setGrpArray]=useState([])
  const [user,setUserId]=useState("")
  useEffect(() => {
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    const userId = samlToken ? samlToken.user.name_id : null
    setUserId(userId)
    let groupName = Cookie.getJSON("inner_option");
    let grpAr=Cookie.getJSON("ADgroups");
    setGrpArray(grpAr)
    setGroupName(groupName)
    if (groupName) {
      axios
        .get(`/notification/getNotificationsGroupName/${groupName.label}`)
        .then(res => {
          setNotifications(res.data.notification)
          setLoading(false)
        })
    }
  }, [])
  const getNotiCount = async (groups,userId) => {
    let ar = groups.map(item =>item.label);
    const unReadNotifications = await axios.post("notification/getNotificationCountByGroups", {
      "groupArray":ar,
      "userId":userId
    })
    try {
        console.log(unReadNotifications.data);
        props.countNotification(unReadNotifications.data.length)
    } catch {
        console.log(unReadNotifications);
    }
}
  const getNotification = (item, index) => {
    setLoading(true)
    setLoadingId(index)
    let id = []
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    let userId = samlToken ? samlToken.user.name_id : null
    id.push(item._id)
    axios
      .post(`/notification/updateCheckedNotification`, {
        id,
        userId,
        checked: userId,
        groups: groupName.label,
      })
      .then(res => {
        setNotifications(res.data.notification)
        setCheckedLength(res.data.checkedLength)
        getNotiCount(grpArray,userId);
        setLoading(false)
        setLoadingId("")
      })
  }

  return (
    <div style={{ minHeight: "100vh" }}>
      <div className={styles.social}>
        <Social lineHeight={0} />
      </div>
      <div className={styles.notif}>
        <div className={styles.notifications}>{groupName?`${groupName.display_name} Notifications`:"Notifications"}</div>
        <div className={styles.notifications_text}>
          Manage your notifications and manage critical issues
        </div>
        {/* {loading?<Spinner style={{display:"flex",margin:"auto"}}/>: */}
        {notifications.length > 0 ? 
        <NotificationTable
          userId={user}
          notifications={notifications}
          getNotification={(item, index) => getNotification(item, index)}
          loading={loading}
          loadingId={loadingId}
        />
        :
        <div style={{display:"flex", justifyContent:"center", paddingTop:"5%"}}><h3>No Available Notifications</h3></div>
        }
        {/* } */}
      </div>
    </div>
  )
}

const mapDispatchToProps = dispatch => {
  return {
    countNotification: count => dispatch(countNotification(count)),
  }
}

export default connect(null, mapDispatchToProps)(Notifications)
