import React, { useState } from 'react'
import { Table } from "react-bootstrap";
import styles from "./style.module.scss";
import moment from "moment"
import { Spinner } from 'reactstrap';
import { navigate } from "gatsby"
import { basePath } from "../../../components/basePath"
function NotificationTable({
    notifications,
    getNotification,
    loading,
    loadingId,
    userId
}) {
    const [isRedirect, setIsRedirect] = useState(false)
    const [redirectPath, setRedirectPath] = useState(false)
    const redirectTo = (route, article_url) => {
        let redirectRoute = ""
        if (route === "Infrared") redirectRoute = `${basePath}/commercialExp/InfraredSales`
        else if (route === "Insider") redirectRoute = `${basePath}/insider?1`
        else if (route === "Clinical Reach") redirectRoute = `${basePath}/insider?2`
        else if (route === "Supply Chain Control Tower") redirectRoute = `${basePath}/supplyChain/controlTower?1`
        else if (route === "Sales & Operation Planner") redirectRoute = `${basePath}/supplyChain/controlTower?2`
        else if (route === "Business Intelligence") redirectRoute = `${basePath}/businessInt/datamanagement`
        else if (route === "Insights") {
            if (article_url) {
                let lastIndex=article_url.lastIndexOf("/")
                redirectRoute = article_url.substring(lastIndex,article_url.length)
            } else redirectRoute = `/insights/`
        }else if (route==="General") redirectRoute="./"
        else redirectRoute = ""
        setRedirectPath(redirectRoute)
        setIsRedirect(true)
    }
    if (isRedirect) {
        if (redirectPath) {
            navigate(redirectPath)
        }
    }

    const handleClick = (item, index) => {
        if(item?.type === "Insights" && item?.article_url!==""){
        redirectTo(item.type, item.article_url)
        }else redirectTo(item.type, null)
    }
    return (
        <div id={styles.tableNoti}>
            <Table className={styles.table}>
                <thead>
                    <tr>
                        <th>DATE</th>
                        <th>PRODUCT</th>
                        <th>NOTIFICATION</th>
                    </tr>
                </thead>
                <tbody>
                    {
                        notifications?.map((item, index) => {
                            return <tr key={index} className={styles.tableHover} onClick={() => handleClick(item, index)}>
                                <td className={styles.tableTextHoverColor}>
                                    {moment(new Date(item.createdAt)).format("DD MMM YYYY")}
                                </td>
                                <td className={styles.tableTextHoverColor}>
                                    {item.type}
                                </td>
                                <td className={styles.tableTextHoverColor} >
                                    {item.header}
                                </td>
                                <td>
                                    <button className={item.checked[userId]===true ? styles.disabledButton : styles.button}
                                        disabled={typeof(item.checked)==="object"?
                                        item.checked[userId]:false}
                                        onClick={(e) => {
                                            e.stopPropagation();
                                            getNotification(item, index)
                                        }}
                                    >
                                        {loadingId === index && loading ? <Spinner style={{ display: "flex", margin: "auto" }} /> : "Mark as read"}
                                    </button>
                                </td>
                            </tr>
                        })
                    }
                </tbody>
            </Table>
        </div>
    )
}

export default NotificationTable