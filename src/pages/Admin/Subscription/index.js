import React, { useState, useEffect } from "react"
import { Accordion, Card, Dropdown, ButtonGroup } from "react-bootstrap"
import styles from "./style.module.scss"
import HeaderCommonComponent from '../../../components/ui/HeaderCommon/HeaderCommon'
import Axios from '../../../../axiosConfig'
import Cookie from 'js-cookie';
import { Spinner } from "reactstrap";
import cogo from "cogo-toast"
import { Link } from "gatsby"

const isBrowser = typeof window !== `undefined`

const SubscriptionPage = () => {
  // const tabs = []
  const [fields, setFields] = useState([]);
  const [status, setStatus] = useState(true);
  const [authData, setAuthData] = useState({
    nameId: "Dummy",
    sessionIndex: "",
    inResponse: "",
  })
  useEffect(() => {
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
      ? JSON.parse(samlCookie.substr(2, samlCookie.length))
      : null
    let group = Cookie.get("inner_option") ? JSON.parse(Cookie.get("inner_option")) : null
    if (samlToken) {
      setAuthData({
          nameId: samlToken.user.name_id,
          sessionIndex: samlToken.user.session_index,
          inResponse: samlToken.response_header.in_response_to,
        })}
      // setAuthData({
      //   nameId: 'skularathna',
      //   sessionIndex: `_040bdd7a-ce24-4fc9-ab52-14a883e6b63b`,
      //   inResponse: `_5aedb13005b09859c9d1f1339579d54a80c1b8752f`,
      // })
    // }
    Axios.post("/subs/subsDatabase", {
      groupName: group ? group.label :
        null
        // "Tableau Singapore - Data Analytics - Demo"
    })
      .then((res) => {
        setFields(res.data.subs);
        if (res.data.subs.length > 0) {
          setStatus(false)
        } else {
          cogo.error("No subscription found")
          setStatus(false)
        }
      })
      .catch((err) => {
        setFields([]);
        cogo.error("Service Error")
        console.error("Failed to get the data", err);
        setStatus(false);
      })
  }, []);

  const columns= [
    "Product",
    "Coverage",
    "Country",
    "Frequency",
    "Start Date",
    "End Date",
    "Status",
    "Actions"
  ]
  return (
    <div>
      <HeaderCommonComponent heading={'My Subscriptions'}
        sub_heading={<SubHeading authData={authData} />} tabs={[]} />
      <div
        className="container-fluid p-0"
        style={{ overflow: "hidden", minHeight: "50vh" }}
      >
        <div className="row">
          <div className="col col-12">
            <Accordion defaultActiveKey="0">
              {
                fields.map((item, key) => {
                  if(key ===0){
                    return <AccordianItems
                    key={key}
                    header={item.solution.toUpperCase()}
                    tableHeader={columns}
                    fields={fields}
                    eventKey={key + 1}
                    status={status}
                  />
                  }else{
                    let temp = key -1
                    if (fields[temp].solution!==item.solution) {
                      return <AccordianItems
                        key={key}
                        header={item.solution.toUpperCase()}
                        tableHeader={columns}
                        fields={fields}
                        eventKey={key + 1}
                        status={status}
                      />
                    }
                  }
                }
                )
              }
            </Accordion>
          </div>
        </div>
      </div>
    </div>
  )
}

export default SubscriptionPage


const SubHeading = ({ authData }) => (<div
  style={{
    marginTop: "30px",
    paddingBottom: "30px",
    textAlign: "center",
    marginLeft: "25%",
    marginRight: "25%",
    color: "#FFFFFF",
    fontSize: "20px",
  }}
>
  <p>
    Hello <strong style={{ color: "#c3d422" }}>{authData.nameId}</strong> (not
    {/* eslint-disable-next-line */}
    {` ${authData.nameId}`} <span
      tabIndex={0}
      role="button"
      onClick={
        () => Axios
          .post("/saml/singleLogout", {
            name: authData.nameId,
            session: authData.sessionIndex,
            in_resp: authData.inResponse,
            status: "not known",
          })
          .then(res => {
            Cookie.remove("inner_option")
            Cookie.remove("ADgroups")
            Cookie.remove("saml_response")
            Cookie.remove("Group_names")
            localStorage.removeItem('subsData')
            if (isBrowser) {
              window.location.href = res.data.redirect
            }
          })
          .catch(e => console.log(e))
      } style={{ color: "#c3d422" }}>Sign out</span>).<br />
      From the subscription table, you can view the the details of the solutions you're subscribed to.
    </p>
  {/* <p style={{ color: "#c3d422", marginTop: "-10px" }}>
    edit your password and account details.
    </p> */}
</div>)

const AccordianItems = ({ header, tableHeader, fields, eventKey, status }) => {
  const [route, setRoute] = useState("./")
  useEffect(() => {
    const localSubsData = [];
    fields.map((item, index) => {
      let localObj = {}
      if (item.status === "Active" || item.status === "Ending Soon") {
        localObj.Product = item.product
        localObj.isSubscribed = true
        localSubsData.push(localObj)
      } else {
        localObj.Product = item.product
        localObj.isSubscribed = false
        localSubsData.push(localObj)
      }
      return item
    })
    localStorage.setItem("subsData", JSON.stringify(localSubsData))
  }, [fields])
  // console.log(header);
  return (
    <Card>
      <Accordion.Toggle
        as={Card.Header}
        className="bg-light-teal"
        eventKey={eventKey}
      >
        <div className="d-flex">
          <div className="flex-grow-1">
            <span
              style={{
                marginLeft: "60px",
                fontWeight: "bold",
                fontSize: "18px",
                color: "#094658",
              }}
              className={styles.mobHead}
            >
              {header}
            </span>
          </div>
          <div>
            <span
              style={{
                marginLeft: "30px",
                fontWeight: "bold",
                fontSize: "18px",
                color: "#094658",
              }}
            >
              <Dropdown as={ButtonGroup}>
                <Dropdown.Toggle
                  variant="transparent"
                  style={{
                    color: "#094658",
                    marginBottom: "-15px",
                    marginRight: "150px",
                  }}
                />
              </Dropdown>
            </span>
          </div>
        </div>
      </Accordion.Toggle>
      <Accordion.Collapse eventKey={eventKey}>
        <Card.Body className={styles.mobCard}>
          <div className="row">
            <div className="col col-12">
              <table
                className="table"
                style={{
                  marginTop: "15px",
                  textAlign: "center",
                  marginLeft: "30px",
                  width: "95%",
                  marginBottom: "15px",
                }}
              >
                <thead>
                  <tr>
                    {tableHeader.map((t, key) => <th key={key}>{t}</th>)}
                  </tr>
                </thead>
                <tbody>
                  {status ?
                    <tr>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td><Spinner color="#0A4658" /></td>
                      <td></td>
                      <td></td>
                      <td></td>
                    </tr>
                    :
                    fields.map((m, key) => (
                      m && m.product && m.solution.toLowerCase().replace(/ /g, "") === header.toLowerCase().replace(/ /g, "") ?
                        <tr key={key}>
                          <td>{m.product}</td>
                          <td>{m.coverage}</td>
                          <td>{m.country}</td>
                          <td>{m.frequency}</td>
                          <td>{m.startDate}</td>
                          <td>{m.endDate}</td>
                          <td>{m.status}</td>
                          <td style={{ paddingTop: "7px" }}>
                            <Link
                              className="btn btn-sm"
                              style={{
                                backgroundColor: "#008080",
                                paddingLeft: "30px",
                                color: "#ffffff",
                                paddingRight: "30px",
                                fontWeight: "700",
                                borderRadius: "20px",
                                textDecoration: "none"
                              }}
                              to={m.status.toLowerCase() === 'active' ?
                                m.product.toLowerCase() === "insider" ? `/auth/insider?1` :
                                  m.product.toLowerCase() === "clinical reach" ? `/auth/insider?2` :
                                    m.product.toLowerCase() === "infrared" ? `/auth/commercialExp/InfraredSales` :
                                      m.product.toLowerCase() === "supply chain control tower" ? `/auth/supplyChain/controlTower?1` :
                                        m.product.toLowerCase() === "sales and operations planner" ? `/auth/supplyChain/planner?2` :
                                          `/auth/businessInt/datamanagement`
                                :
                                '/contact/'
                              }
                            >{
                                m.status.toLowerCase() === "active" ? "VIEW" : "RENEW"
                              }
                            </Link>
                          </td>
                        </tr>
                        : null
                    ))
                  }
                </tbody>
              </table>
            </div>
          </div>
        </Card.Body>
      </Accordion.Collapse>
    </Card>
  )
}