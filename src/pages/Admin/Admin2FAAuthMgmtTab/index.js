import React,{useState} from "react"
import { Table, Container, Row, Col } from "react-bootstrap"
import { ArrowClockwise } from "react-bootstrap-icons"
import styles from "./style.module.scss"
import { connect } from "react-redux"
import cogo from "cogo-toast"
import LoadingOverlay from "react-loading-overlay"
import {
  getAllTwoFactorData,
  resetQR,
} from "../../../services/Redux/actions/adminSession"

const Admin2FAMgmtTab = ({ 
  data=[],
  twoFactorData,
  resetQrCode,
  err,
  notification,
  groupName
}) => {
   // eslint-disable-next-line react-hooks/exhaustive-deps
  React.useEffect(()=>{
    groupName && twoFactorData(groupName)
    setLoading(true)
    // eslint-disable-next-line react-hooks/exhaustive-deps
  },[groupName])
  const [loading, setLoading] = useState(true)
  const [checked, setChecked] = useState({})
  // handle error
  if (err) {
    if (loading) setLoading(false)
  }
  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  // structurize the data from the raw data array
  if (data.length > 0) {
    data = data.map((data, key) => ({
      _id: data._id,
      name: data.name,
      selected: Object.keys(checked).length === 0 ? false : checked[data._id],
    }))
    // check if loading is true, otherwise it is causing infinite render loops
    if (loading) setLoading(false)
  }
  const initateReset = () => {
    let users = [];
    for (let keys in checked) {
      if (checked[keys]!==false) {
        users.push(checked[keys])
      }
    }
    resetQrCode(users);
    setChecked({});
  }

  return (
    <Container fluid>
      <Row className="justify-content-between mt-5 mb-3">
        <Col>
          <p className="h1">
            <b>‘Singapore’</b>
          </p>
        </Col>
        <Col>
          <button className={`btn rounded-pill ${styles.btnRight}`}
            onClick={() => initateReset()}
          >
            <span>
              <ArrowClockwise />
            </span>
            <span>RESET QR CODE</span>
          </button>
        </Col>
      </Row>

      <LoadingOverlay active={loading} spinner>
        <Table className={styles.table}>
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
          <tbody>
            {data.map((d, key) => (
              <tr key={key*97}>
                <td key={key}
                  className={d.selected === d._id ? styles.selected : "hello"}
                ><span role="button"
                tabIndex={0}
                style={{outline:"none"}}
                onMouseDown={() => {
                  checked.hasOwnProperty(d._id) ?
                    checked[d._id] === false ? setChecked({ ...checked, [d._id]: d._id }) :
                      setChecked({ ...checked, [d._id]: false })
                    :
                    setChecked({ ...checked, [d._id]: d._id })
                }}
                onTouchStart={(e) => {
                  e.preventDefault()
                  checked.hasOwnProperty(d._id) ?
                    checked[d._id] === false ? setChecked({ ...checked, [d._id]: d._id }) :
                      setChecked({ ...checked, [d._id]: false })
                    :
                    setChecked({ ...checked, [d._id]: d._id })
                }}
                onKeyPress={()=>{}}>{d.name}
                </span>
                </td>
              </tr>
            ))}
          </tbody>
        </Table>
      </LoadingOverlay>
    </Container>
  )
}

const mapStateToProps = state => ({
  data: state.adminSession.twoFA,
  err: state.adminSession.err,
  notification: state.notification,
  groupName: state.adminSession.grpName
})

const mapDispatchToProps = dispatch => ({
  twoFactorData: (name) => dispatch(getAllTwoFactorData(name)),
  resetQrCode: (users) => (
    dispatch(resetQR(users))),
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(Admin2FAMgmtTab)
