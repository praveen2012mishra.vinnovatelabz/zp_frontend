import cogoToast from 'cogo-toast'
import React, { useState, useEffect } from 'react'
import {
    Container,
    Row,
    Col,
    InputGroup,
    Dropdown,
    Spinner
} from "react-bootstrap"
import axios from '../../../../axiosConfig'
import styles from "../AdminPageSearch/style.module.scss"
import selfStyle from "./style.module.scss"
import Cookie from "js-cookie"
// import axiosInstance from '../../../../axiosConfig'
const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
        ref={ref}
        onClick={onClick}
        className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
    >
        {children}
    </button>
))
function AdminNotification() {
    const [data, setData] = useState([{ display_name: "Select" }])
    const [selected, setSelected] = useState({ display_name: "Select" })
    const [selectedType, setSelectedTypes] = useState("Select")
    const [loading, setLoading] = useState(true)
    const [message, setMessage] = useState("")
    const [url, setUrl] = useState("")
    const types = [
        "General",
        "Infrared",
        "Insider",
        "Supply Chain Control Tower",
        "Sales & Operation Planner",
        "Business Intelligence",
        "Insights"
    ]
    useEffect(() => {
        let changedOption = Cookie.getJSON("inner_option")
        axios.get("/group/fetchGroupOptions").then((res) => {
            if (changedOption && changedOption.country.toLowerCase() !== "regional") {
                let ar = res.data.filter(item => item.country.toLowerCase() === changedOption.country.toLowerCase())
                setData(ar)
            } else {
                setData(res.data)
            }
            setLoading(false)
        }).catch((err) => {
            console.log(err);
            cogoToast.error("Internal Server Error")
            setLoading(false)
        })
    }, [])
    const clearState=()=>{
        setSelected({ display_name: "Select" })
        setSelectedTypes("select")
        setUrl("")
        setMessage("")
    }
    const setNotification = () => {
        let data = {
            "groups": selected.group_name,
            "header": message,
            "type": selectedType,
            "referenceId": Date.now(),
            "article_url": url
        }
        if (!selected.group_name || data.groups.toLowerCase() === "select") {
            cogoToast.error("Please Select a Group Name")
        } else if (data.header === "") {
            cogoToast.error("Please Input the flash heading")
        } else if (data.type.toLowerCase() === "select") {
            cogoToast.error("Please select Type")
        }
        // else if(data.article_url!=="" && !data.article_url.includes("analytics-qa.zuelligpharma.com"||"analytics.zuelligpharma.com")){
        //     cogoToast.error("Please Enter an article url from this site")
        // }
        else {
            axios.post("/notification/saveNotifications", data).then(() => {
                clearState()
                cogoToast.success("Notification has been sent Successfully")
            }).catch(err => cogoToast.error("Internal Server Error"))
        }
    };
    return (
        <div>
            {
                loading ? <div style={{ minHeight: "65vh", display: "flex", backgroundColor: "#fff", justifyContent: "center", alignItems: "center" }}>
                    <Spinner animation="border" role="status" />
                </div> :
                    <Row className="justify-content-between mt-5 mb-3" style={{margin:"10px"}}>
                    <Container className={selfStyle.notifycont}>
                        <p className="h1">
            <b>Notification Settings</b>
          </p>

                        {/* <h5 className={selfStyle.head}>Notification Settings</h5> */}

                        <Row className={selfStyle.notifycontainerdiv}>
                            <Col xs={12}>
                                <label htmlFor="type" className={`d-block mb-2 ${selfStyle.labelnotify}`}>
                                    Notification Message:
                                </label>
                                <textarea
                                    value={message}
                                    rows="3"
                                    cols="30"
                                    className={selfStyle.textarea}
                                    onChange={e => setMessage(e.target.value)}
                                />
                            </Col>
                        </Row>
                        <Row className={selfStyle.notifycontainerdiv}>
                            <Col xs={6}>
                                <DropDownBtn
                                    heading="Group Name:"
                                    selected={selected}
                                    data={data}
                                    onClick={(e) => setSelected(e)} />
                            </Col>
                        
                            <Col xs={6}>
                                <DropDownBtn
                                    heading="Type:"
                                    selected={selectedType}
                                    data={types}
                                    onClick={(e) => setSelectedTypes(e)} />
                            </Col>
                        </Row>
                        {selectedType === "Insights" && <Row className={selfStyle.notifycontainerdiv}><Col xs={12}>
                                <label htmlFor="type" className={`d-block mb-2 ${selfStyle.labelnotify}`}>
                                    Article Url:
                                </label>
                                <textarea
                                    value={url}
                                    rows="3"
                                    cols="30"
                                    className={selfStyle.textarea}
                                    onChange={e => setUrl(e.target.value)}
                                />
                            </Col></Row>}
                        <div
                            className={selfStyle.btnContainer}
                        >
                            <button className={`btn text-uppercase  ${selfStyle.notifysearchbtn}`} onClick={() => setNotification()}>
                                Save
                            </button>
                        </div>

                    </Container>
                    </Row>
            }
        </div>
    )
}

export default AdminNotification

const DropDownBtn = ({ selected, data, onClick, heading }) => (
    <>
        <span className={selfStyle.labelnotify}>{heading}</span>
        <InputGroup className={selfStyle.inputnotify}>
            <Dropdown className={selfStyle.dropdown}>
                <Dropdown.Toggle as={CustomDropdownToggle}>
                    {heading === "Type:" ? selected : selected.display_name}
                </Dropdown.Toggle>
                <Dropdown.Menu>
                    {data.map((item, key) => (
                        <Dropdown.Item
                            key={key}
                            style={{
                                whiteSpace: "normal",
                                wordBreak: "break-word",
                                display: "inline-block"
                            }}
                            onClick={() => onClick(item)}
                        >
                            {heading === "Type:" ? item : item.display_name}
                        </Dropdown.Item>
                    ))}
                </Dropdown.Menu>
            </Dropdown>
        </InputGroup>
    </>
)
