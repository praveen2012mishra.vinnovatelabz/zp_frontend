import React, { useState } from "react"
import { Table, Container, Row, Col } from "react-bootstrap"
import { PencilSquare, Trash } from "react-bootstrap-icons"
import styles from "./style.module.scss"
import SolutionAddEditModal from "./SolutionAddEditModal"
import SolutionInfoModal from "./SolutionInfoModal"
import axios from "../../../../axiosConfig"

// const _= require('underscore')
const AdminSolutionMgmtTab = ({
  // data,
  err,
  // getAllGroupMgmtData,
  deleteGroupMgmtData,
  // notification,
  // switched,
  // test,
}) => {
  let getData={}
  const [modalOpen, toggleModalOpen] = useState(false)
  // const [getData, setGetData] = useState({})
  const [loading, setLoading] = useState(true)
  const [fromEdit, setFromEdit] = useState(false)
  // const [update,setUpdate]= useState("")
  const [solutionModal, toggleSolutionModal] = useState(false)
  const [solutionName, setSolutionName] = useState()
  const modalOnClose = () => {
    toggleModalOpen(false)
    toggleSolutionModal(false)
  }

   
  const modalOnSaveAndExit = async(data) => {
    // do something to save the data
    data.solution = data.type;
    // const updatedData=
    await data
     
    // const response= 
    await axios.post(`/roles/updateSolutionRoles/`,data)
     
    toggleModalOpen(false)
    toggleSolutionModal(false)
  }

  // delete handler
  const onDelete = (id, 
    // username, 
    groupName) => {
    setLoading(true)
    // group name and username is required for saving in activity log
    // currently username is hardcoded
    // TODO: update it to use current username in future
    deleteGroupMgmtData(id, "CPhoa", groupName)
  }

  // handle error
  if (err) {
    if (loading) setLoading(false)
  }

  //handle toggle
  // const handleStatus = (mode, name, key, button) => {
  //   setLoading(true)
  //   switched(button, name, !mode, key)
  // }

  return (
    <Container fluid>
      {modalOpen ? (
        <SolutionAddEditModal
          open={modalOpen}
          onClose={modalOnClose}
          onSaveAndExit={modalOnSaveAndExit}
          fromEdit={fromEdit}
          getData={getData}
          solutionName={solutionName}
        />
      ) : (
          ""
        )}
        {solutionModal ? (
          <SolutionInfoModal 
          open={solutionModal}
          onClose={modalOnClose}
          onSaveAndExit={modalOnSaveAndExit}
          fromEdit={fromEdit}
          getData={getData}
          />
        ) : (
          ""
        )}

      <Row className="justify-content-between mt-5 mb-3">
        <Col>
          <p className="h1">
            <b>Solutions</b>
          </p>
        </Col>
        <Col>
          <button
            className={`btn rounded-pill ${styles.btnRight}`}
            tabIndex={0}
            onClick={() => {
              toggleSolutionModal(true)
              setFromEdit(true)
            }}
          >
            <span>EDIT SOLUTION</span>
          </button>
        </Col>
      </Row>

        <Table className={styles.table}>
          <thead>
            <tr>
              <th>Solutions Name</th>
              <th>Edit</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
                <tr>
                  <td>Insider</td>
                  <td>
                    {/* edit button ################################################################ */}
                    <div className="d-flex justify-content-center align-items-center">
                      <span
                        className={styles.spnBtn}
                        role="button"
                        tabIndex={0}
                        onClick={() => {
                          toggleModalOpen(true)
                          setFromEdit(true)
                          setSolutionName("Insider")
                        }}
                        onKeyDown={()=>{
                          setSolutionName("Insider")
                        }}
                      >
                        <PencilSquare />
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <Trash
                        className={styles.cursorPointer}
                        onClick={() => onDelete( "username")}
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Commercial Excellence</td>
                  <td>
                    {/* edit button ################################################################ */}
                    <div className="d-flex justify-content-center align-items-center">
                      <span
                        className={styles.spnBtn}
                        role="button"
                        tabIndex={0}
                        onClick={() => {
                          toggleModalOpen(true)
                          setFromEdit(true)
                          setSolutionName("Commercial Excellence")
                        }}
                        onKeyDown={()=>{
                          setSolutionName("Commercial Excellence")
                        }}
                      >
                        <PencilSquare />
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <Trash
                        className={styles.cursorPointer}
                        onClick={() => onDelete( "username")}
                      />
                    </div>
                  </td>
                </tr>
                <tr>
                  <td>Sypply Chain</td>
                  <td>
                    {/* edit button ################################################################ */}
                    <div style={{outline: "none"}} className="d-flex justify-content-center align-items-center">
                      <span
                        className={styles.spnBtn}
                        role="button"
                        tabIndex={0}
                        onClick={() => {
                          toggleModalOpen(true)
                          setFromEdit(true)
                          setSolutionName("Supply Chain")
                        }}
                        onKeyDown={()=>{
                          setSolutionName("Supply Chain")
                        }}
                      >
                        <PencilSquare />
                      </span>
                    </div>
                  </td>
                  <td>
                    <div className="d-flex justify-content-center align-items-center">
                      <Trash
                        className={styles.cursorPointer}
                        onClick={() => onDelete( "username")}
                      />
                    </div>
                  </td>
                </tr>
          </tbody>
        </Table>
    </Container>
  )
}

export default AdminSolutionMgmtTab
