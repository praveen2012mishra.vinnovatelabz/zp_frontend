import React, { useState } from "react"
import { Dash } from "react-bootstrap-icons"
import {
  Row,
  Col,
  Dropdown,
} from "react-bootstrap"

import styles from "./style.module.scss"

const InputGroup = ({ data, onModuleChange, onPermissionChange, onDelete, moduleOption=[],
  permissionOption=[] }) => {
  const [module, setModule] = useState("")
  const [permission, setpermission] = useState("")
  React.useEffect(()=>{
    setModule(data.module)
    setpermission(data.permission)
  },[data])
  const onTypeSelect = newType => {
    if (newType !== module) {
      setModule(newType)
      onModuleChange(newType)
    }
  }

  const onPermissionSelect = newType => {
    if (newType !== permission) 
    {setpermission(newType)
      onPermissionChange(newType)
    }
  }
  
  const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
    <button
      ref={ref}
      onClick={onClick}
      className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
    >
      {children}
    </button>
  ))
  
  return (
    <Row className={`py-2 ${styles.borderBottom}`}>
    <Col className={styles.column}>
    <h6>Module</h6>
    <Dropdown
      className={styles.dropdown}
      onSelect={(_, e) =>
        onTypeSelect(e.target.innerText.toLowerCase())
      }
    >
      <Dropdown.Toggle as={CustomDropdownToggle}>
        {module}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {moduleOption.map(type => (
          <Dropdown.Item
            key={type}
            href="#"
            className="text-uppercase"
          >
            {type}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
  </Col>
      <Col className={styles.column}>
        <h6>Permission</h6>
        <Dropdown
      className={styles.dropdown}
      onSelect={(_, e) =>
        onPermissionSelect(e.target.innerText.toLowerCase())
      }
    >
      <Dropdown.Toggle as={CustomDropdownToggle}>
        {permission}
      </Dropdown.Toggle>
      <Dropdown.Menu>
        {permissionOption.map(type => (
          <Dropdown.Item
            key={type}
            href="#"
            className="text-uppercase"
          >
            {type}
          </Dropdown.Item>
        ))}
      </Dropdown.Menu>
    </Dropdown>
      </Col>
      <Col sm={1}>
        <button
          className={`btn rounded-circle ${styles.deleteBtn}`}
          onClick={onDelete}
        >
          <Dash />
        </button>
      </Col>
    </Row>
  )
}

export default InputGroup
