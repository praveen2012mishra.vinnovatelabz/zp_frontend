import React, { useEffect, useState } from "react"
import {
  Modal,
  Row,
  Col,
  Container,
  Accordion,
  Dropdown,
} from "react-bootstrap"
import { X } from "react-bootstrap-icons"
// import cloneDeep from "lodash/cloneDeep"

import styles from "./style.module.scss"
import AccordionHeader from "./AccordianHeader"
import AccordionBody from "./AccordionBody"

// css for custom scrollbar
import "simplebar/dist/simplebar.min.css"
import Simplebar from "simplebar-react"
import axios from "../../../../../axiosConfig"
// import { isEmpty } from "lodash"
// import { compose } from "underscore"
const _ =require ('underscore')
// const seedData = [
//   {
//     name: "Product Manager",
//     items: [
//       { name: "Permissions", elements: [] },
//     ],
//   },
//   {
//     name: "VP",
//     items: [{ name: "Permissions", elements: [] }],
//   },
//   {
//     name: "Solution Admin",
//     items: [{ name: "Permissions", elements: [] }],
//   },
//   {
//     name: "Basic User",
//     items: [{ name: "Permissions", elements: [] }],
//   },
// ]
// array of availabe types
const types = ["Supply Chain", "Insider", "Commercial Excellence"]

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

const SolutionAddEditModal = ({ fromEdit, open, onClose, onSaveAndExit, solutionName }) => {
  const [activeAccordion, toggleActiveAccordion] = useState("0") // activeAccordion takes index as string
  // const [data, setData] = useState(seedData)
  const [roleName, setRoleName] = useState("")
  const [type, setType] = useState(solutionName) // default type to SUPPLY CHAIN
  const [roleModal, setRoleModal] = useState(false)

  const [modules, setModuleOption] = useState([])
  const [permissionOption, setPermissionOption] = useState([])
  const [roles, setRoles] = useState([])
  const [rolesOptions, setRolesOptions] = useState([])
  const [removeSelect, setRemoveSelect] = useState("Select")
  
  const onDataChange = (value, inputIdx, field, idx, type) => {
    let newData = [...roles];
    newData[idx].permissions[inputIdx][field] = value;
    setRoles(newData)

  }
const [solName,setSolName]= useState(solutionName)
  useEffect(() => {
    fetchRolesBySolution()
// eslint-disable-next-line react-hooks/exhaustive-deps
  }, [solName])

  const fetchRolesBySolution = async () => {
    if (fromEdit) {
      const response = await axios.get(`/roles/getRolesBySolution/${solName}`)
      setModuleOption(response.data.data[0].modules);
      setPermissionOption(response.data.data[0].options)
      setRoles(response.data.data[0]? response.data.data[0].roles: [])
      setRolesOptions(response.data.data[0]? response.data.data[0].roles: [])
    }
  }


  const onAddField = (roleName, mainIndex) => {
    let newData = [...roles]
     
    newData[mainIndex].permissions.push({module: "",permission: ""})
    // 
    setRoles(newData)
  }

  const onDeleteField = (deleteIdx, idx) => {
    let newData =[...roles]
      newData[idx].permissions.splice(deleteIdx, 1);
      setRoles(newData)
  }
  const capitalize= (str)=>{
    return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
  }

  const onTypeSelect = newType => {
    newType=newType.split(' ').map(capitalize).join(' ');
    if (newType !== type) setType(newType)
  }
  const onRemoveRoleSelect = selectRole => {
    setRemoveSelect(selectRole)
    let newData = [...rolesOptions];
    let filterData=newData.filter(item=> item.type.toLowerCase() !== selectRole.toLowerCase());
    setRoles(filterData)
  }
  const resetInitial = () => {
    setType("Supply Chain")
    // setData(seedData)
  }
  const onClickSaveAndExit = () => {
    onSaveAndExit({
      type,
      modules,
      roles,
    })
    // resetInitial()
  }
  const onClickSaveRoleAndExit = () => {
    if(roleName!==""){
      let newData = roles;
    newData.push({
      "type": roleName,
      "permissions": [
        {
          "module": "",
          "permission": ""
        }
      ]
    })
    setRoles(newData);
    }
    _.debounce(onClickSaveAndExit(),3000)
  }

  const onCloseModal = () => {
    setRoleModal(false)
  }

  const onAccordianToggleClick = key => {
    if (key !== activeAccordion) toggleActiveAccordion(key)
  }

  return (
    <Modal size="lg" centered show={open} dialogClassName={styles.modal}>
      {roleModal !== true ?
        <Modal.Header className={styles.header}>
          <Container fluid>
            <Row className="justify-content-between align-items-center w-100">
              <Col sm={6}>
                <h2>Module Information</h2>
              </Col>
              <Col sm={6} className="pr-0">
                <Row>
                  <button
                    className={`btn rounded-pill d-block ml-auto ${styles.btn}`}
                    onClick={() => onClickSaveAndExit()}
                  >
                    SAVE & EXIT
                </button>
                  <button
                    className={`btn ${styles.closeBtn}`}
                    onClick={resetInitial && onClose}
                  >
                    <X />
                  </button>
                </Row>
              </Col>
            </Row>
          </Container>
        </Modal.Header>
        :
        <Modal.Header className={styles.header}>
          <Container fluid>
            <Row className="justify-content-between align-items-center w-100">
              <Col sm={6}>
                <h2>Role Information</h2>
              </Col>
              <Col sm={6} className="pr-0">
                <Row>
                  <button
                    className={`btn rounded-pill d-block ml-auto ${styles.btn}`}
                    onClick={onClickSaveRoleAndExit}
                  >
                    SAVE & EXIT
                  </button>
                  <button
                    className={`btn ${styles.closeBtn}`}
                    onClick={resetInitial && onClose && onCloseModal}
                  >
                    <X />
                  </button>
                </Row>
              </Col>
            </Row>
          </Container>
        </Modal.Header>
      }
      {roleModal !== true ?
        <Simplebar style={{ maxHeight: "80%" }} autoHide="false">
          <Modal.Body className={styles.modal__body}>
            <Container className={`mb-3 ${styles.px35}`}>
              <Row>
                <Col>
                  <p
                    className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                  >
                    Solution Name
            </p>
                  <Dropdown
                    className={styles.dropdown}
                    onSelect={(_, e) =>
                      onTypeSelect(e.target.innerText)
                    }
                  >
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                      {type}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {types.map(type => (
                        <Dropdown.Item
                          key={type}
                          href="#"
                          onClick={()=>{
                            setSolName(type)
                          }}
                          className="text-uppercase"
                          disabled={solName === type}
                        >
                          {type}
                        </Dropdown.Item>
                      ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </Col>
                <Col>
                  <button
                    className={`btn rounded-pill ${styles.bttnRight}`}
                    onClick={() => {
                      setRoleModal(true)
                    }}
                  >
                    <span>ADD/REMOVE ROLE</span>
                  </button>
                </Col>
              </Row>
            </Container>

            <div>
              <Accordion defaultActiveKey="0" activeKey={activeAccordion}>
                {roles.map((item, idx) => (
                  <div key={idx}>
                    <AccordionHeader
                      eventKey={`${idx}`}
                      onClick={onAccordianToggleClick}
                    >
                      {item.type}
                    </AccordionHeader>
                    <Accordion.Collapse eventKey={`${idx}`}>
                      <>
                        <AccordionBody
                          title={"Permission"}
                          data={roles[idx].permissions}
                          moduleOption={modules}
                          permissionOption={permissionOption}
                          onDataChange={(value, inputIdx, field) =>
                            onDataChange(
                              value,
                              inputIdx,
                              field,
                              idx,
                              item.type
                            )
                          }
                          onAddField={() => onAddField(item.type, idx)}
                          onDeleteField={deleteData =>onDeleteField(deleteData, idx)
                          }
                        />
                      </>
                    </Accordion.Collapse>
                  </div>
                ))}
              </Accordion>
            </div>
          </Modal.Body>
        </Simplebar>
        : <div>
          <Modal.Body className={styles.modal__body}>
            <Container className={`mb-3 ${styles.px35}`}>
              <Row>
                <Col>
                  <p
                    className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                  >
                    Add Role
                </p>
                  <input
                    className={[styles.input].join(" ")}
                    placeholder="Product Manager"
                    value={roleName}
                    onChange={e => setRoleName(e.target.value)}
                  />
                </Col>
                <Col>
                  <p
                    className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
                  >
                    Remove Role
                  </p>
                  <Dropdown
                    className={styles.dropdownRoles}
                    onSelect={(_, e) =>
                      onRemoveRoleSelect(e.target.innerText.toLowerCase())
                    }
                  >
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                      {removeSelect}
                    </Dropdown.Toggle>
                    <Dropdown.Menu className={styles.drpMenu}>
                      {rolesOptions.map((type, i) => (
                        <Dropdown.Item
                          key={i}
                          href="#"
                          className="text-uppercase"
                          value={i}
                        >
                          {type.type}
                        </Dropdown.Item>
                      ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </Col>
              </Row>
            </Container>
          </Modal.Body>
        </div>
      }
    </Modal>
  )
}

export default SolutionAddEditModal
