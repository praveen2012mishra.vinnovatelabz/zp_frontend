import React, { useEffect, useState } from "react"
import {
  Modal,
  Row,
  Col,
  Container,
  Accordion,
  Dropdown,
} from "react-bootstrap"
import { X } from "react-bootstrap-icons"
import cloneDeep from "lodash/cloneDeep"

import styles from "./style.module.scss"
// import AccordionHeader from "./AccordianHeader"
import AccordionBody from "./AccordionBody"

// css for custom scrollbar
import "simplebar/dist/simplebar.min.css"
import Simplebar from "simplebar-react"
import axios from "../../../../../axiosConfig"

// const seedData = [
//   {
//     name: "Product Manager",
//     items: [
//       { name: "Module Information", elements: [] },
//     ],
//   },
//   {
//     name: "VP",
//     items: [],
//   },
//   {
//     name: "Solution Admin",
//     items: [],
//   },
//   {
//     name: "Basic User",
//     items: [],
//   },
// ]
// array of availabe types
const types = ["Insider",
  "Commercial Excellence",
  "Supply Chain",
  "Business Intelligence",
  "Internal"
  ]
  const options = ["Read",
  "Write",
  "Read and Write",
  "approve"
  ]
const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

const SolutionInfoModal = ({ fromEdit, open, onClose, onSaveAndExit }) => {
  let activeAccordion = "0"
  // const [activeAccordion, toggleActiveAccordion] = useState("0") // activeAccordion takes index as string
  // const [data, setData] = useState(seedData)
  // const [groupName, setGroupName] = useState("")
  const [type, setType] = useState("Insider") // default type to SUPPLY CHAIN

  const [modules, setModuleOption] = useState([])
  // const [permissionOption, setPermissionOption] = useState([])
  const [roles, setRoles] = useState([])
  const onDataChange = (value, inputIdx, field) => {
    let newData = [...modules];
    newData[inputIdx] =value
     
    setModuleOption(newData)
  }

  useEffect(() => {
    fetchRolesBySolution()
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [type])

  const fetchRolesBySolution = async () =>{
    if (fromEdit){
      const response= await axios.get(`/roles/getRolesBySolution/${type}`)
      setModuleOption(response.data.data[0]?response.data.data[0].modules:[]);
      // setPermissionOption(response.data.data[0] ? response.data.data[0].options : [])
      setRoles(response.data.data[0]? response.data.data[0].roles: [])
      
    }
  }

  const onAddField = (key, key2) => {
    let newData = cloneDeep(modules);
    newData.push("")
     
    setModuleOption(newData)
  }

  const onDeleteField = (idx, key) => {
    let newData = cloneDeep(modules);
    newData.splice(idx,1)
     
    setModuleOption(newData)
  }

  const onTypeSelect = newType => {
    if (newType !== type) setType(newType)
  }

  const resetInitial = () => {
    // setGroupName("")
    setType("Supply Chain")
    // setData(seedData)
  }
  const onClickSaveAndExit = () => {
    onSaveAndExit({
      type,
      modules,
      options,
      roles,
    })
    //resetInitial()
  }
  // const onCloseModal = () => {}

  // const onAccordianToggleClick = key => {
  //   if (key !== activeAccordion) toggleActiveAccordion(key)
  // }

  return (
    <Modal size="lg" centered show={open} dialogClassName={styles.modal}>
      <Modal.Header className={styles.header}>
        <Container fluid>
          <Row className="justify-content-between align-items-center w-100">
            <Col sm={6}>
              <h2>Solution Information</h2>
            </Col>
            <Col sm={6} className="pr-0">
              <Row>
                <button
                  className={`btn rounded-pill d-block ml-auto ${styles.btn}`}
                  onClick={onClickSaveAndExit}
                >
                  SAVE & EXIT
                </button>
                <button
                  className={`btn ${styles.closeBtn}`}
                  onClick={resetInitial && onClose}
                >
                  <X />
                </button>
              </Row>
            </Col>
          </Row>
        </Container>
      </Modal.Header>
    
      <Simplebar style={{ maxHeight: "80%" }} autoHide="false">
        <Modal.Body className={styles.modal__body}>
          <Container className={`mb-3 ${styles.px35}`}>
            <Row>
            <Col>
            <p
              className={`text-capitalize text-bold font-weight-bold mb-1 ${styles.colorDarkBlue}`}
            >
              Solution Name
            </p>
            <Dropdown
              className={styles.dropdown}
              onSelect={(_, e) =>
                onTypeSelect(e.target.innerText)
              }
            >
              <Dropdown.Toggle as={CustomDropdownToggle}>
                {type}
              </Dropdown.Toggle>
              <Dropdown.Menu>
                {types.map(type => (
                  <Dropdown.Item
                    key={type}
                    href="#"
                  >
                    {type}
                  </Dropdown.Item>
                ))}
              </Dropdown.Menu>
            </Dropdown>
          </Col>
        </Row>
      </Container>

          <div>
            <Accordion defaultActiveKey="0" activeKey={activeAccordion}>
                <div key="0">
                  <Accordion.Collapse eventKey="0">
                    <>
                        <AccordionBody
                          key={1}
                          title={"Module Information"}
                          data={modules}
                          onDataChange={(value, inputIdx, field) =>
                            onDataChange(
                              value,
                              inputIdx,
                              field,
                            )
                          }
                          onAddField={() => onAddField("item.name", "Module Information")}
                          onDeleteField={idx =>
                            onDeleteField(idx, "Module Information")
                          }
                        />
                    </>
                  </Accordion.Collapse>
                </div>
              
            </Accordion>
          </div>
        </Modal.Body>
      </Simplebar>
    </Modal>
  )
}

export default SolutionInfoModal
