import React from "react"
import { Container, Row, Col } from "react-bootstrap"
import { Plus } from "react-bootstrap-icons"

import styles from "./style.module.scss"
import InputGroup from "./inputGroup"

const AccordionBody = ({
  title,
  data=[],
  onDataChange,
  onAddField,
  onDeleteField,
}) => {
  
  const onModuleChange = (value, idx) => {
    onDataChange(value, idx, "module")
  }

  

  return (
    <Container
      className={[styles.accordion__body, styles.px48, "py-2"].join(" ")}
    >
      <Row className={`py-3 ${styles.borderBottom}`}>
        <Col sm={11}>
          <p className={`text-bold font-weight-bold h6`}>{title}</p>
        </Col>
        <Col sm={1}>
          <button
            className={`btn rounded-circle ${styles.addBtn}`}
            onClick={onAddField}
          >
            <Plus />
          </button>
        </Col>
      </Row>
      {data.map((d, idx) => (
        <InputGroup
          key={idx}
          data={d}
          onModuleChange={e => onModuleChange(e,idx)}
          onDelete={() => onDeleteField(idx)}
        />
      ))}
    </Container>
  )
}

export default AccordionBody
