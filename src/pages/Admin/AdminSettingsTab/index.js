import React, { useEffect } from "react"
import { Container, Row, Col } from "react-bootstrap"
import { createApolloFetch } from "apollo-fetch"
import cogo from "cogo-toast"
import styles from "./style.module.scss"
import ToggleButton from "../../../components/ui/ToggleButton"
import { connect } from "react-redux"
import LoadingOverlay from "react-loading-overlay"
import {
  setAdminSettingsMessage,
  getAdminSettingsAction,
} from "../../../services/Redux/actions/adminSettingsAction"
import Axios from "../../../../axiosConfig"
import { AxisUtils } from "react-vis"

const uri = "https://ezpz.zuelligpharma.com/graphql"
const query = `
query MyQuery {
    posts {
        nodes {
            uri
            title(format: RENDERED)
            excerpt(format: RENDERED)
            id
            postId
            status
            date
            dateGmt
            databaseId
            authorId
            categories {
                nodes {
                    id
                    name
                    parentId
                    uri
                    categoryId
                }
            }
            content(format: RENDERED)
            tags {
                nodes {
                    id
                    tagId
                    name
                }
            }
            authorDatabaseId
            slug
            author {
                node {
                    id
                    name
                    nicename
                    nickname
                    userId
                    username
                    slug
                    email
                    firstName
                    lastName
                    locale
                }
            }
            featuredImage {
                    node {
                        mediaItemUrl
                        mediaItemId
                        mimeType
                        mediaType
                        sizes
                        slug
                        sourceUrl(size: LARGE)
                        title
                        uri
                    }
                }
        }
    }
}`
const apolloFetch = createApolloFetch({ uri })
const mapStateToProps = state => ({
  data: state.adminSettings.settingsData,
  err: state.adminSettings.err,
  notification: state.notification,
  messageData: state.adminSettings.getMessages,
})

const mapDispatchToProps = {
  setAdminSettingsMessage,
  getAdminSettingsAction,
}

const AdminSettingsTab = ({
  data,
  err,
  setAdminSettingsMessage,
  notification,
  getAdminSettingsAction,
  messageData,
}) => {
  const [status, setStatus] = React.useState(false)
  const [message, setMessage] = React.useState("")
  const [loading, setLoading] = React.useState(false)
  const [dataSet, setdataSet] = React.useState([])
  // const [displayMessage, setDisplayMessage] = React.useState("")

  const getInsightData = () => {
    apolloFetch({ query })
      .then(res => {
        setdataSet(res.data.posts.nodes)
      })
      .catch(err => {
        console.log(err)
      })
  }

  useEffect(() => {
    const settingAction = async () => await getAdminSettingsAction()
    settingAction()
    getInsightData()

    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])

  useEffect(() => {
    if (messageData.data?.maintenance_mode_status) {
      setMessage(messageData.data.maintenance_message)
      setStatus(true)
    }
  }, [messageData])

  console.log(messageData)
  const submitMessage = async e => {
    e.preventDefault()
    setLoading(true)
    let obj = {}
    if (!status) {
      obj = {
        maintenance_message: message,
        maintenance_mode_status: status,
      }
    } else {
      obj = {
        message,
        status,
      }
    }

    await setAdminSettingsMessage(obj)
  }

  const handleSync = async () => {
    setLoading(true)
    try {
      let res = await Axios.post("/insights/createArticles", {
        data: dataSet
      })
      if (res.status) {
        cogo.success("Successfully synced.")
      }
      setLoading(false)
    } catch (err) {
      setLoading(false)
      cogo.error("Something went wrong!")
    }
  }

  // handle notifications
  if (notification.notify) {
    if (notification.type === "error") {
      cogo.error(notification.message)
      if (loading) setLoading(false)
    } else if (notification.type === "success") {
      cogo.success(notification.message)
      if (loading) setLoading(false)
    }

    // prevent showing notification in future render without any
    // actions
    notification.notify = false
  }
  if (err) {
    if (loading) setLoading(false)
  }

  return (
    <LoadingOverlay active={loading} spinner>
      <Container fluid className={styles.settings}>
        <p className="h5">
          <b>Maintenance Alert Management</b>
        </p>
        <Row className="mt-5">
          <Col>
            <label htmlFor="type" className="d-block mb-2">
              Maintenance Message:
            </label>
            <textarea style={{width:'-webkit-fill-available',width:'-moz-available'}}
              rows="5"
              cols="50"
              className={styles.textarea}
              disabled={!status}
              value={message}
              onChange={e => setMessage(e.target.value)}
            />
          </Col>
          <Col>
            <label htmlFor="type" className="d-block mb-2">
              Maintenance Mode Message:
            </label>
            <ToggleButton
              checked={status}
              onChange={async () => {
                await setStatus(!status)
                status && setMessage("")
              }}
            />
          </Col>

          <Col xs={12}>
            <button
              className={`btn mt-2 ${styles.btn}`}
              onClick={submitMessage}
            >
              SAVE
            </button>
          </Col>
        </Row>
<Row>
  <Col xs={12} md={6} lg={6}>
        <p className="h5 mt-5">
          <b>Maintenance Alert Management</b>
        </p>
        <Row className="mt-5 align-items-end">
          <Col xs={12} md={12} lg={12}>
            <label htmlFor="type" className="d-block mb-2">
              Password:
            </label>
            <input style={{marginBottom:'1rem'}}
              className={styles.input}
              value={
                messageData.data
                  ? messageData.data.tableau_service_password
                  : null
              }
            />
          </Col>
          <Col xs={2}>
            <button className={`btn ${styles.btn}`}>SAVE</button>
          </Col>
          </Row>
          </Col>
          <Col xs={12} md={6} lg={6}>
          <p className="h5 mt-5">
          <b>Update Insights</b>
        </p>
          <Col className='mt-5 align-items-end p-0'>
            <button onClick={handleSync} className={`btn ${styles.btn}`}>
              SYNC
            </button>
          </Col>
        </Col>
        
        </Row>
      </Container>
    </LoadingOverlay>
  )
}

export default connect(mapStateToProps, mapDispatchToProps)(AdminSettingsTab)
