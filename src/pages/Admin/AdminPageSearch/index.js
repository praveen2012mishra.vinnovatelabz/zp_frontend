import React from "react"
import {
  Container,
  Row,
  Col,
  InputGroup,
  Dropdown,
  FormControl,
} from "react-bootstrap"
import { connect } from "react-redux"
import { Search } from "react-bootstrap-icons"
import styles from "./style.module.scss"
import { search } from "../../../services/Redux/actions/admin"
import { setTypeCountry } from "../../../services/Redux/actions/admin"
import {group} from "../../../services/Redux/actions/adminSession"
import axios from "../../../../axiosConfig"
// import { debounce } from "lodash"
let _ = require('underscore')
// array of availabe types
const types = ["external", "internal"]
// array of available countries
const countries = [
  "Regional",
  "Brunei",
  "Cambodia",
  "Hong Kong",
  "Indonesia",
  "Korea",
  "Macau",
  "Malaysia",
  "Myanmar",
  "Philippines",
  "Singapore",
  "Taiwan",
  "Thailand",
  "Vietnam",
  "Other",
]

const CustomDropdownToggle = React.forwardRef(({ children, onClick }, ref) => (
  <button
    ref={ref}
    onClick={onClick}
    className={`btn btn-outline-primary text-uppercase ${styles.dropdownBtn}`}
  >
    {children}
  </button>
))

const AdminPageSearch = ({
  updateData,
  setTypeCountry,
  activeTab = null,
  setGroupName,
  groupName
  // getAllGroupMgmtData
}) => {
  // let data= [];
  const getD = async () => {
    if (data.length === 0) {
      let response = await axios.get("/group/getAllGroups")
      response.data && setData(response.data)
      response.data && setGroupName(response.data[0].group_name)
    }
  }
  React.useEffect(() => {
    getD()
    groupName && setSelected(groupName)
    // console.log(groupName,"state");
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [groupName])
  const [typeState, setTypeState] = React.useState(types[0]);
  const [countryState, setCountry] = React.useState("Singapore");
  const [selected, setSelected] = React.useState("")
  const [data, setData] = React.useState([])
  // const [groupName, setGroupName]= React.useState("")

  const getTypeCountry = () => {
    let typeCountry = { typeState: typeState, countryState: countryState };
    setTypeCountry(Object.assign({}, typeCountry));
  }

  const searchFilter = text => {
    if (text && text.length >= 3) {
      let items = _.filter(data, (d) => d.group_name ? d.group_name.toLowerCase().includes(text.toLowerCase()) : null)
      updateData(items)
    }
    else {
      updateData(data);
    }
  }
  const search = (text) => _.debounce(searchFilter(text), 500)
  return (
    <Container fluid>
      <Row className="justify-content-between">
        <Col>
          {
            (activeTab && (activeTab === 2 || activeTab === 3)) ? <Row className="align-items-end">
              <Col>
                <label className={styles.label} htmlFor="type">
                  Group Name:
                </label>
                <InputGroup>
                  <Dropdown className={styles.dropdown}>
                    <Dropdown.Toggle as={CustomDropdownToggle}>
                      {selected}
                    </Dropdown.Toggle>
                    <Dropdown.Menu>
                      {data.map(item => (
                        <Dropdown.Item
                          key={item._id}
                          style={{
                            whiteSpace: "normal",
                            wordBreak: "break-word",
                            display: "inline-block" 
                          }}
                          onClick={() => setGroupName(item.group_name)}
                        >
                          {item.group_name}
                        </Dropdown.Item>
                      ))}
                    </Dropdown.Menu>
                  </Dropdown>
                </InputGroup>
              </Col>
            </Row> : (
                <Row className="align-items-end">
                  <Col>
                    <label className={styles.label} htmlFor="type">
                      Type:
              </label>
                    <InputGroup>
                      <Dropdown className={styles.dropdown}>
                        <Dropdown.Toggle as={CustomDropdownToggle}>
                          {typeState}
                        </Dropdown.Toggle>
                        <Dropdown.Menu>
                          {types.map(type => (
                            <Dropdown.Item
                              key={type}
                              // href="#"
                              className="text-uppercase"
                              onClick={() => setTypeState(type)}
                            >
                              {type}
                            </Dropdown.Item>
                          ))}
                        </Dropdown.Menu>
                      </Dropdown>
                    </InputGroup>
                  </Col>
                  <Col>
                    <label className={styles.label} htmlFor="type">
                      Countries:
              </label>
                    <InputGroup>
                      <Dropdown className={styles.dropdown}>
                        <Dropdown.Toggle as={CustomDropdownToggle}>
                          {countryState}
                        </Dropdown.Toggle>
                        <Dropdown.Menu className={styles.countryDropdown}>
                          {countries.map(country => (
                            <Dropdown.Item
                              key={country}
                              // href="#"
                              className="text-uppercase"
                              onClick={() => setCountry(country)}
                            >
                              {country}
                            </Dropdown.Item>
                          ))}
                        </Dropdown.Menu>
                      </Dropdown>
                    </InputGroup>
                  </Col>
                  <Col>
                    <button className={`btn text-uppercase ${styles.searchBtn}`} onClick={() => getTypeCountry()}>
                      search
              </button>
                  </Col>
                </Row>
              )
          }
        </Col>
        <Col className="d-flex align-items-end">
          <InputGroup>
            <FormControl
            placeholder="KEYWORD SEARCH"
              className={styles.keywordSearch}
              onChange={e => {
                search(e.target.value)
              }}
            />
            <InputGroup.Append className={styles.keywordSearchIcon}>
              <InputGroup.Text>
                <Search />
              </InputGroup.Text>
            </InputGroup.Append>
          </InputGroup>
        </Col>
      </Row>
    </Container>
  )
}

const mapStateToProps = state => ({
  groupName: state.adminSession.grpName
})

const mapDispatchToProps = dispatch => ({
  updateData: (data) => dispatch(search(data)),
  setTypeCountry: (data) => dispatch(setTypeCountry(data)),
  setGroupName: (name) => dispatch(group(name))
})

// export default AdminGroupMgmtTab
export default connect(mapStateToProps, mapDispatchToProps)(AdminPageSearch)
