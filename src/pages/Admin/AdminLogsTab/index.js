import React from "react"
import { Container, Row} from "react-bootstrap"
// import { ArrowClockwise } from "react-bootstrap-icons"
import AdminLogsButtonTabs from "../AdminLogsButtonsTab"
import styles from "./style.module.scss"
// import TableauReport from "tableau-react"
import loadable from '@loadable/component'
const TableauReport = loadable(() => import('tableau-react'))
// import {reliseViz} from "./tableau-api"
// import tableau from "tableau-api";
const AdminLogsTab = ({ data }) => {
  const buttonItems = ["Activity Logs", "System Logs", "Database Logs"]
  const [activeTab, setActiveTab] = React.useState(0)
  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
    }
  }
  const tableauLinks = {
    activity:
      "https://zplive.zuelligpharma.com/views/AnalyticsPortalUserStats/UserDashboard?:showAppBanner=false&:display_count=n&:showVizHome=n&:origin=viz_share_link",
    system:
      "https://zplive.zuelligpharma.com/views/MongoDBLogs/SystemLogs?iframeSizedToWindow=true&%3Aembed=y&%3AshowAppBanner=false&%3Adisplay_count=no&%3AshowVizHome=no",
    database:
      "https://zplive.zuelligpharma.com/views/MongoDBLogs/DatabaseLogs?iframeSizedToWindow=true&%3Aembed=y&%3AshowAppBanner=false&%3Adisplay_count=no&%3AshowVizHome=no",
  }
  const options = {
    height: "100vh",
    width: "100%",
    hideTabs: true,
    // marginTop:20
  }

  return (
    <Container fluid>
      <Row className={`justify-content-between mt-5 ${styles.tabRow}`}>
        <AdminLogsButtonTabs
          buttonItems={buttonItems}
          onTabChange={onTabChange}
          activeTab={activeTab}
        />
      </Row>
      {activeTab === 2 ? (
        <Container fluid={true} style={{ marginTop: 20 }}>
          {/* <iframe src={tableauLinks.database} className={styles.iframe} width="100%"/> */}
          <TableauReport url={tableauLinks.database} options={options} />
          {/* {reliseViz(tableauLinks.database)} */}
        </Container>
      ) : activeTab === 1 ? (
        <Container fluid={true}>
          <iframe
            src={tableauLinks.system}
            className={styles.iframe}
            width="100%"
            title="tableauLinksSystem"
          />
        </Container>
      ) : activeTab === 0 ? (
        <Container fluid={true}>
          <iframe
            src={tableauLinks.activity}
            className={styles.iframe}
            width="100%"
            title="tableauLinksActivity"
          />
        </Container>
      ) : null}
    </Container>
  )
}

export default AdminLogsTab

