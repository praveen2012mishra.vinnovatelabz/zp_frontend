import React, { useState, useEffect } from "react"
import DataManagement from "../../../../../assets/images/Asset 15.png"
import DataFeed from "../../../../../assets/images/Asset 17.png"
import DSTIcon from "../../../../../assets/images/Asset 18.png"
import CustomizeReport from "../../../../../assets/images/Asset 19.png"
import RPAIcon from "../../../../../assets/images/Asset 20.png"
import Styles from "./style.module.scss"
import { Container } from "reactstrap"
import { SubHeader } from "../../../../../components/ui/subHeader"
//import styles from "react-loading-overlay/lib/styles"
//import ButtonItem from "../../../../../components/ui/Button/Button"
import Model from "../../../../../components/Header/contactModal"
import DownButton from "../../../../../components/ui/Button/DownButton"
import cogoToast from "cogo-toast"
import { basePath } from "../../../../../components/basePath"
import { getGroupOptionsbyGroupName } from "../../../../../services/Api/authGroups"
import Body from "../../../../../components/ui/ReportView/Body"
import ButtonTab from "../../../../../components/ui/ButtonsTab"
import Tab from "../../../../../components/Tab"
import { Spinner } from "react-bootstrap"
import Cookie from "js-cookie"
import BIs from "../../../../../assets/images/BI.png"
// import { group } from "../../../../../components/getGroupName"

// let group = "Tableau Regional - Data Analytics - Demo";

const circleData = [
  {
    head: "Data Management and Integration",
    icon: DataManagement,
    description: `Store, prepare and join multiple internal and external sources of data into one consolidated data source.`,
    urlItems: false,
  },
  {
    head: "Robotic Process Automation",
    icon: RPAIcon,
    description: `Convert tedious, manual tasks to simple, automated processes.`,
    urlItems: false,
  },
  {
    head: "Customised Reports",
    icon: CustomizeReport,
    description: `Reports tailored to the requirements of our clients, including analysis of data from non-ZP sources.`,
    urlItems: false,
  },
  {
    head: "Datafeed",
    icon: DataFeed,
    description: `Standardising formats and definitions across multiple datasets, sent to client data lakes using traditional or modern mechanisms (APIs).`,
    urlItems: false,
  },
  {
    head: "Data Skills Training",
    icon: DSTIcon,
    description: `Build data, visualization, and coding capabilities for your team and expand business acumen.`,
    urlItems: false,
  },
]
function ServiceOffering(props) {
  const [show, setShow] = useState("")
  const [openModel, setOpenModel] = useState(false)
  const [dataSet, setDataSet] = useState(circleData)
  const [showReport, setShowReport] = useState(false)
  const [linkData, setLinkData] = useState([])
  const [loading, setLoading] = useState(true)
  const [activeTab, setActiveTab] = useState(0)
  const [activePill, setActivePill] = useState(0)
  const [item, setItem] = useState([])
  const [insiderSubscription, setInsiderSubscription] = useState([])
  const [array, setArray] = useState([])
  const [group, setGroup] = useState("")
  const onPillChange = pill => {
    setActivePill(pill)
  }

  const onTabChange = tabIdx => {
    if (activeTab !== tabIdx) {
      setActiveTab(tabIdx)
      setLinkData(array[tabIdx].elements)
      setActivePill(0)
    }
  }

  const Circles = props => {
    let bgColor =
      basePath === "/auth" && group
        ? props.head === "Data Skills Training"
          ? "#094d58"
          : insiderSubscription?.isSubscribed
          ? props.urlItems?.length
            ? "#094d58"
            : "#B6C7CD"
          : "#a8e0e0"
        : "#ffffff"
    let border = basePath === "/auth" && group ? "transparent" : ""
    const handleEvent = () => {
      //
      if (basePath === "/auth" && props.urlItems?.length) {
        // setActiveTab(props.id)
        let idx = item.indexOf(props.head)
        setActiveTab(idx)
        setLinkData(props.urlItems)
        setShowReport(true)
        onPillChange(0)
        props.getInstructionsFromChild(true)
      } else {
        cogoToast.warn("No Dashboard Available")
      }
    }
    return (
      <div
        className={`${Styles.circleOutline} ${Styles.businessBorder}`}
        onClick={handleEvent}
        onKeyDown={() => {
          if (props.urlItems?.length) {
            let idx = item.indexOf(props.head)
            setActiveTab(idx)
          }
        }}
        role="button"
        tabIndex={0}
        style={{
          backgroundColor:
            !insiderSubscription?.isSubscribed && show === props.id
              ? "#ffffff"
              : bgColor,
          borderColor: props.id === show ? "#c3d422" : border,
        }}
      >
        <img src={props.icon} style={{ height: "3rem" }} alt="icon" />
      </div>
    )
  }

  useEffect(() => {
    setLoading(true)
    const get = async () => {
      let groups = Cookie.getJSON("inner_option")
        ? Cookie.getJSON("inner_option")
        : null
      setGroup(groups)
      if (basePath === "/auth" && groups) {
        let subsData = props.subsData
        // console.log(props)
        let insiderSubs = subsData
          ? subsData.find(a => a.Product === "Data Management and Integration")
          : {}
        console.log(insiderSubs)
        setInsiderSubscription(insiderSubs)
        // console.log(insiderSubscription);
        let data = await getGroupOptionsbyGroupName(
          "businessIntelligenceServices"
        )
        // console.log(data);
        try {
          if (insiderSubs?.isSubscribed) {
            // console.log(data[0]);
            if (data[0].type.toLowerCase() === "businessintelligenceservices") {
              let dataAr = []
              let ar = []
              data[0].items.map(i => {
                //console.log(i)
                if (i.elements.length > 0) {
                  ar.push(i.type)
                  dataAr.push(i)
                }
                return i
              })
              //console.log(ar)
              setItem(ar)
              setArray(dataAr)
            }

            setDataSet(
              circleData.map(m => {
                // console.log(m.head.toLowerCase().replace(/ /g, ""));
                const temp = data?.[0]?.items.find(
                  e =>
                    e.type.toLowerCase().replace(/ /g, "") ===
                    m.head.toLowerCase().replace(/ /g, "")
                )
                // console.log(temp,m, ".......THIS")
                m.urlItems = temp ? temp.elements : false
                m.url =
                  m.head === "Data Skills Training"
                    ? "https://ezpz.zuelligpharma.com/"
                    : null
                //  console.log(temp,m, ".......THAT")
                return m
              })
            )
            setLoading(false)
          } else {
            cogoToast.warn("Current Group is not subscribed to this service")
            setLoading(false)
          }
        } catch {
          cogoToast.error("Internal Server Error")
          // console.log(data);
          setLoading(false)
        }
      } else {
        // console.log();
        setLoading(false)
      }
    }
    get()
  }, [props.subsData])

  return (
    <div className={Styles.main}>
      {/* <TabDiv /> */}
      {!loading && !showReport ? (
        <SubHeader
          title="Business Intelligence"
          subTitle="Improve the use of data and insights by deploying custom data management and dashboarding solutions that help make data preparation and insight generation more efficient."
          image={BIs}
        />
      ) : null}
      {showReport ? (
        <div className={Styles.divHead} style={{ paddingTop: "10px" }}>
          <Tab items={item} activeTab={activeTab} onTabChange={onTabChange} />
        </div>
      ) : null}
      <div className={`text-center ${Styles.diagramContainer}`}>
        {!loading && !showReport ? (
          <span className={Styles.centerHead}>
            <span>Business Intelligence Services</span>
          </span>
        ) : null}

        {loading ? (
          <div
            style={{
              minHeight: "13vh",
              display: "flex",
              justifyContent: "center",
              alignItems: "center",
            }}
          >
            <Spinner animation="border" role="status" />
          </div>
        ) : showReport ? (
          <div className="d-flex flex-column w-100">
            <ButtonTab
              items={linkData}
              activeTab={activePill}
              onTabChange={onPillChange}
            />
            <Body
              tabs={array}
              url={linkData[activePill]?.url}
              label={linkData[activePill]?.lable}
              height={linkData[activePill]?.height}
              description={linkData[activePill]?.description}
              backListener={() => {
                setShowReport(false)
                props.getInstructionsFromChild()
              }}
            />
          </div>
        ) : (
          <ul className={Styles.businnessContainer}>
            {dataSet.map((item, key) => (
              <li key={key} style={{ textAlign: "center" }}>
                <a
                  href={item.url ? item.url : null}
                  target="_blank"
                  rel="noreferrer"
                >
                  <span
                    role="button"
                    tabIndex={key}
                    onMouseEnter={() => setShow(key)}
                    onMouseLeave={() => setShow(false)}
                    key={key}
                    heading={item.head}
                  >
                    <Circles
                      urlItems={item.urlItems}
                      head={item.head}
                      icon={item.icon}
                      id={key}
                      getInstructionsFromChild={props.getInstructionsFromChild}
                    />
                    <div
                      className={Styles.item_description}
                      style={{
                        position: "absolute",
                        transition: "all 0.2s",
                        zIndex: "-3",
                        transform: key === show ? "scale(1)" : "scale(0)",
                      }}
                    >
                      <span>
                        <div className={Styles.underline}>
                          <p style={{ fontWeight: "bold" }}>{item.head}</p>
                        </div>
                        <p>{item.description}</p>
                      </span>
                    </div>
                  </span>
                </a>
              </li>
            ))}
          </ul>
        )}
        <Model open={openModel} onClose={() => setOpenModel(false)} />
      </div>
      <DownButton modalListener={() => setOpenModel(true)} />
    </div>
  )
}

export default ServiceOffering
