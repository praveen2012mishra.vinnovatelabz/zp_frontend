import React, { useState, useEffect } from "react"
import ServiceOffering from "./ServiceOffering/index"
import HeaderCommonComponent from '../../../../components/ui/HeaderCommon/HeaderCommon'
import bussinessBanner from '../../../../assets/images/bussiness_intelligent.png'
import axios from "../../../../../axiosConfig"
import { Spinner } from "react-bootstrap"

// import { group } from "../../../../components/getGroupName"
import Cookie from "js-cookie"
// let group = "Tableau Regional - Data Analytics - Demo";


function DataManagement(props) {
  const [tabSelected, setTab] = useState(0)
  const [loading, setLoading] = useState(true)
  const [subs, setSubs] = useState([])
  const [childInstructions, setChildInstrustions] = useState(false)
  const [groupName, setGroupName] = useState(null)
  useEffect(() => {
    // console.log(props.location);
    let group= Cookie.getJSON("inner_option")?Cookie.getJSON("inner_option"):null
    setGroupName(group)
    axios
      .post("/subs/subsDatabase", {
        groupName: group ? group.label : null,

      })
      .then(res => {
        let fields = res.data.subs
        const localSubsData = []
        fields.map((item, index) => {
          let localObj = {}
          if (item.status === "Active" || item.status === "Ending Soon") {
            localObj.Product = item.product
            localObj.isSubscribed = true
            localSubsData.push(localObj)
          } else {
            localObj.Product = item.product
            localObj.isSubscribed = false
            localSubsData.push(localObj)
          }
          return item
        })
        localStorage.setItem("subsData", JSON.stringify(localSubsData))
        setSubs(localSubsData)
        setLoading(false)
      })
      .catch(err => {
        console.error("Failed to get the data", err)
        setLoading(false)
      })
  }, [])


  const tabs = ["Business Intelligence"]
  const onTabChange = tabIdx => {
    if (tabSelected !== tabIdx) {
      setTab(tabIdx)
    }
  }

  const getInstructionsFromChild = () => {
    // console.log(inst);
    setChildInstrustions(!childInstructions)
  }
  return (
    <HeaderCommonComponent
      height={'15%'}
      HeaderImage={bussinessBanner}
      heading={'Business Intelligence'}
      previousProps={props}
      tabSelected={tabSelected}
      tabs={childInstructions ? [] : tabs}
      onTabChange={onTabChange}>
      {subs.length === 0 && loading ? (
        <div style={{ minHeight: "65vh", display: "flex",backgroundColor:"#fff", justifyContent: "center", alignItems: "center" }}>
        <Spinner animation="border" role="status" />
      </div>
      ) : tabSelected===0 && (
          <ServiceOffering
            subsData={subs}
            {...props}
            getInstructionsFromChild={() => getInstructionsFromChild()}
          />
        )}
    </HeaderCommonComponent>

  )
}

export default DataManagement
