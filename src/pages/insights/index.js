import React, { useEffect } from "react"
import { createApolloFetch } from "apollo-fetch"
import { Row, Col, Container, Media } from "reactstrap"
import { Spinner } from "react-bootstrap"
import moment from "moment"
import medicine1 from "../../assets/images/kendal-L4iKccAChOc-unsplash.jpg"
import dummy from "../../assets/images/dummyInsights.jpg"
import styles from "./../Home/style.module.scss"
import { Link } from "gatsby"
import Axios from "../../../axiosConfig"
import Social from "../../components/ui/social2/index"
// const uri = "https://ezpz.zuelligpharma.com/graphql"
// const query = `
// query MyQuery {
//     posts {
//         nodes {
//             uri
//             title(format: RENDERED)
//             excerpt(format: RENDERED)
//             id
//             postId
//             status
//             date
//             dateGmt
//             databaseId
//             authorId
//             categories {
//                 nodes {
//                     id
//                     name
//                     parentId
//                     uri
//                     categoryId
//                 }
//             }
//             content(format: RENDERED)
//             tags {
//                 nodes {
//                     id
//                     tagId
//                     name
//                 }
//             }
//             authorDatabaseId
//             slug
//             author {
//                 node {
//                     id
//                     name
//                     nicename
//                     nickname
//                     userId
//                     username
//                     slug
//                     email
//                     firstName
//                     lastName
//                     locale
//                 }
//             }
//             featuredImage {
//                     node {
//                         mediaItemUrl
//                         mediaItemId
//                         mimeType
//                         mediaType
//                         sizes
//                         slug
//                         sourceUrl(size: LARGE)
//                         title
//                         uri
//                     }
//                 }
//         }
//     }
// }`
// const apolloFetch = createApolloFetch({ uri })

function Insights(props) {
  // const [data, setData] = React.useState("")
  // const [articleUrl, setArticleUrl] = React.useState("")
  const [loading, setLoading] = React.useState(true)
  const [dataSet, setdataSet] = React.useState([])
  const [screenWidth, setScreenWidth] = React.useState(0)
  React.useEffect(() => {
    setScreenWidth(window.screen.width)
    // Axios.post("/insights/createInsights",)
  }, [])
  useEffect(() => {
    let mounted = true

    // apolloFetch({ query })
    Axios.get("insights/getAllArticles")
      .then(res => {
        // setData(res.data)
        if (mounted) {
          if (props.count) {
            setdataSet(res.data.articles.slice(0, props.count))
          } else {
            // setdataSet(res.data.posts.nodes)
            setdataSet(res.data.articles)
          }
        } else {
          return null
        }
        setLoading(false)
      })
      .catch(err => {
        console.log(err)
        setLoading(false)
      })
    return () => {
      mounted = false
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [])
  // const encodeUrl = (obj) => {
  //   var str = "";
  //   for (var key in obj) {
  //     if (str !== "") {
  //       str += "&";
  //     }
  //     str += (key + "=" + encodeURIComponent(obj[key]));
  //   }
  //   return str
  // }

  const getReadTime = obj => {
    var time = null
    if (obj && obj.length > 0) {
      for (var key in obj) {
        if (obj[key].name.includes("MIN")) {
          time = obj[key].name.match(/\d+/)[0] + " MIN READ"
        }
      }
    }
    return time
  }

  const structData = item => {
    let excerptTrim = `${item.excerpt.slice(3, 150)}...`
    // let month = moment("2020-01-12T14:46:15").format("DD-MMMM-YYYY")

    // alert(month)
    const strData = {
      imageUrl: item.featuredImage?.node.sourceUrl,
      // imageUrl:item.imageUrl,
      title: item?.title,
      excerpt: excerptTrim,
      id: item?.id,
      uri: item?.uri,
      content: item?.content,
      tags: item.tags.nodes[0]?.name,
      categories: item.categories?.nodes,
      date: moment(item.date).format("DD MMMM YYYY"),
      readTime: getReadTime(item.tags.nodes),
    }
    // let urlData = JSON.stringify(strData)
    // console.log(strData);
    return strData
  }
  // const readMore = (e, urlData, url) => {
  //   let encodedUrlData = encodeUrl(url)
  //   setArticleUrl(encodedUrlData)
  // }
  // let ar = Array(10).fill(arr)
  return (
    <div className="insightblock">
      {loading ? (
        <Spinner
          animation="border"
          color="#084658"
          style={{ marginTop: "50vh", marginLeft: "50%" }}
        />
      ) : (
        <section className="insights-section">
          <Container fluid>
            {props?.location?.pathname === "/insights/" ||
            props?.location?.pathname === "/insights" ? (
              <Row>
              <abbr className={styles.hideSocial}><Social /></abbr>
                <Col xl={8} lg={9} className={`pt-4 pb-5`}>
                  <h2 
                  style={{paddingLeft:screenWidth>700?"1.5em":0}}
                  className="insightTitle">Our Insights</h2>
                </Col>
              </Row>
            ) : (
              ""
            )}
            <Row style={{paddingRight:screenWidth>769? "3rem":0, paddingLeft:screenWidth>769? "7rem":0}}>
              {dataSet?.length > 0 ? (
                dataSet?.map((item, index) => {
                  // {/* {ar.map((item, index) => {  */ }
                  let structDataVar = structData(item)
                  return (
                    <React.Fragment key={index}>
                      {index === 0 ? (
                        <Col
                          md={screenWidth < 769 ? 12 : 8}
                          sm={12}
                          xs={12}
                          className="mt-4"
                          key={index}
                        >
                          <Media className="custom-media h-100" key={index}>
                            <Media left>
                              <Media
                                className="w-100 h-100 object-cover"
                                object
                                src={structDataVar.imageUrl || dummy}
                                alt="Generic placeholder image"
                              />
                            </Media>
                            <Media
                              body
                              className="border-left-0 pl-0 d-flex flex-column justify-content-between"
                            >
                              <div className={styles.imgtxtWrapper}>
                                <small>{structDataVar.readTime}</small>
                                <h3 className="my-3">{structDataVar.title}</h3>
                                {structDataVar.excerpt}
                              </div>
                              <div className="readMoreContainer">
                                <div className="readMoreDiv2" />
                                <Link
                                  to={`/ReadArticle?${structDataVar.id}`}
                                  // onClick={e => readMore(e, structDataVar, `${structDataVar.id}`)}
                                  state={{ data: structDataVar }}
                                  className="read-more2"
                                >
                                  Read More
                                </Link>
                              </div>
                            </Media>
                          </Media>
                        </Col>
                      ) : index === 1 ? (
                        <Col
                          md={screenWidth === 768 ? 12 : 4}
                          sm={12}
                          className="mt-4"
                        >
                          <Media className="custom-media flex-wrap">
                            <Media left className="w-100">
                              <Media
                                className="w-100"
                                object
                                src={structDataVar.imageUrl || medicine1}
                                alt="Generic placeholder image"
                              />
                            </Media>
                            <Media
                              body
                              className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                            >
                              <div className={styles.imgtxtWrapper}>
                                <small>{structDataVar.readTime}</small>
                                <h4 className="my-3">{structDataVar.title}</h4>
                                <h3 className="my-3">{structDataVar.title}</h3>
                                {structDataVar.excerpt}
                              </div>
                              <div className="readMoreContainer">
                                <div className="readMoreDiv" />
                                <Link
                                  to={`/ReadArticle?${structDataVar.id}`}
                                  // onClick={e => readMore(e, structDataVar, `${structDataVar.id}`)}
                                  state={{ data: structDataVar }}
                                  className="read-more"
                                >
                                  Read More
                                </Link>
                              </div>
                            </Media>
                          </Media>
                        </Col>
                      ) : (
                        <Col
                          md={screenWidth === 768 ? 6 : 4}
                          xs={12}
                          className="mt-4"
                        >
                          <Media className="custom-media flex-wrap">
                            <Media
                              body
                              className="border-top-0 pt-0 d-flex flex-column justify-content-between"
                            >
                              <div className={styles.textWrapperCards}>
                                <small style={{ fontSize: "55%" }}>
                                  {structDataVar.readTime}
                                </small>

                                <h3 className={`${styles.readmoreBody} my-3`}>
                                  {structDataVar.title}
                                </h3>
                              </div>
                              <div className="readMoreContainer">
                                <div className="readMoreDiv" />
                                <Link
                                  to={`/ReadArticle?${structDataVar.id}`}
                                  // onClick={e => readMore(e, structDataVar, `${structDataVar.id}`)}
                                  state={{ data: structDataVar }}
                                  className="read-more"
                                >
                                  Read More
                                </Link>
                              </div>
                            </Media>
                          </Media>
                        </Col>
                      )}
                    </React.Fragment>
                  )
                })
              ) : (
                <div
                  style={{
                    minHeight: "100vh",
                    width: "100%",
                    display: "flex",
                    justifyContent: "center",
                  }}
                >
                  <p
                    style={{
                      color: "#084658",
                      fontSize: "4em",
                      fontWeight: "bold",
                    }}
                  >
                    No Articles available for the moment
                  </p>
                </div>
              )}
            </Row>
          </Container>
        </section>
      )}
    </div>
  )
}

export default Insights
