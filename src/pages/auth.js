import React from "react"
import { Router } from "@reach/router"
import AdminPage from "./Admin"
import AdminRoute from "../components/AdminRoute"
import InfraredSales from "./Admin/CommercialExp/InfraredSales/index"
import InsiderRoute from "./Admin/Insider"
import InsiderReports from "./Admin/Insider/insiderRep/index"
import DoctorPrescription from "./Admin/CommercialExp/DoctorsPrescribing/index"
import SubscriptionRoute from "./Admin/Subscription"
import DataManagement from "./Admin/BusinessIntelligence/DataManagement/index"
// import ControlTower from "./Admin/supplyChain/controlTower"
import ClinicalReach from "./Admin/Insider/Clinical/index"
import Profile from "./Admin/Profile"
import SupplyChainPlanner from "./Admin/supplyChain/supplyChainPlanner"
import Internal from "./Admin/Internal"
import Notifications from "../pages/Admin/AllNotifications"


const Auth = () => {
  return (
    <Router basepath="/auth">
      <AdminRoute path="/admin" component={AdminPage} />
      <AdminRoute path="/insider" component={InsiderRoute} />
      <AdminRoute path="/subscription" component={SubscriptionRoute} />
      <AdminRoute path="/insider/insiderDetails" component={InsiderReports} />
      <AdminRoute path="/insider/clinicalReach" component={ClinicalReach} />
      <AdminRoute
        path="/commercialExp/InfraredSales"
        component={InfraredSales}
      />
      <AdminRoute
        path="/businessInt/datamanagement"
        component={DataManagement}
      />
      <AdminRoute
        path="/commercialExp/InfraredSales/doctorPrescription"
        component={DoctorPrescription}
      />

      {/* <AdminRoute path="/supplyChain/controlTower" component={ControlTower} /> */}
      <AdminRoute path="/supplyChain/controlTower" component={SupplyChainPlanner} />

      <AdminRoute path="/profile" component={Profile} />
      <AdminRoute path="/supplyChain/planner" component={SupplyChainPlanner} />
      <AdminRoute path="/internal" component={Internal} />
      <AdminRoute path="/notifications" component={Notifications} />
    </Router>
  )
}

export default Auth
