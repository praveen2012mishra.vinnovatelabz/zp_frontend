import React from 'react'
import styles from "../style.module.scss"
import TeamGalary from "./TeamGallary"
function OurTeam(props) {
    return (
        <div id="team" style={{ marginLeft: "8%", position: "relative" }}>
            <div className={styles.dempText}>
                <span className={styles.dempLeftBar}>
                    <span className={styles.balls}></span>
                </span>
                <span style={{ paddingBottom: "21%", display: "block" }}>
                    <h6 className={styles.head1} style={{ height: "50px" }}>
                        OUR TEAM
            </h6>
                    <h2 className={styles.head2}>
                        We hail from all walks of life, but what brings us together is an entrepreneurial spirit and common love for impactful data innovation
            </h2>
                    <span className={[styles.paraStyle2, styles.digiEmp].join(" ")}>
                        Our team is young , creative and agile. We are made up of individuals with varied backgrounds, from around the world. We continuously challenge ourselves to actively improve.
            <br /><br />
            Impact for us is about making sure we play a role in helping our clients solve the problems that matter to them, in a way that’s best for them. It’s not about our solutions, it’s about their challenges and opportunities. To us, it’s not an actionable insight if it hasn’t led to tangible improvements.


            </span>
                    {/* <div className={styles.teamSqrText}><a href="#copenings" style={{ textDecoration: "none",
                color: "#094658"}}><span style={{verticalAlign:"super"}}>_________   </span>JOIN US</a></div> */}
                    <div id={styles.Group_388} className={styles.joinUs_Line2}>
                        <a href="#copenings" style={{
                            textDecoration: "none",
                            color: "#094658"
                        }}>
                            <div id={styles.joinUs}>
                                JOIN US
                    </div>
                        </a>
                        <svg className={styles.joinUsLine} viewBox="0 0 141 1">
                            <path id={styles.joinUsLine} d="M 0 0 L 141 0">
                            </path>
                        </svg>
                    </div>
                    <TeamGalary />
                </span>
            </div>
            
            {!props.img?<div className={styles.teamSquare}></div>:
            <img src={props.img} className={styles.teamSquare} alt=""/>}
            {/**
            <div id={styles.Image_placeholder_bh}>
			<span>Image placeholder</span>
            </div>
             */}
        </div>
    )
}

export default OurTeam
