
import React, { useState,useEffect, createRef } from 'react'
import styles from "../style.module.scss"
import axios from "../../../../axiosConfig"
import Carousel from "react-multi-carousel";
import "react-multi-carousel/lib/styles.css";
import { Spinner } from 'reactstrap'
// import { set } from 'js-cookie'
function CurrentOpenings(props) {
    let obj = {
        title:"Zuellig Pharma Analytics",
        description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        scope:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
        url:""
    }
    let arr = [
        {
            title:"Zuellig Pharma Analytics 1",
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur.",
            scope:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam.",
            url:""
        },
        {
            title:"Zuellig Pharma Analytics 2",
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.",
            scope:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit.",
            url:""
        },
        {
            title:"Zuellig Pharma Analytics 3",
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            scope:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.",
            url:""
        },
        {
            title:"Zuellig Pharma Analytics 4",
            description:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.",
            scope:"Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat..",
            url:""
        }
    ]
    const [data,setData] = useState([])
    const [isMobile, setIsMobile] = useState(false);
    const [offHeightJd, setOffJdHeight] = useState(0);
    const [offHeightScope, setOffScopeHeight] = useState(0);
    let scopeRefList = []
    let descrefList = []
    useEffect(()=>{
        axios.get("/dept/getLatestCurrentOpenings").then(res=>{
            if(res.data.status){
                setData(res.data.openings)
                // setStatus(true)
            }
            // else setStatus(true)
        })
        // setData(arr);
    },[])
    useEffect(()=>{
      if(data.length){
          let maxJdHeight = data.sort((a,b)=>{
            return b.description.length-a.description.length
          })
          let maxScopeHeight = data.sort((a,b)=>{
            return b.scope.length-a.scope.length
          })
          descrefList.forEach(item=>{
            if(item.current.id===maxJdHeight[0].description){
              let sh = item.current
              setTimeout(()=>{
                let scrollHeight = sh.scrollHeight
                setOffJdHeight(scrollHeight)
              },500)
            }
          })
          scopeRefList.forEach(item=>{
            if(item.current.id===maxScopeHeight[0].scope){
              let sh = item.current
              setTimeout(()=>{
                let scrollHeight = sh.scrollHeight
                setOffScopeHeight(scrollHeight)
              },500)
            }
          })
   }
    },[data, descrefList, scopeRefList])
    const CustomDots = ({ onClick, ...rest }) => {
      const {
        index,
        active,
      } = rest;
      let left = 28
      return (
        <button
          className={active ? styles.Ellipse_52 : styles.Ellipse_53}
          style={{display:data.length>1?"block":"none"}}
          style={{left:`${left*index}px`}}
          onClick={() => onClick()}
        >
          {/* {React.Children.toArray(deptData)[index]} */}
        </button>
      );
    }
    const responsive = {
      desktop: {
        breakpoint: { max: 3000, min: 1073 },
        items: 3,
        slidesToSlide: 1, // optional, default to 1.
      },
      tablet: {
        breakpoint: { max: 1072, min: 464 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
      },
      mobile: {
        breakpoint: { max: 464, min: 0 },
        items: 1,
        slidesToSlide: 1, // optional, default to 1.
      },
    };
    // eslint-disable-next-line
    function isMobileFun() {
      var check = false;
      (function (a) {
        if (
          // eslint-disable-next-line
          /(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows ce|xda|xiino/i.test(
            a
          ) ||
          // eslint-disable-next-line
          /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(
            a.substr(0, 4)
          )
        )
          check = true;
      })(navigator.userAgent || navigator.vendor || window.opera);
      setIsMobile(check);
      return check;
    }
    return (
        <div id={styles.DE_Image_Placeholder_cv}>
            
            <div className={styles.currentOpenings}>
                current openings
            </div>
            {data?.length ? 
            <Carousel
            swipeable={true}
            draggable={true}
            showDots={data.length>1?true:false}
            responsive={responsive}
            ssr={true}
            infinite={true}
            autoPlay={data.length>1?true:false}
            autoPlaySpeed={2500}
            keyBoardControl={true}
            // customTransition="transform 200ms ease-in-out"
            transitionDuration={200}
            containerClass={styles.carousel_container}
            removeArrowOnDeviceType={["tablet", "mobile","desktop"]}
            deviceType={isMobile ? "mobile" : "desktop"}
            dotListClass={styles.Group_470}
            itemClass={styles.carousel_padding}
            arrows={false}
            customDot={<CustomDots/>}
          >
            {data.map((item,index)=>{
                let desc = item.description.slice(0,400)
                let scope = item.scope.slice(0,650)
                let scopeRefData = createRef()
                scopeRefList.push(scopeRefData)
                let descRefData = createRef()
                descrefList.push(descRefData)
                return <div key={index} className={styles.jobsBackground}>
                    <div>
                      <div className={styles.jobsHeading}>
                        {item.title}
                      </div>
                    </div>
                    <div>
                    <div className={styles.jobsdescheading}>
                        Job Description
                      </div>
                      <div id={item.description} ref={descRefData} className={styles.jobsdescription} style={{height:offHeightJd>0?`${offHeightJd}px`:""}}>
                    {item.description.length>desc.length?`${desc}...`:desc}
                      </div>
                    </div>
                    <div>
                    <div className={styles.scopeHeading}>
                      Scope
                      </div>
                    </div>
                    <div>
                    <div id={item.scope} ref={scopeRefData} className={styles.scopeDescription} style={{height:offHeightScope>0?`${offHeightScope}px`:""}}>
                    {item.scope.length>scope.length?`${scope}...`:scope}
                      </div>
                    </div>
                    <div className={styles.applyBorders}>
                      <div className={styles.borderApply}>
                      </div>
                      <div className={styles.apply_nowN}>
                        Apply Now
                      </div>
                    </div>
                </div>
            })}</Carousel>:<Spinner color="#06333f" style={{margin:"auto",display:"flex"}}/>}
            
        </div>
    )
}

export default CurrentOpenings
