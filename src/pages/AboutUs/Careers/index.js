import React from 'react'
import styles from "../style.module.scss"
import CurrentOpenings from "./CurrentOpenings"
function Careers(props) {
    return (
        <div id="careers" style={{marginLeft: "8%", position:"relative"}}>
            <div className={styles.dempText}>
            <span className={styles.dempLeftBar}>
                <span className={styles.balls}></span>
            </span>
            <span style={{marginBottom:"5%",display:"block", width:"80vw"}}>
            <h6 className={styles.head1} style={{height:"50px"}}>
            CAREERS
            </h6>
            <h2 className={[styles.head2,styles.head22].join(" ")}>
            We leverage cutting-edge analytics and data science methodologies to help our healthcare partners make data driven business decisions -
            <br/>
            and have a bit of fun while doing so
            </h2>
            <span className={[styles.paraStyle2,styles.careerSublines].join(" ")}>
            Our environment is one that is dynamic and fast moving. We value highly collaborative and innovative characters, traits that are of importance within the team, the wider Zuellig Pharma and with external parties.
            <br/><br/>
            The constant desire to learn and improve is something we expect of ourselves. If this is the sort of culture that defines you, we invite you to join us.
            </span>
            <div id="copenings" style={{marginTop:"17%"}}>
              <CurrentOpenings/>
            </div>
            </span>
            </div>
            {!props.img?<div className={`${styles.dempCircle} ${styles.circleTop}`}></div>:
            <img src={props.img} className={styles.dempCircle} alt=""/>}
            {/**
            <div id={styles.Image_placeholder_bh2}>
			<span>Image placeholder</span>
            </div>
             */}
        </div>

    )
}

export default Careers
