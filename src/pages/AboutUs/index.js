import React, {useState,useEffect} from 'react'
import styles from "./style.module.scss"
import Intro from "./intro"
import DigitalEmpowerment from "./DigitalEmpowerment"
import OurTeam from "./OurTeam"
import Careers from "./Careers"
import Social2 from "../../components/ui/social2"
import {Link} from "react-scroll"
let isBrowser = typeof window !== "undefined"
function AboutUs() {
    const [scrollToTop,setScrollTop]  = useState(false)
    const [windowVar,setWindowVar]  = useState(false)
    useEffect(()=>{ 
        setWindowVar(window)
        window.addEventListener("scroll",()=>{
            window.scrollY>90?setScrollTop(true):setScrollTop(false)
        })
    },[])
    // const handleSetActive = (to) => {
    //     console.log(to);
    //   }
    return (
        <div className={`${styles.header}`}>
            {/* <h1 className={`text-center ${styles.title}`}></h1> #d1dde1 #9db0b6 */}
            <div className={`text-center ${styles.titleHeader}`} style={{top:scrollToTop && windowVar.innerWidth<992 ? 0 : !scrollToTop && windowVar.innerWidth<992 ? "72px" : "",position:windowVar.innerWidth<992 && scrollToTop ? "fixed":""}}>

                <Link activeClass={styles.activeHeading} to="demp" spy={true} smooth={true} offset={-150} duration={500}>
                    <span className={styles.subLinks}>DIGITAL EMPOWERMENT</span>
                    </Link>
                <Link activeClass={styles.activeHeading} to="team" spy={true} smooth={true} offset={-150} duration={500}>
                    <span className={styles.subLinks} style={{ borderLeft: "2px solid", borderRight: "2px solid" }}>OUR TEAM</span>
                    </Link>
                <Link activeClass={styles.activeHeading} to="careers" spy={true} smooth={true} offset={-150} duration={500}>
                    <span className={styles.subLinks}>CAREERS</span>
                    </Link>
            </div>
            <abbr className={styles.hideSocial}><Social2 lineHeight={42} /></abbr>
            <Intro />
            <DigitalEmpowerment />
            <OurTeam />
            <Careers />            
        </div>
    )
}

export default AboutUs