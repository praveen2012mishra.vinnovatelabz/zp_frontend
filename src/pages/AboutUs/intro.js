import React from 'react'
import styles from './style.module.scss'

function Intro(props) {
    return (
        <div className={styles.introWrapper}>
            <h6 className={[styles.head1,styles.welcome].join(" ")} style={{marginTop:"15%"}} >
            WELCOME TO ZP ANALYTICS
            </h6>
            <span className={styles.head2_1}>
            <span className={styles.titleLine}>Your ally in solving</span>
            <br/>
             <span className={styles.titleLine}>problems using data</span>
            </span>
            <div className={styles.text}>
                <span className={styles.introLeftBar}></span>
                <span style={{display:"block", width:"100%"}}>
                <span className={[styles.paraStyle2,styles.introDesp].join(" ")}>
                ZP Analytics is the data analytics arm of Zuellig Pharma.
                <br/>
                 Our mission is to help clients make data driven decisions,
                 <br/>
                  which in turn helps them make healthcare more accessible
                <br /><br />
                At ZP Analytics, we believe that every problem is unique
                <br/>
                 and requires its own thoughtfully crafted solution.
                 <br/> 
                 We are much more than a team of data scientists,
                 <br/>
                  we are your ally in solving problems using data.
                </span>
                <span style={{padding:"16% 0", display: "block"}} id="video">
                {/*<video className={styles.videoTag} style={{backgroundColor:"black"}} controls>

                <source src="https://youtu.be/zGLQH0Y5taA" type="video/mp4" />
                <track src="" kind="captions" srclang="en" label="english_captions"/>
                </video> */}
                <iframe className={styles.videoTag} src="https://www.youtube.com/embed/zGLQH0Y5taA?start=3" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"></iframe>
                </span>
                </span>
            </div>
            <div className={styles.introSquare}></div>
            {/* <span className={styles.introSqrText2}></span> */}
            {/* <span className={styles.introSqrText}><span style={{verticalAlign:"super"}}>_________   </span> UNCOVER OUR SECRETS</span> */}
            <div id={styles.Group_388} className={`${styles.newGroup}`}>
                    <div id={styles.apply_now}>
                       <a href="#video" style={{textDecoration:"none", color:"rgba(9, 70, 88, 1)", cursor:"pointer"}}>UNCOVER OUR SECRETS</a> 
                    </div>
                    <svg className={`${styles.Line_22} ${styles.svg_Line3}`} viewBox="0 0 141 1">
                        <path id={styles.Line_22} d="M 0 0 L 141 0">
                        </path>
                    </svg>
                </div>
        </div>  
    )
}

export default Intro
