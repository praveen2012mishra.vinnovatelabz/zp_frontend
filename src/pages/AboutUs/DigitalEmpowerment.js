import React from 'react'
import styles from './style.module.scss'
function DigitalEmpowerment(props) {
    return (
        <div id="demp" name="demp" style={{marginLeft: "8%", position:"relative"}}>
            <div className={styles.dempText}>
            <span className={styles.dempLeftBar}>
                <span className={styles.balls}></span>
            </span>
            <span style={{marginBottom:"20%",display:"block"}}>
            <h6 className={styles.head1} style={{height:"50px"}}>
            DIGITAL EMPOWERMENT
            </h6>
            <h2 className={styles.head2}>
            We believe in making better
            <br/>
            and intelligent decisions through
            <br/>
            the use of data
            </h2>
            <span className={[styles.paraStyle2,styles.digiEmp].join(" ")}>
            Data excellence is a key focus for us. We firmly believe that our clients stand to benefit from the wider use of data. By automating the collection and processing of data, we can then use data in a meaningful manner.
            <br/><br/>
            This is why we have built a practice dedicated to accelerating our clients' data journey - from improving data access to building capabilities. We combine unparalleled industry knowledge and technical expertise with proprietary methodologies to transform complex market data into clear, actionable insights, leading us to recommendations that develop your business. If our existing solutions don’t meet your needs, we will work with you to create one that does.
            <br/><br/>
            Not only have we don’t it for our clients, but we've also invested in ourselves.  We innovate and expedite the time needed to make decisions to drive faster and more accurate decisions
            </span>
            </span>
            </div>
            {!props.img?<div className={styles.dempCircle}></div>:
            <img src={props.img} className={styles.dempCircle} alt=""/>}
            {/*
            <div id={styles.Image_placeholder_bh}>
			<span>Image placeholder</span>
            </div>
             */}
        </div>
    )
}

export default DigitalEmpowerment
