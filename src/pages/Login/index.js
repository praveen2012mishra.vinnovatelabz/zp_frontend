import React from 'react'
import styles from "../Admin/style.module.scss"
import Axios from '../../../axiosConfig';

export default () => {
    Axios.get('/saml')
    .then((res)=>{
        window.location.href = res.data.url
    })
    .catch((e)=>console.log(e))
    return (
        <>
            <div className={`${styles.header}`}>
                <h1 className={`text-center ${styles.title}`}>Redirecting to Login Page...</h1>
                <div style={{ 
                    marginTop: '30px', 
                    paddingBottom:'30px', 
                    textAlign: 'center',    
                    marginLeft: '25%', 
                    marginRight: '25%',
                    color:'#FFFFFF',
                    fontSize: '20px',
                    height:"100vh"

                }}>
                </div>
            </div>
        </>
    )
}
