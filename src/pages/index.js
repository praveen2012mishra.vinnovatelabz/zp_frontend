import React from "react";
import Home from "./Home"
import { Helmet } from "react-helmet"
import ReactHelmet from "./helmet";
const IndexPage = () => <>
    {/* <Helmet>
        <meta charSet="utf-8" />
        <title>ZP Analytics</title>
        <link rel="canonical" href="http://mysite.com/example" /> 
    </Helmet> */}
    <ReactHelmet/>
    <Home /></>;

export default IndexPage
