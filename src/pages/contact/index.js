import React from 'react'
import './style.module.scss'
import MH from '../../assets/images/Mike Andrew.jpg'
import Tristan from '../../assets/images/tristan.jpeg'
import LC from '../../assets/images/chen.jpeg'
import MT from '../../assets/images/Michael, Tanny.jpg'
import YB from '../../assets/images/yongboon.png'
import Zac from '../../assets/images/Zachary.jpg'
import Avatar from '../../assets/images/avatar.jpg'
import Hannah from '../../assets/images/hannah.png'
import JO from "../../assets/images/joanna.png"
import HeaderCommonComponent from '../../components/ui/HeaderCommon/HeaderCommon'
import '../../global.scss'
import styles from './style.module.scss'
import ContactModal from "../../components/Header/contactModal"
import Axios from '../../../axiosConfig'
import Cookie from "js-cookie"
import { Spinner } from "reactstrap"
import Social from "../../components/ui/social2"


function Contact() {
    const title = "Contact Information";
    const subTitle = "Click on the image of your selected crew member to drop him/her an email";

    const tabs = [" "]
    const [modalOpen, setModalOpen] = React.useState(false);
    const [name, setName] = React.useState("");
    const [designation, setDesignation] = React.useState("");
    const [user, setUser] = React.useState([]);
    const [loading, setLoading] = React.useState(true);
    const [screenWidth, setScreen] = React.useState(761);

    React.useEffect(() => {
        setScreen(window.screen.width)
        setLoading(true);
        let samlCookie = Cookie.get("saml_response")
        let samlToken = samlCookie
            ? JSON.parse(samlCookie.substr(2, samlCookie.length))
            : null
        const userName = samlToken ? samlToken.user.name_id : ""

        Axios.get(`/users/getUserProfile/${userName}`)
            .then((res) => {
                setUser(res.data.userProfile.profile_pic)
                setLoading(false)
            })
            .catch((err) => {
                console.error("Failed to get the data", err)
            })
        setLoading(false)
        // window.addEventListener("scroll", handleScroll)
    }, [])

    return (
        loading ? <Spinner color="#0A4658" /> : (<div className={styles.main}>
            <ContactModal open={modalOpen} onClose={() => setModalOpen(false)} reciverName={name} reciverDesignation={designation} />
            <HeaderCommonComponent head={"contact"} heading={'Contact'} tabs={tabs} ></HeaderCommonComponent>
            <div className={`px-lg-3 contact_page_container`} style={{ padding: "1em", position: "relative", display: "flex" }}>
                <div style={{
                    marginRight: "19px",
                    position: "absolute",
                    zIndex: "0",
                    display: screenWidth<760?"none":"block"
                }}>
                    <Social lineHeight={1} />
                </div>
                <div style={{ flex: "1", marginTop: "14em" }}>
                    <div style={{ marginLeft: "10%" }}>
                        <h1>{title}</h1>
                        <h6>{subTitle}</h6>
                    </div>

                    <div id="contact_user_graph" style={{ marginLeft: "7%" }}>

                        <div id="Contact" className={styles.contact_design}>

                            <div id="Group_438">
                                <div id="You">
                                    <span>You</span>
                                </div>
                                <div id="Group_437">
                                    <svg className="Line_165" viewBox="0 0 2 199">
                                        <path id="Line_165" d="M 0 0 L 0 199">
                                        </path>
                                    </svg>
                                    <svg className="Line_166" viewBox="0 0 1030 2">
                                        <path id="Line_166" d="M 0 0 L 1030 0">
                                        </path>
                                    </svg>
                                    <svg className="Line_167" viewBox="0 0 2 145">
                                        <path id="Line_167" d="M 0 0 L 0 145">
                                        </path>
                                    </svg>
                                    <div id="Group_428">
                                        <div id="Michael_Andrew">
                                            <span>Michael, Andrew</span>
                                        </div>
                                        <div id="Commercial_Excellence_Regional">
                                            <span>Commercial Excellence<br />Regional</span>
                                        </div>
                                        <div id="TT">
                                            <span>Tristan, tan</span>
                                        </div>
                                        <div id="TT_TT">
                                            <span>Vice President, Analytics<br />Regional</span>
                                        </div>
                                        {/**line of micheal andrew */}
                                        <svg className="Line_169" style={{ display: "none" }} viewBox="0 0 280 2">
                                            <path id="Line_169" d="M 0 0 L 280 0">
                                            </path>
                                        </svg>
                                        <div id="Group_425">
                                        </div>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Michael Andrew")
                                            setDesignation("Commercial Excellence Regional")
                                        }} style={{zIndex: 10, position: "absolute", left:"381px", top: "16px", outline:"none"}}role="button" tabIndex={0} onKeyDown={() => setName("Michael Andrew")}>
                                            <img style={{ borderRadius: "60px" }} id="Mask_Group_5" src={MH} alt="MH" />

                                        </div>
                                        <svg className="Line_176_MA" viewBox="0 0 225 2">
                                            <path id="Line_176_MA" d="M 225 0 L 0 0">
                                            </path>
                                        </svg>

                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Tristan, tan")
                                            setDesignation("VicePresident, Analitycs Regional")
                                        }} style={{zIndex: 10, position: "absolute", left:"74px", top: "16px", outline:"none"}} role="button" tabIndex={0} onKeyDown={() => setName("Tristan, tan")}>
                                            <img style={{ borderRadius: "60px",objectFit:"cover" }} id="Mask_Group_5_TT" src={Tristan} alt="MH" />
                                        </div>

                                        <div id="Group_432">
                                        </div>
                                    </div>
                                    <svg className="Line_171" viewBox="0 0 2 139">
                                        <path id="Line_171" d="M 0 0 L 0 139">
                                        </path>
                                    </svg>
                                    <div id="Group_429">
                                        <div id="Zachary_Ang">
                                            <span>Zachary, Ang</span>
                                        </div>
                                        <div id="Supply_Chain_Analytics_Regiona">
                                            <span>Supply Chain Analytics<br />Regional</span>
                                        </div>
                                        <svg className="Path_576" style={{ display: "none" }} viewBox="0 0 245 2">
                                            <path id="Path_576" d="M 245 0 L 0 0">
                                            </path>
                                        </svg>
                                        <div id="Group_426">

                                        </div>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Zachary, Ang")
                                            setDesignation("Supply Chain Analytics")
                                        }}
                                            onKeyDown={() => setName("Zachary, Ang")}
                                            role="button" tabIndex={0}
                                        >
                                            <img style={{ borderRadius: "60px" }}
                                                id="Mask_Group_6" src={Zac} alt="Zac" />
                                        </div>

                                    </div>
                                    <div id="Group_430">
                                        <div id="Yong_Boon_Lim">
                                            <span>Yong Boon, Lim</span>
                                        </div>
                                        <div id="Business_Intelligence_Regional">
                                            <span>Business Intelligence<br />Regional</span>
                                        </div>
                                        <svg className="Line_173" viewBox="0 0 2 385">
                                            <path id="Line_173" d="M 0 0 L 0 385">
                                            </path>
                                        </svg>
                                        <div id="Hannah_Koh">
                                            <span>Hannah, Koh</span>
                                        </div>
                                        <div id="Business_Intelligence_Regional_ci">
                                            <span>Business Intelligence<br />Regional</span>
                                        </div>
                                        <svg className="Line_176" viewBox="0 0 225 2">
                                            <path id="Line_176" d="M 225 0 L 0 0">
                                            </path>
                                        </svg>
                                        <svg className="Line_177" style={{ display: "none" }} viewBox="0 0 163 2">
                                            <path id="Line_177" d="M 0 0 L 163 0">
                                            </path>
                                        </svg>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Yong Boon, Lim")
                                            setDesignation("Business Intelligence Regional")
                                        }} role="button" tabIndex={0} onKeyDown={() => setName("Yong Boon, Lim")}>
                                            <img style={{ borderRadius: "60px" }}

                                                id="Mask_Group_5_cl" src={YB} alt="YB" />
                                        </div>

                                        <div id="Group_439">
                                        </div>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Hannah, Koh")
                                            setDesignation("Business Intelligence Regional")
                                        }} role="button" tabIndex={0} onKeyDown={() => setName("Hannah, Koh")}>
                                            <img style={{ borderRadius: "60px" }}
                                                id="Mask_Group_7" src={Hannah} alt="Hannah" />
                                        </div>
                                    </div>
                                    <div id="Group_431">
                                        <div id="Michael_Tanny">
                                            <span>Michael, Tanny</span>
                                        </div>
                                        <div id="Product_Development__Training_">
                                            <span>Product Development & Training<br />Regional</span>
                                        </div>
                                        <svg className="Line_172" viewBox="0 0 2 342">
                                            <path id="Line_172" d="M 0 0 L 0 342">
                                            </path>
                                        </svg>
                                        <svg className="Line_175" style={{ display: "none" }} viewBox="0 0 139 2">
                                            <path id="Line_175" d="M 139 0 L 0 0">
                                            </path>
                                        </svg>
                                        <div id="Group_426_cv">
                                        </div>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Michael, Tanny")
                                            setDesignation("Product Development & Training Regional")
                                        }} onKeyDown={() => setName("Michael, Tanny")} role="button" tabIndex={0}>
                                            <img style={{ borderRadius: "60px" }}
                                                id="Mask_Group_6_cy" src={MT} alt="MT" />
                                                <svg className="Line_176_MT" viewBox="0 0 225 2">
                                            <path id="Line_176_MT" d="M 225 0 L 0 0">
                                            </path>
                                        </svg>
                                        </div>
                                    </div>
                                    <div id="Group_427">
                                        <div id="Llewellyn_Chen">
                                            <span>Joanna, Kwa</span>
                                        </div>
                                        <div id="Business_Development_Regional">
                                            <span>Director, Analytics<br />Regional</span>
                                        </div>
                                        {/**
                                        <svg className="Line_168" viewBox="0 0 163 2">
                                            <path id="Line_168" d="M 0 0 L 163 0">
                                            </path>
                                        </svg>
                                         */}
                                        <div id="Group_421">
                                            <div id="Component_12__1" className="Component_12___1">
                                                <div id="Group_434">
                                                {/**
                                                    <svg className="Path_575" viewBox="0 0 0.017 84.182">
                                                        <path id="Path_575" d="M 0.0166015625 84.182373046875 L 0 0">
                                                        </path>
                                                    </svg>
                                                     */}
                                                    <div id="Jin_Jia_Chew">
                                                    <span>Llewellyn, Chen</span>
                                                    </div>
                                                    <div id="Business_Development_Malaysia_">
                                                        <span>Business Development<br /><br />Regional</span>
                                                    </div>
                                                    {/**
                                                    <svg className="Ellipse_28">
                                                        <ellipse id="Ellipse_28" rx="7" ry="7" cx="7" cy="7">
                                                        </ellipse>
                                                    </svg>
                                                     */}
                                                </div>
                                                <div onClick={() => {
                                                    setModalOpen(true)
                                                    setName("Llewellyn, Chen")
                                                    setDesignation("Business Development Regional")
                                                }} onKeyDown={() => setName("Llewellyn, Chen")} tabIndex={0} role="button">
                                                    <img style={{ borderRadius: "60px" }}
                                                        id="Mask_Group_1_dc" src={LC} alt="" />
                                                </div>

                                            </div>
                                        </div>
                                        <div onClick={() => {
                                            setModalOpen(true)
                                            setName("Joanna, Kwa")
                                            setDesignation("Director, Analitycs Regional")
                                        }} onKeyDown={() => setName("Joanna, Kwa")} tabIndex={0} role="button">
                                            <img style={{ borderRadius: "60px" }}

                                                id="Mask_Group_4" src={JO} alt="" />
                                        </div>
                                    </div>
                                    {loading ? <Spinner style={{ marginLeft: "48em" }} color="#0A4658" /> :
                                        <img style={{ borderRadius: "60px" }} id="Group_420" src={user && user.length > 0 ? user : Avatar} alt="" />
                                    }

                                </div>
                            </div>
                        </div>
                    </div>
                    <div style={{ display: "flex", justifyContent: "center", padding: "2em" }}>
                        <button onClick={() => setModalOpen(true)} className={styles.btn}>
                            Don't know who to contact? Click Here</button></div>
                </div>
            </div>
        </div>)
    )
}

export default Contact
