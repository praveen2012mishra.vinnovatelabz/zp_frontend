// checks localStorage if userType is admin
import Cookies from "js-cookie"
import { twofactorEnforceCheck } from "./Api/2FA.api"
import { group } from "../components/getGroupName"
// let isBrowser = typeof window !== "undefined"

const checkStatus = async (samlToken) => {
  let status = await twofactorEnforceCheck(samlToken)
  return status
}

export const isAdmin = () => {
  // localStorage is not avaialable outside browser
  if (Cookies.get("saml_response")) {
    let grps=group?.label
    let finalCall=checkStatus(grps).then((res,rej)=>{
      if(res){
      let twoFaCookie= Cookies.get("2fa")? JSON.parse(Cookies.get("2fa")).authorized : null
      if (twoFaCookie!==null){
        return true
      }
      else {
        return false
      }
    }else{
      return true
    }
    }).catch(e=>false)
    return finalCall
  } else {
    return false
  }
  // return true
}
