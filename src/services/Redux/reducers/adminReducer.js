import {
  ADMIN_GROUP_DATA_FETCHED_ALL,
  ADMIN_GROUP_DATA_DELETED,
  ADMIN_TOGGLE,
  ADMIN_SEARCH_TYPE_COUNTRY
} from "../actionTypes"

const initialState = {
  group: [],
  err: null,
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case ADMIN_GROUP_DATA_FETCHED_ALL: {
      const newState = { group: payload.data, err: payload.err }

      return newState
    }  
    case ADMIN_GROUP_DATA_DELETED: {
      let updatedData = state.group
      if (!payload.err && payload.id) {
        // filter out the deleted item from the array
        updatedData = state.group.filter(g => g._id !== payload.id)
      }

      const newState = { group: updatedData, err: payload.err }

      return newState
    } 

    case ADMIN_SEARCH_TYPE_COUNTRY:{
      const newState = { setTypeCountry: payload.data }
      //console.log(state,newState,'--------------------->',Object.assign({},state,newState));
      return Object.assign({},state,newState)
    }

    case ADMIN_TOGGLE: {
      let updatedData = state.group
      if (payload.err===null && typeof payload.id.key==="number") {
        if(payload.id.type==="session"){
          updatedData[payload.id.key].single_session_enforced=!state.group[payload.id.key].single_session_enforced
          updatedData=state.group.splice(0,state.group.length,...updatedData)
          const newState={ group: updatedData, err: payload.err }
          return newState
        }
        else {
          updatedData[payload.id.key].two_factor_authentication_enforced=!state.group[payload.id.key].two_factor_authentication_enforced
          updatedData=state.group.splice(0,state.group.length,...updatedData)
          const newState = { group: updatedData, err: payload.err }
          return newState
        }
      }
    } break;
    default:
      return state
  }
}
