import { stat } from "fs"
import { SHOW_NOTIFICATION, COUNT_NOTIFICATION } from "../actionTypes"

const initialState = {
  notify: false,
  message: "",
  type: "",
  count: ""
}

export default (state = initialState, { type, payload }) => {
  switch (type) {
    case SHOW_NOTIFICATION: {
      return {
        notify: true,
        type: payload.type,
        message: payload.message,
      }
    }

    case COUNT_NOTIFICATION: {
      return {
        ...state,
        count: payload.count
      }
    }

    default:
      return state
  }
}
