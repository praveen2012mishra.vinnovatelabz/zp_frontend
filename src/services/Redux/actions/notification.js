import { SHOW_NOTIFICATION, COUNT_NOTIFICATION } from "../actionTypes"

/* function to show a notification */
export const showNotification = (message, type) => ({
  type: SHOW_NOTIFICATION,
  payload: { message, type },
})

/* function to count notification */
export const countNotification = (count) => ({
  type: COUNT_NOTIFICATION,
  payload: { count },
})
