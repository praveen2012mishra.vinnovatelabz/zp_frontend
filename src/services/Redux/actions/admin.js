import axios from "../../../../axiosConfig"
import {
  ADMIN_GROUP_DATA_FETCHED_ALL,
  ADMIN_GROUP_DATA_DELETED,
  ADMIN_TOGGLE,
  ADMIN_SEARCH_TYPE_COUNTRY
} from "../actionTypes"
import { showNotification } from "./notification"

/* dispatches action to set the group management data array */
export const setAllGroupData = (err, data) => ({
  type: ADMIN_GROUP_DATA_FETCHED_ALL,
  payload: { err, data },
})

/* function to retrieve all group management data */
export const getAllGroupData = () => async dispatch => {
  try {
    const response = await axios.get("/group/getAllGroups")
    if (Array.isArray(response.data)) {
      dispatch(setAllGroupData(null, response.data))
    } 
    else {
      // if any other response comes except the group management data
      // array, dispatch with error
      dispatch(setAllGroupData(new Error("Failed to fetch data"), []))
      dispatch(showNotification("Failed to group data", "error"))
    }
  } catch (e) {
    dispatch(setAllGroupData(new Error("Failed to fetch data"), []))
    dispatch(showNotification("Failed to fetch group data", "error"))
  }
}

/* dispatches action to update group array after removing one item */
export const updateGroupDataOnDelete = (err, id) => ({
  type: ADMIN_GROUP_DATA_DELETED,
  payload: { err, id },
})

/* function to delete an group item by it's id */
export const deleteGroupData = (
  _id,
  username,
  group_name
) => async dispatch => {
  try {
    const response = await axios.post("/group/deleteGroup", {
      _id,  
      username,
      group_name,
    })
    if (response.data.deletedCount === 1) {
      dispatch(updateGroupDataOnDelete(null, _id))
      dispatch(showNotification("Deleted successfully", "success"))
    } else {
      // if returned delete count is not 1 (no item deleted), dispatch
      // with error
      console.log("dummy")
      dispatch(updateGroupDataOnDelete(new Error("Failed to delete")))
      dispatch(showNotification("Failed to delete", "error"))
    }
  } catch (e) {
    console.log(e,"<-------catch-------")
    dispatch(updateGroupDataOnDelete(e))
    dispatch(showNotification("Failed to delete", "error"))
  }
}

export const updateGroupDataOnToggle = (err, key) => ({
  type: ADMIN_TOGGLE,
  payload: { err, id:key },
})

export const toggle = (
  button,
  group,
  type,
  key
) => async dispatch => {
  let url=button==="session"?"/session/changeSingleSessionStatus":"/session/change2FAStatus"
  try {
      const response = await axios.post(url,{
          "group_name":group,
          "type": type?"enable":"disable"
      })
      if (response.data.success===true) {
        dispatch(updateGroupDataOnToggle(null,{key:key,type:button}))
      } else {
        dispatch(updateGroupDataOnToggle(new Error("Failed to switch")))
        dispatch(showNotification("Failed to Switch", "error"))
      }
  } catch (e) {
      console.log(e, "<-------catch-------")
      dispatch(showNotification("Failed to switch", "error"))
  }
}

export const search = (data) =>dispatch=> dispatch(setAllGroupData(null,data))
export const setTypeCountry = (data) => ({
  type: ADMIN_SEARCH_TYPE_COUNTRY,
  payload: { data },
})