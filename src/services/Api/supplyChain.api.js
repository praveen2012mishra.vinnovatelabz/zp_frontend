import axios from '../../../axiosConfig'
import Cookie from "js-cookie"
//const dummy="/group/fetchGroupUsers/EX Tab HKAMGEN Insider"
const apiQuery = `/group/fetchGroupUsers/`;
const api = apiQuery;
let samlCookie = Cookie.get("saml_response")
let samlToken = samlCookie
    ? JSON.parse(samlCookie.substr(2, samlCookie.length))
    : null
export const getGroupUsers = async (group_name) => {
    let grpOP = await axios.get(api + group_name)
    // add "group_name" when not dummy 
    //(let grpOP= await axios.get(api+group_name))
    return grpOP.data.data;
}

export const saveUserRoles = async (data) => {
    const url = "/users/saveUserRoles"
    let roles = await axios.post(url, data)
    return roles
}

export const getGroups = async () => {
    const url = "/group/getAllGroups"
    let groups = await axios.get(url)
    return groups
}

export const updateSupplyChainAdmin = async (data) => {
    const url = "/users/updateSupplychainAdmin"
    let admin = await axios.post(url, data)
    return admin
}
export const getSupplyChainAdmin = async () => {
    if (samlToken) {
        const url = `/users/getSupplychainAdmin/${samlToken.user.name_id}`
        let admin = await axios.get(url)
        try{
        return admin
        }catch(err){
            return null
        }
    }else return null
}