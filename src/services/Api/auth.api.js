import axios from "../../../axiosConfig";
import Cookies from "js-cookie";

export const changeSingleSessionStatus = (data) => {
    let res = axios.post("/session/changeSingleSessionStatus", {
        "group_name": data.grpName,
        "type": data.type
    })
    console.log(res);
}
export const samlLogout = (status, authData) => {
    Cookies.remove("inner_option")
    Cookies.remove("2fa")
    axios.post("/saml/singleLogout", {
        name: authData.nameId,
        session: authData.sessionIndex,
        in_resp: authData.inResponse,
        status: status === "2FA" ? "2FA failed" : "not known",
    })
        .then(res => {
            Cookies.remove("inner_option")
            Cookies.remove("2fa")
            window.location.href = res.data.redirect
        })
        .catch(e => console.log(e))

}
