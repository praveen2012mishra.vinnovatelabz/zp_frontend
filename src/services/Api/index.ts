import axios from "axios";
const config = {
    baseURL: "",
    withCredentials: true,
    headers: {'id_token': ""}
  };
export default axios.create(config);
