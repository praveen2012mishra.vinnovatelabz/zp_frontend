// import Axios from "axios";
import axios from "../../../axiosConfig"
import { group } from "../../components/getGroupName"
const _ = require('underscore')
// const dummy="/group/fetchGroupOptions/user/Tableau Regional - Data Analytics - Demo"
const apiQuery = group ? `/group/fetchGroupOptions/user/${group.label}` : "";
const api = apiQuery;
export const getGroupOptionsbyGroupName = async (type) => {

    if (group) {
        let grpOP = await axios.get(api)
        let options = grpOP.data.options
        if (type === "allOptions") {
            return options
        } else if (type === "all") {
            return grpOP.data
        } else {
            return _.filter(options, (i) => i.type === type)
        }
    }
    else {
        return false
    }
}
// const token ="eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIyNzY3OTA5OTQxMzQ1ODc3OSIsIm5hbWUiOiJadWxsaWcgUGhhcm1hIiwiaWF0Ijo4NzU2MjMwMDk4fQ.tpuVnsAWSjeWtwEFbyDteEZYN4BGOz82EOq9pgkBJik"

const gpnm = async () => {
    let body = {
        "type": "",
        "keyword": ""
    }
    const groupNames = await axios.post("/group/adGroups", body)
    return groupNames
}
export const getADGroups = () => {
    let groupNames = gpnm().then((res, rej) => {
        // console.log(res.data.data.groupNames,"hello");
        return res.data.data.groupNames
    }).catch(() => [])
    return groupNames
}

export const getAdGroupsByKeyword = async (type, keyword) => {
    let body = {
        "type": type,
        "keyword": keyword
    }
    let grpOP = await axios.post("/group/adGroups/", body);
    console.log('the response', grpOP);
    return grpOP.data.data.groupNames || [];
}