import axios from "../../../axiosConfig";

export const twofactor = async (user_name) => {

    let responseData = await axios.post('/two-factor-authentication/getQRCode', { username: user_name });

    return responseData.data;

}
export const twofactorOtpVerify = async (user_name, token) => {

    let responseData = await axios.post('/two-factor-authentication/verifyToken', { username: user_name, token: token });

    return responseData.data;

}
export const twofactorEnforceCheck = async (user_name) => {

    let responseData = await axios.get(`/session/check2FAStatus/${user_name}`);

    return responseData.data.enforced;

}