import Cookie from "js-cookie";
import axiosInstance from "../../../axiosConfig";
export const updateStats = (
    service,
    subService, 
) => {
    let gName=Cookie.getJSON('inner_option')?Cookie.getJSON('inner_option').label :null;
    let samlCookie = Cookie.get("saml_response")
    let samlToken = samlCookie
        ? JSON.parse(samlCookie.substr(2, samlCookie.length))
        : null
    let data = {
        "userId": samlToken?samlToken.user.name_id : null,
        "service": service,
        "subService": subService,
        "adGroup":  gName
    }
    axiosInstance.post("/statistics/createUserStats", data)
}