var tableau = require('tableau-api');
var viz;
export function initViz(url) {

    //var containerDiv = document.getElementsByTagName('app-tableau').item(0).getElementsByTagName('div').item(0),
    var placeholderDiv = document.getElementById('vizContainer');
    //url = "https://public.tableau.com/views/USTreasuryInterestRate/Sheet1?:embed=y&:display_count=yes",
    const options = {
        height: 800,
        width: 1300,
        hideTabs: true,
        onFirstInteractive: function () {
             
        }
    };
    if (viz != null) {
        viz.dispose();
    }

    viz = new tableau.Viz(placeholderDiv, url, options);
     

    // Create a viz object and embed it in the container div.
}

export function reliseViz(url) {
    var placeholderDiv = document.getElementById('vizContainer');
    //placeholderDiv.remove();
    // placeholderDiv.innerHTML = "";  

    const options = {
        height: 100,
        width: 100,
        hideTabs: true,
        onFirstInteractive: function () {
             
        }
    };
     
    if (viz) {
         
        viz.dispose();
    }

    return new window.tableau.Viz(placeholderDiv, url, options);

}
// import tableau from "tableau-api";
// export const reliseViz = (url) => {
//     const vizUrl = url

//     const options = {
//         hideTabs: true,
//         width: "100px",
//         height: "100px",
//         onFirstInteractive: () => {
//             const sheet = viz.getWorkbook().getActiveSheet().getWorksheets().get("Table");
//             const options = {
//                 ignoreAliases: false,
//                 ignoreSelection: false,
//                 includeAllColumns: false
//             };
//             sheet.getUnderlyingDataAsync(options).then((t) => {
//                 const tableauData = t.getData();
//                 let data = [];
//                 const pointCount = tableauData.length;
//                 for (let a = 0; a < pointCount; a++) {
//                     data = data.concat({
//                         x: tableauData[a][0].value,
//                         y: Math.round(tableauData[a][3].value, 2)
//                     })
//                 };
//             })
//         }
//     };

//     let viz = new window.tableau.Viz(container, vizUrl, options)
// }