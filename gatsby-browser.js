/**
 * Implement Gatsby's Browser APIs in this file.
 *
 * See: https://www.gatsbyjs.org/docs/browser-apis/
 */

// You can delete this file if you're not using it
export { default as wrapRootElement } from "./src/services/Redux/wrapper";
export const onInitialClientRender = () => {
    setTimeout(function () {
        document.getElementById("___loader").style.display = "none"
    }, 300)
}
export const shouldUpdateScroll = ({
    routerProps: { location },
    getSavedScrollPosition,
}) => {
    const { pathname } = location
    // list of routes for the scroll-to-top-hook
    const scrollToTopRoutes = [`/AboutUs`,
        `/`,
        `/insights`,
        `/auth/insider`,
        `/auth/commercialExp/InfraredSales`,
        `/auth/supplyChain/controlTower`,
        `/auth/businessInt/datamanagement`,
        `/app/insider`,
        `/app/commercialExp/InfraredSales`,
        `/app/supplyChain/controlTower`,
        `/app/businessInt/datamanagement`]
    // if the new route is part of the list above, scroll to top (0, 0)
    if (scrollToTopRoutes.indexOf(pathname) !== -1) {
        window.scrollTo(0, 0)
    }

    return false
}