import express from "express"
const bodyParser = require("body-parser")
const pino = require("express-pino-logger")()
import mongoose from "mongoose"
import registerRoutes from "./routes"
mongoose.Promise = require("bluebird")
const config = require("./config/environment")
const mongoUri=config.mongo.uri
const mongoOptions=config.mongo.options
const PORT = config.port

const app = express()

// Set up mongoose connection
mongoose.connect(mongoUri, mongoOptions)

mongoose.connection.on("error", function (err) {
  console.error(`MongoDB connection error: ${err}`)
  process.exit(-1) // eslint-disable-line no-process-exit
})

mongoose.connection.once("open", function () {
  console.log("Connected to Mongodb")
  app.emit("ready")
})

app.use(bodyParser.urlencoded({ limit: "10mb", extended: true }))
app.use(bodyParser.json({ limit: "10mb", extended: true })) // to parse application/json
app.use(pino)
// Specifying API routes
registerRoutes(app)

app.on("ready", function () {
  app.listen(PORT,'0.0.0.0', () =>
    console.log(`Express server is running on localhost:${PORT}`)
  )
})
