"use strict"
/*eslint no-process-env:0*/

import path from "path"
import _ from "lodash"
const config = require("config")
const port = config.get("app.port")
/*function requiredProcessEnv(name) {
  if(!process.env[name]) {
    throw new Error('You must set the ' + name + ' environment variable');
  }
  return process.env[name];
}*/

// All configurations will extend these options
// ============================================
var all = {
  env: process.env.NODE_ENV,

  // Root path of server
  root: path.normalize(`${__dirname}/../../..`),

  // dev client port
  clientPort: process.env.CLIENT_PORT || 3000,

  // Server port
  port: port || 9000,

  // Server IP
  ip: process.env.IP || "0.0.0.0",

  // Should we populate the DB with sample data?
  seedDB: false,

  // Secret for session, you will want to change this and make it an environment variable
  secrets: {
    session: "ng5template-secret",
    ACCESS_TOKEN_SECRET: "vlorbyrvkzhn3mrgzyijadtupzojvvlk4tioon4fykhzvclohjqq",
  },

  // MongoDB connection options
  mongo: {
    options: {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
    },
  },

  google: {
    clientID: process.env.GOOGLE_ID || "id",
    clientSecret: process.env.GOOGLE_SECRET || "secret",
    callbackURL: `${process.env.DOMAIN || ""}/auth/google/callback`,
  },

  ldap: {
    ldaphost: "192.168.34.150", //LDAP needs the port. AD doesn't need the port
    ldapuser: "svczolplus", //only used for AD
    ldappwd: "S^46wc,ub{@Ec^E",
    ldapcn: "VNZP-Zscaler Internet Access Standard", //only used in AD
    ldapbasedn: "DC=zuelligpharma,DC=interpharma,DC=local", // used in both LDAP and AD
    internal: {
      url: "ldap://10.10.12.24:389",
      baseDN: "dc=zuelligpharma,dc=interpharma,dc=local",
      username: "svcZIPInternal@zuelligpharma.interpharma.local",
      password: "FE`C)^n/:8h!((jQ",
    },
    external: {
      url: "ldap://10.10.12.39:389",
      baseDN: "DC=external,DC=interpharma,DC=local",
      username: "svcZIPExternal@external.interpharma.local",
      password: "[>dF^d}9e`WtcJw{",
    },
  },

  tableau: {
    internalurl: "https://192.168.34.16",
    externalurl: "https://tradeinfrared.zuelligpharma.com",
    server: "https://zplive.zuelligpharma.com/trusted",
  },
  // Zuellig Mail API
  zuelligMailer: {
    api:
      "https://l6088-iflmap.hcisbp.ap1.hana.ondemand.com/http/automatedemail/trigger/v1",
    user: "zpanalyticsdata@zuelligpharma.com",
    authorization: "Basic UzAwMTYwOTMxNzc6RGFuaWVsMSE=",
  },
  // Nodemailer Settings
  nodemailer: {
    service: "Gmail",
    user: "info@qubida.com",
    pass: "Qubida15",
  },
  postgres: {
    server: "10.10.22.69",
    port: 8060,
    username: "readonly",
    pwd: "Password123",
    database: "workgroup",
  },
  //API
  api: {
    routes: "http://localhost:3001",
    API: "http://localhost:3001/api",
  },
  saml: {
    saml_entity_id: "analytics.zuelligpharma.com",
    assert_endpoint:
      "https://analytics.zuelligpharma.com/api/saml/postResponse",
    private_key: "analytics.zuelligpharma.com.key",
    certificate: "analytics.zuelligpharma.com.cert",
    adfs_certificates: "sso-adfs.cert",
    login: "https://sso.zuelligpharma.com/adfs/ls/",
  },
  suggestedPO: {
    PO: "http://zip_portal:Zip_Portal2020@10.10.13.14:5000/GeneratePO",
  },
  statistics:{
    turn:"on"
  }
}

// Export the config object based on the NODE_ENV
// ==============================================

module.exports =
  _.merge(all, require("./shared"), require(`./${process.env.NODE_ENV}.js`)) ||
  {}
