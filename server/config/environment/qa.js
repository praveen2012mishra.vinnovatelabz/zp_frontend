'use strict';
/*eslint no-process-env:0*/

// Production specific configuration
// =================================
module.exports = {
    // Server IP
    ip: process.env.OPENSHIFT_NODEJS_IP
        || process.env.ip
        || undefined,

    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT
        || process.env.QAPORT
        || 3000,

    // MongoDB connection options
    mongo: {
        uri: process.env.MONGODB_URI_QA
    },
    saml:{
        saml_entity_id:"analytics-qa.zuelligpharma.com",
        assert_endpoint:"https://analytics-qa.zuelligpharma.com/api/saml/postResponse",
        private_key:"analytics_qa.zuelligpharma.com.key",
        certificate:"analytics_qa.zuelligpharma.com.cert",
        adfs_certificates:"sso-adfs.cert",
        login:"https://sso.zuelligpharma.com/adfs/ls/"
    },
    api:{
        routes:"https://analytics-qa.zuelligpharma.com/",
        API:"https://analytics-qa.zuelligpharma.com/api"
    },
    statistics:{
        turn:process.env.STATS_API_STATUS_QA
    }
};
