'use strict';
/*eslint no-process-env:0*/

// Production specific configuration
// =================================
module.exports = {
    // Server IP
    ip: process.env.OPENSHIFT_NODEJS_IP
        || process.env.ip
        || undefined,

    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT
        || process.env.PRODPORT
        || 3000,

    // MongoDB connection options
    mongo: {
        uri: process.env.MONGODB_URI_PROD
    },
    saml:{
        saml_entity_id:"analytics.zuelligpharma.com",
        assert_endpoint:"https://analytics.zuelligpharma.com/api/saml/postResponse",
        private_key:"analytics.zuelligpharma.com.key",
        certificate:"analytics.zuelligpharma.com.cert",
        adfs_certificates:"sso-adfs.cert",
        login:"https://sso.zuelligpharma.com/adfs/ls/"
    },
    api:{
        routes:"https://analytics.zuelligpharma.com/",
        API:"https://analytics.zuelligpharma.com/api"
    },
    statistics:{
        turn:process.env.STATS_API_STATUS_PROD
    }
};
