const config = require("../config/environment/index")
const pg = require("pg")

// function getConnString() {
//     var port =
//         config.postgres.port === null || config.postgres.port == ""
//             ? "5432"
//             : config.postgres.port
//     var connString =
//         "postgres://" +
//         config.postgres.username +
//         ":" +
//         config.postgres.pwd +
//         "@" +
//         config.postgres.server +
//         ":" +
//         port +
//         "/" +
//         config.postgres.database

//     return connString
// }

// const connString = getConnString()
// export const client = new pg.Client(connString)
const Pool = require('pg').Pool
export const client = new Pool({
  user: config.postgres.username,
  host: config.postgres.server,
  database: config.postgres.database,
  password: config.postgres.pwd,
  port: config.postgres.port
})