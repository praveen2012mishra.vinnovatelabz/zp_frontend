const express = require('express');
const router = express.Router();
const hsts = require('hsts');
router.use(hsts({
    maxAge: 15552000,  // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true
  }))
// Require the controllers WHICH WE DID NOT CREATE YET!!
const logger_controller = require('../controllers/logger.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', logger_controller.test);
router.post('/storeSystemLogs', logger_controller.storeSystemLogs);
router.post('/storeActivityLogs', logger_controller.storeActivityLogs);


module.exports = router;




