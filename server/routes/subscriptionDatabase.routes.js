const express = require('express');
const router = express.Router();

const subscriptionDatabase_controller = require('../controllers/subscriptionDatabase.controller');

router.post('/subsDatabase', subscriptionDatabase_controller.getSubscriptionData);
router.post('/createAnalyticsSubscription', subscriptionDatabase_controller.createAnalyticsSubscription);
// Subscription database from sap hana, uncomment next line
// router.post('/getSubsDatabaseSapHana', subscriptionDatabase_controller.getSubsDatabaseSapHana);
router.get('/test', subscriptionDatabase_controller.test);


module.exports = router;