const express = require("express")
const router = express.Router()
const hsts = require("hsts")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
// Require the controllers WHICH WE DID NOT CREATE YET!!
const group_controller = require("../controllers/group.controller")
const ad_group_controller = require("../controllers/activeDirectory.controller")

// a simple test url to check that all of our files are communicating correctly.
router.get("/test", group_controller.test)
router.get("/getAllGroups", group_controller.getAllGroups)
router.get("/getGroup/:groupName", group_controller.getGroup)
router.post("/createGroup", group_controller.updateGroup)
router.post("/updateGroup", group_controller.updateGroup)
router.post("/deleteGroup", group_controller.deleteGroup)
router.get("/fetchGroupOptions", group_controller.getGroupOptions)
router.post("/updateGroupOptions", group_controller.updateGroupOptions)
router.get("/getGroups/user/:username", group_controller.getGroupForUser)
router.get("/updateInternalAndAdmin", group_controller.updateInternalAndAdmin)

router.get("/getADGroups/user/:username", ad_group_controller.getGroupsForUser)
router.get("/fetchGroupOptions/:mappingId", group_controller.getGroupOptionById)
router.get("/fetchGroupOptions/user/:groupName", group_controller.getGroupOptionByGroupName)
router.get("/fetchGroupUsers/:groupname", ad_group_controller.getUsersForGroup)
router.post("/adGroups", ad_group_controller.searchGroups)


module.exports = router
