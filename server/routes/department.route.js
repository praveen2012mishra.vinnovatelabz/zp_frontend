const express = require("express")
const router = express.Router()
const hsts = require("hsts")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
const departmentController = require("../controllers/departments.controller")
router.post("/createDepartments", departmentController.createDepartments)
router.get("/getAllDepartments", departmentController.getAllDepartments)

router.post("/createOpenings", departmentController.createOpenings)
router.get("/getLatestCurrentOpenings", departmentController.getLatestCurrentOpenings)
router.get("/deleteOpeningsById/:id", departmentController.deleteOpeningsById)
module.exports=router