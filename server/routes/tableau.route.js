const express = require("express")
const router = express.Router()
const hsts = require("hsts")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
// Require the controllers WHICH WE DID NOT CREATE YET!!
const tableau_controller = require("../auth/tableau")

// a simple test url to check that all of our files are communicating correctly.
// Routes for new QR code based 2fa
router.post("/getTableauTicket", tableau_controller.getTableauTicket)

module.exports = router
