const express = require("express")
const router = express.Router()
const hsts = require("hsts")
const notificationController = require("../controllers/notifications.controller")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
router.post("/saveNotifications", notificationController.saveNotification)
// router.post(
//   "/saveGroupNotification",
//   notificationController.saveGroupNotification
// )
router.get(
  "/getNotificationsByUserId/:userId",
  notificationController.getNotificationsByUserId
)

router.get(
  "/getNotificationsGroupName/:groupName",
  notificationController.getNotificationsGroupName
)

router.post(
  "/updateCheckedNotification",
  notificationController.updateCheckedNotification
)

router.post(
  "/saveUserNotification",
  notificationController.saveUserNotificattion
)
router.post('/getNotificationCountByGroups',
notificationController.getNotificationCount
)
module.exports = router
