const express = require('express');
const router = express.Router();
const hsts = require('hsts');
router.use(hsts({
    maxAge: 15552000,  // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true
  }))
// Require the controllers WHICH WE DID NOT CREATE YET!!
const session_management_controller = require('../controllers/sessionManagement.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', session_management_controller.test);
router.get('/getSessionInfobyGroup/:groupName', session_management_controller.getSessionInfobyGroup);
router.get('/getListofUserswithEnforcedSingleSession', session_management_controller.getListofUserswithEnforcedSingleSession);
router.get('/getListofUserswithTwoFactorAuthentication', session_management_controller.getListofUserswithTwoFactorAuthentication);
router.post('/resetQRCode', session_management_controller.resetQRCode);
router.post('/resetSessionCount', session_management_controller.resetSessionCount);
router.get('/checkSSOStatus/:userName', session_management_controller.checkSSOStatus);
router.get('/check2FAStatus/:groupName', session_management_controller.check2FAStatus);
router.post('/changeSingleSessionStatus', session_management_controller.changeSingleSessionStatus);
router.post('/change2FAStatus', session_management_controller.change2FAStatus);


module.exports = router;