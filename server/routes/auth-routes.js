const { controllers } = require('chart.js');

const express = require('express');
const router = express.Router();
const auth_controllers=require("../controllers/auth-controller")

router.get('/test',auth_controllers.test);

//auth login
router.get('/auth',auth_controllers.login);

//logout
// router.get('/logout',(req,res)=>{
//     //handle passport
//     res.send('logging out with microsoft')
// })

// auth with microsoft 
// router.get('/microsoft',(req,res)=>{
//     //handle passport
//     res.send('logging in with microsoft')
// })

module.exports = router;