const express = require("express")
const router = express.Router()
// import authenticateToken from "../auth/jwt/index"
const hsts = require("hsts")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
// Require the controllers WHICH WE DID NOT CREATE YET!!
const statistics_controller = require("../controllers/statistics.controller")

// a simple test url to check that all of our files are communicating correctly.
router.post("/createUserStats", statistics_controller.createUserStats)

module.exports = router
