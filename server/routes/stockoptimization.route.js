const express = require('express');
const router = express.Router();

// Require the controllers WHICH WE DID NOT CREATE YET!!
const stockoptimization_controller = require('../controllers/stockoptimization.controller');


// a simple test url to check that all of our files are communicating correctly.
router.get('/test', stockoptimization_controller.test);
router.get('/getScenarios', stockoptimization_controller.getScenarios);
router.get('/removeScenario', stockoptimization_controller.removeScenario);
router.get('/addScenarios', stockoptimization_controller.addScenarios);
router.get('/getPlant', stockoptimization_controller.getPlant);
router.get('/getATCCode', stockoptimization_controller.getATCCode);
router.get('/getProductCode', stockoptimization_controller.getProductCode);
router.get('/getMaterial', stockoptimization_controller.getMaterial);
router.get('/getMapData', stockoptimization_controller.getMapData);
router.get('/saveForecastData', stockoptimization_controller.saveForecastData);
router.get('/saveData', stockoptimization_controller.saveData);
router.get('/deleteForecast', stockoptimization_controller.deleteForecast);



module.exports = router;