const express = require("express")
const router = express.Router()
const hsts = require("hsts")
const articlesController = require("../controllers/articles.controller")
router.use(
  hsts({
    maxAge: 15552000, // 180 days in seconds
    includeSubDomains: true, // Must be enabled to be approved
    preload: true,
  })
)
router.post("/createArticles", articlesController.createArticles)
router.get("/getAllArticles", articlesController.getAllArticles)
router.get("/getArticlesById/:id", articlesController.getArticlesById)

module.exports = router