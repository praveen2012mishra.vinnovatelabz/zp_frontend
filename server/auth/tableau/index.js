import axios from "axios"
import httpStatus from "http-status"

export const getTableauTicket = async (req, res) => {
  try {
    let userId = req.body.userId
    let serverName=req.body.server
    let type=req.body.type
    if (!userId) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Invalid UserName" })
    }
    let headers = {
      //   "Content-Length": 100,
      //   "Content-Type": "application/json",
      //   "X-Tableau-Auth": "",
    }

    // Hostname or Ip Address of Tableau Server
    // let serverName = "https://192.168.34.16:8850"
    let api = `https://${serverName}.zuelligpharma.com`
    let user = "external.interpharma.local" + "\\" +  userId;
    if(type.toLowerCase() === "internal"){
      user = "zuelligpharma.interpharma.local"+ "\\" +  userId;
    }
    let userIn = user.replace(/'/g, "\\'");
    console.log("the request",{ username:userIn});
    let response = await axios.post(`${api}/trusted`, {
      username:userIn
    })
    if (response) {
      console.log("response",response);
      return res.status(200).json({
        data: response.data
      })
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}
