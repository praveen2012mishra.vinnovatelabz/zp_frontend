"use strict"
var cors = require("cors")

import authenticateToken from "./auth/jwt/index"
const requestIp = require('request-ip');
 
// inside middleware handler
const ipMiddleware = function(req, res, next) {
  const clientIp = requestIp.getClientIp(req); 
  next();
};
export default function (app) {
  // Insert routes below
  // user routes
  const user = require("./routes/user.route") // Imports routes for the users
  app.use("/api/users", user)

  //Admin ######################################################
  // Group routes for admin group management
  const group = require("./routes/group.route") // Imports routes for the group
  app.use("/api/group", authenticateToken, group)

  //session routes for enforced single session and 2 factor list
  const sessions = require("./routes/sessionManagement.route") // Imports routes for the group
  app.use("/api/session", authenticateToken, sessions)

  // Investigator routes
  const investigator = require("./routes/investigator.route") // Imports routes for the investigator
  app.use("/api/investigator", authenticateToken, investigator)

  // Stockoptimization routes
  const stockoptimization = require("./routes/stockoptimization.route") // Imports routes for the investigator
  app.use("/api/stockoptimization", authenticateToken, stockoptimization)

  // Log routes
  const logger = require("./routes/logger.route") // Imports routes for the logs
  app.use("/api/logger", authenticateToken, logger)

  // Settings routes
  const settings = require("./routes/settings.route") // Imports routes for the settings
  app.use("/api/settings", settings)

  // Two factor authentication routes
  const twofactorauth = require("./routes/twofactorauth.route") // Imports routes for the settings
  app.use("/api/two-factor-authentication", authenticateToken, twofactorauth)

  //for login with microsoft
  const mailer = require("./routes/mailer.route") // Imports routes for the settings
  app.use("/api/mail", authenticateToken, mailer)

  const samlAuth = require("./auth/saml/index") // Imports routes for the settings
  app.use("/api/saml", samlAuth)

  //inventory planner
  const supply = require("./routes/supplychain.routes")
  app.use("/api/supplyChain", authenticateToken, supply)

  //notification
  const notifications = require("./routes/notification.routes") // Imports routes for the notifications
  app.use("/api/notification", authenticateToken, notifications)
  //subscription route
  const subs = require("./routes/subscriptionDatabase.routes")
  app.use("/api/subs", authenticateToken, subs)

  //roles route
  const roles = require("./routes/roles.route")
  app.use("/api/roles", authenticateToken, roles)

  //departments
  const dept = require("./routes/department.route")
  app.use("/api/dept", dept)

  //insights
  const insights = require("./routes/articles.routes")
  app.use("/api/insights", insights)

  //statistics
  const statistics = require("./routes/statistics.routes")
  app.use("/api/statistics",ipMiddleware, statistics)
  //insights
  const tableau = require("./routes/tableau.route")
  app.use("/api/tableau", tableau)
}
