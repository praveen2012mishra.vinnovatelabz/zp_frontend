'use strict';

import mongoose, { Schema } from 'mongoose';

var DatabaseLoggerSchema = new Schema({
	collection_name: String,
        operation: String,
        data_modified: Schema.Types.Mixed,
        query: Schema.Types.Mixed,
        timestamp: Date
        
}, { collection: 'DatabaseLogs'},{strict: false });

  
module.exports = mongoose.model('DatabaseLog', DatabaseLoggerSchema);