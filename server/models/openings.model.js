"use strict"

import mongoose, { Schema } from "mongoose"

var OpeningSchema = new Schema(
  {
      title:String,
      description:String,
      scope:String,
      url:String
  },
  { collection: "openings", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("Openings", OpeningSchema)
