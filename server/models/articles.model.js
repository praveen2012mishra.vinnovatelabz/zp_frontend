"use strict"

import mongoose, { Schema } from "mongoose"

var ArticlesSchema = new Schema(
  {
    author: {},
    authorDatabaseId: Number,
    authorId: String,
    categories: {},
    content: String,
    databaseId: Number,
    date: Date,
    dateGmt: Date,
    excerpt: String,
    featuredImage: {},
    id: String,
    postId: Number,
    slug: String,
    status: String,
    tags: {},
    title: String,
    uri: String
  },
  { collection: "articles", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("Articles", ArticlesSchema)


