"use strict"

import mongoose, { Schema } from "mongoose"
const DatabaseLog = require("../models/databaselogger.model")

var NotificationSchema = new Schema(
  {
    userId: String,
    type: {
      type: String,
      required: true,
    },
    checked: {
      type: Object,
      default: false,
    },
    referenceId: {
      type: Number,
    },
    route: String,
    header: {
      type: String,
      required: true,
    },
    article_url:{
      type:String,
      default:""
    },
    details: String,
    data: Object,
    groups: Array,
  },
  { collection: "notifications", timestamps: true, versionKey: false }
)

NotificationSchema.post("save", function (next) {
  var log_record = {}
  log_record.collection_name = "notifications"
  log_record.operation = "update"
  log_record.data_modified = JSON.stringify(this)
  log_record.timestamp = new Date(new Date().toUTCString())

  var log_object = new DatabaseLog(log_record)
  log_object.save()
})

module.exports = mongoose.model("notifications", NotificationSchema)
