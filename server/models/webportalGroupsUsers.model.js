const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let WebportalGroupsUsers = new Schema({
    groupname: String,
    username: String
}, { collection: 'zp_webportal_groups_users', timestamps: true, versionKey: false });

module.exports = mongoose.model('zp_webportal_groups_users', WebportalGroupsUsers);