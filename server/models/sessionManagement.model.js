'use strict';

import mongoose, { Schema } from 'mongoose';
const DatabaseLog = require('../models/databaselogger.model');

var SessionManagerSchema = new Schema({
  group_name: String,
  single_session_enforced: Boolean,
  two_factor_authentication_enforced: Boolean
}, { collection: 'SessionManagement', timestamps: true, versionKey: false });


SessionManagerSchema
  .post('save', function (next) {
    var log_record = {};
    log_record.collection_name = 'SessionManagement';
    log_record.operation = 'update';
    log_record.data_modified = JSON.stringify(this);
    log_record.timestamp = new Date(new Date().toUTCString());


    var log_object = new DatabaseLog(log_record);
    log_object.save();
  });

module.exports = mongoose.model('SessionManager', SessionManagerSchema);
