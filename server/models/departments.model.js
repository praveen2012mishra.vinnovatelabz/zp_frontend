"use strict"

import mongoose, { Schema } from "mongoose"

var DepartmentSchema = new Schema(
  {
    header:String,
    description:String,
    imgURLList:Array
  },
  { collection: "departments", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("Departments", DepartmentSchema)
