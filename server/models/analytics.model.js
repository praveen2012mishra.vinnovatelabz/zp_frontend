"use strict"

import mongoose, { Schema } from "mongoose"

var AnalyticsSchema = new Schema(
  {
      Group:String,
      Product:String,
      Coverage:String,
      Country:String,
      EndDate:String,
      StartDate:String,
      Frequency:String,
      Solution:String,
      Contract:String
  },
  { collection: "Analytics_Website_Subscriptions", timestamps: true, versionKey: false }
)

module.exports = mongoose.model("Analytics_Website_Subscriptions", AnalyticsSchema)
