const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let InvestigatorSchema = new Schema({
    firstName: String,
    lastName: String,
    email: String,
    password: String,
    isAdmin: Boolean
}, { collection: 'investigators', timestamps: true, versionKey: false });