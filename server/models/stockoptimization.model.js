const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let StockoptimizationSchema = new Schema({
    forecast_dates: String,
    forecast_vals: String,
    indicator: [
        { name: String, value: String}
    ],
    ATC_Code: String,
    Plant: String,
    Material_Code: String,
    product: String,
    user_name: String,
    description: String,
    active:Number
}, { collection: 'ScenarioPlanningRuns', timestamps: true, versionKey: false });

module.exports = mongoose.model('Stockoptimization', StockoptimizationSchema);