'use strict';

const  Settings = require('../models/settings.model');



function handleError(res, err) {
    return res.status(500).json(err);
}


export function setMaintenanceSettings (req, res){
    var message = req.body.message;
    var type = req.body.status;
    if (req.body.message != '' || req.body.status != ''){
        Settings.updateOne({}, { $set: { maintenance_message: message, maintenance_mode_status: type } }, function(error, docs){
            if (error) {handleError(res, error)}
            else{
                return res.status(200).json({
                    success: true,
                    message: "Settings updated successfully"
                });
            }
        });
    }
}


export function getMaintenanceSettings (req, res){
    Settings.findOne({}, function(error, docs){
        if (error) {handleError(res, error)}
        else{
            return res.status(200).json({
                success: true,
                message: "Settings fetched successfully",
                data: docs
            });
        }
    });
}

export function setTableauSettings (req, res){
    var password = req.body.password;
    if (req.body.password != ''){
        Settings.updateOne({}, { $set: { tableau_service_password: password } }, function(error, docs){
            if (error) {handleError(res, error)}
            else{
                return res.status(200).json({
                    success: true,
                    message: "Settings updated successfully"
                });
            }
        });
    }
}


export function getTableauSettings (req, res){
    Settings.findOne({}, function(error, docs){
        if (error) {handleError(res, error)}
        else{
            return res.status(200).json({
                success: true,
                message: "Settings fetched successfully",
                data: docs
            });
        }
    });
}
