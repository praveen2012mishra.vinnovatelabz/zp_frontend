const httpStatus = require("http-status");
const Departments = require("../models/departments.model")
const Openings = require("../models/openings.model")


export const createDepartments = async(req,res)=>{
    try{
        const dept = await Departments.create(req.body)
        if(dept){
            return res.status(httpStatus.OK).json({message:"Departments successfully created.", dept})
        }else{
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message:"Please Try Again"})
        }
    }catch(err){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
}

export const getAllDepartments = async(req,res)=>{
    try{
        const dept = await Departments.find({})
        if(dept){
            return res.status(httpStatus.OK).json({message:" All Departments.", dept})
        }else{
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message:"Please Try Again"})
        }
    }catch(err){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
}

export const createOpenings = async(req,res) => {
    try{
        let body = req.body
        let openings = await Openings.create(body)
        if(openings){
            return res.status(httpStatus.OK).json({message:"Openings created successfully", status: true, openings})
        }else{
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message:"No openings found", status: true, openings}) 
        }
    }catch(err){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
}

export const getLatestCurrentOpenings = async(req,res) => {
    try{
        let openings = await Openings.find()
        .sort({createdAt:-1})
        if(openings.length){
            return res.status(httpStatus.OK).json({message:"Openings fetched successfully", status: true, openings})
        }else{
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message:"No openings found", status: true, openings}) 
        }
    }catch(err){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
}
export const deleteOpeningsById = async(req,res) => {
    try{
        let _id = req.params.id
        let openings = await Openings.deleteOne({_id})
        if(openings.length){
            return res.status(httpStatus.OK).json({message:"Openings deleted successfully", status: true, openings})
        }else{
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message:"No openings found", status: true, openings}) 
        }
    }catch(err){
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
}