import request from 'request';
var config = require('../config/environment/index');

export function test(req, res) {
    console.log("hello mailer test");
}

export function automatedMail(req, res) {
    // console.log(req.body);
    var receiverMail = req.body.receiver;
    
    request({
        url: config.zuelligMailer.api,
        method: 'POST',
        headers: {
            sender: config.zuelligMailer.user,
            receiver: receiverMail,
            Authorization: config.zuelligMailer.authorization,
            subject: "Query Submitted, Team Zuellig Pharma", 
            bodytext: "Your resposne has been submitted sucessfully,we will get back to you soon",
            'Content-Type': 'text/html'
        },
    }, function(err, response) {
        if(err) {
            return res.status(500).json({error: err});
        } else {
            return res.status(200).json({
                message: "Automated mail Sent"
            })
        }
    });
}

export function sendInformation(req, res) {
    // console.log(req.body);
    var mailBody = req.body.body;
    var senderMail = req.body.sender;
    var mailSubject = "Product Inquiry";

    let name = mailBody.name;
    let companyName = mailBody.companyName;
    let country = mailBody.country;
    let designation = mailBody.designation
    let contact = mailBody.contact;
    let email = mailBody.email;
    let areaOfIntrest = mailBody.areaOfIntrest;
    let remarks = mailBody.remarks;
    let selectedReceiver =mailBody.reciverName || "NOT SELECTED";

    let msg = `Name = ${name ? name : "NIL"}, Company Name = ${companyName ? companyName : "NIL"}, Country = ${country ? country : "NIL"}, Designation = ${designation ? designation : "NIL"}, Contact = ${contact ? contact : "NIL"}, Email = ${email ? email : "NIL"}, Area Of Intrest = ${areaOfIntrest ? areaOfIntrest.map((index, key) => index) : "NIL"}, Remarks = ${remarks ? remarks : "NIL"}, Contact Person = ${selectedReceiver ? selectedReceiver : "NIL"}`
    request({
        url: config.zuelligMailer.api,
        method: 'POST',
        headers: {
            sender: config.zuelligMailer.user,
            receiver: config.zuelligMailer.user,
            Authorization: config.zuelligMailer.authorization,
            subject: mailSubject, 
            bodytext: msg,
            'content-Type': 'html/text'
        },
    }, function(err, response) {
        if(err) {
            return res.status(500).json({error: err});
        } else {
            console.log(response.statusCode);
            return res.status(200).json({
                message: "Mail Sent"
            })
        }
    });
}
