'use strict';
const SessionManager = require('../models/sessionManagement.model');
const WebportalGroupsUsers = require("../models/webportalGroupsUsers.model")
import httpStatus from 'http-status';
import User from '../models/user.model';
// import {getGroupsForSessionManagement} from "./activeDirectory.controller"
const ActiveDirectory = require('activedirectory2');
const mainConfig = require("../config/environment/index")
const config = mainConfig.ldap.internal;
const configExternal = mainConfig.ldap.external;
const Group = require("../models/group.model")

function handleError(res, err) {
  return res.status(500).json(err);
}

export function changeSingleSessionStatus(req, res) {
  var group = req.body.group_name;
  var type = req.body.type;
  if (type == "enable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.single_session_enforced = true;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });

        }
        else {
          var sessionInformation = new SessionManager({ group_name: group, single_session_enforced: true, two_factor_authentication_enforced: false });

          // save model to database
          sessionInformation.save(function (er, groupInfo) {
            if (er) { handleError(res, er) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInfo.group_name
              });
            }

          });
        }
      }
    })

  }
  else if (type == "disable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.single_session_enforced = false;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });
        }
        else {
          return res.status(200).json({
            success: false,
            group: group
          });
        }
      }
    })
  }
}



export function change2FAStatus(req, res) {
  var group = req.body.group_name;
  var type = req.body.type;

  if (type == "enable") {
    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.two_factor_authentication_enforced = true;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });

        }
        else {
          var sessionInformation = new SessionManager({ group_name: group, single_session_enforced: false, two_factor_authentication_enforced: true });

          // save model to database
          sessionInformation.save(function (er, groupInfo) {
            if (er) { handleError(res, er) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInfo.group_name
              });
            }

          });
        }
      }
    })
  }
  else if (type == "disable") {

    SessionManager.findOne({ 'group_name': group }, (err, doc) => {
      if (err) { handleError(res, err) }
      else {
        if (doc) {
          doc.two_factor_authentication_enforced = false;
          // save model to database
          doc.save(function (error, groupInformation) {
            if (error) { handleError(res, error) }
            else {
              return res.status(200).json({
                success: true,
                group: groupInformation.group_name
              });
            }

          });
        }
        else {
          return res.status(200).json({
            success: false,
            group: group
          });
        }
      }
    })
  }

}

//Simple version, without validation or sanitation
export function test(req, res) {
  res.send('Greetings from the session');
};


export function getSessionInfobyGroup(req, res) {
  SessionManager.find({ 'group_name': req.params.groupName }, (err, group) => {
    if (err) { handleError(res, err) }
    return res.status(200).json(group);
  })

};

export function checkSSOStatus(req, res) {
  var user_name = req.params.userName;
  // groupsAD.then((res,rej)=>console.log(res,rej)).catch(err=>console.log(err))
  SessionManager.find({ 'single_session_enforced': true }, async (err, groups) => {
    if (err) {
      handleError(res, err);
    }
    else {
      if (groups.length > 0) {
        
        let group_name_array = groups.map(v => v.group_name);
        // let results = await WebportalGroupsUsers.find({ username: `${user_name}` },{ groupname: 1 })
        try {
          let pickedConfig = config;
          if (user_name) {
              let sAMAccountName = user_name;
              if (sAMAccountName.includes("-") || sAMAccountName.includes("_")) {
                  pickedConfig = configExternal;
              }
  
              const mongoGroups = await Group.find({}, function (err, result) {
                  if (err) {
                      return []
                  } else {
                      return result;
                  }
              });
              var ad = new ActiveDirectory(pickedConfig);
              ad.getGroupMembershipForUser(sAMAccountName, function (err, groups) {
                  if (err) {
                      return res
                          .status(httpStatus.UNPROCESSABLE_ENTITY)
                          .json({
                              status: 406,
                              ok: false,
                              message: "Please try again later",
                              data: { Error: err }
                          }).send('not enforced');
                  }
                  if (!groups) {
                      return res
                          .status(httpStatus.UNPROCESSABLE_ENTITY)
                          .json({
                              status: 404,
                              ok: false,
                              message: 'User: ' + sAMAccountName + ' not found.',
                              data: {}
                          }).send('not enforced');
                  }
                  else {
                      // let groupMongo = updatedGrp();
                      let groupsMap = new Map(mongoGroups.map(i => [(i.group_name ? i.group_name : '').toLowerCase(), i]));
                      let groupNames = []
                      if (groups.length > 0) {
                          for (const group of groups) {
      
                              let mdGroup = groupsMap.get(group.cn.toLowerCase());
                              if (mdGroup) {
                                  let groupObj= mdGroup.toObject();
                                  let gName = { groupname: group.cn}
                                  groupNames.push(gName)
                              }
                          }
                      }
                      if (groupNames.length > 0) {
                        let user_groups_array = groupNames.map(g => g.groupname);
                        let found = group_name_array.some(r => user_groups_array.indexOf(r) >= 0)
                        if (found) {
                          return res.send('enforced');
                        } else {
                          return res.send('not enforced');
                        }
                      }else{
                        return res.send('not enforced');
                      }
                  }
              });
          } else {
              res
                  .status(httpStatus.UNPROCESSABLE_ENTITY)
                  .json({
                      status: 406,
                      ok: false,
                      message: "Please Provide a username",
                      data: {}
                  }).send('not enforced')
          }
      } catch (err) {
          res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
              status: 500,
              ok: false,
              message: "Internal Server Error",
              data: {}
          }).send('not enforced')
      }
        // try {
        //   if (results.length > 0) {
        //     let user_groups_array = results.map(g => g.groupname);
        //     let found = group_name_array.some(r => user_groups_array.indexOf(r) >= 0)

        //     if (found) {
        //       return res.send('enforced');
        //     } else {
        //       return res.send('not enforced');
        //     }
        //   }
        //   else {
        //     return res.send('not enforced');
        //   }
        // } catch { return res.send('not enforced') }
      }
      else {
        return res.send('not enforced');
      }
    }
  });
}


// export function check2FAStatus(req, res) {
//   var user_name = req.params.userName;


//   SessionManager.find({ 'two_factor_authentication_enforced': true }, async (err, groups) => {
//     if (err) {
//       handleError(res, err);
//     }
//     else {
//       if (groups.length > 0) {
//         var group_name_array = groups.map(v => v.group_name);
//         let result = await WebportalGroupsUsers.find({
//           username: user_name
//         }, { groupname: 1 })
//         if (result.length > 0) {
//           var user_groups_array = result.map(g => g.groupname);
//           var found = group_name_array.some(r => user_groups_array.indexOf(r) >= 0)
//           if (found) {
//             return res.status(200).json({ enforced: true });
//           } else {
//             return res.status(200).json({ enforced: false });
//           }
//         }
//         else {
//           return res.status(200).json({ enforced: false });
//         }
//       }
//       else {
//         return res.status(200).json({ enforced: false });
//       }
//     }
//   });
// }

export function check2FAStatus(req, res) {
  var group_name = req.params.groupName;

 // BUG This should be req.params.groupName not req.params.userName

  SessionManager.find({"group_name":group_name}, async (err, groups) => {
    if (err) {
      handleError(res, err);
    }
    else {
      // console.log(groups);
      if (groups.length>0 && groups[0].two_factor_authentication_enforced) {
            return res.status(200).json({ enforced: true });
        }
        else {
          console.log("else");
          return res.status(200).json({ enforced: false });
        }
      }
  });
}


export function getListofUserswithTwoFactorAuthentication(req, res) {
  try {
    SessionManager.find({ 'two_factor_authentication_enforced': true }, async (err, groups) => {
      if (err) { handleError(res, err) }
      else {
        var enforced_groups = groups.map(g => g.group_name);
        let result = await WebportalGroupsUsers.find({
          groupname: { $in: enforced_groups }
        }, { username: 1 })
        var all_users_array = result.map(u => u.username);
        var unique_users = all_users_array.filter(function (elem, index, self) {
          return index == self.indexOf(elem);
        })
        return res.status(200).json(unique_users);
      }
    })
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}



export function resetQRCode(req, res) {
  var users = req.body;
  let error = null;
  for (let i = 0; i < users.length; i++) {
    let username = users[i];
    User.update({ name: username }, {
      otp_auth_url: '',
      secret: ''
    }, function (err, affected, resp) {
      if (err) {
        error = err;
      }
    })

  }
  if (error !== null) {
    return res.status(401).json({
      error: {
        message: 'Error in updating auth information',
        statusCode: 401
      }
    });
  }
  else {
    return res.status(200).json({
      success: true
    });
  }
}



export function getListofUserswithEnforcedSingleSession(req, res) {
  SessionManager.find({ 'single_session_enforced': true }, async (err, groups) => {
    if (err) { handleError(res, err) }
    else {
      let enforced_groups = groups.map(g => g.group_name);
      let result = await WebportalGroupsUsers.find({
        groupname: { $in: enforced_groups }
      },{username:1})
      try {
        let all_users_array = result.map(u => u.username);
        let unique_users = all_users_array.filter(function (elem, index, self) {
          return index == self.indexOf(elem);
        })
        return res.status(200).json(unique_users);
      } catch {
        return res.status(500).json({ msg: "Something went wrong!" })
      }
    }
  })
}


export function resetSessionCount(req, res) {
  var users = req.body;
  let errors = null;
  for (let i = 0; i < users.length; i++) {
    let username = users[i];
    User.update({ name: username }, {
      active: false
    }, function (err, affected, resp) {
      if (err) {
        errors = err;
      }
    })

  }
  if (errors !== null) {
    return res.status(401).json({
      error: {
        message: 'Error in updating session information',
        statusCode: 401
      }
    });
  }
  else {
    return res.status(200).json({
      success: true
    });
  }
}




