import httpStatus from "http-status"
const Article = require("../models/articles.model")

export const createArticles = async (req, res) => {
  try {
    let body = req.body.data
    let articles = await Article.find({})
    let articlesInDb = []
    articles.map(item => {
      articlesInDb.push(item.id)
    })
    let createdArticlesArr = []
    for (let i = 0; i < body.length; i++) {
      if (!articlesInDb.includes(body[i].id)) {
        let createArticles = await Article.create(body[i])
        createdArticlesArr.push(createArticles)
      }
    }
    return res.status(httpStatus.OK).json({
      message: "Articles Successfully Created.",
      articles: createdArticlesArr,
      updatedCount: createdArticlesArr.length,
      status: true,
    })
  } catch (err) {
    return res
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ err, status: false })
  }
}

export const getAllArticles = async (req, res) => {
  try {
    let articles = await Article.find({})
    if (articles) {
      return res.status(httpStatus.OK).json({
        message: "Articles fetched successfully",
        articles,
        status: true,
      })
    } else {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No Articles Found", status: false })
    }
  } catch (err) {
    return res
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ err, status: false })
  }
}

export const getArticlesById = async (req, res) => {
  try {
    let id = req.params.id
    let articles = await Article.find({ id })
    if (articles) {
      return res.status(httpStatus.OK).json({
        message: "Article fetched successfully",
        articles,
        status: true,
      })
    } else {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Please Try Again", status: false })
    }
  } catch (err) {
    return res
      .status(httpStatus.INTERNAL_SERVER_ERROR)
      .json({ err, status: false })
  }
}
