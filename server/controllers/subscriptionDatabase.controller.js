import httpStatus from 'http-status';
import { endsWith } from 'lodash';
const AnalyticsSubscription = require('../models/analytics.model');
const multer = require('multer');
const upload = multer();
var crypto = require('crypto');

var hdb    = require('hdb');
var shell = require('shelljs');

function getConnString() {
    var port = '32415';
    var host = 'zpsgbdphanadb';
    var user = 'ZP_CSM';
    var pwd = 'ZPcsmhana02';

    var connString = { host: host, port: port, user: user, password: pwd };
    return connString;
}

export async function getSubscriptionData(req, res) {
    let currentDate = new Date();
    let d = new Date();

    // current date
    let dd = String(currentDate.getDate()).padStart(2, '0');
    let mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    let yyyy = currentDate.getFullYear();
    currentDate = yyyy + '-' + mm + '-' + dd;
    // date of months from now

    try {
        let Group = req.body.groupName;
    if(Group) {
       let result = await AnalyticsSubscription.find({Group})
    if(result){
        let subs= [];
                    if(result[0]){
                        result.forEach(element => {
                            let sub = {}
                            sub.group = element.Group || "";
                            sub.product = element.Product|| "";
                            sub.coverage = element.Coverage|| "";
                            sub.country = element.Country || "";
                            sub.startDate = element.StartDate || "";
                            sub.endDate = element.EndDate || "";
                            sub.frequency = element.Frequency || "";
                            sub.solution = element.Solution || "";

                            // make logic for
                            const dateChecker = () => {
                                let status = "";
                                if(currentDate > sub.endDate) {
                                    status = "expired"
                                } else if (currentDate > sub.startDate && currentDate < sub.endDate) {
                                    status = "Active"
                                } else if (currentDate) {
                                    status = "Ending soon"
                                }
                                else if (sub.startDate > currentDate) {
                                    status = "Active Soon"
                                }
                                return status;
                            }

                            sub.status = dateChecker() || "";
                            subs.push(sub);

                        });

                    };
                 return res.status(httpStatus.OK).json({message: "Subscription data", subs, status: true})
    }else{
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message: "Please Try again", status: false})
    }
    }else{
        return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message: "Group name invalid", status: false})
    }
    } catch (error) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({message: "Internal server error", status: false})
    }
    
}

export const createAnalyticsSubscription = async (req, res) => {
    try {
        let body = req.body
        let createSubs = await AnalyticsSubscription.create(body)
        if (createSubs) {
            return res.status(httpStatus.OK).json({message: "Analytics created sucessfully", data: createSubs, status: true})
        } else {
            return res.status(httpStatus.UNPROCESSABLE_ENTITY).json({message: "Please try again", status: false})
        }
    } catch (error) {
        return res.status(httpStatus.INTERNAL_SERVER_ERROR).json({message: "Internal server error", status: false})
    }
}

export const getSubsDatabaseSapHana = (req, res) => {
    // var connString = getConnString();
    // var client = new hdb.createClient(connString);
    // let currentDate = new Date();
    // let d = new Date();

    // // current date
    // let dd = String(currentDate.getDate()).padStart(2, '0');
    // let mm = String(currentDate.getMonth() + 1).padStart(2, '0'); //January is 0!
    // let yyyy = currentDate.getFullYear();
    // currentDate = yyyy + '-' + mm + '-' + dd;

    // let Group = req.body.groupName;

    // client.connect(function (err) {
    //     if (err) { handleError(res, err)
    //         console.log("31",err);
    //     }
    //     else {
    //         console.log(req.body);
    //         //let ar = ['WEBPORTAL-ZPDEMO', 'WEBPORTAL-ADMIN']
    //         let sqlQuery = `Select * from "ZP_ANALYTICS"."Analytics_Website_Subscriptions" where "Group" IN (\'${Group}\')`
    //         // + '\'' + group_names +  '\'';
    //         //let sqlQuery = `Select * from "ZP_ANALYTICS"."Analytics_Website_Subscriptions"`
            
    //         client.exec(sqlQuery, function (err, result) {
    //             if (err) { handleError(res, err)
    //                 console.log("37", err);
    //             }
    //             else {
    //                 let subs= [];
    //                 if(result[0]){
    //                     result.forEach(element => {
    //                         let sub = {}
    //                         sub.group = element.Group || "";
    //                         sub.product = element.Product|| "";
    //                         sub.atc = element.ATC|| "";
    //                         sub.country = element.Country || "";
    //                         sub.startDate = element.StartDate || "";
    //                         sub.endDate = element.EndDate || "";
    //                         sub.frequency = element.Frequency || "";
    //                         sub.solution = element.Solution || "";

    //                         // make logic for
    //                         const dateChecker = () => {
    //                             let status = "";
    //                             if(currentDate > sub.endDate) {
    //                                 status = "expired"
    //                             } else if (currentDate > sub.startDate && currentDate < sub.endDate) {
    //                                 status = "Active"
    //                             } else if (currentDate) {
    //                                 status = "Ending soon"
    //                             }
    //                             else if (sub.startDate > currentDate) {
    //                                 status = "Active Soon"
    //                             }
    //                             return status;
    //                         }

    //                         sub.status = dateChecker() || "";
    //                         subs.push(sub);

    //                     });

    //                 };

    //                 res.status(200).json(subs);

    //                 /*
    //                 client.exec(sqlDirect, function (error, data) {
    //                     if (error) { hamdleError(res, error) }
    //                     else{
    //                         client.exec(sqlInirect, function (error, inDirect) {
    //                             if (error) { handleError(res, error) }
    //                             else {
    //                                 client.end();
    //                                 return res.status(200).json({ result: data, inDirectdata: inDirect })
    //                             }
    //                         })
    //                     }
    //                 })
    //                 */
    //             }

    //         })
    //     }
    // })
}
export function test(req, res) {
    res.send('Greetings from the Subscription controller!');

};


function handleError(res, err) {
    return res.status(500).json(err);
}

