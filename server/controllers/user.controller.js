import { SSL_OP_ALLOW_UNSAFE_LEGACY_RENEGOTIATION } from "constants"

const User = require("../models/user.model")
const GroupOptions = require("../models/groupOptions.model")
const multer = require("multer")
const upload = multer()
var crypto = require("crypto")
const httpStatus = require("http-status")

//Simple version, without validation or sanitation
export function test(req, res) {
  res.send("Greetings from the User controller!")
}

export const getUserProfileByName = async (req, res) => {
  try{
   const userName = req.params.userName
  if (userName) {
    await User.findOne({ name: userName }, (err, resp) => {
      if (err) {
        console.log(err)
      } else {
        if(resp){
        resp.firstName = resp.firstName || "";
        resp.lastName = resp.lastName || "";
        resp.profile_pic = resp.profile_pic || "";
        resp.time_locale = resp.time_locale || {
          country:"",
          time_zone:""
        }
        return res.status(200).json({
          success: true,
          userProfile: resp,
        })
        } 
      }
    })
  } 
  }catch(err){
    return res.status(500).json(err)
  }
}

export const updateUserProfile = async (req, res) => {
  let body = req.body
  if (body.profileData._id)
    User.findByIdAndUpdate(
      { _id: body.profileData._id },
      body.profileData,
      function (err, resp) {
        if (err) {
          console.log("222", err)
        } else {
          return res
            .status(200)
            .json({ status: true, message: "Profile Updated Successfully" })
        }
      }
    )
}

export const saveUserRoles = async (req, res) => {
  try {
    if (req.body.userName && req.body.groupName && req.body.roleName && req.body.email) {
      let name_id = req.body.userName;
      let group_id = req.body.groupName;
      let role_id = req.body.roleName;
      let email_id = req.body.email;

      if(role_id != "Basic User"){
        GroupOptions.findOne({ group_name: group_id }, (err, doc) => {
          if (err) {
            return res.status(500).json(err)
          } else {
            if (doc) {
              let role = {role:role_id,userId:name_id}
              let roles = [];
              if(doc.userRoles && doc.userRoles.length >0){
                roles = doc.userRoles;
              }
              roles.push(role);
              doc.userRoles=roles
              doc.save((err, data) => {
                if (err) {
                  handleError(data, err)
                } else {
                  console.log("success saving roles");
                }
              })
            }
          }
        });

      }

      User.findOne({
        name: name_id
      }, function (err, user) {
        if (err) {
          return res
            .status(httpStatus.UNPROCESSABLE_ENTITY)
            .json( {
              status:406,
              ok:false,
              message:"Please try again later",
              data:{Error:err}
            });
        }
        else if (user !== null)
        {     let currentRoles = user.roles|| [];
          currentRoles = currentRoles.filter(v => v.groupId !== group_id)
          currentRoles.push({groupId:group_id, role:role_id});
          user.set({ roles: currentRoles})
          user.save(function (err, updatedUser) {
            if (err){
              return res
                .status(httpStatus.UNPROCESSABLE_ENTITY)
                .json( {
                  status:406,
                  ok:false,
                  message:"Please try again later",
                  data:{Error:err}
                });
            }
            else{
              res.status(httpStatus.OK).json({
                status:200,
                ok:true,
                message:"Sucess Updating Saving user Role",
                data:updatedUser
              });
            }
          } );


        } else {
          let newUserRole = [{groupId:group_id, role:role_id}]
          var newUser = new User({
            name: name_id,
            provider: 'activedirectory',
            active: false,
            email:email_id,
            roles:newUserRole
          })
          newUser.save(
            function (err) {
              if (err) {
                return res
                  .status(httpStatus.UNPROCESSABLE_ENTITY)
                  .json( {
                    status:406,
                    ok:false,
                    message:"Please try again later",
                    data:{Error:err}
                  });
              }
              res.status(httpStatus.OK).json({
                status:200,
                ok:true,
                message:"Sucess Updating Saving user Role",
                data:newUser
              });
            })
        }
      })

    } else {
      res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json( {
          status:406,
          ok:false,
          message:"Required data missing",
          data:{}
        })
    }
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
      status:500,
      ok:false,
      message:"Internal Server Error",
      data:{}
    })
  }

}



function migrateUserRoles(){
  User.find({}, (err, users) => {
    if (err) {
      console.log("error")
    }
    for (const user of users) {
      user.roles = [{groupId : 'default',role:'Basic User'}];
      user.save((err, data) => {
        if(err){console.log("error saving data")}
      })

    }
  })
}
export const updateSupplyChainAdmin= async (req,res)=>{
  let userName=req.body.userName;
  let state=req.body.adminState;

  await User.findOneAndUpdate({name:userName},{$set:{isSupplyChainAdmin:state}},(err,data)=>{
    if(err){
      res.status(500).json(err)
    }else{
      res.status(200).json({
        previousState:data ? data.isSupplyChainAdmin : null
      })
    }
  })
}
export const getSupplyChainAdmin= async (req,res)=>{
  let userName=req.params.userName;

  await User.findOne({name:userName},(err,data)=>{
    if(err){
      res.status(500).json(err)
    }else{
      res.status(200).json({
        isSupplyChainAdmin:data?data.isSupplyChainAdmin || false : false
      })
    }
  })
}
//migrateUserRoles();

