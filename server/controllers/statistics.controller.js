const config = require("../config/environment/index")
import { client } from "../services/postgresCon"
const httpStatus = require("http-status")
// import moment from "moment"

function handleError(res, err) {
  return res.status(500).json(err.message)
}
let turn=config.statistics.turn
// function uuidv4() {
//   return "xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx".replace(/[xy]/g, function (c) {
//     var r = (Math.random() * 16) | 0,
//       v = c == "x" ? r : (r & 0x3) | 0x8
//     return v.toString(16)
//   })
// }
export const createUserStats = (req, res) => {
  let update = async () => {
    // let randomId = uuidv4()
    let ip = req.headers['x-forwarded-for'] ||
      req.connection.remoteAddress ||
      req.socket.remoteAddress ||
      (req.connection.socket ? req.connection.socket.remoteAddress : null);
    let data = req.body
    let userId = data.userId || `guest_${ip}`
    let dateTime = new Date().getTime()
    let service = data.service
    let subService = data.subService
    let adGroup = data.adGroup
    let browserIp = ip || "0.0.0.0"
    client.query(
      `insert into "ZiPPortal".analytics_user_stats("userId", "dateTime", "service", "subService", "browserIp", "adGroup") VALUES ($$${userId}$$,$$${dateTime}$$,$$${service}$$,$$${subService}$$,$$${browserIp}$$,$$${adGroup}$$)`,
      (err, ress) => {
        if (err) {
          return res
            .status(httpStatus.UNPROCESSABLE_ENTITY)
            .json({ status: false, err: err.message })
        } else {
          // console.log(datas);
          return res
            .status(httpStatus.OK)
            .json({ status: true, data: ress })
        }
      })
  }
  if(turn.toLowerCase()==="on") update()
  else return res.status(200).json({status:"off"})
}
