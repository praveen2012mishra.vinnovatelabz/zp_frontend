"use strict"
const ActiveDirectory = require('activedirectory2');
const httpStatus = require("http-status");
const UserModel = require("../models/user.model");
const Roles = require("../models/roleManagement.model")
const mainConfig = require("../config/environment/index")
const config = mainConfig.ldap.internal;
const configExternal = mainConfig.ldap.external;
const Group = require("../models/group.model")
// const _ =require("lodash")

//Use this to get the groups every user is member of
export const getGroupsForUser = async (req, res) => {
    try {
        let pickedConfig = config;
        if (req.params.username) {
            let sAMAccountName = req.params.username;
            if (sAMAccountName.includes("-") || sAMAccountName.includes("_")) {
                pickedConfig = configExternal;
            }

            const mongoGroups = await Group.find({}, function (err, result) {
                if (err) {
                    return []
                } else {
                    return result;
                }
            });
            var ad = new ActiveDirectory(pickedConfig);
            ad.getGroupMembershipForUser(sAMAccountName, function (err, groups) {
                if (err) {
                    return res
                        .status(httpStatus.UNPROCESSABLE_ENTITY)
                        .json({
                            status: 406,
                            ok: false,
                            message: "Please try again later",
                            data: { Error: err }
                        });
                }
                if (!groups) {
                    return res
                        .status(httpStatus.UNPROCESSABLE_ENTITY)
                        .json({
                            status: 404,
                            ok: false,
                            message: 'User: ' + sAMAccountName + ' not found.',
                            data: {}
                        });
                }
                else {
                    let groupsMap = new Map(mongoGroups.map(i => [(i.group_name ? i.group_name : '').toLowerCase(), i]));
                    let groupNames = []
                    if (groups.length > 0) {
                        for (const group of groups) {

                            let mdGroup = groupsMap.get(group.cn.toLowerCase());
                            if (mdGroup) {
                                let groupObj = mdGroup.toObject();
                                let gName = {
                                    group_name: group.cn,
                                    _id: groupObj._id,
                                    displayName: groupObj.display_name,
                                    isAdmin: groupObj.isAdmin,
                                    isInternal: groupObj.isInternal,
                                    country:groupObj.country
                                }
                                groupNames.push(gName)
                            }
                        }
                    }
                    res.status(httpStatus.OK).json({
                        status: 200,
                        ok: true,
                        message: "groups fetched",
                        data: { groupnames: groupNames }
                    });
                }
            });
        } else {
            res
                .status(httpStatus.UNPROCESSABLE_ENTITY)
                .json({
                    status: 406,
                    ok: false,
                    message: "Please Provide a username",
                    data: {}
                })
        }
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 500,
            ok: false,
            message: "Internal Server Error",
            data: {}
        })
    }
}

export const getUsersForGroup = async (req, res) => {
    try {
        const mongoUsers = await UserModel.find({}, function (err, result) {
            if (err) {
                return []
            } else {
                return result;
            }
        });
        if (mongoUsers.length == 0) {
            res
                .status(httpStatus.UNPROCESSABLE_ENTITY)
                .json({
                    status: 406,
                    ok: false,
                    message: "Please try again later",
                    data: { Error: err }
                });
        }

        let usersMap = new Map(mongoUsers.map(i => [i.name, i]));

        const rolePermissions = await Roles.find({}, function (err, result) {
            if (err) {
                return []
            } else {
                return result;
            }
        });

        if (rolePermissions.length == 0) {
            res
                .status(httpStatus.UNPROCESSABLE_ENTITY)
                .json({
                    status: 406,
                    ok: false,
                    message: "Please try again later",
                    data: { Error: err }
                });
        }

        let pickedConfig = config;

        if (req.params.groupname) {
            let groupname = req.params.groupname;
            if (groupname.includes("EX Tab") || groupname.includes("WEBPORTAL")) {
                pickedConfig = configExternal;
            }
            var ad = new ActiveDirectory(pickedConfig);
            ad.getUsersForGroup(groupname, function (err, Users) {
                if (err) {
                    return res
                        .status(httpStatus.UNPROCESSABLE_ENTITY)
                        .json({
                            status: 406,
                            ok: false,
                            message: "Please try again later",
                            data: { Error: err }
                        });
                }
                if (!Users) {
                    return res
                        .status(httpStatus.UNPROCESSABLE_ENTITY)
                        .json({
                            status: 404,
                            ok: false,
                            message: 'Group: ' + groupname + ' not found.',
                            data: {}
                        });
                }
                else {
                    let userData = []
                    if (Users.length > 0) {
                        for (const user of Users) {
                            let rolesIn = []
                            let onZip = false;
                            let mdUser = usersMap.get(user.sAMAccountName);
                            let isSupplyChainAdmin=false
                            if (mdUser) {
                                onZip = true;
                                rolesIn = mdUser.roles;
                                isSupplyChainAdmin=mdUser.isSupplyChainAdmin
                            }
                            userData.push({
                                onZip: onZip,
                                name: user.displayName,
                                _id: user.sAMAccountName,
                                email: user.mail,
                                roles: rolesIn,
                                isSupplyChainAdmin:isSupplyChainAdmin
                            })
                        }
                    }
                    res.status(httpStatus.OK).json({
                        status: 200,
                        ok: true,
                        message: "groups fetched",
                        data: { userData: userData, rolePermissions: rolePermissions }
                    });
                }
            });
        } else {
            res
                .status(httpStatus.UNPROCESSABLE_ENTITY)
                .json({
                    status: 406,
                    ok: false,
                    message: "Please Provide a username",
                    data: {}
                })
        }
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 500,
            ok: false,
            message: "Internal Server Error",
            data: {}
        })
    }

}


export const searchGroups = async (req, res) => {

    try {
        let groupNames = [];
        let keyword = req.body.keyword;
        let query = 'cn=*tab*';
        if (keyword) {
            query = 'cn=*' + keyword + '*';
        }
        let _ = require('underscore');
        let opts = {
            includeMembership: ['group'], // Optionally can use 'all'
            includeDeleted: false
        };
        let pickedConfig = config;
        let type = req.body.type;
        if (type == "external") {
            pickedConfig = configExternal;
        }
        var ad = new ActiveDirectory(pickedConfig);
        ad.find(query, function (err, results) {
            if ((err) || (!results)) {
                return res
                    .status(httpStatus.UNPROCESSABLE_ENTITY)
                    .json({
                        status: 406,
                        ok: false,
                        message: "Please try again later",
                        data: { Error: err }
                    });
            }
            else {
                _.each(results.groups, function (group) {
                    groupNames.push(group.cn);
                });
                res.status(httpStatus.OK).json({
                    status: 200,
                    ok: true,
                    message: "groups fetched",
                    data: { groupNames: groupNames }
                });
            }
        });
    } catch (err) {
        res.status(httpStatus.INTERNAL_SERVER_ERROR).json({
            status: 500,
            ok: false,
            message: "Internal Server Error",
            data: {}
        })
    }
}

//Use this method to get all the users for a group
function getcheckGroupUsersTest(configIn, name) {
    var groupName = name;
    var ad = new ActiveDirectory(configIn);
    ad.getUsersForGroup(groupName, function (err, users) {
        if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            return;
        }

        if (!users) console.log('Group: ' + groupName + ' not found.');
        else {
            for (const user of users) {
                console.log(JSON.stringify(user.sAMAccountName));
            }
        }
    });
}

//Use this method to get all the groups of a user.
function getGroupsTest(configIn, name) {
    var sAMAccountName = name;
    var ad = new ActiveDirectory(configIn);
    ad.getGroupMembershipForUser(sAMAccountName, function (err, groups) {
        if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            return;
        }
        if (!groups) console.log('User: ' + sAMAccountName + ' not found.');
        else {
            for (const group of groups) {
                console.log(JSON.stringify(group.cn));
            }
        }
    });
}

//Use this method to get all the groups of a user.
function getMembershipGroupsTest() {
    var groupName = 'SGRO Analytics Users';
    var ad = new ActiveDirectory(config);
    ad.getGroupMembershipForGroup(groupName, function (err, groups) {
        if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            return;
        }

        if (!groups) console.log('Group: ' + groupName + ' not found.');
        else console.log(JSON.stringify(groups));
    });
}

//Get all groups.
function getAllGroupsTest(configIn) {
    var _ = require('underscore');
    var query = 'cn=*';
    var opts = {
        includeMembership: ['group'], // Optionally can use 'all'
        includeDeleted: false
    };
    var ad = new ActiveDirectory(configIn);
    ad.find(query, function (err, results) {
        if ((err) || (!results)) {
            console.log('ERROR: ' + JSON.stringify(err));
            return;
        }
        _.each(results.groups, function (group) {
            console.log('  ' + group.cn);
        });
    });
}

//Get all groups v2.
function getAllGroupsV2Test(configIn) {
    let _ = require('underscore');
    var query = 'CN=*Admin*';
    var ad = new ActiveDirectory(config);
    ad.findGroups(query, function (err, groups) {
        if (err) {
            console.log('ERROR: ' + JSON.stringify(err));
            return;
        }

        if ((!groups) || (groups.length == 0)) console.log('No groups found.');
        else {
            _.each(groups, function (group) {
                console.log('findGroups: ' + group.cn);
            });

        }
    });
}
