"use strict"
const Roles = require("../models/roleManagement.model")
const httpStatus = require("http-status")

export const saveRoles = async (req, res) => {
  const body = req.body
  try {
    if (body.solution || body.roles || body.modules) {
        
        let roles = {
            modules:body.modules,
            options:["Read","Write","Read and Write","approve"],
            roles:body.roles,
          };

          Roles.findOneAndUpdate(
            {solution:body.solution}, // find a document with that filter
            roles, // document to insert when nothing was found
            {upsert: true, new: true, runValidators: true}, // options
            function (err, doc) { // callback
                if (err) {
                    return res
                    .status(httpStatus.UNPROCESSABLE_ENTITY)
                    .json(
                       {
                           status:406,
                           ok:false,
                           message:"Please try again",
                           data:{error:err}
                          } 
                   )
                } else {
                    res.status(httpStatus.OK).json({
                        status:200,
                        ok:true,
                        message:"module saved",
                        data:{}
                       })
                }
            }
        );
    } else {
      res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Missing requried data" })
    }
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const getRolesBySolution = async (req, res) => {
    const solution = req.params.solution
    try {
      if (!solution) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "No data found" })
      } else {
        const roles = await Roles.find({ solution });
        if (roles.length> 0) {
          return res.status(httpStatus.OK).json({
            status:200,
            ok:true,
            message:"Data successfully fetched",
            data:roles
           })
        } else {
          return res
            .status(httpStatus.OK)
            .json({
                status:404,
                ok:false,
                message:"No role data found",
                data:[]
               } )
        }
      }
    } catch (err) {
      return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
    }
  }












