const stockoptimization = require('../models/stockoptimization.model');
const multer = require('multer');
const upload = multer();
var crypto = require('crypto');

var hdb    = require('hdb');
var dateFormat = require('dateformat');
var in_array = require('in_array');
const axios = require('axios').default;

var mongoose = require('mongoose');
//var URL = 'http://10.10.13.14:5000';
var URL = 'http://localhost:5000';



//Simple version, without validation or sanitation
export function test(req, res) {
    res.send('Greetings from the stockoptimization controller!');
};

export function getScenarios(req, res) {
    let atcCode = req.body.atcCode;
    let plant = req.body.plant;
    let productCode = req.body.productCode;
    let materialCode = req.body.materialCode;
    return Stockoptimization.find({"ATC_Code":atcCode, "Plant":plant, "Material_Code":materialCode, "product":productCode}).sort({"createdAt": -1}).exec()
    .then(datas => {
        res.status(200).json(datas);
    })
    .catch(handleError(res));
}

export function removeScenario(req, res) {
    let id = req.body.id;    
    return Stockoptimization.remove({"_id":id}).exec()
    .then(datas => {
        res.status(200).json(datas);
    })
    .catch(handleError(res));
}

export function addScenarios(req, res) {
    var newData = new Stockoptimization(req.body);
    return newData.save()
        .then(datas => {
            return res.status(200).json({ result: datas }); 
        })
        .catch(validationError(res));
}

export function getPlant(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let sqlQuery = 'SELECT DISTINCT "Plant" from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY' ;

            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    let plant=[];
                    result.forEach(element => { 
                        plant.push(element['Plant']); 
                      }); 
                    return res.status(200).json({ result: plant });
                }//else
            });//query
        }//else
    });//connect
}
export function getATCCode(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString);
    var plant = req.body.plant; 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let sqlQuery = 'SELECT DISTINCT "ATC Code" from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY WHERE "Plant"=\'' + plant + '\'' ;

            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    let atcCode=[];
                    result.forEach(element => { 
                        atcCode.push(element['ATC Code']); 
                      }); 
                    return res.status(200).json({ result: atcCode });
                }//else
            });//query
        }//else
    });//connect
}



export function getProductCode(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    var atcCode = req.body.atcCode;
    var plant = req.body.plant;
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let sqlQuery = 'SELECT DISTINCT "Product Group Code" from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY WHERE "ATC Code"=\'' + atcCode + '\' AND "Plant"=\'' + plant + '\'' ;

            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    let productCode=[];
                    result.forEach(element => { 
                        productCode.push(element['Product Group Code']); 
                      }); 
                    return res.status(200).json({ result: productCode });
                }//else
            });//query
        }//else
    });//connect
}

export function getMaterial(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    var atcCode = req.body.atcCode;
    var productCode = req.body.productCode;
    var plant = req.body.plant;
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let sqlQuery = 'SELECT DISTINCT "Material Code" from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY WHERE "ATC Code"=\'' + atcCode + '\' AND "Product Group Code"=\'' + productCode + '\' AND "Plant"=\'' + plant + '\'' ;

            client.exec(sqlQuery, function (err, result) {
                if (err) { handleError(res, err) }
                else {
                    let material=[];
                    result.forEach(element => { 
                        material.push(element['Material Code']); 
                      }); 
                    return res.status(200).json({ result: material });
                }//else
            });//query
        }//else
    });//connect
}

export function getMapData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let atcCode = req.body.atcCode;
             let plant = req.body.plant;
             let productCode = req.body.productCode;
             let materialCode = req.body.materialCode;
            
             let sqlHistorical = 'SELECT * from ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY where "Data Type" = \'Historical\' AND "ATC Code"=\'' + atcCode + '\' AND "Plant"=\'' + plant + '\' AND "Product Group Code"=\'' + productCode + '\' AND "Material Code"=\'' + materialCode + '\' order by "Date" asc';
            
            client.exec(sqlHistorical, function (err, historical) {
                if (err) { handleError(res, err) }
                else {                   
                    let sqlForecast = 'SELECT * from ZP_ANALYTICS.SCENARIO_PLANNING_FORECAST_DUMMY where "Data Type" = \'Forecast\' AND "ATC Code"=\'' + atcCode + '\' AND "Plant"=\'' + plant + '\' AND "Product Group Code"=\'' + productCode + '\' AND "Material Code"=\'' + materialCode + '\' order by "Date" asc';
                    
                    client.exec(sqlForecast, function (error, forecast) {
                        if (error) {
                            handleError(res, error)
                        }
                        else{
                            client.end();                                                    
                            return res.status(200).json({ historical: historical, forecast: forecast});
                        }
                    })
                }//else
            });//query
        }//else
    });//connect
}

export function saveForecastData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let materialCode = req.body.materialCode;
             let datatype = req.body.type;
             let indigator = JSON.parse(req.body.indigator);       
             
             let sqlForecast = 'SELECT DISTINCT * from ZP_ANALYTICS.SCENARIO_PLANNING_FORECAST_DUMMY where "Data Type" = \'Forecast\' AND "Material Code"=\'' + materialCode + '\' order by "Date" asc';
                    
                    client.exec(sqlForecast, function (error, forecast) {
                        if (error) {
                            handleError(res, error)
                        }
                        else{
                            client.end();                         
                           
                            let start_date = forecast[0].Date;
                            let end_date = forecast[forecast.length-1].Date;
                            let BrandLaunchIndicator = [];
                            let PromotionIndicator = [];
                            let WeatherIndicator = [];
                           
                            for(let i=0; i<forecast.length; i++){
                                for(let x=0; x<=2; x++){
                                    if(indigator[x]['selectedMonth'].length > 0){
                                        var selectMonth = [];
                                        for(let j=0; j<indigator[x]['selectedMonth'].length; j++){
                                            selectMonth.push(indigator[x]['selectedMonth'][j]['month'])
                                        }                                       
                                            if(datatype == 'forecast'){
                                                var date = new Date(forecast[i].Date);
                                                var date_format = dateFormat(date, "mmm yyyy");                                               
                                                if(x == 0){ 
                                                    if(in_array(date_format, selectMonth)){
                                                        WeatherIndicator.push(1);
                                                    }
                                                    else{
                                                        WeatherIndicator.push(0);
                                                    }
                                                }
                                                if(x == 1){                                    
                                                    if(in_array(date_format, selectMonth)){
                                                        PromotionIndicator.push(1);
                                                    }
                                                    else{
                                                        PromotionIndicator.push(0);
                                                    }
                                                }
                                                if(x == 2){                                    
                                                    if(in_array(date_format, selectMonth)){
                                                        BrandLaunchIndicator.push(1);
                                                    }
                                                    else{
                                                        BrandLaunchIndicator.push(0);
                                                    }
                                                }
                                            }                                       
                                    }
                                    else{
                                        if(x == 0){                                  
                                            WeatherIndicator.push(0);
                                        }
                                        if(x == 1){                                    
                                            PromotionIndicator.push(0);
                                        }
                                        if(x == 2){                                    
                                            BrandLaunchIndicator.push(0);
                                        }
                                    }
                                    
                                }
                                
                            }
                           
                            let data = {
                                "start_date": start_date,
                                "end_date": end_date,
                                "BrandLaunchIndicator": BrandLaunchIndicator,
                                "PromotionIndicator": PromotionIndicator,
                                "WeatherIndicator": WeatherIndicator,
                                "sku": materialCode}
                            axios.post(URL+'/predict', data)
                            .then(response => {                               
                                return res.status(200).json({ 'result': "success", 'mapData': response.data});
                            })
                            .catch(function (error) {                                
                                return res.status(200).json({ 'result': "error"});
                            });
                        }
                    });               
                        
             
        }//else
    });//connect
}

export function saveData(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let materialCode = req.body.materialCode;
             let datatype = req.body.type;
             let indigator = JSON.parse(req.body.indigator);            
             
             for(let x=0; x<=2; x++){
                 for(let j=0; j<indigator[x]['selectedMonth'].length; j++){ 
                     let sql ="";              
                    if (datatype == 'historical'){
                        if(x == 0){
                            sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY SET "Weather Indicator" = 1 where "Material Code"=\'' + materialCode + '\' AND "Actual Quantity"=\'' + indigator[x]['selectedMonth'][j]['value'] + '\'';
                        }                            
                        if(x== 1){
                            sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY set "Promotion Indicator" = 1 where "Material Code"=\'' + materialCode + '\' AND "Actual Quantity"=\'' + indigator[x]['selectedMonth'][j]['value'] + '\'';
                        }                            
                        if(x== 2){
                            sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_HISTORICAL_DUMMY set "Brand Launch Indicator" = 1 where "Material Code"=\'' + materialCode + '\' AND "Actual Quantity"=\'' + indigator[x]['selectedMonth'][j]['value'] + '\'';
                        }
                            
                    }
                    
                    
                    client.exec(sql, function (error, result) {
                        if (error) {
                            handleError(res, error)
                        }
                        
                    })
                 }
             }         
               
             return res.status(200).json({ 'result': "success"});
                        
             
        }//else
    });//connect
}

export function deleteForecast(req, res) {
    var connString = getConnString();
    var client = new hdb.createClient(connString); 
    client.connect(function (err) {
        if (err) { handleError(res, err) }
        else {
             let materialCode = req.body.materialCode;
             let quantity = req.body.quantity;
             let index = req.body.index;
             let sql ="";
             if(index == 0){
                sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_FORECAST_DUMMY SET "Weather Indicator" = 0 where "Material Code"=\'' + materialCode + '\' AND "Forecast Quantity"=\'' + quantity + '\'';
             }

             if(index == 1){
                sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_FORECAST_DUMMY SET "Promotion Indicator" = 0 where "Material Code"=\'' + materialCode + '\' AND "Forecast Quantity"=\'' + quantity + '\'';
             }

             if(index == 2){
                sql = 'UPDATE ZP_ANALYTICS.SCENARIO_PLANNING_FORECAST_DUMMY SET "Brand Launch Indicator" = 0 where "Material Code"=\'' + materialCode + '\' AND "Forecast Quantity"=\'' + quantity + '\'';
             }
             
            client.exec(sql, function (err, result) {
                if (err) { handleError(res, err) }
                else {                   
                    return res.status(200).json({ 'result': "success"});
                }//else
            });//query
        }//else
    });//connect
}

function validationError(res, statusCode) {
    statusCode = statusCode || 422;
    return function(err) {
        return res.status(statusCode).json(err);
    };
}

function validateString(str)
{
    if (str == null || str == "")
    {
        return false;
    }
    else{
        return true;
    }
}

function getConnString() {
    var port = '32415';
    var host = 'zpsgbdphanadb';
    var user = 'ZP_CSM';
    var pwd = 'ZPcsmhana02';

    var connString = { host: host, port: port, user: user, password: pwd };
    return connString;
}


function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function(err) {
        return res.status(statusCode).send(err);
    };
}