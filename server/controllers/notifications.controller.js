"use strict"

const Notification = require("../models/notifications.model")
const httpStatus = require("http-status")
const GroupOptions = require("../models/groupOptions.model")

export const saveNotification = async (req, res) => {
  const body = req.body
  console.log(body);
  try {
    if (body.userId || body.groups) {
      if (!body.type || !body.header) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "Please enter all fields." })
      } else {
        const notifications = await Notification.create(body)
        if (notifications) {
          return res.status(httpStatus.OK).json({
            status: true,
            message: "Notifications Created successfully",
          })
        } else {
          return res
            .status(httpStatus.UNPROCESSABLE_ENTITY)
            .json({ message: "Please try again" })
        }
      }
    } else {
      res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Please select either user or groups/roles" })
    }
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const getNotificationsByUserId = async (req, res) => {
  const userId = req.params.userId
  try {
    if (!userId) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No data found" })
    } else {
      const notification = await Notification.find({ userId }).sort({ createdAt: -1 })
      const checkedLength = await Notification.find({ userId, checked: false })
      if (notification.length) {
        return res.status(httpStatus.OK).json({ status: true, notification, checkedLength: checkedLength.length })
      } else {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "No data found", checkedLength: checkedLength.length })
      }
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const getNotificationsGroupName = async (req, res) => {
  const groupName = req.params.groupName
  try {
    if (!groupName) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No data found" })
    } else {
      const notification = await Notification.find({ groups: { $in: groupName } }).sort({ createdAt: -1 })
      const checkedLength = await Notification.find({ groups: { $in: groupName }, checked: false })
      if (notification.length) {
        return res.status(httpStatus.OK).json({ status: true, notification, checkedLength: checkedLength.length })
      } else {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "No data found", checkedLength: checkedLength.length })
      }
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}

export const updateCheckedNotification = async (req, res) => {
  const body = req.body
  try {
    if (!body.id) {
      return res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "No Notifications available." })
    } else {
      await Notification.findOne({ _id: body.id }, (err, doc) => {
        if (err) {
          return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
        } else {
          let userId = body.userId
          doc.checked = { ...doc.checked, [userId]: true }
          doc.save((err, response) => {
            if (err) {
              return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
            } else {
              console.log("Notification updated Successfully!", "  ", response);
            }
          })
        }
      })
      let notification = await Notification.find({ groups: body.groups }).sort({ createdAt: -1 })
      let notifiedLength = await Notification.find({ userId: body.userId, checked: false })
      return res.status(httpStatus.OK).json({
        status: true,
        message: "Notification successfully updated",
        notification,
        checkedLength: notifiedLength.length
      })
    }
  } catch (err) {
    return res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}


export const saveUserNotificattion = async (req, res) => {
  const body = req.body;
  const sendTo = req.body.to;
  try {
    if (body.group) {
      if (!body.type || !body.header) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "Please enter all fields." })
      } else {

        let roles = [];

        await GroupOptions.findOne(
          { group_name: body.group },
          (err, options) => {
            if (!err) {
              if (options === null) {
                roles = [];
              } else {
                roles = options.userRoles;
              }
            }
          }
        )

        let noti = [];
        let groups = []
        groups.push(body.group);
        roles.forEach(element => {
          if (element.role == sendTo) {
            let message = {};
            message.type = body.type;
            message.header = body.header;
            message.userId = element.userId;
            message.groups = groups;
            noti.push(message);
          }
        });

        if (noti.length > 0) {

          const notifications = await Notification.insertMany(noti)
          if (notifications) {
            return res.status(httpStatus.OK).json({
              status: true,
              message: "Notifications Created successfully",
            })
          } else {
            return res
              .status(httpStatus.UNPROCESSABLE_ENTITY)
              .json({ message: "Please try again" })
          }

        }

        res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ status: false, message: "No users found to send the notification" })

      }
    } else {
      res
        .status(httpStatus.UNPROCESSABLE_ENTITY)
        .json({ message: "Please select either user or groups/roles" })
    }
  } catch (err) {
    res.status(httpStatus.INTERNAL_SERVER_ERROR).json(err)
  }
}


export const userNotificattionService = async (bodyIn) => {
  const body = bodyIn;
  const sendTo = bodyIn.to;
  try {
    if (body.group) {
      if (!body.type || !body.header) {
        return res
          .status(httpStatus.UNPROCESSABLE_ENTITY)
          .json({ message: "Please enter all fields." })
      } else {
        let roles = [];
        await GroupOptions.findOne(
          { group_name: body.group },
          (err, options) => {
            if (!err) {
              if (options === null) {
                roles = [];
              } else {
                roles = options.userRoles;
              }
            }
          }
        )
        let noti = [];
        let groups = []
        groups.push(body.group);
        roles.forEach(element => {
          if (element.role == sendTo) {
            let message = {};
            message.type = body.type;
            message.header = body.header;
            message.userId = element.userId;
            message.groups = groups;
            noti.push(message);
          }
        });
        if (noti.length > 0) {
          const notifications = await Notification.insertMany(noti)
          if (notifications) {
            return true;
          } else {
            return false;
          }
        }
        return false;
      }
    } else {
      return false;
    }
  } catch (err) {
    return false;
  }
}

export const getNotificationCount = async (req, res) => {
  let body = req.body;
  await Notification.find({ "groups": { "$in": body.groupArray } }, (err, response) => {
    if (err) {
      res.status(404).json(err)
    } else {
      let data = response.filter(item => item.checked===false || !item.checked.hasOwnProperty(body.userId))
      res.status(200).json(data)
    }
  })
}