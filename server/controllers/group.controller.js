"use strict"

// import config from '../../config/environment';
const Group = require("../models/group.model")
const SessionManager = require("../models/sessionManagement.model")
const GroupOptions = require("../models/groupOptions.model")
const ActivityLog = require("../models/activitylogger.model")
const config = require("../config/environment/index")
const pg = require("pg")
const httpStatus = require("http-status")

//Simple version, without validation or sanitation
export function test(req, res) {
  res.send("Greetings from the Group controller!")
}
function getConnString() {
  var port =
    config.postgres.port === null || config.postgres.port == ""
      ? "5432"
      : config.postgres.port
  var connString =
    "postgres://" +
    config.postgres.username +
    ":" +
    config.postgres.pwd +
    "@" +
    config.postgres.server +
    ":" +
    port +
    "/" +
    config.postgres.database
  return connString
}

export function getGroup(req, res) {
  Group.find({ group_name: req.params.groupName }, (err, group) => {
    if (err) {
      handleError(res, err)
    }
    return res.status(200).json(group)
  })
}
export function getGroupForUser(req, res) {
  var connString = getConnString()
  var client = new pg.Client(connString)
  // console.log("postgres here,HI!")
  client.connect(function (err) {
    if (err) {
      handleError(res, err)
    } else {
      client.query(
        `
              select groupname from zp_webportal_groups_users
              where lower(username)=lower('${req.params.username}')`,
        function (err, result) {
          if (err) {
            handleError(res, err)
          } else {
            client.end()
            if (result.rows.length > 0) {
              var groupNames = result.rows.map(g => g.groupname)
              Group.find({ group_name: { $in: groupNames } }, (err, groups) => {
                if (err) {
                  handleError(res, err)
                }
                return res.status(200).json(groups)
              })
            } else {
              handleError(res, err)
            }
          } //else
        }
      ) //query
    } //else
  }) //connect
}

export function getAllGroups1(req, res) {
  Group.find({}, (err, groups) => {
    if (err) {
      handleError(res, err)
    }
    return res.status(200).json(groups)
  })
}

export const updateInternalAndAdmin = async (req, res) => {
  try {
    const mongoGroups = await Group.find({})
    mongoGroups.map(async item => {
      return await Group.update(
        { _id: item._id, type: "Internal" },
        { isInternal: true }
      )
    })
    return res.status(httpStatus.OK).json(mongoGroups)
  } catch (err) {}
}

export const getAllGroups = async (req, res) => {
  try {
    const mongoGroups = await Group.find({}, function (err, result) {
      if (err) {
        return []
      } else {
        return result
      }
    })
    if (mongoGroups.length == 0) {
      res.status(httpStatus.UNPROCESSABLE_ENTITY).json({
        status: 406,
        ok: false,
        message: "Please try again later",
        data: { Error: err },
      })
    }

    const mongoSessions = await SessionManager.find({}, function (err, result) {
      if (err) {
        return []
      } else {
        return result
      }
    })

    let sessionsMap = new Map(mongoSessions.map(i => [i.group_name, i]))

    for (const groupInfo of mongoGroups) {
      let mdGroup = sessionsMap.get(groupInfo.group_name)
      if (mdGroup) {
        groupInfo.single_session_enforced =
          mdGroup.single_session_enforced || false
        groupInfo.two_factor_authentication_enforced =
          mdGroup.two_factor_authentication_enforced || false
      } else {
        groupInfo.single_session_enforced = false
        groupInfo.two_factor_authentication_enforced = false
      }
      groupInfo.options = []
      groupInfo.mappingId = groupInfo._id
    }

    return res.status(200).json(mongoGroups)
  } catch (err) {
    handleError(res, err)
  }
}

export function updateGroup(req, res) {
  let username = req.body.username
  let clientIp = req.body.clientIp

  var group_insertion_details = {}
  group_insertion_details.group_name = req.body.group_name
  group_insertion_details.IP = clientIp
  let group_options = req.body.group_options

  delete req.body.username
  delete req.body.clientIp

  if (req.body._id) {
    //updation
    Group.update({ _id: req.body._id }, req.body, { upsert: true }, function (
      err,
      doc
    ) {
      Group.findOne({ _id: req.body._id }, function (err, doc) {
        group_insertion_details.action = "Group Updation"
        group_insertion_details.data = req.body
        group_insertion_details.updated_by = username
        group_insertion_details.updation_time = new Date(
          new Date().toUTCString()
        )

        var activityLog = new ActivityLog(group_insertion_details)
        activityLog.save()
        return res.status(200).json(doc)
      })
    })
  } //insertion
  else {
    Group.findOneAndUpdate(
      { group_name: req.body.group_name },
      req.body,
      { upsert: true, new: true },
      (error, doc) => {
        if (error) {
          return handleError(res, error)
        }
        if (!doc) {
          return res.send(404)
        }
        let groupId = doc._id
        let groupOption = new GroupOptions({
          group_name: group_options.group_name,
          display_name: group_options.group_name,
          mappingId: groupId,
          type: group_options.type,
          country: group_options.country,
          options: group_options.options,
          isInternal: group_options.isInternal,
          isAdmin: group_options.isAdmin,
        })
        console.log(group_options)
        groupOption.save((err, data) => {
          if (err) handleError(data, err)
          else
            res.status(200).json({
              group_name: data.group_name,
            })
        })
      }
    )
  }
}

export function deleteGroup(req, res) {
  var group_deletion_details = {}
  group_deletion_details.action = "Group Deletion"
  group_deletion_details.group_name = req.body.group_name
  group_deletion_details.deleted_by = req.body.username
  group_deletion_details.IP = req.body.clientIp
  group_deletion_details.deletion_time = new Date(new Date().toUTCString())
  // console.log(req.body._id)
  Group.deleteOne({ _id: req.body._id }, (error, doc) => {
    if (error) {
      return handleError(res, error)
    } else {
      delete req.body.username
      delete req.body.clientIp
      group_deletion_details.data = req.body

      var activityLog = new ActivityLog(group_deletion_details)
      activityLog.save()
      GroupOptions.deleteOne({ mappingId: req.body._id }, (error, doc) => {
        if (error) {
          console.log("options don't exixts")
        } else {
          console.log("options deleted")
        }
      })
      return res.status(200).json(doc)
    }
  })
}

export const getGroupOptions = (req, res) => {
  GroupOptions.find({}, (err, groupOptions) => {
    if (err) {
      handleError(res, err)
    }
    return res.status(200).json(groupOptions)
  })
}

export const updateGroupOptions = (req, res) => {
  let d = req.body
  //data to sent=> group_name,type,country,options,group _id as mappingId**
  GroupOptions.findOne({ mappingId: d.mappingId }, (err, doc) => {
    if (err) {
      return res.status(500).json(err)
    } else {
      Group.findOne({ _id: d.mappingId }, (err, resp) => {
        if (err) {
          res.status(404).json(err)
        } else {
          resp.isAdmin = d.isAdmin
          resp.isInternal = d.isInternal
          resp.display_name = d.display_name
          resp.save((err, response) => {
            if (err) {
              handleError(response, err)
            } else {
              console.log("Group updated Successfully!")
            }
          })
        }
      })
      if (doc) {
        doc.type = d.type
        doc.country = d.country
        doc.options = d.options
        doc.group_name = d.group_name
        doc.display_name = d.display_name
        ;(doc.isInternal = d.isInternal), (doc.isAdmin = d.isAdmin)
        doc.save((err, data) => {
          if (err) {
            handleError(data, err)
          } else {
            res.status(200).json({
              groupName: data.group_name,
            })
          }
        })
      } else {
        let groupOption = new GroupOptions({
          group_name: d.group_name,
          mappingId: d.mappingId,
          type: d.type,
          country: d.country,
          options: d.options,
          display_name: d.display_name,
          isInternal: d.isInternal,
          isAdmin: d.isAdmin,
        })
        groupOption.save((err, data) => {
          if (err) handleError(data, err)
          else
            res.status(200).json({
              group_name: data.group_name,
            })
        })
      }
    }
  })
}
export function getGroupOptionById(req, res) {
  GroupOptions.findOne({ mappingId: req.params.mappingId }, (err, options) => {
    if (options === null) {
      return res.status(500).json(err)
    } else {
      return res.status(200).json(options)
    }
  })
}
export const getGroupOptionByGroupName = (req, res) => {
  GroupOptions.findOne({ group_name: req.params.groupName }, (err, options) => {
    if (options === null) {
      return res.status(500).json(err)
    } else {
      return res.status(200).json(options)
    }
  })
}
function handleError(res, err) {
  return res.status(500).json(err)
}

function migrateUiElements() {
  Group.find({}, (err, groups) => {
    if (err) {
      console.log("error")
    }
    for (const group of groups) {
      let UiElements = group.ui_elements_list
      let solutions = []

      let internal = UiElements.slice(0, 5)
      let insider = UiElements.slice(5, 11)
      let infrared = UiElements.slice(11, 14)

      let finalIn = []
      let finalInsider = []
      let finalInf = []

      for (const element of internal) {
        let item = {
          type: element.name,
          elements: [{ lable: "main", url: element.url }],
        }
        finalIn.push(item)
      }

      for (const element of insider) {
        let item = {
          type: element.name,
          elements: [{ lable: "main", url: element.url }],
        }
        finalInsider.push(item)
      }

      for (const element of infrared) {
        let item = {
          type: "infrared",
          elements: [{ lable: element.name, url: element.url }],
        }
        finalInf.push(item)
      }

      solutions.push({ type: "insider", name: "Insider", items: finalInsider })
      solutions.push({ type: "Internal", name: "Internal", items: finalIn })
      solutions.push({
        type: "commercialExcellence",
        name: "Commercial Excellence",
        items: finalInf,
      })

      let groupOption = new GroupOptions({
        group_name: group.group_name,
        mappingId: group._id,
        type: group.type || "",
        country: group.country || "",
        options: solutions,
      })
      groupOption.save((err, data) => {
        if (err) {
          console.log("error saving data")
        }
      })
    }
  })
}
