FROM node:14 as builder
WORKDIR /app
COPY package.json .
RUN npm i gatsby-cli
RUN npm install
COPY . .
RUN ["npm", "run", "build"]
CMD ["npm", "run", "serve"]
